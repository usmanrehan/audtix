//
//  PlayerListCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/17/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PlayerListCell: UITableViewCell {

    @IBOutlet weak var labelChapEpisodeNo: UILabel!
    @IBOutlet weak var labelChapEpisodeDescribtion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


