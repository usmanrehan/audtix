//
//  FavoriteCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class FavoriteCell: MGSwipeTableCell {

    
    @IBOutlet weak var imageViewFvt: UIImageView!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func BindData(podcastModel: PodcastSubscribtionModel) {
        self.labelTitle.text = podcastModel.Title
        self.labelNarrator.text = podcastModel.ArtistName
        
        if let imageURL = URL(string: podcastModel.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewFvt.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewFvt.image = #imageLiteral(resourceName: "placeholder_image")
        }

    }
    
    func BindData(bookModel: BookModel) {
        self.labelTitle.text = bookModel.BookName
        self.labelNarrator.text = bookModel.NarratorName
        
        if let imageURL = URL(string: bookModel.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewFvt.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewFvt.image = #imageLiteral(resourceName: "placeholder_image")
        }
        
//        self.labelTitle.text = "TED Radio Show"
    }
    
    
    
}
