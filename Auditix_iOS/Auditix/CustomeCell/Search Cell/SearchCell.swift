//
//  SearchCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var imageviewNews: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var stackNarrator: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func BindData(with searchItem: PodcastModel) {
        self.labelTitle.text = searchItem.Name
        self.labelNarrator.text = searchItem.ArtistName
        
        if let imageURL = URL(string: searchItem.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageviewNews.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageviewNews.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with searchItem: BookModel) {
        self.labelTitle.text = searchItem.BookName
        self.labelNarrator.text = searchItem.NarratorName
        
        if let imageURL = URL(string: searchItem.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageviewNews.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageviewNews.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }

    func BindData(with searchItem: NewsCategory) {
        self.labelTitle.text = searchItem.SourceName
        self.labelNarrator.text = "-"
        
        if let imageURL = URL(string: searchItem.SourceImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageviewNews.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageviewNews.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }

}
