//
//  PodcastCollectionCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btnSubscription: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func BindData(with podcast: PodcastModel) {
        self.labelTitle.text = podcast.Name
        
        if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with podcast: PodcastSubscribtionModel) {
        self.labelTitle.text = podcast.Title
        
        if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with subscribedNews: NewsListModel) {
        self.labelTitle.text = subscribedNews.Name
        if let imageUrl = subscribedNews.ImageUrl {
            let url = URL(string: imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            self.imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageView.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
