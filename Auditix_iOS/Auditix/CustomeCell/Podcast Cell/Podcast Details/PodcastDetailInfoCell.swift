//
//  PodcastInfoCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class PodcastDetailInfoCell: UITableViewCell {

    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var BtnPlayPause: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var imageviewPodcast: UIImageView!
    @IBOutlet weak var constrainPlayPauseTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageviewPodcast.hero.modifiers = [.arc]
        self.imageviewPodcast.hero.id = "DetailToPlayer"
        self.viewRating.isUserInteractionEnabled = false
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func BindData(with podcastDetail: PodcastDetailModel) {
        
        self.labelTitle.text = podcastDetail.Title
        self.labelNarrator.text = podcastDetail.Author
        self.labelGenre.text = podcastDetail.Genre
        
        self.viewRating.rating = podcastDetail.Rating
        
        if let imageURL = URL(string: podcastDetail.Image!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageviewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageviewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    func BindData(with podcast: PodcastModel) {
        if (podcast.Title ?? "").count > 0{
            self.labelTitle.text = podcast.Title
        }
        if (podcast.Name ?? "").count > 0{
            self.labelTitle.text = podcast.Name
        }
        self.labelNarrator.text = podcast.ArtistName
        self.labelGenre.text = podcast.Genre
        
        self.viewRating.rating = podcast.Rating
        
        if let imageURL = URL(string: (podcast.ImageUrl ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageviewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageviewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
