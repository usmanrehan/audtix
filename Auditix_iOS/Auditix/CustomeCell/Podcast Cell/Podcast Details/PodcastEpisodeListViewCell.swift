//
//  PodcastEpisodeListViewCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastEpisodeListViewCell: UITableViewCell {
    @IBOutlet weak var lblEpisodeName: UILabel!
    @IBOutlet weak var lblEpisodeDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
