//
//  PodcastDetailActionCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastDetailActionCell: UITableViewCell {

    @IBOutlet weak var btnAddToFvt: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
