//
//  PodcastEpisodeListCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView

class PodcastEpisodeListCell: UITableViewCell {

    @IBOutlet weak var BtnDownload: DownloadButton!
    @IBOutlet weak var episodeName: UILabel!
    @IBOutlet weak var progressView: UAProgressView!
    @IBOutlet weak var imageDownloadPlay: UIImageView!
    
    var traceID: Int?
    
    var isDownloaded: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.progressView.frame.size.width * 0.9, height: self.progressView.frame.size.height))
        //                label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        label.textColor = Constants.THEME_ORANGE_COLOR
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        self.progressView.centralView = label
        self.progressView.progressChangedBlock = {
            (progressView, progress) -> Void in
            print("progress \(progress)")
            if let label = progressView?.centralView as? UILabel {
                label.text = String(format: "%2.0f%%", progress * 100)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskIsInProgress), name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskCompleted), name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
        
        // Initialization code
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
    }
    
    @objc func downloadTaskIsInProgress( data: NSNotification) {
        let aditionalText = data.userInfo
        if self.traceID == aditionalText!["traceID"] as? Int {
            self.progressView.isHidden = false
            self.imageDownloadPlay.isHidden = true
            self.BtnDownload.isHidden = true
            self.progressView.setProgress(CGFloat(aditionalText!["progress"] as! Double), animated: true)
        }
    }
    
    @objc func downloadTaskCompleted( data: NSNotification) {
        let aditionalText = data.userInfo
        if self.traceID == aditionalText!["traceID"] as? Int {
            self.BtnDownload.isHidden = false
            self.progressView.isHidden = true
            self.imageDownloadPlay.isHidden = false
            self.imageDownloadPlay.image = #imageLiteral(resourceName: "play-circle")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray), object: nil, userInfo: aditionalText)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
