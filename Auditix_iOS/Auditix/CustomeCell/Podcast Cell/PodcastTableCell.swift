//
//  PodcastTableCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastTableCell: UITableViewCell {

    @IBOutlet weak var imageViewPodcast: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    
//    @IBOutlet weak var btnSubscrptionTrailing: UIButton!
    @IBOutlet weak var BtnSubscrptionBottom: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  //  @IBOutlet weak var constraintSubscriptionLeftTrailing: NSLayoutConstraint!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func BindData(with podcast: PodcastModel) {
        let podName = podcast.Name ?? ""
        let podTitle = podcast.Title ?? ""
        if !podName.isEmpty{
            self.labelTitle.text = podName
        }
        if !podTitle.isEmpty{
            self.labelTitle.text = podTitle
        }
        self.labelNarrator.text = podcast.ArtistName
        if let imgURL = podcast.ImageUrl{
            if let imageURL = URL(string: imgURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
        }
        else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with podcastByCategoryID: PodcastByCategoryIDModel) {
        self.labelTitle.text = podcastByCategoryID.Name
        self.labelNarrator.text = podcastByCategoryID.ArtistName
        if let imageURL = URL(string: podcastByCategoryID.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with item: NewsListModel) {
        self.labelTitle.text = item.Name
        self.labelNarrator.text = item.NarratorName
        if let imageUrl = item.ImageUrl {
            let url = URL(string: imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) 
            self.imageViewPodcast.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
