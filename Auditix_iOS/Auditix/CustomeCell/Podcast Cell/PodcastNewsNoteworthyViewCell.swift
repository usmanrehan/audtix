//
//  PodcastNewsNoteworthyViewCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 7/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastNewsNoteworthyViewCell: UITableViewCell {

    @IBOutlet weak var podcastImageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
