//
//  RangeSliderViewCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RangeSeekSlider

class RangeSliderViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var rangeSeekSlider: RangeSeekSlider!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.rangeSeekSlider.tintColor = Constants.THEME_ORANGE_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
