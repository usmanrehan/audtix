//
//  PodcastSeeAllCell.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastSeeAllCell: UITableViewCell {

    
    @IBOutlet weak var imageViewPodcast: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    
    @IBOutlet weak var btnSubscrptionTrailing: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func BindData(with podcast: PodcastModel) {
        self.labelTitle.text = podcast.ArtistName//podcast.Name
        self.labelNarrator.text = podcast.ArtistName
        if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with podcast: PodcastSubscribtionModel) {
        self.labelTitle.text = podcast.Title//podcast.Name
        self.labelNarrator.text = podcast.ArtistName
        if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
}
