//
//  PodcastDownloadedCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class PodcastDownloadedCell: MGSwipeTableCell {

    @IBOutlet weak var imageViewPodcast: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelDownloadedDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindData(podcast: PodcastDetailModel, downloadedDate: Date) {
        self.labelTitle.text = podcast.Title
        self.labelNarrator.text = podcast.Author
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        self.labelDownloadedDate.text = formatter.string(from: downloadedDate)
        print("downloadedDate \(downloadedDate)")
        if let imageURL = URL(string: podcast.Image!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }

    func bindData(podcast: PodcastModel, downloadedDate: Date) {
        self.labelTitle.text = podcast.Name
        self.labelNarrator.text = podcast.ArtistName
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        self.labelDownloadedDate.text = formatter.string(from: downloadedDate)
        print("downloadedDate \(downloadedDate)")
        if let imageURL = URL(string: (podcast.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
            self.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
