//
//  SideBarCell.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import BadgeSwift
class SideBarCell: UITableViewCell {

    @IBOutlet weak var viewCounter: BadgeSwift!
    @IBOutlet weak var labelMenu: UILabel!
    @IBOutlet weak var imageViewMenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
