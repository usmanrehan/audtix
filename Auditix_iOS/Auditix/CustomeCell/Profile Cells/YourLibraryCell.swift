//
//  YourLibraryCell.swift
//  Auditix
//
//  Created by shardha on 1/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
import MGSwipeTableCell

class YourLibraryCell: MGSwipeTableCell{

    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func BindData(with book: BookModel) {
        self.labelGenre.text = book.Genre
        self.labelAuthor.text = book.AuthorName
        self.labelNarrator.text = book.NarratorName
        self.labelName.text = book.BookName
        
        if let imageURL = URL(string: book.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
