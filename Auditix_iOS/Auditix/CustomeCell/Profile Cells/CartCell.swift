//
//  CartCell.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var ButtonCross: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func actionDelete(_ sender: UIButton) {
        
    }
    
    func BindData(with book: BookModel, showCrossBtn: Bool) {
        self.labelGenre.text = book.Genre
        self.labelAuthor.text = book.AuthorName
        self.labelNarrator.text = book.NarratorName
        self.labelName.text = book.BookName
        //self.labelPrice.text = ParserHelper.returnPriceString(with: book.Price, isPaid: book.IsPaid)
        if let imageURL = URL(string: book.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
        
        self.ButtonCross.isHidden = !showCrossBtn
        
    }
}
