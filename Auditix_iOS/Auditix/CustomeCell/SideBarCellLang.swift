//
//  SideBarCellLang.swift
//  Auditix
//
//  Created by shardha on 7/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SideBarCellLang: UITableViewCell {

    @IBOutlet weak var labelMenu: UILabel!
    @IBOutlet weak var imageViewMenu: UIImageView!
    
    @IBOutlet weak var switchLanguage: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

