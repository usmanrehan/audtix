//
//  NewsEpisodeInfoCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsEpisodeInfoCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var imageViewNews: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageViewNews.heroModifiers = [.arc]
        self.imageViewNews.heroID = "DetailToPlayer"
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bindData(with newsModel: NewsModel) {
        self.labelTitle.text = newsModel.title
        self.labelDuration.text = ParserHelper.getDuration(with: newsModel.duration)
        
        if let imageURL = URL(string: newsModel.image_url!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewNews.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewNews.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
