//
//  NewsDetailShowsCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView

class NewsDetailShowsCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
//    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var BtnDownload: DownloadButton!
    @IBOutlet weak var progressView: UAProgressView!
    @IBOutlet weak var imageDownloadPlay: UIImageView!
    
    @IBOutlet weak var imageViewNews: UIImageView!
    
//    @IBOutlet weak var constraintSeparatorViewBottom: NSLayoutConstraint!
    @IBOutlet weak var bottomViewSeparator: UIView!
    
    
    var traceID: Int?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.progressView.frame.size.width * 0.9, height: self.progressView.frame.size.height))
        //                label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        label.textColor = Constants.THEME_ORANGE_COLOR
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        self.progressView.centralView = label
        self.progressView.progressChangedBlock = {
            (progressView, progress) -> Void in
            print("progress \(progress)")
            if let label = progressView?.centralView as? UILabel {
                label.text = String(format: "%2.0f%%", progress * 100)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskIsInProgress), name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskCompleted), name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
    }

    @objc func downloadTaskIsInProgress( data: NSNotification) {
        let aditionalText = data.userInfo
        if self.traceID == aditionalText!["traceID"] as? Int {
            self.progressView.isHidden = false
            self.imageDownloadPlay.isHidden = true
            self.BtnDownload.isHidden = true
            self.progressView.setProgress(CGFloat(aditionalText!["progress"] as! Double), animated: true)
            //            print(aditionalText!)
        }
    }
    
    @objc func downloadTaskCompleted( data: NSNotification) {
        let aditionalText = data.userInfo
        if self.traceID == aditionalText!["traceID"] as? Int {
            self.progressView.isHidden = true
            self.imageDownloadPlay.isHidden = false
            self.BtnDownload.isHidden = false
            self.imageDownloadPlay.image = #imageLiteral(resourceName: "play-circle")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray), object: nil, userInfo: aditionalText)
        }
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func BindData(with newsModel: NewsModel) {
        self.labelName.text = newsModel.title
        self.labelDuration.text = ParserHelper.getDuration(with: newsModel.duration)
        if let imageURL = URL(string: newsModel.image_url!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageViewNews.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewNews.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
