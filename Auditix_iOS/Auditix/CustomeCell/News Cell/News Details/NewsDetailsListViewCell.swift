//
//  NewsDetailsListViewCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsDetailsListViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
