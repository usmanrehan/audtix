//
//  NewsDetailCategoryCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsDetailCategoryCell: UITableViewCell {
    @IBOutlet weak var imageViewNewsCategory: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAbout: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var BtnSubscrptionBottom: UIButton!
    @IBOutlet weak var buttonDownloadAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
