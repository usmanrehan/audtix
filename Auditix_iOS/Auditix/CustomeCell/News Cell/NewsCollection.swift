//
//  NewsCollectionCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsCollection: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
