//
//  NewsCollectionCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageNewsCover: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func BindData(newsCategoryItem: NewsCategory) {
        self.labelName.text = newsCategoryItem.SourceName
        if let imageURL = URL(string: newsCategoryItem.SourceImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageNewsCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageNewsCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with category: BookCategory) {
        self.labelName.text = category.Name
        if let imageUrl = category.ImageUrl {
            let url = URL(string: imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            self.imageNewsCover.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageNewsCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
}
