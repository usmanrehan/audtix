//
//  NewsFavoriteCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell
class NewsFavoriteCell: MGSwipeTableCell {

    @IBOutlet weak var imageNewsChannel: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
