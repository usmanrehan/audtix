//
//  NewsChannelsCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsChannelsCell: UITableViewCell {

    @IBOutlet weak var imageviewNews: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
