//
//  BookDetailDescriptionCell.swift
//  Auditix
//
//  Created by shardha on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BookDetailDescriptionCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var lableDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
