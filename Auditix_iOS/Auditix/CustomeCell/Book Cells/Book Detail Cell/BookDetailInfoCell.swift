//
//  BookDetailInfoCell.swift
//  Auditix
//
//  Created by shardha on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class BookDetailInfoCell: UITableViewCell {

    @IBOutlet weak var imageCover: UIImageView!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelNarrator: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelChapter: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    @IBOutlet weak var buttonDownloadCompleteBook: UIButton!
    @IBOutlet weak var viewRating: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageCover.heroModifiers = [.arc]
        self.imageCover.heroID = "DetailToPlayer"
        self.viewRating.isUserInteractionEnabled = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func BindData(with book: BookModel) {
        self.labelGenre.text = book.Genre
        self.labelAuthor.text = book.AuthorName
        self.labelNarrator.text = book.NarratorName
        self.labelName.text = book.BookName
        self.viewRating.rating = book.Rating
        self.labelChapter.text = ParserHelper.returnChaptersString(with: book.Chapters?.Chapter.count ?? 0)
        self.labelDuration.text = ParserHelper.getDuration(with: Int(book.Duration)) 
        //self.labelPrice.text = ParserHelper.returnPriceString(with: book.Price, isPaid: book.IsPaid)
        if let imageURL = URL(string: book.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
             self.imageCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    


}
