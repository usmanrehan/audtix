//
//  BookDetailActionsCell.swift
//  Auditix
//
//  Created by shardha on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BookDetailActionsCell: UITableViewCell {

    @IBOutlet weak var buttonListenPreview: UIButton!
    @IBOutlet weak var buttonAddToCart: UIButton!
    @IBOutlet weak var buttonAddToFavourite: UIButton!
    @IBOutlet weak var buttonRateThis: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
