//
//  BookCollectionCell.swift
//  Auditix
//
//  Created by shardha on 1/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BookCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageBookCover: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var btnSubscription: UIButton!
    
    func BindData(with book: BookModel) {
        self.labelName.text = book.BookName
        if let imageURL = URL(string: book.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageBookCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageBookCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with podcast: PodcastModel) {
        self.labelName.textAlignment = .center

        //Set podcast episode title
        if podcast.episodes.count > 0 {
            self.labelName.text = podcast.episodes.first?.EpisodeTitle
        } else {
            self.labelName.text = "-"
        }
        
        //Set podcast image
        if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.imageBookCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageBookCover.image = #imageLiteral(resourceName: "placeholder_image")
        }
    }
    
    func BindData(with featuredBook: FeatureBookModel) {
        self.labelName.text = MiscStrings.bookTitle.text
    }
}
