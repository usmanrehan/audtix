//
//  BooksCollection.swift
//  Auditix
//
//  Created by shardha on 1/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BooksCollection: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
