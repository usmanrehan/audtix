//
//  AutoDownloadViewCell.swift
//  Auditix
//
//  Created by Ahmed Shahid on 7/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class AutoDownloadViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toggleSwith: UISwitch!
    @IBOutlet weak var channelImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
