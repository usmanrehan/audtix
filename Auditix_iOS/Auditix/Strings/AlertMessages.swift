//
//  AlertMessages.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

enum AlertMessages: String {
    
    // MARK: API MANAGER
    case errorInternetConnection  = "Your Internet connection is not working properly or server is not responding, please retry."
    case errorInResponce           = "Server is not responding correctly, please retry."
    
    // MARK: NEXT MODULE
    case WillImplementInNextMode = "Functionality will be implemented in next phase."
    
    // MARK: Login and SignUp
    case emailNotValid = "Please provide a valid Email Address."
    case passwordNotValid = "Password should contain atlest 6 characters."
    case passwordInValidShort = "At least 6 characters."
    case passwordNotMatch = "New password and confirm password does not match."
    case passwordNotMatchShort = "Password does'nt match."
    case PhoneNumber = "The phone number should be numeric with 8 or more digits."
    case AllFieldsRequired = "All Fields are required!"
    case LogoutMessage = "Are you sure you want to logout?"
    case invalidDate = "Please enter a valid date."
    case googleSignInFail = "Failed to sign-in with google. Please try again or later."
    case allFieldsRequired = "All fields are required."
    case profileUpdated = "Your profile has been updated."
    case NameNotEmpty = "Name can't be empty."
    case guestLogin = "Please Sign-In or Login-In to proceed!"
    case faceBookLoginError = "Email permission not granted! Please grant it from your facebook account to continue."
    
    // MARK: BOOKS
    case allBooks = "All Books"
    case BestOfBest = "Best of the best"
    case aboutTheBook = "ABOUT THE BOOK"
    case aboutTheNarator = "NARRATOR INTRODUCTION"
    case addToLibrary = "Add to Library"
    case addToLibrarySuccss = "Your Book has been added to library successfully"
    case addToCartBookSuccessMessage = "This book has been added to your cart list"
    case ListenBook = "Listen Book"
    case UnableToAddToCart = "Sorry! We are unable to add this item in your cart."
    case bookChapters = "Chapters"
    case cantRateThisBook = "Please purchase this book for rate."
    case NoDataFound = "No data found."
    
    // MARK: Privacy,TnC,About Us
    case aboutUsTitle = "About Us"
    case TncTitle = "Terms And Condition"
    
    // MARK: PODCAST
    case aboutThePodcast = "ABOUT THE PODCAST"
    case removeFromFvt = "Remove from Favorite"
    case addToFavorite = "Add to Favorite"
    case subscribe = "Subscribe"
    case unsubscribe = "Unsubscribe"
    case noEpisodesAvaialble = "No Episodes are available for this podcast"
    case unableToDeletePodcast = "Unable to delete this podcast. Please try again or later."
    case newEpisodesAndNoteWorthy = "New & Noteworthy"
    case episodes = "Episodes"
    case mySubscription = "My Subscription"
    case downloads = "Downloads"
    case news = "Subscribed News"
    case books = "Favorite Books"
    
    // MARK: NEWS
    case aboutTheNews = "ABOUT THE NEWS"
    case unableToOpenNews = "Unable to play this news. Try again or later"
    case noNewNewsAvailable = "No News at this time"
    
    // MARK: Side MENU, SEARCH & SETTINGS
    case usingDataNetwork = "Its seems like you are using data network, kindly change the settings to download this."
    case noResultSearch = "No Results for"
    case comingSoon = "Coming Soon!"
    
    //SingleMethod For the Usage of Localization.
    var message: String { return NSLocalizedString( self.rawValue, comment: "") }
}


