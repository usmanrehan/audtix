//
//  MiscStrings.swift
//  Auditix
//
//  Created by shardha on 8/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

enum MiscStrings: String {
    case email = "Email"
    case password = "Password"
    case confirmPassword = "Confirm Password"
    
    //MARK: - BookCollectionCell
    case bookTitle  = "BookTitle"
    
    //MARK: - BottomView
    case episode = "Episode"
    case deleted = "deleted..."
    case chapter = "Chapter"
    
    //MARK: - AdvertiseView
    case invalidAdvertismentURL = "Invalid Advertisment URL"
    
    //MARK: - ParserHelper
    case chapters = "Chapters"
    case free = "FREE"
    
    //MARK: - Utility
    case ok = "Ok"
    case yes = "YES"
    case no = "NO"
    
    //MARK: - APIManagerBase
    case somethingWentWrongPleaseTryAgain = "Something went wrong. Please try again"
    
    //MARK: - Player
    case bookPreview = "Book Preview"
    
    //MARK: - News
    case mySubscription = "My Subscription"
    case newsChannels = "News Channels"
    case bESTOFTHEBEST = "BEST OF THE BEST"
    
    //MARK: - NewsList
    case noDataFound = "No data found."
    
    //MARK: - NewsDetails
    case today = "Today"
    case yesterday = "Yesterday"
    case older = "Older"
    case todayShows = "Today shows"
    case yesterdayShows = "Yesterday shows"
    case laterShows = "Later shows"
    case noEpisodeAvailable = "No Episode available"
    case message = "Message"
    
    //MARK: - NewsEpisodeDetail
    case noDescriptionAvailable = "No Description available"
    
    //MARK: - NewsFilter
    case duration = "Duration (min)"
    case subscribers = "Subscribers"
    case countries = "Countries"
    
    //MARK: - NewsEpisodeDetails
    case fileIsalreadyDownloaded = "File is already downloaded"
    case sharingUrlNotFound = "Sharing url not found"
    case invalidURL = "Sharing url is not valid"
    
    //MARK: - Podcast
    case newAndNoteworthy = "New & Noteworthy"
    case mostRecommended = "Most Recommended"
    case categories = "Categories"
    
    //MARK: - PodcastDetail
    case youHaveAlreadyGivenRatingToThisPodcast = "You have already given rating to this podcast"
    case yourRatingHasBeenSubmitted = "Your rating has been submitted"
    
    //MARK: - Books
    case recommended = "Recommended"
    
    //MARK: - BookDetail
    case downloading = "Downloading"
    case noPreviewAvailable = "No preview available"
    case youHaveAlreadyGivenRatingToThisBook = "You have already given rating to this book"
    
    //MARK: - BookAll
    case books = "Books"
    
    //MARK: - Favorites
    case news = "News"
    case podcast = "Podcast"
    case podcasts = "Podcasts"
    
    //MARK: - ContactUs
    case typeYourMessageHere = "Type your message here..."
    case pleaseTypeYourMessage = "Please type your message."
    case weWillGetBackToYouSoon = "We will get back to you soon."
    
    //MARK: - EditProfile
    case dateofBirth = "Date of Birth"
    case done = "Done"
    case cancel = "Cancel"
    case uploadPhoto = "Upload Photo"
    case howDoYouWantToSetYourPhoto = "How do you want to set your photo?"
    case camera = "Camera"
    case gallery = "Gallery"
    
    //MARK: - YourPodcast
    case episodeNumber = "Episode Number"
    
    case deleteWhenFinished = "Delete when finished"
    case keepUntilIDelete = "Keep until I delete"
    
    //MARK: - NotificationController
    case podcastChannels = "Podcast Channels"
    
    //MARK: - SideMenu
    case home = "Home"
    case settings = "Settings"
    case about = "About"
    case contactUs = "Contact Us"
    case changeLanguage = "Change Language"
    case login = "Login"
    case logOut = "Log Out"
    
    //MARK: - WalkThroughController
    case letsBegin = "Lets begin!"
    case next = "Next"
    
    //MARK: - Login
    case accountIsNotVerifiedAndPhoneNumberIsNotProvided = "Account is not verified and phone number is not provided"
    
    //MARK: - ResetCode
    case enterEmailVerificationCode = "Enter email verification code"
    case codeHasBeenSentToYour = "Code has been sent to your"
    case enterResetCode = "Enter Reset Code"
    case resetCodeHasBeenSentToYourEmail = "Reset code has been sent to your email"
    case didNotRecieveCodeYet = "Didn't get a code yet? Resend Code"
    
    //MARK: - Language Selection
    case pleaseRestartTheApplication = "Please restart the application."
    
    
    //SingleMethod For the Usage of Localization.
    var text: String { return NSLocalizedString( self.rawValue, comment: "") }
}
