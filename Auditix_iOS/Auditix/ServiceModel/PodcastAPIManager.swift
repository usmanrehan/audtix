//
//  PodcastAPIManager.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PodcastAPIManager: APIManagerBase {
    
    func addListeningEventPodcast(with params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.addListeningEventPodcast, params: params)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetListeningEventPodcast
    func getNewAndNoteworthyPodcasts(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getListeningEventPodcast)!
        self.getRequestWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetAllCategories
    func getPodcastCategories(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodcastCategories, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }

    func getPodcastCategoriesByCountryIds(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodcastCategoriesByCountryId, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }

    // MARK: - /GetAccountPodcasts
    func getSubscribePodcasts(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.subscribePodcast)!
        self.getRequestWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetDefaultPodCast
    func getPodcasts(with param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodcasts, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - SetAutoDownloadPodcast
    func setAutodownloadPodcast(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.SetAutodownloadPodcasts, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }

    // MARK: - UnsetAutoDownloadPodcast
    func unsetAutodownloadPodcast(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.UnsetAutodownloadPodcasts, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }

//    // MARK: - /GetPodCastByCategoryId
//    func getPodcastsByCategoryID(with param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = URLforRoute(route: Constants.getPodCastByCategoryId, params: param)! as URL
//        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
//    }
    
    // MARK: - GetPodCastByCategoryId
    func getPodcastsByCategoryID(with param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodCastByCategoryId, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetPodcastFeedByTrackId
    func getPodcastDetail(with param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodcastDetail, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }

    func getPodcastEpisodes(with param: [String : Any], success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getPodcastEpisodes, params: param)! as URL
        self.getRequestWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }

    // MARK: - /AddUpdateFavoritePodcast
    func updateFavoritePodcast(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.updateFavoritePodcast, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
//        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /SubscribePodcast
    func subscribeThisPodcast(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {     
        let route: URL = URLforRoute(route: Constants.subscribeThisPodcast, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /UnsubscribePodcast
    func unSubscribeThisPodcast(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.unSubscribeThisPodcast, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /Get All Podcast Category
    func getPodcastFilterCategories(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getFilterPodcastCategories, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    // MARK: - /Get Podcast Location List
    func getPodcastLocationListing(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetLocationList, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    // MARK: - /AddPodcastRating
    func addPodcastRating(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.AddPodcastRating, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetFavoriteAccountPodcasts
    func GetFavoriteAccountPodcasts(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetFavoriteAccountPodcasts)!
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    
    //MARK: - AddToListen
    func AddToListen(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.AddToListen, params: params)! as URL
        self.postRequestForBoolWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
}

