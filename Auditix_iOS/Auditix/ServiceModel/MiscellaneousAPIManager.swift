//
//  MiscellaneousAPIManager.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MiscellaneousAPIManager: APIManagerBase {
    // MARK: - /GetSearch
    func getSearch(parameters: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getSearch, params: parameters)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - CMS
    func getAllCMS(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, param: Parameters) {
        let route: URL = GETURLfor(route: Constants.CMS)!
        self.getRequestWith(route: route, parameters: param, success: success, failure: failure, withHeader: false)
    }
    
    // MARK: - /GET ADVERTISE
    func getAdvertise(params: Parameters ,success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetAdvertisement, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
        //self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - Get All News Podcasts
    func getAllNewsPodcasts(success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetAllNewsPodcast)!
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - SetAutoDownloadPodcast
    func setNotificationSettings(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.SetNotificationSettings, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - UnsetAutoDownloadPodcast
    func unsetNotificationSettings(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.UnsetNotificationSettings, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - Get All Notificaitons
    func getAllNotifications(param: Parameters,success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetAllNotifications, params: param)! as URL
        self.postRequestArrayWith(route: route, parameters: [:], success: success, failure: failure, withHeaders: true)
    }
    
    // MARK: - /GET Pooling
    func getNewsPodcastPolling(param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetNewsPodcastPolling, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - //RemoveNotification
        func removeNotification(param: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
            let route : URL = URLforRoute(route: Constants.RemoveNotifications, params: param)! as URL
            //let route: URL = POSTURLforRoute(route: Constants.RemoveNotifications)!
            self.postRequestForBoolWith(route: route, parameters: param, success: success, failure: failure, withHeaders: true)
        }
    //MARK: - /Comment
    func ContactUs(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.Contact, params: params)! as URL
        self.postRequestForBoolWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
}
