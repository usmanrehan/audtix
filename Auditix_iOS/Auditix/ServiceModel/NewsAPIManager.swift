//
//  NewsAPIManager.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class NewsAPIManager: APIManagerBase {
    
    // MARK: - /GetAllNewsCategory
    func getAllNewsCategory(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getAllNewsCategory)!
        self.getRequestWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetAllNewsByCategoryId
    func GetAllNewsByCategoryId(param: [String : Any], success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getAllNewsByCategoryId, params: param)! as URL
        print(route)
        print(param)
        self.getRequestWith(route: route, parameters: param, success: success, failure: failure, withHeader: true)
    }

    // MARK: - GetSortedNewsEpisodes
    func GetSortedNewsEpisodes(param: [String : Any], success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.getSortedNewsEpisodes, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }

    // MARK; - /SubscribeThisNewsChannel
    func SubscribeThisNewsChannel(param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.subscribeNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    // MARK; - /UnSubscribeThisNewsChannel
    func UnSubscribeThisNewsChannel(param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.unSubscribeNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }

    // MARK; - /GetAllSubscribedNewsChannel
    func GetAllNewsSubscriptions(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getAllNewsSubscriptions)!
        self.getRequestWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /FavoriteNews
    func makeThisNewsAFavorite(param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.favoriteNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }

    // MARK: - /UnFavoriteNews
    func unFavoriteNews(param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.unFavoriteNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /GetAllFavoriteNews
    func getAllFavoriteNews(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getAllFavoriteNews)!
        self.getRequestWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - SetAutoDownloadNews
    func setAutodownloadNews(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.SetAutodownloadNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - UnsetAutoDownloadNews
    func unsetAutodownloadNews(with param: [String : Any], success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.UnsetAutodownloadNews, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
}
