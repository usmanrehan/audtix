//
//  BooksAPIManager.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UAProgressView

class BooksAPIManager: APIManagerBase {
    
    // MARK: - /DefaultCategoryBooks
    func BookHome(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.BookHome, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /CATEGORY ID BOOK
    func getCategoryIDBooks(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.BooksByCategoryId, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - GetAllCategories
    func getBookCategories(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetBookCategories, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }

    
    // MARK: - /Get All Genre
    func getBooksAllFilterGenre(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetAllGenre, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    // MARK: - /BOOK DETAIL
    func getBookDetails(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.BookDetail, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /AddToLibrary
    func addToLibrary(params: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.AddBooksToLibrary, params: params)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /MyLibraryBooks
    func getMyLibraryBooks(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.MyLibraryBooks, params: params)! as URL
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /Download This File
    func startDownloadBook(with downloadUrl: String, saveUrl: String, success: @escaping DefaultDownloadSuccessClosure, progressDownload: @escaping DefaultDownloadProgressClosure, failure: @escaping DefaultDownloadFailureClosure) {
        self.downloadWith(downloadUrl: downloadUrl, saveUrl: saveUrl, index: 0, success: success, downloadProgress: progressDownload, failure: failure)
    }
    
    // MARK: - /GET FAVORITE BOOKS
    func getFavoriteBooks(with param: [String : Any],  success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {

        let route: URL = URLforRoute(route: Constants.GetFavoriteBooks, params: param)! as URL

        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }

    func getFeaturedBooks(with param: [String : Any],  success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetFeaturedBooks, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /MARK THIS BOOK AS FVT
    func addThisBooksToFvtList(with param: [String : Any],  success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.AddBookToFavorite, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    
    // MARK: - /REMOVE FROM FVT
    func removeFromFvt(with param: [String : Any],  success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.RemoveBookFromFavorite, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }

    // MARK: - /RATE THIS BOOK
    func rateThisBook(with param: [String : Any],  success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = URLforRoute(route: Constants.BookRating, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
}
