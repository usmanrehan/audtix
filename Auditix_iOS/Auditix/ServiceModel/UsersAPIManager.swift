//
//  SellerApiManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/24/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    
    //MARK: - /Login
    func LoginUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Login)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    // MARK: - /SocialLogin
    func socailLogin(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.SocialLogin)!
        self.postRequestWith(route: route, parameters: param, success: success, failure: failure, withHeaders: false)
    }
    // MARK: - /GET GOOGLE PROFILE
    func getGoogleProfile(with token: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getGoogleUserInfo + token)!
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK: - /Register
    func RegisterUser(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Register)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - RegisterPushNotification
    func registerDevice(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.RegisterDevice)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK: - /GuestRegistration
    func  GuestRegistration(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.GuestRegistration)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /ResendSMSCode
    func ResetPassword(param: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.ResetPassword, params: param)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: false)
    }
    // MARK: - /Validate Code
    func validCode(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.ValidateCode, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    // MARK: - //VerifySMSCode
    func verifyPhoneCode(param: Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.VerifyPhoneCode)!
        self.postRequestForBoolWith(route: route, parameters: param, success: success, failure: failure, withHeaders: true)
    }
    //MARK: - /Edit Profile
    func editProfile(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.EditProfile)!
        self.postRequestWithMultipart(route: route, parameters: param, success: success, failure: failure)
    }
    // MARK: -  /Get All Notifications
    func getAllNotifications(param: Parameters,success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetAllNotifications)!
        self.postRequestArrayWith(route: route, parameters: param, success: success, failure: failure, withHeaders: true)
    }
    // MARK: - /GetAllWalkThrough
    func getAllWalkThrough(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, params: Parameters) {
        let route: URL = GETURLfor(route: Constants.GetAllWalkThrough)!
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- /GetPodcastBookNews
    func getPodcastBookNews(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)
    {
        let route: URL = URLforRoute(route: Constants.GetPodcastBookNews, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    // MARK: - /GetFilterData
    func getFilterData(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetFilterData, params: params)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK:- /GetPodcastBookNews
    func getPodcastNewsAndBookByEpisodesId(param: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route: URL = URLforRoute(route: Constants.GetPodcastNewsAndBookByEpisodesId, params: param)! as URL
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: true)
    }
    // MARK: - /GetFilterResponse
    func getFilterResponse(params: Parameters,success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.GetFilterResponse, params: params)! as URL
        self.postRequestArrayWith(route: route, parameters: Parameters(), success: success, failure: failure, withHeaders: true)
    }
    // MARK: - /ChangeLanguage
    func getChangeLanguage(params: Parameters,success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Constants.ChangeLanguage, params: params)! as URL
        self.getRequestForBoolWith(route: route, success: success, failure: failure, withHeader: true)
    }
    //MARK: - AddToListen
    func AddToListen(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddToListen)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
}

