//
//  APIManager.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit

typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
typealias DefaultArrayResultAPISuccessClosure = (Array<AnyObject>) -> Void
typealias DefaultImageResultClosure = (UIImage) -> Void
// download closures
typealias DefaultDownloadSuccessClosure = (Data) -> Void
typealias DefaultDownloadProgressClosure = (Double, Int) -> Void
typealias DefaultDownloadFailureClosure = (NSError, Data, Bool) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}


class APIManager: NSObject {
    
    
    static let sharedInstance = APIManager()
    
    var serverToken: String? {
        get {
            return AppStateManager.sharedInstance.loggedInUser.Token?.access_token
        }
    }
    
    var accessToken: String? {
        get {
            return AppStateManager.sharedInstance.loggedInUser.Token?.token_type
        }
    }
    
    let usersAPIManager = UsersAPIManager()
    let bookApiManager = BooksAPIManager()
    let podcastApiManager = PodcastAPIManager()
    let newsApiManager = NewsAPIManager()
    let miscellaneousAPIManager = MiscellaneousAPIManager()
    
}
