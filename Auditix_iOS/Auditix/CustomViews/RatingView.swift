//
//  RatingView.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Cosmos
class RatingView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var BtnCross: UIButton!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var BtnSubmit: UIButton!
    
    class func instanceFromNib() -> RatingView {
        return UINib(nibName: "RatingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RatingView
    }
}
