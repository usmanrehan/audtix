//
//  RoundView.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit

class CircularView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let buttonHeight = self.frame.height
        self.layer.cornerRadius = CGFloat(buttonHeight / 2.0)
        self.clipsToBounds = true
    }
}

class CircularImageView: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let buttonHeight = self.frame.height
        self.layer.cornerRadius = CGFloat(buttonHeight / 2.0)
        self.clipsToBounds = true
    }
}
