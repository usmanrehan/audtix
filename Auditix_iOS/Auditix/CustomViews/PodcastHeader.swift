//
//  PodcastHeader.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastHeader: UIView {

    // btns
    @IBOutlet weak var BtnToday: UIButton!
    @IBOutlet weak var BtnThisMonth: UIButton!
    @IBOutlet weak var BtnOlder: UIButton!
    
    //labels
    @IBOutlet weak var labelToday: UILabel!
    @IBOutlet weak var labelThisMonth: UILabel!
    @IBOutlet weak var labelOlder: UILabel!
    
    //views
    @IBOutlet weak var viewToday: UIView!
    @IBOutlet weak var viewThisMonth: UIView!
    @IBOutlet weak var viewOlder: UIView!
    
    let orangeColor = UIColor(red: 255, green: 126, blue: 89) //255,126,89
    let textColor = UIColor(red: 104, green: 104, blue: 104)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionYourPodcastSectionTab), name: NSNotification.Name(rawValue: Constants.yourPodcastSectionTab), object: nil)
        //custom logic goes here
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    @objc func actionYourPodcastSectionTab( data: NSNotification) {
        let buttonInfo = data.userInfo
        let btntag = buttonInfo?["BtnTag"] as! Int
        
        switch btntag {
        case 0:
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear], animations: {
                self.labelToday.textColor = self.orangeColor
                self.viewToday.backgroundColor = self.orangeColor
                
                self.labelThisMonth.textColor = self.textColor
                self.viewThisMonth.backgroundColor = UIColor.clear
                
                self.labelOlder.textColor = self.textColor
                self.viewOlder.backgroundColor = UIColor.clear
            })
            break
        case 1:
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear], animations: {
                self.labelToday.textColor = self.textColor
                self.viewToday.backgroundColor = UIColor.clear
                
                self.labelThisMonth.textColor = self.orangeColor
                self.viewThisMonth.backgroundColor = self.orangeColor
                
                self.labelOlder.textColor = self.textColor
                self.viewOlder.backgroundColor = UIColor.clear
            })
            break
        case 2:
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear], animations: {
                self.labelToday.textColor = self.textColor
                self.viewToday.backgroundColor =  UIColor.clear
                
                self.labelThisMonth.textColor = self.textColor
                self.viewThisMonth.backgroundColor = UIColor.clear
                
                self.labelOlder.textColor = self.orangeColor
                self.viewOlder.backgroundColor = self.orangeColor
            })
            break
            
        default:
            print("invalid")
        }
    }
    
    
}
