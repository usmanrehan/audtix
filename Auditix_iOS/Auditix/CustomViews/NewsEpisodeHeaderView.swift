//
//  NewsEpisodeHeaderView.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsEpisodeHeaderView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDownloadAll: UIButton!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    class func instanceFromNib() -> NewsEpisodeHeaderView {
        return UINib(nibName: "NewsEpisodeHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NewsEpisodeHeaderView
    }
}
