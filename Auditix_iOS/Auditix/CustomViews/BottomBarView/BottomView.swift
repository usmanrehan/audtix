//
//  BottomView.swift
//  Bottom Player Testing
//
//  Created by Ahmed Shahid on 3/27/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import Jukebox
import LGSideMenuController
import RealmSwift
import UAProgressView

enum BottomBarStates {
    case minimize
    case maximize
    case close
}

class BottomView: UIView {
    
    // MARK: - IBOUTLETS
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var imageviewBottomBar: UIImageView!
    @IBOutlet weak var labelMainTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var btnPlayPauseBottomBar: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var imageviewBtnPlayPauseBottomBar: UIImageView!
    @IBOutlet weak var imageviewCenter: UIImageView!
    @IBOutlet weak var stackViewTapingArea: UIStackView!
    @IBOutlet weak var labelMainTitleCenter: MarqueeLabel!
    @IBOutlet weak var labelSubTitleCenter: MarqueeLabel!
    @IBOutlet weak var btnSpeaker: UIButton!
    @IBOutlet weak var btnPlayPauseCenter: UIButton!
    @IBOutlet weak var btnTimeTop: UIButton!
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var progressView: UAProgressView!
    @IBOutlet weak var LabelSeekbarTime: UILabel!
    @IBOutlet weak var indicatorCenter: UIActivityIndicatorView!
    @IBOutlet weak var viewBottomBar: UIView!
    @IBOutlet weak var viewPlayerFullScreen: UIView!
    //    @IBOutlet weak var viewCentre: UIView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    @IBOutlet weak var viewExtraPadding: UIView!
    @IBOutlet weak var ViewbackBtn: UIView!
    
    var currentBarState: BottomBarStates? = .close // hold state ky view minimize hai yaa maximize yaa band hai
    var isPresented: Bool = false
    
    static let sharedInstance = instanceFromNib()
    private var panGesture: UIPanGestureRecognizer?
    private var tapGesture: UITapGestureRecognizer?
    
    private var tabBarHeight: CGFloat = 0.0
    
    private var jukebox: Jukebox?
    
    public var currentPlayingURL: String? = ""
    
    public var playingType = PlayingType.podcast
    var currentTrackIndex: Int = 0
    
    // for remote controller
    let commandCenter = MPRemoteCommandCenter.shared()
    
    // public properties for podcast
    //public var currentPodcast: PodcastDetailModel!
    
    public var trackIndex = 0
    
    public var podcast: PodcastModel!
    public var episodeIndex = 0
    
    public var currentBook: BookModel!
    public var chapterIndex = 0
    
    //public var currentNews: NewsModel!
    public var newsObject: NewsListModel!
    public var newsIndex = 0
    var isVolumeOn: Bool = true // for Bug #9262
    var playerYCoordinate : CGFloat = 0.0
    
    //public var currentPodcastTrackID: Int!
    //public var playingThisEpisode: Int = 0 // if user directly tab on episode
    

    
    // MARK: - INIT METHODS
    override func awakeFromNib() {
        
        self.jukebox = Jukebox(delegate: self, items: [])
        self.setTheTabBarHeight()
        self.isPresented = true
        self.panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragUp(_:)))
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showWholeContent))
        
        self.addGestureRecognizer(tapGesture!)
        self.addGestureRecognizer(panGesture!)
        
        self.tableview.register(UINib(nibName: "PlayerListCell", bundle: nil), forCellReuseIdentifier: "PlayerListCell")
        
        self.addNotificationObservers()
        
        // UI Customisation
        self.uiCustomization()
        
        setupProgressView()
    }
    
    class func instanceFromNib() -> BottomView {
        return UINib(nibName: "BottomView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomView
    }
    
    func setTheTabBarHeight() {
        // reference https://stackoverflow.com/questions/25550831/whats-the-height-of-a-uitabbar-on-ios-8-ios-9-ios-10-and-ios-11
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436: // for iphone x tab bar
                self.tabBarHeight = 83.0
            default:
                self.tabBarHeight = 49.0
            }
        }
        
        //self.tabBarHeight -= 35 // padding of 35 points
    }
    
    func setupProgressView() {
        //Setup Progress View
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.progressView.frame.size.width * 0.9, height: self.progressView.frame.size.height))
        label.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        label.textColor = Constants.THEME_ORANGE_COLOR
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        self.progressView.centralView = label
        self.progressView.tintColor = Constants.THEME_ORANGE_COLOR
        
        self.progressView.progressChangedBlock = {
            (progressView, progress) -> Void in
            print("progress \(progress)")
            if let label = progressView?.centralView as? UILabel {
                label.text = String(format: "%2.0f%%", progress * 100)
            }
        }
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskCompleted), name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskIsInProgress), name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadCompleteSoUpdateArray), name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.stopPlayerAndClearAllData), name: NSNotification.Name(rawValue: Constants.stopPlaying), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.stopPlayerAndClearAllDataAndRemove), name: NSNotification.Name(rawValue: Constants.removePlayerAndStopPlaying), object: nil)
        
    }
    
    private func processStartDownload() {
        if self.playingType == .book {
            if let chapter = self.currentBook.Chapters?.Chapter[self.currentTrackIndex] {
                let fileUrl = Utility.main.returnBookFileUrl(with: self.currentBook.BookID, chapterID: chapter.ChapterID)
                let downloadPath = chapter.AudioUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let downloader = Downloader.init(downloadPath!, downloadPath!, chapter.ChapterID, fileUrl.absoluteString)
                downloader.startDownload()
            }
        } else if self.playingType == .podcast {
            let episode = self.podcast.episodes[self.currentTrackIndex]
            let fileURL = Utility.main.returnPodcastFileUrl(with: self.podcast.PodcastId, EpisodeID: episode.PodcastEpisodeID)
            let downloadPath = episode.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let downloader = Downloader.init(downloadPath!, downloadPath!, self.podcast.TrackId, fileURL.absoluteString)
            downloader.startDownload()
        } else if self.playingType == .news {
            let episode = self.newsObject.episodes[self.currentTrackIndex]
            let fileURL = Utility.main.returnNewsFileUrl(with: self.newsObject.NewsID, EpisodeID: episode.NewsEpisodeID)
            let downloadPath = episode.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let downloader = Downloader.init(downloadPath!, downloadPath!, self.newsObject.NewsID, fileURL.absoluteString)
            downloader.startDownload()
        }
    }
    
    private func closeAndHideBottomPlayer() {
        let window: UIWindow? = UIApplication.shared.keyWindow
        let yOutSideTheScreen = window?.rootViewController?.view.frame.size.height
        self.frame = CGRect(x: self.frame.origin.x, y: yOutSideTheScreen!, width: self.frame.size.width, height: self.frame.size.height)
        self.currentBarState = .close
    }
    
    // MARK: - Delegates
    func didMinimize() {
        self.viewBottomBar.isHidden = false
        self.tapGesture?.isEnabled = true
    }
    
    func didMaximize() {
        self.viewBottomBar.isHidden = true
        self.tapGesture?.isEnabled = false
    }
    
    // MARK: - HELPER METHODS
    // call this method to show bottom bar first time
    
    func uiCustomization(){
        self.slider.setThumbImage(UIImage(named: ""), for: .normal)
        
        self.slider.setMinimumTrackImage(UIImage(named: "back_slider"), for: UIControlState.normal)
        self.slider.setMaximumTrackImage(UIImage(named: "front_slider"), for: UIControlState.normal)
    }
    
    func remoateCommandCentreClosure() {
        commandCenter.pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the pause command
            print("pauseCommand")
            
            if let currentTime = self.jukebox?.currentItem?.currentTime {
                if currentTime >= 0.0 {
                    switch self.jukebox?.state {
                    case .ready? :
                        self.jukebox?.play(atIndex: 0)
                    case .playing? :
                        self.jukebox?.pause()
                    case .paused? :
                        self.jukebox?.play()
                    default:
                        self.jukebox?.stop()
                    }
                    return .success
                }
                else {
                    return .commandFailed
                }
            }
            return .commandFailed
        }
        commandCenter.playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("playCommand")
            
            if let currentTime = self.jukebox?.currentItem?.currentTime {
                if currentTime >= 0.0 {
                    switch self.jukebox?.state {
                    case .ready? :
                        self.jukebox?.play(atIndex: 0)
                    case .playing? :
                        self.jukebox?.pause()
                    case .paused? :
                        self.jukebox?.play()
                    default:
                        self.jukebox?.stop()
                    }
                    return .success
                }
                else {
                    return .commandFailed
                }
            }
            
            return .commandFailed
            
            
        }
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("nextTrackCommand")
            return .success
        }
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("previousTrackCommand")
            return .success
        }
    }
    
    public func showBottomView() {
        self.viewPlayerFullScreen.alpha = 0.0
        var window: UIWindow? = UIApplication.shared.keyWindow
        
        if window == nil {
            window = UIApplication.shared.windows[0]
        }
        
        let tempHeight = window?.rootViewController?.view.frame.size.height ?? 0
        let tempWidth = window?.rootViewController?.view.frame.size.width ?? 0
        
        //Set initial frame
        //self.frame = CGRect(x: 0, y: tempHeight, width: tempWidth, height: /*tempHeight! +*/ tempHeight * Constants.bottomBarHeightSize)
        //self.constraintHeight.constant = tempHeight * Constants.bottomBarHeightSize
        //self.frame = CGRect(x: 0, y: tempHeight, width: tempWidth, height: 60)
        //self.constraintHeight.constant = 60//self.viewBottomBar.frame.size.height
        window?.rootViewController?.view.addSubview(self)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            //self.frame = CGRect(x: 0, y: (tempHeight - (tempHeight * 0.15)) - self.tabBarHeight, width: tempWidth, height: /*tempHeight! +*/ tempHeight * Constants.bottomBarHeightSize)
            self.frame = CGRect(x: 0, y: tempHeight - (self.tabBarHeight + 60 + 8), width: tempWidth, height: 60)
            self.didMinimize()
        }) { _ in
            self.viewBottomBar.dropShadow(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        }
    }
    
    private func minimizeTheBar() {
        self.viewPlayerFullScreen.alpha = 0.0
        var window: UIWindow? = UIApplication.shared.keyWindow
        if window == nil {
            window = UIApplication.shared.windows[0]
        }
        let tempHeight = window?.rootViewController?.view.frame.size.height ?? 0
        let tempWidth = window?.rootViewController?.view.frame.size.width ?? 0
        
        self.frame = CGRect(x: 0, y: tempHeight - (self.tabBarHeight + 60 + 8),
                            width: tempWidth, height: 60)
        self.currentBarState = .minimize
        self.didMinimize()
    }
    private func populateLabelWithTime(_ label : UILabel, time: Double) {
        let minutes = Int(time / 60)
        let seconds = Int(time) - minutes * 60
        
        label.text = "-" + String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
    
    private func showBuffering() {
        self.indicator.alpha = 1
        self.indicatorCenter.alpha = 1
        self.indicator.startAnimating()
        self.indicatorCenter.startAnimating()
        
        self.btnPlayPauseCenter.alpha = 0
        self.btnPlayPauseBottomBar.alpha = 0
        self.imageviewBtnPlayPauseBottomBar.alpha = 0
        self.btnPlayPauseBottomBar.isHidden = true
        self.btnPlayPauseCenter.isEnabled = false
        self.btnPlayPauseBottomBar.isEnabled = false
        
        self.slider.isUserInteractionEnabled = false
    }
    
    private func hideBuffering() {
        self.indicator.alpha = 0
        self.indicator.stopAnimating()
        self.indicatorCenter.alpha = 0
        self.indicatorCenter.stopAnimating()
        
        self.btnPlayPauseCenter.alpha = 1
        self.btnPlayPauseBottomBar.alpha = 1
        self.imageviewBtnPlayPauseBottomBar.alpha = 1
        self.btnPlayPauseBottomBar.isHidden = false
        self.btnPlayPauseCenter.isEnabled = true
        self.btnPlayPauseBottomBar.isEnabled = true
        
        self.slider.isUserInteractionEnabled = true
    }
    
    @objc private func stopPlayerAndClearAllDataAndRemove() {
        self.stopPlayerAndClearAllData()
        self.jukebox?.stop()
        
        //self.currentPodcast = nil
        self.podcast = nil
        self.currentBook = nil
        self.newsObject = nil
        self.closeAndHideBottomPlayer()
    }
    @objc private func stopPlayerAndClearAllData() {
        self.jukebox?.stop()
        if let jukeBoxItem = self.jukebox?.currentItem {
            self.jukebox?.remove(item: jukeBoxItem)
        }
        
        self.slider.value = 0.0
    }
}

//MARK: - Target-Action Methods
extension BottomView {
    @objc func didDragUp(_ gesture: UIPanGestureRecognizer) {
        if gesture.state == .ended{
            if gesture.location(in: window?.rootViewController?.view).y >= self.playerYCoordinate{
                self.playerYCoordinate = ((window?.rootViewController?.view.center.y)! * 2)
                self.showWholeContent()
            }
            else{
                self.playerYCoordinate = 0.0
                self.minimizeTheBar()
            }
        }
    }
    
    @objc func showWholeContent() {
        self.viewPlayerFullScreen.alpha = 1.0
        let tempHeight = window?.rootViewController?.view.frame.size.height
        let tempWidth = window?.rootViewController?.view.frame.size.width
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1334:
                self.frame = CGRect(x: 0, y:  -(tempHeight! * Constants.bottomBarHeightSize), width: tempWidth!, height: tempHeight! + tempHeight! * Constants.bottomBarHeightSize)
            case 1920, 2208:
                self.frame = CGRect(x: 0, y:  -((tempHeight! * Constants.bottomBarHeightSize) - 10), width: tempWidth!, height: tempHeight! + tempHeight! * Constants.bottomBarHeightSize)
            default:
                self.frame = CGRect(x: 0, y:  -(tempHeight! * Constants.bottomBarHeightSize), width: tempWidth!, height: tempHeight! + tempHeight! * Constants.bottomBarHeightSize)
            }
        }
        
        self.didMaximize()
    }
}

// MARK: - IBAction Methods
extension BottomView {
    @IBAction func actionBtnClose(_ sender: Any) {
        self.minimizeTheBar()
    }
    
    @IBAction func actionPlayPause(_ sender: Any) {
        switch jukebox?.state {
        case .ready? :
            jukebox?.play(atIndex: 0)
        case .playing? :
            jukebox?.pause()
        case .paused? :
            jukebox?.play()
        default:
            jukebox?.stop()
        }
    }
    
    @IBAction func actionBackward(_ sender: Any) {
        switch self.playingType {
        case .book:
            self.playPreviousChapter()
            break
        case .podcast:
            self.playPreviousPodcast()
            break
        case .news:
            self.playPreviousNews()
            break
        default:
            break
        }
        
        //self.tableview.reloadData()
    }
    
    @IBAction func actionForward(_ sender: Any) {
        switch self.playingType {
        case .book:
            self.playNextChapter()
            break
        case .podcast:
            self.playNextPodcast()
            break
        case .news:
            self.playNextNews()
            break
        default:
            break
        }
        
        //self.tableview.reloadData()
    }
    
    @IBAction func actionSeekBarValueChanged(_ sender: Any) {
        if let sliderObj = sender as? UISlider {
            if let duration = jukebox?.currentItem?.meta.duration {
                jukebox?.seek(toSecond: Int(Double(sliderObj.value) * duration))
            }
        }
    }
    
    @IBAction func actionFavorite(_ sender: Any) {
        switch self.playingType {
        case .podcast:
            updatePodcastFavoriteStatus(with: self.podcast.IsFavorite, trackID: self.podcast.TrackId)
            break
        case .book:
            if self.currentBook.IsFavorite {
                removeFromFavoriteBook(with: self.currentBook.BookID)
            } else {
                addToFavoriteBook(with: self.currentBook.BookID)
            }
            break
        case .news:
            if self.newsObject.IsFavorite {
                removeFromFavoriteNews(with: self.newsObject.NewsID)
            } else {
                addToFavoriteNews(with: self.newsObject.NewsID)
            }
            break
        default:
            break
        }
    }
    
    @IBAction func actionVolume(_ sender: Any) {
        let button = sender as! UIButton
        if(button.isSelected) {
            self.jukebox?.volume = 1
            self.isVolumeOn = true
        } else {
            self.jukebox?.volume = 0
            self.isVolumeOn = false
        }
        
        button.isSelected = !button.isSelected
    }
    
    @IBAction func actionClose(_ sender: UIButton) {
        self.jukebox?.stop()
        
        //self.currentPodcast = nil
        self.podcast = nil
        self.currentBook = nil
        self.newsObject = nil
        self.closeAndHideBottomPlayer()
    }
    
    @IBAction func actionDownload(_ sender: UIButton) {
        switch self.playingType {
        case .book:
            self.downloadThisBookChapter(with: self.currentTrackIndex)
            break
        case .podcast:
            self.downloadThisPodcastEpisode(with: self.currentTrackIndex)
            break
        case .news:
            self.downloadThisNewsEpisode(with: self.currentTrackIndex)
            break
        default:
            break
        }
    }
    
    @IBAction func actionShare(_ sender: UIButton) {
        switch self.playingType {
        case .podcast:
            if let urlToShare = self.podcast.episodes[currentTrackIndex].EpisodeWebUrl {
                Utility.main.shareAcitivityWithURL(urlToShare,
                                                   title: self.podcast.Name,
                                                   subtitle: self.podcast.episodes[currentTrackIndex].EpisodeTitle)
            } else {
                Utility.main.showAlert(message: NSLocalizedString("Share url not found", comment: ""), title: NSLocalizedString("Message", comment: ""))
            }
            break
        case .news:
            if let urlToShare = self.newsObject.episodes[currentTrackIndex].EpisodeWebUrl {
                Utility.main.shareAcitivityWithURL(urlToShare,
                                                   title: self.newsObject.Name,
                                                   subtitle: self.newsObject.episodes[currentTrackIndex].EpisodeTitle)
            } else {
                Utility.main.showAlert(message: NSLocalizedString("Share url not found", comment: ""), title: NSLocalizedString("Message", comment: ""))
            }
            break
        case .book:
            if let urlToShare = self.currentBook.Chapters?.Chapter[currentTrackIndex].EpisodeWebUrl {
                Utility.main.shareAcitivityWithURL(urlToShare,
                                                   title: self.currentBook.BookName,
                                                   subtitle: String(format: "Chapter %02d", self.currentBook.Chapters?.Chapter[currentTrackIndex].ChapterNumber ?? 0))
            } else {
                Utility.main.showAlert(message: NSLocalizedString("Share url not found", comment: ""), title: NSLocalizedString("Message", comment: ""))
            }
            break
        }
    }
}

// MARK: - Web APIs Methods
extension BottomView {
    public func addToFavoriteBook(with bookID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = [
            "bookID": bookID
        ]
        APIManager.sharedInstance.bookApiManager.addThisBooksToFvtList(with: param, success: {(success) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = true
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    public func removeFromFavoriteBook(with bookID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = [
            "bookID" : bookID
        ]
        APIManager.sharedInstance.bookApiManager.removeFromFvt(with: param, success: { (responceObj) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = false
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
        }, failure: {
            (failure) in
            Utility.main.hideLoader()
        })
    }
    
    public func updatePodcastFavoriteStatus(with markIsFvt: Bool, trackID: Int) {
        Utility.main.showLoader()
        let params: [String: Any] = ["TrackId": trackID, "favorite": markIsFvt]
        
        APIManager.sharedInstance.podcastApiManager.updateFavoritePodcast(with: params, success: { (responceObj) in
            Utility.main.hideLoader()
            self.podcast.IsFavorite = markIsFvt
            if markIsFvt {
                self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
            } else {
                self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
            }
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
    }
    
    public func addToFavoriteNews(with newsID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId": newsID]
        APIManager.sharedInstance.newsApiManager.makeThisNewsAFavorite(param: param, success: { (successBool) in
            Utility.main.hideLoader()
            self.newsObject.IsFavorite = true
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
    }
    
    public func removeFromFavoriteNews(with newsID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId": newsID]
        APIManager.sharedInstance.newsApiManager.unFavoriteNews(param: param, success: { (successBool) in
            Utility.main.hideLoader()
            self.newsObject.IsFavorite = false
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
    }
}

// MARK: - Player Controller
extension BottomView {
    public func play() {
        switch jukebox?.state {
        case .ready? :
            jukebox?.play(atIndex: 0)
        case .playing? :
            jukebox?.pause()
        case .paused? :
            jukebox?.play()
        default:
            jukebox?.stop()
        }
    }
}

// MARK: - PlayerListing Delegate
extension BottomView: PlayerListingDelegate {
    public func didChapEpisodeSelect(chapEpisodeNo: Int) {
        print("didChapEpisodeSelect")
    }
}

// MARK: - Jukebox Delegate
extension BottomView: JukeboxDelegate {
    public func jukeboxStateDidChange(_ jukebox: Jukebox) {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.indicator.alpha = jukebox.state == .loading ? 1 : 0
            self.indicatorCenter.alpha = jukebox.state == .loading ? 1 : 0
            if jukebox.state == .loading {
                self.indicator.startAnimating()
                self.indicatorCenter.startAnimating()
            }
            else {
                self.indicator.stopAnimating()
                self.indicatorCenter.stopAnimating()
                
            }
            self.imageviewBtnPlayPauseBottomBar.alpha = jukebox.state == .loading ? 0 : 1
            self.btnPlayPauseCenter.alpha = jukebox.state == .loading ? 0 : 1
            self.btnPlayPauseBottomBar.isEnabled = jukebox.state == .loading ? false : true
            self.btnPlayPauseCenter.isEnabled = jukebox.state == .loading ? false : true
        })
        
        self.imageviewBtnPlayPauseBottomBar.image = UIImage(named: "play-icon")
        self.btnPlayPauseCenter.setImage(UIImage(named: "play-icon"), for: .normal)
        
        if jukebox.state == .ready {
            self.imageviewBtnPlayPauseBottomBar.image = UIImage(named: "play-icon")
            self.btnPlayPauseCenter.setImage(UIImage(named: "play-icon"), for: .normal)
        } else if jukebox.state == .loading  {
            self.imageviewBtnPlayPauseBottomBar.image = UIImage(named: "pause-icon")
            self.btnPlayPauseCenter.setImage(UIImage(named: "pause-icon"), for: .normal)
        } else {
            let image: UIImage!
            switch jukebox.state {
            case .playing, .loading:
                image = UIImage(named: "pause-icon")
                self.checkIfFileDownloaded()
            case .paused, .failed, .ready:
                image = UIImage(named: "play-icon")
                self.checkIfFileDownloaded()
            }
            
            self.imageviewBtnPlayPauseBottomBar.image = image
            self.btnPlayPauseCenter.setImage(image, for: .normal)
        }
        
        print("Jukebox state changed to \(jukebox.state)")
        switch jukebox.state {
        case .playing:
            if !self.btnSpeaker.isSelected{
                self.jukebox?.volume = 1
            } else {
                self.jukebox?.volume = 0
            }
            self.checkIfFileDownloaded()
        default:
            self.checkIfFileDownloaded()
            break
        }
    }
    public func checkIfFileDownloaded(){
        switch playingType{
        case .podcast:
            if let podCast = self.podcast{
                if Utility.main.isFileExist(directoryName: "podcast", fileName: "\(podCast.episodes[self.currentTrackIndex].PodcastEpisodeID)"){
                    
                }
            }
        case .book:
            if let book = self.currentBook{
                if Utility.main.isFileExist(directoryName: "book", fileName: "\(String(describing: book.Chapters?.Chapter[self.currentTrackIndex].ChapterID))"){
                    
                }
            }
            
        case .news:
            if let news = self.newsObject{
                if Utility.main.isFileExist(directoryName: "news", fileName: "\(String(describing: news.episodes[self.currentTrackIndex].NewsEpisodeID))"){
                    self.btnDownload.isEnabled = false
                }
                else{
                    self.btnDownload.isEnabled = true
                }
            }
        }
    }
    public func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        self.checkIfFileDownloaded()
        if let currentTime = jukebox.currentItem?.currentTime , let duration = jukebox.currentItem?.meta.duration {
            self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(duration)), for: UIControlState())
            // buffering
            if currentTime == 0.0 {
                self.showBuffering()
            }
            else {
                self.hideBuffering()
            }
            
            if((duration - currentTime) > 0) {
                let value = Float((currentTime / duration))
                slider.value = value
                self.populateLabelWithTime(self.LabelSeekbarTime, time: duration - currentTime)
            } else {
                if let _ = self.newsObject{
                    let newsEpisodeID = self.newsObject.episodes[currentTrackIndex].NewsEpisodeID
                    let episodeNo = self.newsObject.episodes[currentTrackIndex].EpisodeNo
                    if Utility.main.isFileExist(directoryName: "news", fileName: "\(newsEpisodeID)") && !Utility.main.isKeepUntilIDeleteOn(){
                        if Utility.main.deleteDownloadedFile(directoryName: "news", fileName: "\(newsEpisodeID)"){
                            let deletedItemsCSV = AppStateManager.sharedInstance.loggedInUser.deletedItems ?? ""
                            var arrDeletedItems = deletedItemsCSV.components(separatedBy: ",")
                            for (i,obj) in arrDeletedItems.enumerated(){
                                if obj == "\(newsEpisodeID)"{
                                    arrDeletedItems.remove(at: i)
                                    Utility.main.showToast(message: "\(MiscStrings.episode.text) \(episodeNo) \(MiscStrings.deleted.text)")
                                    try! Global.APP_REALM?.write(){
                                        AppStateManager.sharedInstance.loggedInUser.deletedItems = arrDeletedItems.joined(separator: ",")
                                        Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                                    }
                                    break
                                }
                            }
                        }
                    }
                }
                if self.isContinuousPlayOn(){
                    switch playingType{
                    case .podcast:
                        self.playNextPodcast()
                    case .news:
                        self.playNextNews()
                    case .book:
                        self.playNextChapter()
                    }
                    self.slider.value = 0.0
                }
            }
        }
    }
    
    public func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
        self.checkIfFileDownloaded()
//        self.btnSpeaker.isSelected = !self.isVolumeOn
//        if self.isVolumeOn {
//            jukebox.volume = 1
//        } else {
//            jukebox.volume = 0
//        }
    }
    
    public func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
        self.checkIfFileDownloaded()
//        self.btnSpeaker.isSelected = !self.isVolumeOn
//        if self.isVolumeOn {
//            jukebox.volume = 1
//        } else {
//            jukebox.volume = 0
//        }
    }
    func isContinuousPlayOn() -> Bool {
        if let contiBool = Constants.USER_DEFAULTS.value(forKey: Constants.ContinuousPlay) as? Bool {
            if contiBool {
                return true
            }
        }
        return false
    }
    
}

// MARK: - UITableView DataSource Delegate
extension BottomView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if self.playingType == .podcast {
//            return self.podcast.episodes.count
//        } else if self.playingType == .book {
//            return (self.currentBook.Chapters?.Chapter.count)!
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.playingType == .podcast {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerListCell") as? PlayerListCell else { return UITableViewCell() }
            if indexPath.row == self.currentTrackIndex {
                cell.labelChapEpisodeDescribtion.textColor = Constants.THEME_ORANGE_COLOR
                cell.labelChapEpisodeNo.textColor = Constants.THEME_ORANGE_COLOR
            }
            else {
                cell.labelChapEpisodeDescribtion.textColor = UIColor.darkGray
                cell.labelChapEpisodeNo.textColor = UIColor.darkGray
            }
            //cell.labelChapEpisodeDescribtion.text = "\(self..TrackList[indexPath.row].Name ?? "")".uppercased
            cell.labelChapEpisodeDescribtion.text = self.podcast.Name?.uppercased()
            cell.labelChapEpisodeNo.text = "\(MiscStrings.episode.text) \(indexPath.row + 1)".uppercased()
            return cell
        } else if self.playingType == .book {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerListCell") as? PlayerListCell else { return UITableViewCell() }
            cell.labelChapEpisodeDescribtion.isHidden = true
            if indexPath.row == self.currentTrackIndex {
                cell.labelChapEpisodeDescribtion.textColor = Constants.THEME_ORANGE_COLOR
                cell.labelChapEpisodeNo.textColor = Constants.THEME_ORANGE_COLOR
            }
            else {
                cell.labelChapEpisodeDescribtion.textColor = UIColor.darkGray
                cell.labelChapEpisodeNo.textColor = UIColor.darkGray
            }
            cell.labelChapEpisodeNo.text = "\(MiscStrings.chapter.text) \(self.currentBook.Chapters?.Chapter[indexPath.row].ChapterNumber ?? 0)".uppercased()
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        //        if indexPath.row == self.currentTrackIndex {
        //            return
        //        }
        //        if self.playingType == .podcast {
        //             self.playPodcast(withPodcast: indexPath.row)
        //        } else if self.playingType == .book {
        //            self.playThisChapter(with: indexPath.row)
        //        }
        //
        //        tableView.reloadData()
    }
}

//MARK: - Podcast Methods
extension BottomView {
    public func initPodcast() {
        self.currentTrackIndex = self.episodeIndex
        
        //Setup UI
        if episodeIndex >= 0, episodeIndex < self.podcast.episodes.count {
            self.labelSubTitle.text = self.podcast.episodes[episodeIndex].EpisodeTitle
            self.labelSubTitleCenter.text = self.podcast.episodes[episodeIndex].EpisodeTitle
        }
        
        
        self.labelMainTitle.text = self.podcast.Name
        self.labelMainTitleCenter.text = self.podcast.Name
        if let imageURL = URL(string: (self.podcast.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
            self.imageviewBottomBar.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
            self.imageviewCenter.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageviewBottomBar.image =  #imageLiteral(resourceName: "placeholder_image")
            self.imageviewCenter.image =  #imageLiteral(resourceName: "placeholder_image")
        }
        
        self.btnHeart.isHidden = false
        if self.podcast.IsFavorite {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
        } else {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
        }
        
        //self.initPodcastFirstEpisode()
        self.playThisPodcast(with: self.episodeIndex)
        
        self.tableview.isHidden = false
        self.tableview.reloadData()
        
        self.currentBarState = .minimize
    }
    
    public func playThisPodcast(with index: Int) {
        if index >= 0, self.podcast.episodes.count > index {
            let episode = self.podcast.episodes[index]
            
            self.labelSubTitle.text = self.podcast.episodes[index].EpisodeTitle
            self.labelSubTitleCenter.text = self.podcast.episodes[index].EpisodeTitle
            
            self.stopPlayerAndClearAllData()
            
            if Utility.main.isThisPodcastFileExist(with: self.podcast.PodcastId, EpisodeID: episode.PodcastEpisodeID) {
                let fileURL =  Utility.main.returnPodcastFileUrl(with: self.podcast.PodcastId, EpisodeID: episode.PodcastEpisodeID)
                self.jukebox?.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
            } else {
                let filePath = episode.FilePath ?? ""
                if let url = URL(string: (filePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
                    self.jukebox?.append(item: JukeboxItem(URL: url), loadingAssets: true)
                }
            }
            
            self.jukebox?.play()
        }
    }
    
    public func playPreviousPodcast() {
        if let time = self.jukebox?.currentItem?.currentTime, time > 5.0 || self.currentTrackIndex == 0 {
            jukebox?.replayCurrentItem()
            return
        }
        
        let index = self.currentTrackIndex - 1
        
        if index >= 0, self.podcast.episodes.count > index {
            self.currentTrackIndex = index
            self.playThisPodcast(with: self.currentTrackIndex)
        }
    }
    
    public func playNextPodcast() {
        let index = self.currentTrackIndex + 1
        
        if index >= 0, self.podcast.episodes.count > index {
            self.currentTrackIndex = index
            self.playThisPodcast(with: self.currentTrackIndex)
        }
    }
    
    public func downloadThisPodcastEpisode(with index: Int) {
        if index >= 0, index < self.podcast.episodes.count {
            let episode = self.podcast.episodes[index]
            
            if Utility.main.isThisPodcastFileExist(with: self.podcast.PodcastId, EpisodeID: episode.PodcastEpisodeID) {
                Utility.main.showAlert(message: NSLocalizedString("File is already downloaded", comment: ""), title: NSLocalizedString("Message", comment: ""))
            } else {
                self.processStartDownload()
            }
        }
    }
}

//MARK: - Book Methods
extension BottomView {
    public func initBook() {
        self.currentTrackIndex = self.chapterIndex
        
        //Setup UI
        self.labelMainTitle.text = self.currentBook.BookName
        self.labelMainTitleCenter.text = self.currentBook.BookName
        self.labelSubTitle.text = ""
        self.labelSubTitleCenter.text = ""
        self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(self.currentBook.Duration)), for: UIControlState())
        
        if let imageURL = URL(string: (self.currentBook.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
            self.imageviewBottomBar.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
            self.imageviewCenter.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageviewBottomBar.image =  #imageLiteral(resourceName: "placeholder_image")
            self.imageviewCenter.image =  #imageLiteral(resourceName: "placeholder_image")
        }
        
        self.btnHeart.isHidden = false
        if self.currentBook.IsFavorite {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
        } else {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
        }
        
        //self.initBookFirstChapter()
        self.playThisChapter(with: self.chapterIndex)
        
        self.tableview.reloadData()
        
        self.currentBarState = .minimize
    }
    
    public func playThisChapter(with index: Int) {
        guard self.currentBook.Chapters?.Chapter != nil else {
            return
        }
        
        if index >= 0, (self.currentBook.Chapters?.Chapter.count)! > index {
            let chapter = self.currentBook.Chapters?.Chapter[index]
            
            self.labelSubTitle.text = String(format: "\(MiscStrings.chapter.text) %02d", (chapter?.ChapterNumber)!)
            self.labelSubTitleCenter.text = String(format: "\(MiscStrings.chapter.text) %02d", (chapter?.ChapterNumber)!)
            
            self.stopPlayerAndClearAllData()
            
            if Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: (chapter?.ChapterID)!) {
//                let fileURL = Utility.main.returnBookFileUrl(with: self.currentBook.BookID, chapterID: (chapter?.ChapterID)!)
//                self.jukebox?.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
                let filePath = chapter?.AudioUrl ?? ""
                if let url = URL(string: (filePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
                    self.jukebox?.append(item: JukeboxItem(URL: url), loadingAssets: true)
                }
            } else {
                let filePath = chapter?.AudioUrl ?? ""
                if let url = URL(string: (filePath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
                    self.jukebox?.append(item: JukeboxItem(URL: url), loadingAssets: true)
                }
            }
            self.btnSpeaker.isSelected = false
            self.jukebox?.play()

        }
    }
    
    public func playPreviousChapter() {
        if let time = self.jukebox?.currentItem?.currentTime, time > 5.0 || self.currentTrackIndex == 0 {
            jukebox?.replayCurrentItem()
            return
        }
        
        let index = self.currentTrackIndex - 1
        let totalChapters = self.currentBook.Chapters?.Chapter.count ?? 0
        
        if index >= 0, index < totalChapters {
            self.currentTrackIndex = index
            self.playThisChapter(with: self.currentTrackIndex)
        }
    }
    
    func playNextChapter() {
        let index = self.currentTrackIndex + 1
        let totalChapters = self.currentBook.Chapters?.Chapter.count ?? 0
        
        if index >= 0, index < totalChapters {
            self.currentTrackIndex = index
            self.playThisChapter(with: index)//self.trackIndex)
        }
    }
    
    func downloadThisBookChapter(with index: Int) {
        guard self.currentBook.Chapters?.Chapter != nil else {
            return
        }
        
        if index >= 0, (self.currentBook.Chapters?.Chapter.count)! > index {
            let chapter = self.currentBook.Chapters?.Chapter[index]
            
            if Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: (chapter?.ChapterID)!) {
                Utility.main.showAlert(message: NSLocalizedString("File is already downloaded", comment: ""), title: NSLocalizedString("Message", comment: ""))
            } else {
                self.processStartDownload()
            }
        }
    }
}

//MARK: - News Methods
extension BottomView {
    public func initNews() {
        self.currentTrackIndex = self.trackIndex
        
        //Setup UI
        self.labelMainTitle.text = self.newsObject.Name
        self.labelMainTitleCenter.text = self.newsObject.Name
        self.labelSubTitle.text = self.newsObject.Description
        self.labelSubTitleCenter.text = self.newsObject.Description
        //self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(self.newsObject.)), for: UIControlState())
        
        if let imageURL = URL(string: (self.newsObject.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
            self.imageviewBottomBar.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
            self.imageviewCenter.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.imageviewBottomBar.image =  #imageLiteral(resourceName: "placeholder_image")
            self.imageviewCenter.image =  #imageLiteral(resourceName: "placeholder_image")
        }
        
        self.btnHeart.isHidden = false
        if self.newsObject.IsFavorite {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat1"), for: UIControlState())
        } else {
            self.btnHeart.setImage( #imageLiteral(resourceName: "herat2"), for: UIControlState())
        }
        
        self.playThisNews(with: self.currentTrackIndex)
        
        self.tableview.reloadData()
        
        self.currentBarState = .minimize
    }
    
    public func playThisNews(with index: Int) {
        if index >= 0, index < self.newsObject.episodes.count {
            let episode = self.newsObject.episodes[index]
            
            self.labelSubTitle.text = String(format: "\(MiscStrings.episode.text) %02d", episode.EpisodeNo)
            self.labelSubTitleCenter.text = String(format: "\(MiscStrings.episode.text) %02d", episode.EpisodeNo)
            
            self.stopPlayerAndClearAllData()
            
            if let url = URL(string: (episode.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
                self.jukebox?.append(item: JukeboxItem(URL: url), loadingAssets: true)
            }
            self.btnSpeaker.isSelected = false
            self.jukebox?.play()
        }
    }
    
    public func playPreviousNews() {
        if let time = self.jukebox?.currentItem?.currentTime, time > 5.0 || self.currentTrackIndex == 0 {
            jukebox?.replayCurrentItem()
            return
        }
        
        let index = self.currentTrackIndex - 1
        
        if index >= 0, self.newsObject.episodes.count > index {
            self.currentTrackIndex = index
            self.playThisNews(with: self.currentTrackIndex)
        }
    }
    
    public func playNextNews() {
        let index = self.currentTrackIndex + 1
        
        if index >= 0, self.newsObject.episodes.count > index {
            self.currentTrackIndex = index
            self.playThisNews(with: self.currentTrackIndex)
        }
    }
    
    public func downloadThisNewsEpisode(with index: Int) {
        if index >= 0, index < self.newsObject.episodes.count {
            let episode = self.newsObject.episodes[index]
            if Utility.main.isFileExist(directoryName: "news", fileName: "\(episode.NewsEpisodeID)"){
                Utility.main.showAlert(message: NSLocalizedString("File is already downloaded", comment: ""), title: NSLocalizedString("Message", comment: ""))
            }
//            if Utility.main.isThisNewsFileExist(with: self.newsObject.NewsID, EpisodeID: episode.NewsEpisodeID) {
//                Utility.main.showAlert(message: NSLocalizedString("File is already downloaded", comment: ""), title: NSLocalizedString("Message", comment: ""))
//            }
            else {
                self.processStartDownload()
            }
        }
    }
}

//MARK: - Notification call backs
extension BottomView {
    @objc func downloadCompleteSoUpdateArray( data: NSNotification) {
    }
    
    @objc func downloadTaskIsInProgress( data: NSNotification) {
        let aditionalText = data.userInfo
        self.progressView.isHidden = false
        //self.btnDownload.isEnabled = false
        if let temp = aditionalText!["progress"] as? Double {
            self.progressView.setProgress(CGFloat(temp), animated: true)
        }
    }
    
    @objc func downloadTaskCompleted( data: NSNotification) {
        // Downlaod completed
        self.progressView.isHidden = true
        //self.btnDownload.isEnabled = false
        
        guard self.playingType != nil else { return }
        
        if self.podcast != nil {
            if self.playingType == .podcast {
                PodcastRealmHelper.sharedInstance.addThisPodcast(podcast: self.podcast, trackID: self.podcast.TrackId)
            }
        }
    }
}
