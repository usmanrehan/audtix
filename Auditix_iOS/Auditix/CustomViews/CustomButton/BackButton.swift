//
//  File.swift
//  Auditix
//
//  Created by Usman Bin Rehan on 10/1/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
class BackButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if LanguageManager.sharedInstance.getSelectedLocale() == "ar"{
            self.setImage(#imageLiteral(resourceName: "back-btn-arabic"), for: .normal)
        }else{
            self.setImage(#imageLiteral(resourceName: "back-btn"), for: .normal)
        }
    }
}
class ArrowSettings: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if LanguageManager.sharedInstance.getSelectedLocale().contains("en"){
            self.image = #imageLiteral(resourceName: "right-arrow")
        }else{
            self.image = #imageLiteral(resourceName: "left-arrow")
        }
    }
}
