//
//  DownloadButton.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView
class DownloadButton: UIButton {
    
    var indexPath: IndexPath?
    var index: Int?
    var progressView: UAProgressView?
    
    var isDownloaded: Bool = false
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
