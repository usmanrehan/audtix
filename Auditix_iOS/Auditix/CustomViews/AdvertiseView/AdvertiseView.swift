//
//  AdvertiseView.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/22/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit
import Jukebox

@objc protocol AdvertiseDelegate {
    @objc optional func advertisementIsStop()
}

class AdvertiseView: UIView {
    
    static let sharedInstance = instanceFromNib()
    
    var advertiseJukebox: Jukebox?
    
    var delegate: AdvertiseDelegate? = nil
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.advertiseJukebox = Jukebox(delegate: self, items: [])
    }
    
    class func instanceFromNib() -> AdvertiseView {
        return UINib(nibName: "AdvertiseView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AdvertiseView
    }
    
    func showAdvertiseView() {
        var window: UIWindow? = UIApplication.shared.keyWindow
        
        let tempHeight = window?.rootViewController?.view.frame.size.height
        let tempWidth = window?.rootViewController?.view.frame.size.width
        
        if window == nil {
            window = UIApplication.shared.windows[0]
        }
        
        self.frame = CGRect(x: 0, y: -20, width: tempWidth!, height: tempHeight! + 20)
        window?.rootViewController?.view.addSubview(self)
        
        self.startAdvertise()
    }
    
    private func startAdvertise() {
        let singleton = Singleton.sharedInstance
        //let randomIndex = Int(arc4random_uniform(UInt32(singleton.currentAdvertiseArray.count)))
        guard let AudioUrl = singleton.currentAdvertiseArray.first?.AudioPath else {
            Utility.main.showToast(message: MiscStrings.invalidAdvertismentURL.text)
            self.removeFromSuperview()
            return
        }
//        guard let AudioPath = singleton.currentAdvertiseArray.first?.AudioPath else {
//            Utility.main.showToast(message: "Invalid Advertisment FilePath")
//            self.removeFromSuperview()
//            return
//        }
        if let advertiseURL = URL(string: (AudioUrl).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            self.advertiseJukebox?.append(item: JukeboxItem(URL: advertiseURL), loadingAssets: true)
            self.advertiseJukebox?.play()
        }
    }
}

extension AdvertiseView: JukeboxDelegate
{
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.activityIndicator.alpha = jukebox.state == .loading ? 1 : 0
            if jukebox.state == .loading {
                self.activityIndicator.startAnimating()
            }
            else {
                self.activityIndicator.stopAnimating()
                
            }
            self.label.alpha = jukebox.state == .loading ? 0 : 1
        })
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        if let currentTime = jukebox.currentItem?.currentTime , let duration = jukebox.currentItem?.meta.duration {
            // buffering
            if currentTime == 0.0 {
                self.activityIndicator.alpha = 1
                self.activityIndicator.startAnimating()
                self.activityIndicator.alpha = 1
                self.label.alpha = 0
            }
            else {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.alpha = 0
                self.label.alpha = 1
            }
            
            if((duration - currentTime) > 0) {
                // while playing
            }
            else {
                self.removeFromSuperview()
                self.advertiseJukebox?.stop()
                self.delegate?.advertisementIsStop!()
                // while stop
            }
        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        
    }
    
    
}

