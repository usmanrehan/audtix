//
//  AppDelegate.swift
//  Auditix
//
//  Created by shardha on 12/14/17.
//  Copyright © 2017 Ingic. All rights reserved.
//

import UIKit
import UserNotifications
import Hero
import Fabric
import RealmSwift
import Crashlytics
import Google
import Toast_Swift
import FBSDKCoreKit
import LGSideMenuController
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import GooglePlacePicker
import GooglePlaces
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let googlePlaces = "AIzaSyCDLprKpq7ulXd4pfGSbGKLFWEATX7M3ZQ"
    var userLiveLocation = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    var Country = "Saudi Arabia"
    var locationManager = CLLocationManager()
    var placesClient: GMSPlacesClient!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        application.applicationIconBadgeNumber = 0
        
        IQKeyboardManager.sharedManager().enable = true
        
        // initlizating crashlytics
        Fabric.with([Crashlytics.self])
        
        // Enabling google sign in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        GIDSignIn.sharedInstance().delegate = self
        
        // Realm Migration Scripting...
        self.processRealmMigration()
        
        // Enabling facebook sign in
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // get walkthrough data
        self.getWalkThroughData()
        
        // Push Notification
        FIRApp.configure()
        self.registerForPushNotifications(application)
        
        //Check if user is logged in
        
        // user setting when user first time install app
        if Constants.USER_DEFAULTS.value(forKey: Constants.ApplyingUserSettingsForFirstTime) == nil {
            Constants.USER_DEFAULTS.set(true, forKey: Constants.ApplyingUserSettingsForFirstTime)
            Constants.USER_DEFAULTS.set(false, forKey: Constants.OnlyUseWifi) // download only on wifi
            Constants.USER_DEFAULTS.set(true, forKey: Constants.UseBothWifiAndCellular) // download on both wifi and cellur network
            Constants.USER_DEFAULTS.set(true, forKey: Constants.ContinuousPlay) // allowing the continuous play
        }
        self.changeRootViewController()
        
        GMSPlacesClient.provideAPIKey(googlePlaces)
        self.placesClient = GMSPlacesClient.shared()
        self.initLocationManager()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        application.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return  GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication!, annotation: annotation)  || FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    
    func showSplash()  {
        let storyboard = AppStoryboard.LoginModule.instance
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "Splash"))
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        //        navigationController.heroNavigationAnimationType = .zoomSlide(direction: .up)
        self.window?.rootViewController = navigationController
    }
    
    func showLanguageSelection() {
        let storyboard = AppStoryboard.LoginModule.instance
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "LanguageSelection"))
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        self.window?.rootViewController = navigationController
    }
    
    func showLoginScreen() {
        let storyboard = AppStoryboard.LoginModule.instance
        let navigationController = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "LoginSelection"))
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        self.window?.rootViewController = navigationController
    }
    
    func showHome() {
        let storyboard = AppStoryboard.Home.instance
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
        let leftViewController =  AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let rightViewController = storyboard.instantiateViewController(withIdentifier: "BookFilter") as! BookFilter
        
        leftViewController.delegate = rootViewController
        
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: leftViewController,
                                                      rightViewController: rightViewController)
        
        sideMenuController.leftViewWidth = UIScreen.main.bounds.width * 0.75
        sideMenuController.rightViewWidth = UIScreen.main.bounds.width * 0.75
        sideMenuController.rootView?.layer.shadowColor = UIColor.white.cgColor
        let coverColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
        sideMenuController.rootViewCoverColorForLeftView = coverColor
        sideMenuController.leftViewPresentationStyle = .slideBelow
        sideMenuController.rightViewPresentationStyle = .slideAbove
        sideMenuController.isLeftViewSwipeGestureEnabled = false
        sideMenuController.isRightViewSwipeGestureEnabled = false
        self.window?.rootViewController = sideMenuController
    }
    
    func showHomeTabbar() {
        
    }
    
    func changeRootViewController() {
        if let user = AppStateManager.sharedInstance.loggedInUser, user.IsVerified {
            self.showHome()
        } else {
            self.showSplash()
            //self.showLoginScreen()
        }
    }
    
    func processRealmMigration() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        
        Realm.Configuration.defaultConfiguration = config
    }
    
    // MARK: GOOGLE SIGN IN
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            //            let email = user.profile.email
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!,
                withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    // getWalkThroughData
    func getWalkThroughData() {
        var param = [String:Any]()
        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
            param["culture"] = 0
        }
        else{
            param["culture"] = 1
        }
        APIManager.sharedInstance.usersAPIManager.getAllWalkThrough(success: {(responseObj) in
            var walkthroughTempArray = [WalkThroughModel]()
            for walkthroughItem in responseObj {
                walkthroughTempArray.append(WalkThroughModel(value: walkthroughItem as! [String : Any]))
            }
            Singleton.sharedInstance.walkThroughDataArray = walkthroughTempArray
        }, failure: {(failure) in}, params: param)
        //        APIManager.sharedInstance.usersAPIManager.getAllWalkThrough(success: {(responseObj) in
        //            var walkthroughTempArray = [WalkThroughModel]()
        //            for walkthroughItem in responseObj {
        //                walkthroughTempArray.append(WalkThroughModel(value: walkthroughItem as! [String : Any]))
        //            }
        //            Singleton.sharedInstance.walkThroughDataArray = walkthroughTempArray
        //        }, failure: {(failure) in
        //        })
    }
}
extension AppDelegate: CLLocationManagerDelegate{
    // MARK: CLLocationManager Delegate
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location updated")
        if locations.count > 0 {
            self.userLiveLocation = (locations.first?.coordinate)!
            self.getCurrentLocationName()
        }
    }
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to update location")
    }
    func initLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
}
//Reverse Geocoding
extension AppDelegate{
    func getCurrentLocationName() {
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    let arrays : NSArray = place.addressComponents! as NSArray;
                    for i in 0..<arrays.count {
                        let dics : GMSAddressComponent = arrays[i] as! GMSAddressComponent
                        let str : String = dics.type
                        if (str == "country") {
                            self.Country = dics.name
                        }
                    }
                }
            }
        })
    }
}
//MARK:- Notification Delegates
//MARK: - PUSH NOTIFICATION METHODS
extension AppDelegate {
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            Constants.DEVICE_TOKEN = "\(refreshedToken)"
        }
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        //FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
    }
}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        completionHandler()
    }
    func registerForPushNotifications(_ application: UIApplication) {
        //Register for remote notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            FIRMessaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
}
extension AppDelegate : FIRMessagingDelegate {
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}

