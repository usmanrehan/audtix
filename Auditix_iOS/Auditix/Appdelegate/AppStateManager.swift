//
//  AppStateManager.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!
    
    
    override init() {
        super.init()
        
        if(!(realm != nil)){
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(User.self).last
    }
    
    func isUserLoggedIn() -> Bool{
        
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    
    func createUser(with userModel: User) {
        self.loggedInUser = userModel
        
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add(self.loggedInUser, update: true)
        }
        
        //        if !self.loggedInUser.isGuestLogin {
        //            try! Global.APP_REALM?.write(){
        //                Global.APP_REALM?.add(self.loggedInUser, update: true)
        //            }
        //        }
        
        print(AppStateManager.sharedInstance.loggedInUser)
    }
    
    func updateUserVerification(with status: Bool) {
        if self.loggedInUser != nil {
            try! Global.APP_REALM?.write {
                self.loggedInUser.IsVerified = status
            }
        }
    }
    
    
    func logoutUser() {
        
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.delete(self.loggedInUser)
            self.loggedInUser = nil
        }
        Constants.APP_DELEGATE.showLoginScreen()
    }
    
    func isGuestLogin() -> Bool{
        return self.loggedInUser.isGuestLogin
    }
    
    
}

