//
//  Result.swift
//
//  Created by Hamza Hasan on 3/12/18
//  Copyright (c) . All rights reserved.
//


import RealmSwift

public class SearchModel: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let iD = "ID"
        static let description = "Description"
        static let name = "Name"
        static let baseUrl = "BaseUrl"
        static let imageUrl = "ImageUrl"
        static let genre = "Genre"
        static let type = "Type"
        static let narratedBy = "NarratedBy"
    }
    
    // MARK: Properties
    @objc dynamic var ID = 0
    @objc dynamic var Description: String? = ""
    @objc dynamic var Name: String? = ""
    @objc dynamic var BaseUrl: String? = ""
    @objc dynamic var ImageUrl: String? = ""
    @objc dynamic var Genre: String? = ""
    @objc dynamic var `Type`: String? = ""
    @objc dynamic var NarratedBy: String? = ""

}
