//
//  Result.swift
//
//  Created by Hamza Hasan on 3/19/18
//  Copyright (c) . All rights reserved.
//


import RealmSwift

public class Advertise: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let updateBy = "UpdateBy"
        static let wowzaPort = "WowzaPort"
        static let fileSize = "FileSize"
        static let audioPath = "AudioPath"
        static let wowzaURL = "WowzaURL"
        static let advertisementType = "AdvertisementType"
        static let priority = "Priority"
        static let createdDate = "CreatedDate"
        static let audioUrl = "AudioUrl"
        static let advertisementId = "AdvertisementId"
        static let imagePath = "ImagePath"
        static let wowzaAppName = "WowzaAppName"
        static let sequenceNo = "SequenceNo"
        static let name = "Name"
        static let inActive = "InActive"
        static let createdBy = "CreatedBy"
        static let duration = "Duration"
        static let deleted = "Deleted"
    }
    
    // MARK: Properties
    @objc dynamic var UpdateBy = 0
    @objc dynamic var WowzaPort: String? = ""
    @objc dynamic var FileSize = 0.0
    @objc dynamic var AudioPath: String? = ""
    @objc dynamic var WowzaURL: String? = ""
    @objc dynamic var AdvertisementType = 0
    @objc dynamic var Priority = 0
    @objc dynamic var CreatedDate: String? = ""
    @objc dynamic var AudioUrl: String? = ""
    @objc dynamic var AdvertisementId = 0
    @objc dynamic var ImagePath: String? = ""
    @objc dynamic var WowzaAppName: String? = ""
    @objc dynamic var SequenceNo = 0
    @objc dynamic var Name: String? = ""
    @objc dynamic var InActive = false
    @objc dynamic var CreatedBy = 0
    @objc dynamic var Duration = 0.0
    @objc dynamic var Deleted = false
    
    
}

