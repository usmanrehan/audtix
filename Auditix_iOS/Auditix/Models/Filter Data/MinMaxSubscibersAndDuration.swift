//
//  MinMaxSubscibersAndDuration.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MinMaxSubscibersAndDuration: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let maxDuration = "MaxDuration"
    static let minDuration = "MinDuration"
    static let minSubscriber = "MinSubscriber"
    static let maxSubscriber = "MaxSubscriber"
    static let id = "id"

  }

  // MARK: Properties
  @objc dynamic var maxDuration = 0
  @objc dynamic var minDuration = 0
  @objc dynamic var minSubscriber = 0
  @objc dynamic var maxSubscriber = 0
  @objc dynamic var id = 0
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    maxDuration <- map[SerializationKeys.maxDuration]
    minDuration <- map[SerializationKeys.minDuration]
    minSubscriber <- map[SerializationKeys.minSubscriber]
    maxSubscriber <- map[SerializationKeys.maxSubscriber]
    id <- map[SerializationKeys.id]
  }


}
