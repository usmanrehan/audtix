//
//  EntityModel.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class EntityModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updateBy = "UpdateBy"
    static let isFeatured = "IsFeatured"
    static let arabicName = "ArabicName"
    static let bookCategoryID = "BookCategoryID"
    static let createdDate = "CreatedDate"
    static let updatedDate = "UpdatedDate"
    static let sequenceNo = "SequenceNo"
    static let name = "Name"
    static let inActive = "InActive"
    static let categoryType = "CategoryType"
    static let imageUrl = "ImageUrl"
    static let createdBy = "CreatedBy"
    static let deleted = "Deleted"
    static let isSelected = "isSelected"
    
  }

  // MARK: Properties
  @objc dynamic var updateBy = 0
  @objc dynamic var isFeatured = false
  @objc dynamic var arabicName: String? = ""
  @objc dynamic var bookCategoryID = 0
  @objc dynamic var createdDate: String? = ""
  @objc dynamic var updatedDate: String? = ""
  @objc dynamic var sequenceNo = 0
  @objc dynamic var name: String? = ""
  @objc dynamic var inActive = false
  @objc dynamic var categoryType = 0
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var createdBy = 0
  @objc dynamic var deleted = false
    @objc dynamic var isSelected = false
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "bookCategoryID"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updateBy <- map[SerializationKeys.updateBy]
    isFeatured <- map[SerializationKeys.isFeatured]
    arabicName <- map[SerializationKeys.arabicName]
    bookCategoryID <- map[SerializationKeys.bookCategoryID]
    createdDate <- map[SerializationKeys.createdDate]
    updatedDate <- map[SerializationKeys.updatedDate]
    sequenceNo <- map[SerializationKeys.sequenceNo]
    name <- map[SerializationKeys.name]
    inActive <- map[SerializationKeys.inActive]
    categoryType <- map[SerializationKeys.categoryType]
    imageUrl <- map[SerializationKeys.imageUrl]
    createdBy <- map[SerializationKeys.createdBy]
    deleted <- map[SerializationKeys.deleted]
    isSelected <- map[SerializationKeys.isSelected]
  }


}
