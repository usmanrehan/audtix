//
//  EntityModel.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class PodcastEntityModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let podcastCategoryId = "PodcastCategoryId"
    static let updateBy = "UpdateBy"
    static let isFeatured = "IsFeatured"
    static let updatedDate = "UpdatedDate"
    static let imagePath = "ImagePath"
    static let inActive = "InActive"
    static let title = "Title"
    static let createdDate = "CreatedDate"
    static let createdBy = "CreatedBy"
    static let deleted = "Deleted"
    static let isSelected = "isSelected"
    
  }

  // MARK: Properties
  @objc dynamic var podcastCategoryId = 0
  @objc dynamic var updateBy = 0
  @objc dynamic var isFeatured = false
  @objc dynamic var updatedDate: String? = ""
  @objc dynamic var imagePath: String? = ""
  @objc dynamic var inActive = false
  @objc dynamic var title: String? = ""
  @objc dynamic var createdDate: String? = ""
  @objc dynamic var createdBy = 0
  @objc dynamic var deleted = false
    @objc dynamic var isSelected = false
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "podcastCategoryId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    podcastCategoryId <- map[SerializationKeys.podcastCategoryId]
    updateBy <- map[SerializationKeys.updateBy]
    isFeatured <- map[SerializationKeys.isFeatured]
    updatedDate <- map[SerializationKeys.updatedDate]
    imagePath <- map[SerializationKeys.imagePath]
    inActive <- map[SerializationKeys.inActive]
    title <- map[SerializationKeys.title]
    createdDate <- map[SerializationKeys.createdDate]
    createdBy <- map[SerializationKeys.createdBy]
    deleted <- map[SerializationKeys.deleted]
    isSelected <- map[SerializationKeys.isSelected]
  }


}
