//
//  EntityModel.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class NewsEntityModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updateBy = "UpdateBy"
    static let isFeatured = "IsFeatured"
    static let isNewsSubscribed = "IsNewsSubscribed"
    static let updatedDate = "UpdatedDate"
    static let sourceImageUrl = "SourceImageUrl"
    static let sourceName = "SourceName"
    static let inActive = "InActive"
    static let isFavoriteNews = "IsFavoriteNews"
    static let createdDate = "CreatedDate"
    static let createdBy = "CreatedBy"
    static let deleted = "Deleted"
    static let id = "Id"
    static let isSelected = "isSelected"

  }

  // MARK: Properties
  @objc dynamic var updateBy = 0
  @objc dynamic var isFeatured = false
  @objc dynamic var isNewsSubscribed = false
  @objc dynamic var updatedDate: String? = ""
  @objc dynamic var sourceImageUrl: String? = ""
  @objc dynamic var sourceName: String? = ""
  @objc dynamic var inActive = false
  @objc dynamic var isFavoriteNews = false
  @objc dynamic var createdDate: String? = ""
  @objc dynamic var createdBy = 0
  @objc dynamic var deleted = false
  @objc dynamic var id = 0
    @objc dynamic var isSelected = false
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updateBy <- map[SerializationKeys.updateBy]
    isFeatured <- map[SerializationKeys.isFeatured]
    isNewsSubscribed <- map[SerializationKeys.isNewsSubscribed]
    updatedDate <- map[SerializationKeys.updatedDate]
    sourceImageUrl <- map[SerializationKeys.sourceImageUrl]
    sourceName <- map[SerializationKeys.sourceName]
    inActive <- map[SerializationKeys.inActive]
    isFavoriteNews <- map[SerializationKeys.isFavoriteNews]
    createdDate <- map[SerializationKeys.createdDate]
    createdBy <- map[SerializationKeys.createdBy]
    deleted <- map[SerializationKeys.deleted]
    id <- map[SerializationKeys.id]
    isSelected <- map[SerializationKeys.isSelected]
  }


}
