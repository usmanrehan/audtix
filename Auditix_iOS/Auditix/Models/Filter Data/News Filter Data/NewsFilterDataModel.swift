//
//  NewsFilterDataModel.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class NewsFilterDataModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let locations = "Locations"
    static let minMaxSubscibersAndDuration = "MinMaxSubscibersAndDuration"
    static let entityModel = "EntityModel"
    static let id = "Id"

  }

  // MARK: Properties
  var locations = List<Locations>()
    @objc dynamic var minMaxSubscibersAndDuration: MinMaxSubscibersAndDuration?
  var entityModel = List<NewsEntityModel>()
    @objc dynamic var id = 0
    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required public convenience init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    locations <- (map[SerializationKeys.locations], ListTransform<Locations>())
    minMaxSubscibersAndDuration <- map[SerializationKeys.minMaxSubscibersAndDuration]
    entityModel <- (map[SerializationKeys.entityModel], ListTransform<NewsEntityModel>())
    id <- map[SerializationKeys.id]
  }


}
