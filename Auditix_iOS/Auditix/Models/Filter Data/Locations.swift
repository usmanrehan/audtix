//
//  Locations.swift
//
//  Created by Hamza Hasan on 7/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Locations: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let createdDate = "CreatedDate"
    static let updateBy = "UpdateBy"
    static let updatedDate = "UpdatedDate"
    static let countryName = "CountryName"
    static let inActive = "InActive"
    static let phoneCode = "PhoneCode"
    static let createdBy = "CreatedBy"
    static let countryCode = "CountryCode"
    static let deleted = "Deleted"
    static let id = "Id"
    static let isSelected = "isSelected"
  }

  // MARK: Properties
  @objc dynamic var createdDate: String? = ""
  @objc dynamic var updateBy = 0
  @objc dynamic var updatedDate: String? = ""
  @objc dynamic var countryName: String? = ""
  @objc dynamic var inActive = false
  @objc dynamic var phoneCode: String? = ""
  @objc dynamic var createdBy = 0
  @objc dynamic var countryCode: String? = ""
  @objc dynamic var deleted = false
  @objc dynamic var id = 0
  @objc dynamic var isSelected = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    createdDate <- map[SerializationKeys.createdDate]
    updateBy <- map[SerializationKeys.updateBy]
    updatedDate <- map[SerializationKeys.updatedDate]
    countryName <- map[SerializationKeys.countryName]
    inActive <- map[SerializationKeys.inActive]
    phoneCode <- map[SerializationKeys.phoneCode]
    createdBy <- map[SerializationKeys.createdBy]
    countryCode <- map[SerializationKeys.countryCode]
    deleted <- map[SerializationKeys.deleted]
    id <- map[SerializationKeys.id]
    isSelected <- map[SerializationKeys.isSelected]
  }


}
