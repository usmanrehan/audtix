//
//  UserSettings.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift
class UserSettings: Object {
    @objc dynamic var downloadOnWifi = false
    @objc dynamic var downloadOnWifiAndCellular = true
}
