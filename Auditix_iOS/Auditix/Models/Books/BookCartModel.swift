//
//  BookCartModel.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class BookCartModel: Object {
    
    @objc dynamic var AccountID = 0
    @objc dynamic var CartBook: BookModel!
    
}
