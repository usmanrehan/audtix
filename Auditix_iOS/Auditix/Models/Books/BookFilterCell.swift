//
//  BookFilterCell.swift
//  Auditix
//
//  Created by shardha on 1/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BookFilterCell: UITableViewCell {

    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var btnWhole: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
