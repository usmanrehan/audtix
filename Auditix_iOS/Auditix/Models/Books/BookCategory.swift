//
//  BookCategory.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

public class BookCategory: Object {
    @objc dynamic var BookCategoryID = 0
    @objc dynamic var SequenceNo = 0
    @objc dynamic var CategoryType = 0
    @objc dynamic var Name : String?
    @objc dynamic var ArabicName : String?
    @objc dynamic var ImageUrl: String?
    @objc dynamic var BookCategoryML : String?
    @objc dynamic var CreatedDate : String?
    @objc dynamic var UpdatedDate : String?
    @objc dynamic var Deleted = false
    @objc dynamic var InActive = false
    @objc dynamic var IsFeatured = false
}
