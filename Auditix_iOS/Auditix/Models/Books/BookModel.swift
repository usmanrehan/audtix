/*
 Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation
import RealmSwift
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class BookModel: Object {
    
    @objc dynamic var BookID = 0
    @objc dynamic var BookName : String?
    @objc dynamic var AuthorName : String?
    @objc dynamic var NarratorName : String?
    @objc dynamic var Rating = 0.0
    @objc dynamic var Genre : String?
    @objc dynamic var TotalChapters = 0
    @objc dynamic var Duration = 0.0
    @objc dynamic var Size = 0.0
    @objc dynamic var AboutTheBook : String?
    @objc dynamic var AboutTheNarrator : String?
    @objc dynamic var ImageUrl : String?
    @objc dynamic var IsPaid = false
    @objc dynamic var IsPurchased = false
    @objc dynamic var IsFavorite = false
    @objc dynamic var IsRated = false
    @objc dynamic var Price = 0.0
    @objc dynamic var Chapters : BookAudioManagerModel?
}

