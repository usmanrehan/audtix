//
//  FeatureBookModel.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import RealmSwift

class FeatureBookModel: Object {
    @objc dynamic var BookID = 0
    @objc dynamic var BookCategoryID = 0
}
