//
//  NotificationModel.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class NotificationModel: Object {
    /*      "UserNotificationID": 1,
     "NotificationID": 1,
     "AccountID": 1,
     "AccountDeviceID": 1,
     "NotificationTitle": "New Book Added ",
     "NotificationMessage": "New Book Added "
     */
    @objc dynamic var UserNotificationID = 0
    @objc dynamic var NotificationID = 0
    @objc dynamic var AccountID = 0
    @objc dynamic var AccountDeviceID = 0
    @objc dynamic var NotificationTitle: String = ""
    @objc dynamic var NotificationMessage: String = ""
}
