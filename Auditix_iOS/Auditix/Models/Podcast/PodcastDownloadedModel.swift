//
//  PodcastDownloadedModel.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class PodcastDownloadedModel: Object {
    @objc dynamic var AccountID = 0
    @objc dynamic var createdDate: Date!
    @objc dynamic var updatedDate: Date!
    @objc dynamic var trackID = 0
    @objc dynamic var DownloadedPodcast: PodcastModel!
}
