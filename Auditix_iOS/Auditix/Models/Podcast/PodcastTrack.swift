//
//  TrackList.swift
//
//  Created by Hamza Hasan on 1/29/18
//  Copyright (c) Ingic. All rights reserved.
//


//import ObjectMapper
import RealmSwift
//import ObjectMapper_Realm

public class PodcastTrack: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let FileUrl = "FileUrl"
        static let Duration = "Duration"
        static let Name = "Name"
        static let DateAdded = "DateAdded"
        static let WowzaURL = "WowzaURL"
        static let WowzaPort = "WowzaPort"
        static let WowzaAppName = "WowzaAppName"
    }
    
    // MARK: Properties
    @objc dynamic var Id = 0
    @objc dynamic var FileUrl: String? = ""
    @objc dynamic var Duration: String? = ""
    @objc dynamic var Name: String? = ""
    @objc dynamic var DateAdded: String? = ""
    @objc dynamic var isEpisodeDownloaded = false
    @objc dynamic var WowzaURL: String? = ""
    @objc dynamic var WowzaPort: String? = ""
    @objc dynamic var WowzaAppName: String? = ""
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        fileUrl = json[SerializationKeys.fileUrl].string
//        duration = json[SerializationKeys.duration].string
//        name = json[SerializationKeys.name].string
//        dateAdded = json[SerializationKeys.dateAdded].string
//    }
    
    
}

