//
//  Result.swift
//
//  Created by Hamza Hasan on 1/29/18
//  Copyright (c) Ingic. All rights reserved.
//


//import ObjectMapper
import RealmSwift
//import ObjectMapper_Realm

public class PodcastBaseCamp: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let DefaultCategories = "DefaultCategories"
        static let FeaturedCategories = "FeaturedCategories"
    }
    
    // MARK: Properties
    var FeaturedCategories = List<FeaturedPodcast>()
    var DefaultCategories = List<DefaultPodcast>()
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        if let items = json[SerializationKeys.defaultCategories].array { defaultCategories = items.map { DefaultCategories(json: $0) } }
//        if let items = json[SerializationKeys.featuredCategories].array { featuredCategories = items.map { FeaturedCategories(json: $0) } }
//    }
    
    
}

