//
//  Podcastdetails.swift
//
//  Created by Hamza Hasan on 1/29/18
//  Copyright (c) Ingic. All rights reserved.
//



import RealmSwift

public class PodcastModel: Object {
    
    // MARK: Properties
    @objc dynamic var UpdateBy = 0
    @objc dynamic var Country: String? = ""
    @objc dynamic var ArtistName: String? = ""
    @objc dynamic var CollectionId = 0
    @objc dynamic var PodcastId = 0
    @objc dynamic var ReleaseDate: String? = ""
    @objc dynamic var FeedUrl: String? = ""
    @objc dynamic var Genre: String? = ""
    @objc dynamic var CreatedDate: String? = ""
    @objc dynamic var EpisodeCount = 0
    @objc dynamic var Rating = 0.0
    @objc dynamic var IsEpisodesAdded = false
    @objc dynamic var UpdatedDate: String? = ""
    @objc dynamic var IsSubscribed = false
    @objc dynamic var Name: String? = ""
    @objc dynamic var Title: String? = ""
    @objc dynamic var IsFavorite = false
    @objc dynamic var InActive = false
    @objc dynamic var ImageUrl: String? = ""
    @objc dynamic var Description: String? = ""
    @objc dynamic var TrackId = 0
    @objc dynamic var CreatedBy = 0
    @objc dynamic var CategoryId = 0
    @objc dynamic var Deleted = false
    @objc dynamic var IsRated = false
    @objc dynamic var AutoDownload = false
    @objc dynamic var NotificationEnabled = false

    let episodes = List<PodcastEpisode>()
}
