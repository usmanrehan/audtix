//
//  PodcastPollingModel.swift
//  Auditix
//
//  Created by shardha on 7/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class PodcastPollingModel: Object {
    @objc dynamic var TrackId = 0
    var PodcastEpisodes = List<PodcastEpisode>()
}
