//
//  Result.swift
//
//  Created by Hamza Hasan on 1/29/18
//  Copyright (c) Ingic. All rights reserved.
//


//import ObjectMapper
import RealmSwift
//import ObjectMapper_Realm

public class PodcastDetailModel: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let Author = "Author"
        static let Language = "Language"
        static let Title = "Title"
        static let Image = "Image"
        static let Genre = "Genre"
        static let Description = "Description"
        static let Rating = "Rating"
        static let TrackList = "TrackList"
        static let IsSubscribed = "IsSubscribed"
        static let PublishDate = "PublishDate"
        static let IsEpisodeAdded = "IsEpisodeAdded"
        static let IsFavorite = "IsFavorite"
    }
    
    // MARK: Properties
    @objc dynamic var Author: String? = ""
    @objc dynamic var Language: String? = ""
    @objc dynamic var Title: String? = ""
    @objc dynamic var Image: String? = ""
    @objc dynamic var Genre: String? = ""
    @objc dynamic var Description: String? = ""
    @objc dynamic var Rating = 0.0
    var TrackList = List<PodcastTrack>()
    @objc dynamic var IsSubscribed = false
    @objc dynamic var PublishDate: String? = ""
    @objc dynamic var IsEpisodeAdded = false
    @objc dynamic var IsFavorite = false
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        author = json[SerializationKeys.author].string
//        language = json[SerializationKeys.language].string
//        title = json[SerializationKeys.title].string
//        image = json[SerializationKeys.image].string
//        genre = json[SerializationKeys.genre].string
//        description = json[SerializationKeys.description].string
//        rating = json[SerializationKeys.rating].int
//        if let items = json[SerializationKeys.trackList].array { trackList = items.map { TrackList(json: $0) } }
//        isSubscribed = json[SerializationKeys.isSubscribed].boolValue
//        publishDate = json[SerializationKeys.publishDate].string
//        isEpisodeAdded = json[SerializationKeys.isEpisodeAdded].boolValue
//        isFavorite = json[SerializationKeys.isFavorite].boolValue
//    }
    
    
}

