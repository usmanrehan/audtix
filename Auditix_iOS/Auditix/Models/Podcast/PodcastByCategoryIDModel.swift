//
//  Results.swift
//
//  Created by Hamza Hasan on 4/9/18
//  Copyright (c) . All rights reserved.
//

import RealmSwift

public class PodcastByCategoryIDModel: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let country = "Country"
        static let artistName = "ArtistName"
        static let collectionId = "CollectionId"
        static let releaseDate = "ReleaseDate"
        static let episodeCount = "EpisodeCount"
        static let genre = "Genre"
        static let feedUrl = "FeedUrl"
        static let rating = "Rating"
        static let isSubscribed = "IsSubscribed"
        static let name = "Name"
        static let isFavorite = "IsFavorite"
        static let imageUrl = "ImageUrl"
        static let trackId = "TrackId"
    }
    
    // MARK: Properties
    @objc dynamic var Country: String? = ""
    @objc dynamic var ArtistName: String? = ""
    @objc dynamic var CollectionId = 0
    @objc dynamic var ReleaseDate: String? = ""
    @objc dynamic var EpisodeCount = 0
    @objc dynamic var Genre: String? = ""
    @objc dynamic var FeedUrl: String? = ""
    @objc dynamic var Rating = 0.0
    @objc dynamic var IsSubscribed = false
    @objc dynamic var Name: String? = ""
    @objc dynamic var IsFavorite = false
    @objc dynamic var ImageUrl: String? = ""
    @objc dynamic var TrackId = 0
    
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        country = json[SerializationKeys.country].string
//        artistName = json[SerializationKeys.artistName].string
//        collectionId = json[SerializationKeys.collectionId].int
//        releaseDate = json[SerializationKeys.releaseDate].string
//        episodeCount = json[SerializationKeys.episodeCount].int
//        genre = json[SerializationKeys.genre].string
//        feedUrl = json[SerializationKeys.feedUrl].string
//        rating = json[SerializationKeys.rating].int
//        isSubscribed = json[SerializationKeys.isSubscribed].boolValue
//        name = json[SerializationKeys.name].string
//        isFavorite = json[SerializationKeys.isFavorite].boolValue
//        imageUrl = json[SerializationKeys.imageUrl].string
//        trackId = json[SerializationKeys.trackId].int
//    }
    
    
}

