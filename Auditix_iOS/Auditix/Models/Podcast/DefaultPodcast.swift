//
//  DefaultPodcast.swift
//
//  Created by Hamza Hasan on 1/29/18
//  Copyright (c) Ingic. All rights reserved.
//



import RealmSwift


public class DefaultPodcast: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let CategoryTitle = "CategoryTitle"
        static let CategoryId = "CategoryId"
        static let Podcastdetails = "Podcastdetails"
    }
    
    // MARK: Properties
    @objc dynamic var CategoryTitle: String? = ""
    @objc dynamic var CategoryId = 0
    var Podcastdetails = List<PodcastModel>()
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        categoryTitle = json[SerializationKeys.categoryTitle].string
//        categoryId = json[SerializationKeys.categoryId].int
//        if let items = json[SerializationKeys.podcastdetails].array { podcastdetails = items.map { Podcastdetails(json: $0) } }
//    }
    
    
}

