//
//  PodcastEpisode.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class PodcastEpisode: Object {
    @objc dynamic var PodcastEpisodeID = 0
    @objc dynamic var PodcastId = 0
    @objc dynamic var EpisodeNo = 0
    @objc dynamic var FileSize: NSNumber? = 0.0
    @objc dynamic var FileDuration: NSNumber? = 0.0
    @objc dynamic var FilePath: String?
    @objc dynamic var EpisodeWebUrl: String?
    @objc dynamic var EpisodeTitle: String?
    @objc dynamic var CreatedDate: String?
    @objc dynamic var TrackId = 0
    @objc dynamic var ImageUrl: String?
    @objc dynamic var ArtistName: String?
    @objc dynamic var Podcast : PodcastModel?
}
