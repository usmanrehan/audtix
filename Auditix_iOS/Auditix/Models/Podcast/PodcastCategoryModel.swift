//
//  PodcastCategoryModel.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class PodcastCategoryModel: Object {
    @objc dynamic var PodcastCategoryId = 0
    @objc dynamic var Title: String?
    @objc dynamic var ImagePath: String?
}
