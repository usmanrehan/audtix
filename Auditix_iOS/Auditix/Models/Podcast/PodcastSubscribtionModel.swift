//
//  PodcastSubscribtionModel.swift
//
//  Created by Hamza Hasan on 2/3/18
//  Copyright (c) . All rights reserved.
//



import RealmSwift

public class PodcastSubscribtionModel: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let accountId = "AccountId"
        static let rating = "Rating"
        static let artistName = "ArtistName"
        static let collectionId = "CollectionId"
        static let publishDate = "PublishDate"
        static let liked = "Liked"
        static let favoritePodcastId = "FavoritePodcastId"
        static let title = "Title"
        static let feedUrl = "FeedUrl"
        static let imageUrl = "ImageUrl"
        static let trackId = "TrackId"
    }
    
    // MARK: Properties
    @objc dynamic var AccountId = 0
    @objc dynamic var Rating = 0.0
    @objc dynamic var ArtistName: String? = ""
    @objc dynamic var CollectionId = 0
    @objc dynamic var PublishDate: String? = ""
    @objc dynamic var Liked = false
    @objc dynamic var FavoritePodcastId = 0
    @objc dynamic var Title: String? = ""
    @objc dynamic var FeedUrl: String? = ""
    @objc dynamic var ImageUrl: String? = ""
    @objc dynamic var TrackId = 0
    
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        accountId = json[SerializationKeys.accountId].int
//        rating = json[SerializationKeys.rating].int
//        artistName = json[SerializationKeys.artistName].string
//        collectionId = json[SerializationKeys.collectionId].int
//        publishDate = json[SerializationKeys.publishDate].string
//        liked = json[SerializationKeys.liked].boolValue
//        favoritePodcastId = json[SerializationKeys.favoritePodcastId].int
//        title = json[SerializationKeys.title].string
//        feedUrl = json[SerializationKeys.feedUrl].string
//        imageUrl = json[SerializationKeys.imageUrl].string
//        trackId = json[SerializationKeys.trackId].int
//    }
    
    
}

