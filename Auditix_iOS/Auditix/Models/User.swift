/*
 Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation
import RealmSwift
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class User: Object {
    
    @objc dynamic var AccountID = 0
    @objc dynamic var MasterAccountID = 0
    @objc dynamic var AccountRole = 0
    @objc dynamic var FullName : String?
    @objc dynamic var EmailAddress : String?
    @objc dynamic var Password : String?
    @objc dynamic var PhoneNo : String?
    @objc dynamic var Country : String?
    @objc dynamic var Token : Token?
    @objc dynamic var AuthType = 0
    @objc dynamic var DOB : String?
    @objc dynamic var Credits = 0
    @objc dynamic var Gender = 0
    @objc dynamic var ImageName: String?
    @objc dynamic var ResetInfo: String?
    @objc dynamic var CreatedBy = 0
    @objc dynamic  var UpdateBy = 0
    @objc dynamic var CreatedDate: String?
    @objc dynamic var UpdatedDate: String?
    @objc dynamic  var Deleted = false
    @objc dynamic  var InActive = false
    @objc dynamic var isGuestLogin = false
    @objc dynamic var IsVerified = false
    @objc dynamic var deletedItems : String?
    override public static func primaryKey() -> String? {
        return "AccountID"
    }
    
}

