//
//  NewsList.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import RealmSwift

class NewsListModel: Object {

    // MARK: Properties
    @objc dynamic var NewsID = 0
    @objc dynamic var NewsCategoryID = 0
    @objc dynamic var Name: String?
    @objc dynamic var Description: String?
    @objc dynamic var NarratorName: String?
    @objc dynamic var ImageUrl: String?
    @objc dynamic var IsEpisodesAdded: Bool = false
    @objc dynamic var EpisodeCount: Int = 0
    @objc dynamic var CreatedDate: String?
    @objc dynamic var UpdatedDate: String?
    @objc dynamic var Deleted: Bool = false
    @objc dynamic var InActive: Bool = false
    @objc dynamic var IsSubscribed: Bool = false
    @objc dynamic var IsFavorite: Bool = false
    @objc dynamic var AutoDownload: Bool = false
    @objc dynamic var NotificationEnabled = false

    let episodes = List<NewsEpisodeModel>()
}
