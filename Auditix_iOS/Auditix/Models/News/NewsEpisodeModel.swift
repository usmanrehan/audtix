//
//  NewsEpisodeModel.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import RealmSwift

class NewsEpisodeModel: Object {
    @objc dynamic var NewsEpisodeID = 0
    @objc dynamic var NewsID = 0
    @objc dynamic var EpisodeNo = 0
    @objc dynamic var FileSize: NSNumber? = 0.0
    @objc dynamic var FileDuration: NSNumber? = 0.0
    @objc dynamic var FilePath: String?
    @objc dynamic var CreatedDate: String?
    @objc dynamic var UpdatedDate: String?
    @objc dynamic var Deleted = false
    @objc dynamic var InActive = false
    @objc dynamic var EpisodeWebUrl: String?
    @objc dynamic var EpisodeTitle: String?
    @objc dynamic var CoverImage: String?
    @objc dynamic var NarratorName: String?
    @objc dynamic var Name: String?
    @objc dynamic var Description: String?
}
