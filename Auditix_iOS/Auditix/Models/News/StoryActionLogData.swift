//
//  StoryActionlogData.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import Foundation

public class StoryActionlogData: NSObject {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let index = "_index"
        static let type = "_type"
        static let id = "_id"
    }
    
    // MARK: Properties
    @objc dynamic var index: String? = ""
    @objc dynamic var type: String? = ""
    @objc dynamic var id: String? = ""
    

    public required init(json: [String : Any]) {
        index = json[SerializationKeys.index] as? String
        type = json[SerializationKeys.type] as? String
        id = json[SerializationKeys.id] as? String
    }
    
    
}

