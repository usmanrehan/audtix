//
//  NewsCategory.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import RealmSwift

public class NewsCategory: Object {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let updateBy = "UpdateBy"
        static let sourceName = "SourceName"
        static let sourceImageUrl = "SourceImageUrl"
        static let inActive = "InActive"
        static let createdDate = "CreatedDate"
        static let createdBy = "CreatedBy"
        static let deleted = "Deleted"
        static let id = "Id"
        static let isFavoriteNews = "IsFavoriteNews"
        static let isNewsSubscribed = "IsNewsSubscribed"
    }
    
    // MARK: Properties
    @objc dynamic var UpdateBy = 0
    @objc dynamic var SourceName: String? = ""
    @objc dynamic var SourceImageUrl: String? = ""
    @objc dynamic var InActive = false
    @objc dynamic var CreatedDate: String? = ""
    @objc dynamic var IsNewsSubscribed: Bool = false
    @objc dynamic var IsFavoriteNews: Bool = false
    @objc dynamic var CreatedBy = 0
    @objc dynamic var Deleted = false
    @objc dynamic var Id = 0
    
//    // MARK: SwiftyJSON Initializers
//    /// Initiates the instance based on the object.
//    ///
//    /// - parameter object: The object of either Dictionary or Array kind that was passed.
//    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        updateBy = json[SerializationKeys.updateBy].int
//        sourceName = json[SerializationKeys.sourceName].string
//        sourceImageUrl = json[SerializationKeys.sourceImageUrl].string
//        inActive = json[SerializationKeys.inActive].boolValue
//        createdDate = json[SerializationKeys.createdDate].string
//        createdBy = json[SerializationKeys.createdBy].int
//        deleted = json[SerializationKeys.deleted].boolValue
//        id = json[SerializationKeys.id].int
//    }
    
    
}

