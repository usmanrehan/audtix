//
//  Result.swift
//
//  Created by Hamza Hasan on 4/11/18
//  Copyright (c) . All rights reserved.
//


import RealmSwift

public class NewsSubscribedModel: Object {
    
    // MARK: Properties
    @objc dynamic var NewsSubscriptionId = 0
    @objc dynamic var NewsId = 0
    @objc dynamic var AccountId = 0
    @objc dynamic var NewsName: String?
    @objc dynamic var Description: String?
    @objc dynamic var CreatedDate: String?
    @objc dynamic var UpdatedDate: String?
    @objc dynamic var NewsImageUrl: String?
    @objc dynamic var Deleted: Bool = false
    @objc dynamic var InActive: Bool = false
    @objc dynamic var IsFavorite: Bool = false
    @objc dynamic var IsSubscribed: Bool = false
}

