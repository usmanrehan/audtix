//
//  Result.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//

import Foundation

public class NewsModel: NSObject {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let source = "source"
        static let audioDuration = "audio_duration"
        static let storyActionlogData = "story_actionlog_data"
        static let sourceImageUrl = "source_image_url"
        static let descriptionValue = "description"
        static let category = "category"
        static let timestamp = "timestamp"
        static let id = "_id"
        static let sourceName = "source_name"
        static let type = "_type"
        static let title = "title"
        static let imageUrl = "image_url"
        static let duration = "duration"
        static let index = "_index"
        static let audioLink = "audio_link"
        static let IsSubscribed = "IsSubscribed"
        static let IsFavourite = "IsFavourite"
    }
    
    // MARK: Properties
    @objc dynamic var source: String? = ""
    @objc dynamic var audio_duration = 0
    @objc dynamic var story_actionlog_data: StoryActionlogData?
    @objc dynamic var source_image_url: String? = ""
    @objc dynamic var descriptionValue: String? = ""
    @objc dynamic var category: String? = ""
    @objc dynamic var timestamp: String? = ""
    @objc dynamic var _id: String? = ""
    @objc dynamic var source_name: String? = ""
    @objc dynamic var _type: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var image_url: String? = ""
    @objc dynamic var duration = 0
    @objc dynamic var _index: String? = ""
    @objc dynamic var audio_link: String? = ""
    @objc dynamic var IsSubscribed = false
    @objc dynamic var IsFavourite = false
    

    init(json: [String : Any]) {
        source = json[SerializationKeys.source] as? String
        audio_duration = json[SerializationKeys.audioDuration] as! Int
        story_actionlog_data = StoryActionlogData(json: json[SerializationKeys.storyActionlogData] as! [String : Any])
        source_image_url = json[SerializationKeys.sourceImageUrl] as? String
        descriptionValue = json[SerializationKeys.descriptionValue] as? String
        category = json[SerializationKeys.category] as? String
        timestamp = json[SerializationKeys.timestamp] as? String
        _id = json[SerializationKeys.id] as? String
        source_name = json[SerializationKeys.sourceName] as? String
        _type = json[SerializationKeys.type] as? String
        title = json[SerializationKeys.title] as? String
        image_url = json[SerializationKeys.imageUrl] as? String
        duration = json[SerializationKeys.duration] as! Int
        _index = json[SerializationKeys.index] as? String
        audio_link = json[SerializationKeys.audioLink] as? String
        IsSubscribed = (json[SerializationKeys.IsSubscribed] as? Bool)!
        IsFavourite = (json[SerializationKeys.IsFavourite] as? Bool)!
    }
    
    public override init() {
        super.init()
    }
}

