//
//  WalkThroughModel.swift
//
//  Created by Hamza Hasan on 1/19/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import RealmSwift

public class WalkThroughModel: Object {
    
//    // MARK: Declaration for string constants to be used to decode and also serialize.
//    private struct SerializationKeys {
//        static let title = "Title"
//        static let baseUrl = "BaseUrl"
//        static let walkThroughId = "WalkThroughId"
//        static let imageUrl = "ImageUrl"
//        static let sequenceNo = "SequenceNo"
//        static let description = "Description"
//    }
    
    // MARK: Properties
    @objc dynamic var Title: String? = ""
    @objc dynamic var BaseUrl: String? = ""
    @objc dynamic var WalkThroughId = 0
    @objc dynamic var ImageUrl: String? = ""
    @objc dynamic var SequenceNo = 0
    @objc dynamic var Description: String? = ""
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
//    public convenience init(object: Any) {
//        self.init(json: JSON(object))
//    }
//
//    /// Initiates the instance based on the JSON that was passed.
//    ///
//    /// - parameter json: JSON object from SwiftyJSON.
//    public required init(json: JSON) {
//        title = json[SerializationKeys.title].string
//        baseUrl = json[SerializationKeys.baseUrl].string
//        walkThroughId = json[SerializationKeys.walkThroughId].int
//        imageUrl = json[SerializationKeys.imageUrl].string
//        sequenceNo = json[SerializationKeys.sequenceNo].int
//        description = json[SerializationKeys.description].string
//    }
    
    
}

