//
//  Player.swift
//  Auditix
//
//  Created by shardha on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MediaPlayer
import Jukebox
import AVKit
import AVFoundation
import Cosmos
import RealmSwift
//import MarqueeLabel

import LGSideMenuController

enum PlayingType {
    case book
    case podcast
    case news
}

enum PlayPauseState {
    case play
    case pause
}



class Player: UIViewController {
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var labelBookPodcastName: MarqueeLabel!
    @IBOutlet weak var labelChaperEpisodeNo: MarqueeLabel!
    @IBOutlet weak var labelBookPodcastNameDown: UILabel!
    @IBOutlet weak var labelnarrator: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelSeekbarTime: UILabel!
    
    @IBOutlet weak var imageViewCenter: UIImageView!
    @IBOutlet weak var imageViewDown: UIImageView!
    
    @IBOutlet weak var BtnPlayPause: UIButton!
    @IBOutlet weak var BtnForward: UIButton!
    @IBOutlet weak var BtnBackward: UIButton!
    @IBOutlet weak var btnTimeTop: UIButton!
    @IBOutlet weak var BtnListChap: UIButton!
    @IBOutlet weak var BtnHeart: UIButton!
    
    @IBOutlet weak var cosmosRatingView: CosmosView!
    
    @IBOutlet weak var viewBottomData: UIView! // bottom view outlet for hero animation
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var stackAuthor: UIStackView!
    @IBOutlet weak var stackDuration: UIStackView!
    
    public var playingType: PlayingType!
    
    public var currentBook: BookModel!
    //public var currentPodcast: PodcastDetailModel!
    
    public var podcast: PodcastModel!
    public var episodeIndex = 0
    //public var podcastEpisode: PodcastEpisode?
    
    public var newsObject: NewsListModel!
    public var trackIndex = 0
    
    //public var currentPodcastTrackID: Int!
    public var currentBookTrackID: Int!
    //public var currentNews: NewsModel!
    
    var currentAdvertiseArray = [Advertise]()
    public var playingThisEpisode: Int = 0 // if user directly tab on episode
    
    public var heroID = ""
    
    var previewLink: URL?
    var isOnlyShowPreview: Bool = true
    
    var chapEpisodeNumber: Int = 0
    var currentSeekBarTime: Double = 0
    
    var isPlaying: PlayPauseState = .pause
    
    //    var timer  = Timer()
    //    var seekbarSeconds: Int = 0
    var seekBarSecondsDecrease: Int = 0
    
    //    var audioPlayer: AudioPlayer?
    
    var jukebox: Jukebox! = nil
    
    var advertismentJukeBoxItem: JukeboxItem?
    
    public var isComingFromLibrary: Bool = false
    
    var itsTimeForAdvertise: Bool = true // high this flag it you want to show advertise after/before episode/chapter/news
    
    var isVolumeOn: Bool = true // for Bug #9262
    
    // for remote controller
    let commandCenter = MPRemoteCommandCenter.shared()
    
    var advertiseView = AdvertiseView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // hero work
        self.imageViewCenter.heroID = "DetailToPlayer"
        self.imageViewCenter.heroModifiers = [.arc]
        
        self.viewBottomData.heroID = self.heroID
        self.viewBottomData.heroModifiers = [.arc]
        
        self.jukebox = Jukebox(delegate: self, items: [])
        
        // init and fill book data
        if self.playingType == .book {
            self.initBookData()
        }
        else if self.playingType == .podcast {
            self.initPodcastData()
        } else if self.playingType == .news {
            self.initNewsData()
        }
        
        // customizing ui
        self.uiCustomization()
        
        self.cosmosRatingView.isUserInteractionEnabled = false
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.slider.addGestureRecognizer(tapGestureRecognizer)
        
        
        // begin receiving remote events
        UIApplication.shared.beginReceivingRemoteControlEvents()
        self.remoateCommandCentreClosure()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // marquee the episdoe/chapter name/number label
        self.labelChaperEpisodeNo.trailingBuffer = 20
        
        self.labelChaperEpisodeNo.speed = .duration(12)
        self.labelChaperEpisodeNo.animationDelay = 1
        
        self.labelChaperEpisodeNo.type = .continuous
        self.labelChaperEpisodeNo.speed = .duration(15)
        self.labelChaperEpisodeNo.animationCurve = .easeInOut
        self.labelChaperEpisodeNo.fadeLength = 10.0
        
        // marquee the name label
        self.labelBookPodcastName.trailingBuffer = 20
        
        self.labelBookPodcastName.speed = .duration(12)
        self.labelBookPodcastName.animationDelay = 1
        
        self.labelBookPodcastName.type = .continuous
        self.labelBookPodcastName.speed = .duration(15)
        self.labelBookPodcastName.animationCurve = .easeInOut
        self.labelBookPodcastName.fadeLength = 10.0
        
    }
    deinit {
        print("I am deinit")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.jukebox.stop()
        self.commandCenter.pauseCommand.removeTarget(nil)
        self.commandCenter.playCommand.removeTarget(nil)
        self.commandCenter.nextTrackCommand.removeTarget(nil)
        self.commandCenter.previousTrackCommand.removeTarget(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initBookData() {
        
//        if self.isComingFromLibrary {
//            self.BtnListChap.isHidden = false
//        }
//        else if !self.currentBook.IsPurchased {
//            self.BtnListChap.isHidden = true
//        }
//
//        self.labelBookPodcastName.text = self.currentBook.BookName
//        self.labelChaperEpisodeNo.text = ""
//
//        self.labelBookPodcastNameDown.text = self.currentBook.BookName
//        self.labelnarrator.text = self.currentBook.NarratorName
//        self.labelAuthor.text = self.currentBook.AuthorName
//        self.labelGenre.text = self.currentBook.Genre
//        self.labelDuration.text = ParserHelper.getDuration(with: Int(self.currentBook.Duration))
//
//        self.cosmosRatingView.rating = self.currentBook.Rating
//        //        self.labelLocation.text = self.currentBook
//
//        self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(self.currentBook.Duration)), for: UIControlState())
//
//        if let imageURL = URL(string: self.currentBook.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
//            self.imageViewCenter.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//            self.imageViewDown.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//        }
//        else {
//            self.imageViewCenter.image = #imageLiteral(resourceName: "placeholder_image")
//            self.imageViewDown.image = #imageLiteral(resourceName: "placeholder_image")
//        }
//
//        if self.currentBook.IsFavorite {
//            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: UIControlState())
//        }
//        else {
//            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: UIControlState())
//        }
//
//        // selected episode is playing
//        if self.playingThisEpisode != -1 {
//            self.chapEpisodeNumber = self.playingThisEpisode
//        }
//        //        self.getBookDetails(with: self.currentBook.BookID)
//
//        if(self.currentBook.IsPurchased) { // give full book access
//
//            for (index, chap) in (self.currentBook.Chapters?.Chapter.enumerated())! {
//
//
//                if(chap.ChapterNumber != 0) {
//                    if self.isThisFileExist(with: chap, index: index) {
//                        chap.isChapterDownloaded = true
//                        let fileURL = Utility.main.returnBookFileUrl(with: self.currentBook.BookID, chapterNumber: chap.ChapterNumber, fileName: chap.AudioUrl!)
//                        self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                    }
//                    else {
//                        if let url = ParserHelper.creatLink(bookAudioManagerModel: self.currentBook.Chapters!, chapAudioFile: chap.AudioUrl!) {
//                            self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                        }
//                    }
//                }
//                else {
//                    self.currentBook.Chapters?.Chapter.remove(at: index)
//                }
//            }
//
//            print(self.jukebox.queuedItems)
//            self.populateLabelWithTime(self.labelSeekbarTime, time: 0.0)
//            self.increaseChapNumber(bool: true)
//
//            self.isOnlyShowPreview = false
//        }
//        else { // show only preview
//
//            for (_, previewChap) in (self.currentBook.Chapters?.Chapter)!.enumerated() {
//                if(previewChap.ChapterNumber == 0) {
//                    //                        self.setMaximumValueToSeekbar(with: (previewChap.FileDuration))
//                    self.seekBarSecondsDecrease = Int(previewChap.FileDuration)
//                    if let url =  ParserHelper.creatLink(bookAudioManagerModel: self.currentBook.Chapters!, chapAudioFile: previewChap.AudioUrl!) {
//                        self.previewLink = url
//                    }
//                }
//            }
//            self.labelChaperEpisodeNo.text = "Book Preview"
//            self.populateLabelWithTime(self.labelSeekbarTime, time: 0.0)
//            self.BtnForward.isEnabled = false
//            self.BtnBackward.isEnabled = false
//            self.isOnlyShowPreview = true
//
//            self.jukebox.append(item: JukeboxItem(URL: self.previewLink!), loadingAssets: true)
//        }
//        if self.playingThisEpisode != -1 {
//            self.jukebox.play(atIndex: self.playingThisEpisode)
//        }
    }
    
    func initPodcastData() {
        self.chapEpisodeNumber = self.playingThisEpisode
        
        self.labelBookPodcastName.text = self.podcast?.Name
        self.labelChaperEpisodeNo.text = String(format: "\(MiscStrings.episode.text) %2d", self.podcast.episodes[episodeIndex].EpisodeNo)
        
//        self.labelBookPodcastNameDown.text = self.currentPodcast.Title
//        self.labelnarrator.text = self.currentPodcast.Author
//        self.stackAuthor.isHidden = true
//        self.labelGenre.text = self.currentPodcast.Genre
//        self.stackDuration.isHidden = true
        
        self.labelBookPodcastNameDown.text = self.podcast?.Name
        self.labelnarrator.text = self.podcast?.ArtistName
        self.labelGenre.text = self.podcast?.Genre
        self.stackAuthor.isHidden = true
        self.stackDuration.isHidden = true

//        if self.currentPodcast.IsFavorite {
//            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: UIControlState())
//        }
//        else {
//            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: UIControlState())
//        }
        
        if (self.podcast?.IsFavorite)! {
            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: UIControlState())
        }
        else {
            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: UIControlState())
        }

        //self.cosmosRatingView.rating = self.currentPodcast.Rating
        self.cosmosRatingView.rating = (self.podcast?.Rating)!
        
//        if let imageURL = URL(string: (self.currentPodcast.Image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//            self.imageViewCenter.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//            self.imageViewDown.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//        }
//        else {
//            self.imageViewCenter.image = #imageLiteral(resourceName: "placeholder_image")
//            self.imageViewDown.image = #imageLiteral(resourceName: "placeholder_image")
//        }
        
        if let imageURL = URL(string: (self.podcast?.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
            self.imageViewCenter.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            self.imageViewDown.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            self.imageViewCenter.image = #imageLiteral(resourceName: "placeholder_image")
            self.imageViewDown.image = #imageLiteral(resourceName: "placeholder_image")
        }

//        if self.currentPodcast.TrackList.count > 0 {
//            let episdoe: PodcastTrack = self.currentPodcast.TrackList[self.chapEpisodeNumber]
//            if Utility.main.isThisPodcastFileExist(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!) {
//                let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//            }
//            else {
//                if self.currentPodcast.IsEpisodeAdded {
//                    if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                        self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                    }
//                }
//                else {
//                    if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                        self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                    }
//                }
//            }
//
//        }
        //<adv>
        self.getAdvertise()
        
//        print(self.jukebox.queuedItems)
    }
    
    func initPodcastFirstEpisode() {
        if Utility.main.isThisPodcastFileExist(with: self.podcast.PodcastId, EpisodeID: self.podcast.episodes[episodeIndex].PodcastEpisodeID) {
            let fileURL = Utility.main.returnPodcastFileUrl(with: self.podcast.PodcastId, EpisodeID: self.podcast.episodes[episodeIndex].PodcastEpisodeID)
            self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
        } else {
            if let url = URL(string: (self.podcast.episodes[episodeIndex].FilePath!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
            }
        }
        
//        let filename = String(format: "episode %2d", (self.podcastEpisode?.EpisodeNo)!)
//        if Utility.main.isThisPodcastFileExist(with: (self.podcast?.PodcastId)!, EpisodeID: (self.podcastEpisode?.PodcastEpisodeID)!, fileName: filename) {
//            let fileURL =  Utility.main.returnPodcastFileUrl(with: (self.podcast?.PodcastId)!, EpisodeID: (self.podcastEpisode?.PodcastEpisodeID)!, fileName: filename)
//            self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//        } else {
//            if let url = URL(string: (self.podcastEpisode?.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//            }
//        }
        
        self.jukebox.play()
        
        
//        if self.currentPodcast.TrackList.count > 0 {
//            let episdoe: PodcastTrack = self.currentPodcast.TrackList[self.chapEpisodeNumber]
//            if Utility.main.isThisPodcastFileExist(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!) {
//                let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//            }
//            else {
//                if self.currentPodcast.IsEpisodeAdded {
//                    if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                        self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                    }
//                }
//                else {
//                    if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                        self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                    }
//                }
//            }
//            self.jukebox.play()
//        }
    }
    
    func initNewsData() {
//        self.labelBookPodcastName.text = self.currentNews.source_name
//        self.labelChaperEpisodeNo.text = self.currentNews.title
//
//        self.BtnListChap.isHidden = true
//        self.BtnForward.isUserInteractionEnabled = false
//        self.BtnBackward.isUserInteractionEnabled = false
//
//        if let imageURL = URL(string: (self.currentNews.image_url!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
//            self.imageViewCenter.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//            self.imageViewDown.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
//        }
//        else {
//            self.imageViewCenter.image = #imageLiteral(resourceName: "placeholder_image")
//            self.imageViewDown.image = #imageLiteral(resourceName: "placeholder_image")
//        }
//
//        self.getAdvertise()
//        print(self.jukebox.queuedItems)
    }
    
    func uiCustomization(){
        self.slider.setThumbImage(UIImage(named: ""), for: .normal)
        
        self.slider.setMinimumTrackImage(UIImage(named: "back_slider"), for: UIControlState.normal)
        self.slider.setMaximumTrackImage(UIImage(named: "front_slider"), for: UIControlState.normal)
    }
    
    func initAdvertiseView() {
        
        advertiseView.frame = CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)
        self.view.addSubview(advertiseView)
    }
    
    func populateLabelWithTime(_ label : UILabel, time: Double) {
        let minutes = Int(time / 60)
        let seconds = Int(time) - minutes * 60
        
        label.text = "-" + String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
    
    // call this method on player forward/backward actions, it will change the title of chapter number with chapter's array validation
    func increaseChapNumber(bool: Bool) {
        if(bool) {
            if(self.chapEpisodeNumber < (self.currentBook.Chapters?.Chapter.count)!) {
                self.chapEpisodeNumber += 1
            }
        }
        else {
            if(chapEpisodeNumber > 1) {
                self.chapEpisodeNumber -= 1
            }
        }
        self.labelChaperEpisodeNo.text = "\(MiscStrings.chapter.text) \(self.chapEpisodeNumber)"
    }
    
    func setChapNumber(num: Int) {
        
        if(!self.currentBook.IsPurchased) {
            self.labelChaperEpisodeNo.text = MiscStrings.bookPreview.text
        }
        else {
            self.chapEpisodeNumber = num + 1
            self.labelChaperEpisodeNo.text = "\(MiscStrings.chapter.text) \(self.chapEpisodeNumber)"
        }
    }
    
    func setEpisodeName(index: Int) {
        //self.labelChaperEpisodeNo.text = self.currentPodcast.TrackList[index].Name
        self.labelChaperEpisodeNo.text = String(format: "\(MiscStrings.episode.text) %2d", index)
    }
    
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        
        let positionOfSlider: CGPoint = slider.frame.origin
        let widthOfSlider: CGFloat = slider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(slider.maximumValue) / widthOfSlider)
        // set player to my desire time
        if self.playingType == .book {
            if let duration = self.currentBook.Chapters?.Chapter[jukebox.playIndex].FileDuration {
                jukebox.seek(toSecond: Int(Double(newValue) * duration))
                slider.setValue(Float(newValue), animated: true)
            }
        }
        else if self.playingType == .podcast || self.playingType == .news {
            if let duration = jukebox.currentItem?.meta.duration {
                jukebox.seek(toSecond: Int(Double(newValue) * duration))
                slider.setValue(Float(newValue), animated: true)
            }
        }
    }
    
    func remoateCommandCentreClosure() {
        commandCenter.pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the pause command
            print("pauseCommand")
            
            if let currentTime = self.jukebox.currentItem?.currentTime {
                if currentTime >= 0.0 {
                    switch self.jukebox.state {
                    case .ready :
                        self.jukebox.play(atIndex: 0)
                    case .playing :
                        self.jukebox.pause()
                    case .paused :
                        self.jukebox.play()
                    default:
                        self.jukebox.stop()
                    }
                    if self.playingType == .book {
                        self.setChapNumber(num: self.jukebox.playIndex)
                    }
                    else if self.playingType == .podcast {
                        self.setEpisodeName(index: self.jukebox.playIndex)
                    }
                    return .success
                }
                else {
                    return .commandFailed
                }
            }
            return .commandFailed
        }
        commandCenter.playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("playCommand")
            
            if let currentTime = self.jukebox.currentItem?.currentTime {
                if currentTime >= 0.0 {
                    switch self.jukebox.state {
                    case .ready :
                        self.jukebox.play(atIndex: 0)
                    case .playing :
                        self.jukebox.pause()
                    case .paused :
                        self.jukebox.play()
                    default:
                        self.jukebox.stop()
                    }
                    if self.playingType == .book {
                        self.setChapNumber(num: self.jukebox.playIndex)
                    }
                    else if self.playingType == .podcast {
                        self.setEpisodeName(index: self.jukebox.playIndex)
                    }
                    return .success
                }
                else {
                    return .commandFailed
                }
            }
            
            return .commandFailed
            
            
        }
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("nextTrackCommand")
            return .success
        }
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            print("previousTrackCommand")
            return .success
        }
    }
    
//    func isThisFileExist(with bookChap: BookChapter, index: Int) -> Bool {
//        let filePath = Utility.main.returnBookFileUrl(with: (self.currentBook?.BookID)!, chapterNumber: bookChap.ChapterNumber, fileName: bookChap.AudioUrl!.removingWhitespaces())
//        let fileManager = FileManager.default
//        if fileManager.fileExists(atPath: filePath.path) {
//            print("FILE AVAILABLE")
//            return true
//        }
//        return false
//    }
    
    func isContinuousPlayOn() -> Bool {
        if let contiBool = Constants.USER_DEFAULTS.value(forKey: Constants.ContinuousPlay) as? Bool {
            if contiBool {
                return true
            }
        }
        return false
    }
}
// MARK: - IBACTIONS
extension Player
{
    @IBAction func actionBackward(_ sender: Any) {
        
        if self.playingType == .book {
            if let time = jukebox.currentItem?.currentTime, time > 5.0 || jukebox.playIndex == 0 {
                jukebox.replayCurrentItem()
            } else {
                jukebox.playPrevious()
                if self.playingType == .book {
                    self.increaseChapNumber(bool: false)
                }
                else if self.playingType == .podcast {
                    self.setEpisodeName(index: self.jukebox.playIndex)
                }
            }
        }
        else if self.playingType == .podcast {
//            if let time = jukebox.currentItem?.currentTime, time > 5.0 || self.chapEpisodeNumber == 0 {
//                jukebox.replayCurrentItem()
//            } else {
//                if self.currentPodcast.TrackList.count > 0 {
//                    if self.chapEpisodeNumber - 1 >= 0  {
//                        self.setEpisodeName(index: self.chapEpisodeNumber - 1)
//                        let episdoe: PodcastTrack = self.currentPodcast.TrackList[self.chapEpisodeNumber - 1]
//                        self.chapEpisodeNumber -= 1
//                        self.jukebox.stop()
//                        self.jukebox.remove(item: self.jukebox.currentItem!)
//
//                        // load previous episode
//                        if episdoe.isEpisodeDownloaded {
//                            let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                            self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                        }
//                        else {
//                            if self.currentPodcast.IsEpisodeAdded {
//                                if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                                    self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                                }
//                            }
//                            else {
//                                if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                                    self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                                }
//                            }
//                        }
//                    }
//                }
//                jukebox.play()
//                return
//            }
        }
    }
    @IBAction func actionPlayPause(_ sender: Any) {

        switch jukebox.state {
        case .ready :
            jukebox.play(atIndex: 0)
        case .playing :
            jukebox.pause()
        case .paused :
            jukebox.play()
        default:
            jukebox.stop()
        }
        
        if self.itsTimeForAdvertise {
            self.initAdvertiseView()
        } else {
            if self.playingType == .book {
                self.setChapNumber(num: jukebox.playIndex)
            }
            else if self.playingType == .podcast {
                self.setEpisodeName(index: self.chapEpisodeNumber/*jukebox.playIndex*/)
            }
        }
    }
    @IBAction func actionForward(_ sender: Any) {
        if self.playingType == .book {
            self.increaseChapNumber(bool: true)
            
            if (jukebox.playIndex == 0) {
                jukebox.play(atIndex: jukebox.playIndex + 1)
            }
            else {
                
                jukebox.playNext()
            }
        }
        else if self.playingType == .podcast {
//            if self.currentPodcast.TrackList.count > 0 {
//                if self.currentPodcast.TrackList.count > self.chapEpisodeNumber + 1 {
//                    self.setEpisodeName(index: self.chapEpisodeNumber + 1)
//                    let episdoe: PodcastTrack = self.currentPodcast.TrackList[self.chapEpisodeNumber + 1]
//                    self.chapEpisodeNumber += 1
//                    self.jukebox.stop()
//                    self.jukebox.remove(item: self.jukebox.currentItem!)
//                    if episdoe.isEpisodeDownloaded {
//                        let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                        self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                    }
//                    else {
//                        if self.currentPodcast.IsEpisodeAdded {
//                            if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                            }
//                        }
//                        else {
//                            if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                            }
//                        }
//                    }
//                }
//            }
//            jukebox.play()
//            return
        }
    }
    
    @IBAction func actionVolume(_ sender: Any) {
        let button = sender as! UIButton
        if(button.isSelected) {
            jukebox.volume = 1
            isVolumeOn = true
        }
        else {
            jukebox.volume = 0
            isVolumeOn = false
        }
        button.isSelected = !button.isSelected
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSeekBarValueChanged(_ sender: Any) {
        if let sliderObj = sender as? UISlider {
            if self.playingType == .book {
                if let duration = self.currentBook.Chapters?.Chapter[jukebox.playIndex].FileDuration {
                    jukebox.seek(toSecond: Int(Double(sliderObj.value) * duration))
                }
            }
            else if self.playingType == .podcast || self.playingType == .news {
                if let duration = jukebox.currentItem?.meta.duration {
                    jukebox.seek(toSecond: Int(Double(sliderObj.value) * duration))
                }
            }
        }
        
    }
    
    @IBAction func actionChapPodcastList(_ sender: Any) {
        let playerListing = Constants.ChapterPodcastRightSideMenu
        playerListing.delegate = self
        playerListing.listType = self.playingType
        if self.playingType == .book {
            Singleton.sharedInstance.chaptersArray = self.currentBook.Chapters?.Chapter
            Singleton.sharedInstance.currentlyPlayingChapEpisodeNumber = self.jukebox.playIndex
        }
        else if self.playingType == .podcast {
            //Singleton.sharedInstance.podcastEpisodeArray = self.currentPodcast.TrackList
            //Singleton.sharedInstance.currentlyPlayingChapEpisodeNumber = self.chapEpisodeNumber
        }
        
        sideMenuController?.rightViewController = playerListing
        sideMenuController?.showRightView()
    }
    
    @IBAction func actionHeart(_ sender: Any) {
        if self.playingType == .book {
            if !self.currentBook.IsFavorite {
                self.addToFavoriteBook(with: self.currentBook.BookID)
            }
            else {
                self.removeFromFavoriteBook(with: self.currentBook.BookID)
            }
        }
        else if self.playingType == .podcast {
            self.updatePodcastFavoriteStatus(with: (self.podcast?.IsFavorite)!, trackID: (self.podcast?.TrackId)!)
            //self.updatePodcastFavoriteStatus(with: !self.currentPodcast.IsFavorite, trackID: self.currentPodcastTrackID)
        }
    }
    
}
// MARK: - SEEK BAR HELPER METHODS
extension Player
{
    // this method set the meximum of seek bar
    func setMaximumValueToSeekbar(with duration: Double) {
        self.slider.maximumValue = Float(duration)
    }
    
    // increase seek bar after 1 sec
    func increaseSeekbar(to seconds: Int) {
        
        UIView.animate(withDuration: 1, delay: 0, options: [.curveLinear], animations: {
            self.slider.value = Float(seconds)
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func setSeekToZero() {
        self.slider.value = 0.00
    }
    
    
}

// MARK: - API CALLING
extension Player
{
    func getBookDetails(with bookID: Int) {
//        let param: [String : Any] = [
//            "BookID" : ParserHelper.convertIntToString(value: bookID)
//        ]
//
//        Utility.main.showLoader()
//        APIManager.sharedInstance.bookApiManager.getBookDetails(params: param, success: {(success) in
//            self.currentBook = BookModel(value: success)
//            if(self.currentBook.IsPurchased) { // give full book access
//
//                for (index, chap) in (self.currentBook.Chapters?.Chapter.enumerated())! {
//
//
//                    if(chap.ChapterNumber != 0) {
//                        if self.isThisFileExist(with: chap, index: index) {
//                            chap.isChapterDownloaded = true
//                            let fileURL = Utility.main.returnBookFileUrl(with: self.currentBook.BookID, chapterNumber: chap.ChapterNumber, fileName: chap.AudioUrl!)
//                            self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                        }
//                        else {
//                            if let url = ParserHelper.creatLink(bookAudioManagerModel: self.currentBook.Chapters!, chapAudioFile: chap.AudioUrl!) {
//                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                            }
//                        }
//                    }
//                    else {
//                        self.currentBook.Chapters?.Chapter.remove(at: index)
//                    }
//                }
//
//                print(self.jukebox.queuedItems)
//                self.populateLabelWithTime(self.labelSeekbarTime, time: 0.0)
//                self.increaseChapNumber(bool: true)
//
//                self.isOnlyShowPreview = false
//            }
//            else { // show only preview
//
//                for (_, previewChap) in (self.currentBook.Chapters?.Chapter)!.enumerated() {
//                    if(previewChap.ChapterNumber == 0) {
//                        //                        self.setMaximumValueToSeekbar(with: (previewChap.FileDuration))
//                        self.seekBarSecondsDecrease = Int(previewChap.FileDuration)
//                        if let url =  ParserHelper.creatLink(bookAudioManagerModel: self.currentBook.Chapters!, chapAudioFile: previewChap.AudioUrl!) {
//                            self.previewLink = url
//                        }
//                    }
//                }
//                self.labelChaperEpisodeNo.text = "Book Preview"
//                self.populateLabelWithTime(self.labelSeekbarTime, time: 0.0)
//                self.BtnForward.isEnabled = false
//                self.BtnBackward.isEnabled = false
//                self.isOnlyShowPreview = true
//
//                self.jukebox.append(item: JukeboxItem(URL: self.previewLink!), loadingAssets: true)
//            }
//            Utility.main.hideLoader()
//        }, failure: {failure in
//            Utility.main.hideLoader()
//        })
    }
    
    func addToFavoriteBook(with bookID: Int) {
        let param: [String : Any] = [
            "bookID" : bookID
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.addThisBooksToFvtList(with: param, success: {(success) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = true
            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: UIControlState())
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func removeFromFavoriteBook(with bookID: Int) {
        let param: [String : Any] = [
            "bookID" : bookID
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.removeFromFvt(with: param, success: {
            (responceObj) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = false
            self.BtnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: UIControlState())
        }, failure: {
            (failure) in
            Utility.main.hideLoader()
        })
    }
    
    func updatePodcastFavoriteStatus(with markIsFvt: Bool, trackID: Int) {
        let param: [String : Any] = [
            "TrackId" : trackID,
            "favorite" : markIsFvt
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.updateFavoritePodcast(with: param, success: {
            (responceObj) in
            //self.currentPodcast.IsFavorite = markIsFvt
            self.podcast?.IsFavorite = markIsFvt
            
            if markIsFvt {
                self.BtnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: UIControlState())
            }
            else {
                self.BtnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: UIControlState())
            }
            Utility.main.hideLoader()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()})
    }
    
    func getAdvertise() {
        //MARK#
        let AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let Country = AppDelegate.shared.Country
        let params : [String:Any] = ["AccountID":AccountID,"Country":Country]
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAdvertise(params: params, success: { (responseObject) in
            self.currentAdvertiseArray.append(Advertise(value: responseObject as [String : Any]))
            
            if let advertiseURL = URL(string: (self.currentAdvertiseArray[0].AudioUrl! + self.currentAdvertiseArray[0].AudioPath!).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                //                self.jukebox.append(item: , loadingAssets: true)
                self.advertismentJukeBoxItem = JukeboxItem(URL: advertiseURL)
                self.jukebox.append(item: self.advertismentJukeBoxItem!, loadingAssets: true)
            }

            print(self.jukebox.queuedItems)
            Utility.main.hideLoader()
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
}
// MARK: - PlayerListing Delegate
extension Player: PlayerListingDelegate
{
    func didChapEpisodeSelect(chapEpisodeNo: Int) {
        if self.playingType == .podcast {
//            if self.currentPodcast.TrackList.count > 0 {
//                if self.currentPodcast.TrackList.count > chapEpisodeNo  {
//                    self.setEpisodeName(index: chapEpisodeNo)
//                    let episdoe: PodcastTrack = self.currentPodcast.TrackList[chapEpisodeNo]
//                    self.chapEpisodeNumber = chapEpisodeNo
//                    self.jukebox.stop()
//                    self.jukebox.remove(item: self.jukebox.currentItem!)
//                    if episdoe.isEpisodeDownloaded {
//                        let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                        self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                    }
//                    else {
//                        if self.currentPodcast.IsEpisodeAdded {
//                            if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                            }
//                        }
//                        else {
//                            if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                            }
//                        }
//                    }
//                }
//            }
//            jukebox.play()
        }
        else if self.playingType == .book {
            self.setChapNumber(num: chapEpisodeNo)
            jukebox.play(atIndex: chapEpisodeNo)
        }
    }
}

// MARK: - Jukebox Delegate
extension Player: JukeboxDelegate
{
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        // when play or pause
        print("jukeboxStateDidChange")
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.indicator.alpha = jukebox.state == .loading ? 1 : 0
            if jukebox.state == .loading {
                self.indicator.startAnimating()
            }
            else {
                self.indicator.stopAnimating()
                
            }
            self.BtnPlayPause.alpha = jukebox.state == .loading ? 0 : 1
            self.BtnPlayPause.isEnabled = jukebox.state == .loading ? false : true
        })
        
        if jukebox.state == .ready {
            BtnPlayPause.setImage(#imageLiteral(resourceName: "play-icon"), for: UIControlState())
        } else if jukebox.state == .loading  {
            self.BtnPlayPause.setImage(#imageLiteral(resourceName: "pause-icon"), for: UIControlState())
        } else {
            let image: UIImage!
            switch jukebox.state {
            case .playing, .loading:
                image = #imageLiteral(resourceName: "pause-icon")
            case .paused, .failed, .ready:
                image = #imageLiteral(resourceName: "play-icon")
            }
            self.BtnPlayPause.setImage(image, for: UIControlState())
        }
        
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        var duration: Double?
        if self.playingType == .book {
            duration = self.currentBook.Chapters?.Chapter[jukebox.playIndex].FileDuration
        }
        else if self.playingType == .podcast || self.playingType == .news {
            duration = jukebox.currentItem?.meta.duration
            self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(duration!)), for: UIControlState())
        }
        if let currentTime = jukebox.currentItem?.currentTime , let duration = duration {
            
            // buffering
            if currentTime == 0.0 {
                self.indicator.alpha = 1
                self.indicator.startAnimating()
                self.BtnPlayPause.alpha = 0
                self.BtnPlayPause.isEnabled = false
                self.slider.isUserInteractionEnabled = false
            }
            else {
                self.indicator.alpha = 0
                self.indicator.stopAnimating()
                self.BtnPlayPause.alpha =  1
                self.BtnPlayPause.isEnabled = true
                self.slider.isUserInteractionEnabled = true
            }
            if((duration - currentTime) > 0) {
                
                let value = Float((currentTime / duration))
                slider.value = value
                
                self.populateLabelWithTime(self.labelSeekbarTime, time: duration - currentTime)
            }
            else {
                if self.playingType == .book {
                    if !self.isContinuousPlayOn() {
                        self.BtnPlayPause.setImage(#imageLiteral(resourceName: "play-icon"), for: UIControlState())
                        self.populateLabelWithTime(self.labelSeekbarTime, time: duration)
                        self.setSeekToZero()
                        jukebox.stop()
                    } else {
                        // if last chapter, stop the jukebox
                        if jukebox.queuedItems.count - 1 == self.jukebox.playIndex {
                            self.BtnPlayPause.setImage(#imageLiteral(resourceName: "play-icon"), for: UIControlState())
                            self.populateLabelWithTime(self.labelSeekbarTime, time: duration)
                            self.setSeekToZero()
                            jukebox.stop()
                            return
                        }
                        self.increaseChapNumber(bool: true)
                        if (jukebox.playIndex == 0) {
                            jukebox.play(atIndex: jukebox.playIndex + 1)
                        }
                        else {
                            
                            jukebox.playNext()
                        }
                    }
                    // other wise normal behaviour mein set chale ga...
                    
                } else if self.playingType == .podcast {
                    // play the next
                    if self.isContinuousPlayOn() {
//                            if self.currentPodcast.TrackList.count > 0 {
//                                if self.currentPodcast.TrackList.count > self.chapEpisodeNumber + 1 {
//                                    self.setEpisodeName(index: self.chapEpisodeNumber + 1)
//                                    let episdoe: PodcastTrack = self.currentPodcast.TrackList[self.chapEpisodeNumber + 1]
//                                    self.chapEpisodeNumber += 1
//                                    self.jukebox.stop()
//                                    self.jukebox.remove(item: self.jukebox.currentItem!)
//                                    if episdoe.isEpisodeDownloaded {
//                                        let fileURL =  Utility.main.returnPodcastFileUrl(with: self.currentPodcastTrackID, EpisodeID: self.chapEpisodeNumber, fileName: (episdoe.Name?.removingWhitespaces())!)
//                                        self.jukebox.append(item: JukeboxItem(URL: fileURL), loadingAssets: true)
//                                    }
//                                    else {
//                                        if self.currentPodcast.IsEpisodeAdded {
//                                            if let url = URL(string: (ParserHelper.creatLinkForPodcast(podcastTrackModel: episdoe, episodeAudioFile: episdoe.FileUrl!)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                                            }
//                                        }
//                                        else {
//                                            if let url = URL(string: (episdoe.FileUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
//                                                self.jukebox.append(item: JukeboxItem(URL: url), loadingAssets: true)
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                            jukebox.play()
                    } else if self.playingType == .news {
                        
                    }
                    else {
                        self.BtnPlayPause.setImage(#imageLiteral(resourceName: "play-icon"), for: UIControlState())
                        self.populateLabelWithTime(self.labelSeekbarTime, time: duration)
                        self.setSeekToZero()
                    }
                }
            }
            
            if self.isVolumeOn {
                jukebox.volume = 1
            }
            else {
                jukebox.volume = 0
            }
        }
        
        
//        if self.itsTimeForAdvertise {
//            var duration: Double?
//            duration = jukebox.currentItem?.meta.duration
//            self.btnTimeTop.setTitle(ParserHelper.getDuration(with: Int(duration!)), for: UIControlState())
//
//            if let currentTime = jukebox.currentItem?.currentTime , let duration = duration {
//                if((duration - currentTime) <= 0)  {
//                    self.itsTimeForAdvertise = false
//                    self.advertiseView.removeFromSuperview()
//                    if self.playingType == .news {
//                        if self.playingType == .news {
//                            if let sourceURL = URL(string: (self.currentNews.audio_link?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
//                                self.jukebox.remove(item: self.advertismentJukeBoxItem!)
//
//                                self.jukebox.append(item: JukeboxItem(URL: sourceURL), loadingAssets: true)
//                                self.jukebox.play()
//                            }
//                        }
//                    }
//                    else if self.playingType == .podcast {
//                        self.initPodcastFirstEpisode()
//                    }
//
//                }
//            }
//
//        } else {
//        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }
    
}



class CustomSLide: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        //set your bounds here
        return CGRect(origin: CGPoint.init(x: 0, y: 0), size: CGSize(width:bounds.width, height:30))
        
    }
}
