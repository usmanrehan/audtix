//
//  News.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class News: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var showingWinless: Bool = false
    
    var arrNewsCategories = Array<NewsCategory>()
    var arrSubscribedNews = Array<NewsListModel>()
    
    var isFilter = false
    var arrNews = Array<NewsListModel>()
    
    //var newsCategoryArray = [NewsCategory]()
    //var subscribedChannelsArray = [NewsSubscribedModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Following three lines of code is to disable sticky header in tableview.
        let dummyViewHeight = CGFloat(40)
        self.tableview.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableview.bounds.size.width, height: dummyViewHeight))
        self.tableview.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.tableview.register(UINib(nibName: "NewsCollection", bundle: nil), forCellReuseIdentifier: "NewsCollection")
        self.tableview.register(UINib(nibName: "PodcastCollection", bundle: nil), forCellReuseIdentifier: "PodcastCollection")
        self.tableview.register(UINib(nibName: "PodcastTableCell", bundle: nil), forCellReuseIdentifier: "PodcastTableCell")
//        self.getNewsCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNewsCategories()
        self.getSubscribedNews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigateToNewsDetails(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsDetails") as? NewsDetails {
            if !self.isFilter{
                controller.newsObject = self.arrSubscribedNews[indexPath.row]
            }
            else{
                controller.newsObject = self.arrNews[indexPath.row]
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func navigateToNewsListWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsList") as? NewsList {
            controller.title = self.arrNewsCategories[indexPath.row].SourceName ?? ""
            controller.categoryId = self.arrNewsCategories[indexPath.row].Id
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func navigateToSubscribedNewsList() {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsList") as? NewsList {
            controller.title = AlertMessages.mySubscription.message
            controller.flag = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK: - IB ACTIONS
extension News {
    @objc func actionSeeAll() {
        navigateToSubscribedNewsList()
    }
    
    @IBAction func actionMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }

    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionUnSubcribed(_ sender: Any) {
        if let index: Int = (sender as? UIButton)?.tag {
            self.unSubscribeThisNews(with: self.arrSubscribedNews[index].NewsID)
        }
    }
    
    @IBAction func actionFilterBtn(_ sender: Any) {
        let podcastFilter = Constants.newsFilterRightSideMenu
        podcastFilter.delegate = self
        self.sideMenuController?.rightViewController = podcastFilter
        self.sideMenuController?.showRightView()
    }
}
// MARK: - API CALLING
extension News {
    public func getSubscribedNews() {
        APIManager.sharedInstance.newsApiManager.GetAllNewsSubscriptions(success: { (responseArray) in
            self.arrSubscribedNews.removeAll()
            for item in responseArray {
                let model = NewsListModel(value: item as! [String: Any])
                self.arrSubscribedNews.append(model)
            }
            self.tableview.reloadData()
        }) { (failure) in
            print(failure.localizedDescription)
        }
    }

    public func getNewsCategories() {
        Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.getAllNewsCategory(success: { (responseArray) in
            Utility.main.hideLoader()
            self.arrNewsCategories.removeAll()
            for item in responseArray {
                self.arrNewsCategories.append(NewsCategory(value: item as! [String : Any]))
            }
            self.tableview.reloadData()
        }) { (failure) in
            Utility.main.hideLoader()
            print(failure.localizedDescription)
        }
    }
    
    public func unSubscribeThisNews(with newsId: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId": newsId]
        APIManager.sharedInstance.newsApiManager.UnSubscribeThisNewsChannel(param: param, success: { (isSuccess) in
            Utility.main.hideLoader()            
            Utility.main.postSubscribedNewsChannelNotification()
            for (index, subscribedModel) in self.arrSubscribedNews.enumerated() {
                if subscribedModel.NewsID == newsId {
                    self.arrSubscribedNews.remove(at: index)
                    break
                }
            }
            self.tableview.reloadData()
        }, failure: { (failure) in
            Utility.main.hideLoader()
            print(failure.localizedDescription)
        })
    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension News: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isFilter{
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isFilter{
            return 1
        }
        else{
            return self.arrNews.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !self.isFilter{
            if indexPath.section == 0 { //My Subscriptions
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastCollection") as? PodcastCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "PodcastCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PodcastCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                
                cell.collectionView.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                cell.backgroundColor = UIColor.clear
                
                return cell
            }
            else { //News Channel
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCollection") as? NewsCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "NewsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NewsCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                
                cell.collectionView.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                cell.backgroundColor = UIColor.clear
                
                return cell
            }
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastTableCell") as? PodcastTableCell else { return UITableViewCell() }
            let objNews = self.arrNews[indexPath.row]
            cell.BindData(with: objNews)
            cell.BtnSubscrptionBottom.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !self.isFilter{
            let booksHeader: BooksHeader = .fromNib()
            booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
            booksHeader.backgroundColor = UIColor.clear
            booksHeader.buttonSeeAll.tag = section
            booksHeader.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAll), for: .touchUpInside)
            switch section {
            case 0:
                booksHeader.labelTitle.text = MiscStrings.mySubscription.text.uppercased()
                booksHeader.buttonSeeAll.isHidden = false
                break
            case 1:
                booksHeader.labelTitle.text = MiscStrings.newsChannels.text.uppercased()
                booksHeader.buttonSeeAll.isHidden = true
                break
            case 2:
                booksHeader.labelTitle.text = MiscStrings.bESTOFTHEBEST.text.uppercased()
                booksHeader.buttonSeeAll.isHidden = true
                break
            default:
                booksHeader.labelTitle.text = "".uppercased()
                booksHeader.buttonSeeAll.isHidden = true
            }
            
            return booksHeader
        }
        else{
            return nil
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.isFilter{
            switch indexPath.section {
            case 0:
                return (self.view.frame.size.width / 4.0) + 60.0 //Add padding for subscribe button and text label
            case 1:
                let height = ((self.view.frame.size.width - (8 * 2)) / 3.0) + 30.0 //Add padding text label
                return ceil(CGFloat(self.arrNewsCategories.count) / 3.0) * height
            default:
                return 0
            }
        }
        else{
            return 120
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !self.isFilter{
            switch section {
            case 0:
                return 50
            case 1:
                return 50
            default:
                return 0
            }
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isFilter{
            self.navigateToNewsDetails(indexPath)
        }
    }
}

extension News: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0: //My Subscription
            return self.arrSubscribedNews.count
            //return self.showingWinless ? 10 : self.arrSubscribedNews.count
        case 1: //News Channels
            return self.arrNewsCategories.count
            //return self.showingWinless ? 10 : self.arrNewsCategories.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PodcastCollectionCell", for: indexPath) as? PodcastCollectionCell else { return UICollectionViewCell() }
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            cell.labelTitle.textColor = UIColor.darkGray
            if !self.showingWinless {
                cell.BindData(with: self.arrSubscribedNews[indexPath.row])
            }
            cell.btnSubscription.tag = indexPath.row
            cell.btnSubscription.addTarget(self, action: #selector(self.actionUnSubcribed(_:)), for: .touchUpInside)
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionCell", for: indexPath) as? NewsCollectionCell else { return UICollectionViewCell() }
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            cell.heroID = "\(indexPath.row) + \(indexPath.section)"
            cell.heroModifiers = [.arc]
            if !self.showingWinless {
                cell.BindData(newsCategoryItem: self.arrNewsCategories[indexPath.row])
            }
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            return cell

        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 0: //My Subscription
            let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
            return CGSize(width: width, height: width + 60.0)
        case 1:
            let width = (collectionView.frame.size.width - (8 * 2)) / 3.0
            return CGSize(width: width, height: width + 30.0)
        default:
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch collectionView.tag {
        case 0: //My Subscription
            self.navigateToNewsDetails(indexPath)
            break
        case 1:
            self.navigateToNewsListWith(indexPath)
            break
        default:
            break
        }
    }
}

// MARK: - NewsFilter Delegate
extension News: NewsFilterDelegate {
    func didApplyFilterSelect(filterData: NewsFilterDataSet) {
        print(filterData)
        if filterData.shouldFilter{
            self.getFilteredNews(data: filterData)
        }
        else{
            self.isFilter = false
            self.tableview.reloadData()
        }
    }
}

// MARK: - NewsDetails Delegate
extension News: NewsDetailsDelegate {
    func didUnSubscribeThisChannel(with Id: Int) {
        for (index, subscribedModel) in self.arrSubscribedNews.enumerated() {
            if subscribedModel.NewsID == Id {
                self.arrSubscribedNews.remove(at: index)
                break
            }
        }
        self.tableview.reloadData()
    }
    
    func didSubscribedThisChannel(with Id: Int) {
        self.arrSubscribedNews.removeAll()
        
        //self.getAllSubscribedNews(withGetNewsService: false)
    }
}
//MARK:- GetFilterResponse
extension News{
    private func getFilteredNews(data:NewsFilterDataSet){
        let MinDuration = data.minDuration
        let MaxDuration = data.maxDuration
        let MinSubscriber = data.minSubscribers
        let MaxSubscriber = data.maxSubscribers
        let Type = 3 //news
        let pageNumber = 1
        let count = 100
        //let isInternational = data.isInternational
        let CountryIds = data.commaSeparatedLocationIDs
        let CategoryIds = data.commaSeparatedEntityIDs
        var params : [String:Any] = [
            "MinDuration":MinDuration,
            "MaxDuration":MaxDuration,
            "MinSubscriber":MinSubscriber,
            "MaxSubscriber":MaxSubscriber,
            "Type":Type,
            "pageNumber":pageNumber,
            "count":count
        ]
        if Validation.validateStringLength(CountryIds){
            params["CountryIds"] = CountryIds
        }
        if Validation.validateStringLength(CategoryIds){
            params["CategoryIds"] = CategoryIds
        }
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getFilterResponse(params: params, success: { (responseArray) in
            Utility.main.hideLoader()
            //newsModel
            self.arrNews.removeAll()
            for item in responseArray {
                let model = NewsListModel(value: item as! [String: Any])
                self.arrNews.append(model)
            }
            
            self.isFilter = true
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}
