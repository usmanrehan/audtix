//
//  NewsFilter.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import LGSideMenuController
import ObjectMapper
import RangeSeekSlider

protocol NewsFilterDelegate {
    func didApplyFilterSelect(filterData: NewsFilterDataSet)
}
struct NewsFilterDataSet{
    var minDuration = 0
    var maxDuration = 0
    var minSubscribers = 0
    var maxSubscribers = 0
    var commaSeparatedLocationIDs = ""
    var commaSeparatedEntityIDs = ""
    var shouldFilter = false
    var isInternational = false
}
struct NewsFilterData {
    var id: Int
    var CountryName : String
    var CountryCode: String
    var isSelected: Bool
}

class NewsFilter: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var durationSeekBar = RangeSeekSlider()
    var subscribersSeekBar = RangeSeekSlider()
    var isInternational = UISwitch()
    
    var filterDataSet = NewsFilterDataSet()//Return in delegate for filtered data
    var filterDataService = NewsFilterDataModel()//Service dataset
    var flagCategories = true//Toggle
    var flagCountries = true//Toggle
    var isClear = false
    var is_international = false
    var delegate: NewsFilterDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getNewsFilterData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let mainViewController = sideMenuController!
        mainViewController.delegate = self
    }
    @IBAction func actionCross(_ sender: Any) {
        sideMenuController?.hideRightView()
    }
    @IBAction func actionApply(_ sender: Any) {
        sideMenuController?.hideRightView()
        let minSubscribers = self.subscribersSeekBar.selectedMinValue
        let maxSubscribers = self.subscribersSeekBar.selectedMaxValue
        let minDuration = self.durationSeekBar.selectedMinValue
        let maxDuration = self.durationSeekBar.selectedMaxValue
        let commaSeparatedLocationIDs = self.getAllSelectedCountryIDs()
        let commaSeparatedEntityIDs = self.getAllSelectedCategoryIDs()
        if self.isInternational.isOn{
           self.filterDataSet = NewsFilterDataSet(minDuration: Int(minDuration), maxDuration: Int(maxDuration), minSubscribers: Int(minSubscribers), maxSubscribers: Int(maxSubscribers), commaSeparatedLocationIDs: commaSeparatedLocationIDs, commaSeparatedEntityIDs: commaSeparatedEntityIDs,shouldFilter: !self.isClear,isInternational:true)
        }
        else{
            self.filterDataSet = NewsFilterDataSet(minDuration: Int(minDuration), maxDuration: Int(maxDuration), minSubscribers: Int(minSubscribers), maxSubscribers: Int(maxSubscribers), commaSeparatedLocationIDs: commaSeparatedLocationIDs, commaSeparatedEntityIDs: commaSeparatedEntityIDs,shouldFilter: !self.isClear,isInternational:false)
        }
        print(self.filterDataSet)
        self.delegate?.didApplyFilterSelect(filterData: self.filterDataSet)
        //self.delegate?.didApplyFilterSelect(idsStringCommaSeparated: self.getAllSelectedIDs())
    }
    
    @IBAction func clearApply(_ sender: Any) {
        for i in self.filterDataService.locations{
            i.isSelected = false
        }
        for i in self.filterDataService.entityModel{
            i.isSelected = false
        }
        self.flagCountries = true
        self.flagCategories = true
        self.isClear = true
        self.is_international = false
        self.tableView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
extension NewsFilter: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0,1:
            return 1
        case 2:
            return 0
//            if !flagCategories{
//                return self.filterDataService.entityModel.count
//            }
//            else{
//                return 0
//            }
        default:
            if !flagCountries{
                return self.filterDataService.locations.count
            }
            else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell_country = Bundle.main.loadNibNamed("CheckBoxCell", owner: self, options: nil)?.first as! CheckBoxCell
        let cell_newsCategories = Bundle.main.loadNibNamed("CheckBoxCell", owner: self, options: nil)?.first as! CheckBoxCell
        let cell_duration_slider = Bundle.main.loadNibNamed("FilterRangeCell", owner: self, options: nil)?.first as! FilterRangeCell
        let cell_subscriber_slider = Bundle.main.loadNibNamed("FilterRangeCell", owner: self, options: nil)?.first as! FilterRangeCell
        switch indexPath.section {
        case 0:
            cell_duration_slider.rangeSeekBar.addTarget(self, action: #selector(didUpdateValues(sender:)), for: .editingChanged)
            cell_duration_slider.lblTitle.text = MiscStrings.duration.text
            cell_duration_slider.rangeSeekBar.minValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.minDuration) ?? 0)
            cell_duration_slider.rangeSeekBar.maxValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.maxDuration) ?? 0)
            cell_duration_slider.rangeSeekBar.selectedMinValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.minDuration) ?? 0)
            cell_duration_slider.rangeSeekBar.selectedMaxValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.maxDuration) ?? 0)
            cell_duration_slider.selectionStyle = .none
            self.durationSeekBar = cell_duration_slider.rangeSeekBar
            return cell_duration_slider
        case 1:
            cell_subscriber_slider.rangeSeekBar.addTarget(self, action: #selector(didUpdateValues(sender:)), for: .editingChanged)
            cell_subscriber_slider.lblTitle.text = MiscStrings.subscribers.text
            cell_subscriber_slider.rangeSeekBar.minValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.minSubscriber) ?? 0)
            cell_subscriber_slider.rangeSeekBar.maxValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.maxSubscriber) ?? 0)
            cell_subscriber_slider.rangeSeekBar.selectedMinValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.minSubscriber) ?? 0)
            cell_subscriber_slider.rangeSeekBar.selectedMaxValue = CGFloat((self.filterDataService.minMaxSubscibersAndDuration?.maxSubscriber) ?? 0)
            cell_subscriber_slider.selectionStyle = .none
            self.subscribersSeekBar = cell_subscriber_slider.rangeSeekBar
            return cell_subscriber_slider
        case 2:
            cell_newsCategories.lableTitle.text = self.filterDataService.entityModel[indexPath.row].sourceName ?? "-"
            cell_newsCategories.btnCheckBox.isSelected = self.filterDataService.entityModel[indexPath.row].isSelected
            cell_newsCategories.btnWhole.addTarget(self, action: #selector(self.selectCategories(sender:)), for: .touchUpInside)
            cell_newsCategories.btnCheckBox.addTarget(self, action: #selector(self.selectCategories(sender:)), for: .touchUpInside)
            cell_newsCategories.btnWhole.tag = indexPath.row
            cell_newsCategories.btnCheckBox.tag = indexPath.row
            cell_newsCategories.selectionStyle = .none
            return cell_newsCategories
        case 3:
            cell_country.lableTitle.text = self.filterDataService.locations[indexPath.row].countryName ?? "-"
            cell_country.btnCheckBox.isSelected = self.filterDataService.locations[indexPath.row].isSelected
            cell_country.btnWhole.addTarget(self, action: #selector(self.selectCountry(sender:)), for: .touchUpInside)
            cell_country.btnCheckBox.addTarget(self, action: #selector(self.selectCountry(sender:)), for: .touchUpInside)
            cell_country.btnWhole.tag = indexPath.row
            cell_country.btnCheckBox.tag = indexPath.row
            cell_country.selectionStyle = .none
            return cell_country
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section{
        case 0,1:
            return 120
        default:
            return 44
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let countryHeader: CountryHeader = .fromNib()
        countryHeader.btnToggle.addTarget(self, action: #selector(onBtnToggle(sender:)), for: .touchUpInside)
        countryHeader.btnToggle.tag = section
        switch section{
        case 0,1:
            return nil
        case 2:
            return nil
//            countryHeader.lblInternational.isHidden = true
//            countryHeader.isInternational.isHidden = true
//            countryHeader.lblHeaderTitle.text = "Categories"
//            return countryHeader
        case 3:
            countryHeader.lblInternational.isHidden = false
            countryHeader.isInternational.isHidden = false
            self.isInternational = countryHeader.isInternational
            countryHeader.isInternational.isOn = self.is_international
            countryHeader.isInternational.addTarget(self, action: #selector(onSwitchInternation(sender:)), for: .touchUpInside)
            countryHeader.lblHeaderTitle.text = MiscStrings.countries.text
            return countryHeader
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0,1,2:
            return 0
        default:
            return 50
        }
    }
    @objc func selectCategories(sender: UIButton){
        self.isClear = false
        if self.filterDataService.entityModel[sender.tag].isSelected{
            self.filterDataService.entityModel[sender.tag].isSelected = false
        }
        else{
            self.filterDataService.entityModel[sender.tag].isSelected = true
        }
        self.tableView.reloadData()
    }
    @objc func selectCountry(sender: UIButton){
        self.isClear = false
        if self.filterDataService.locations[sender.tag].isSelected{
            self.filterDataService.locations[sender.tag].isSelected = false
        }
        else{
            self.filterDataService.locations[sender.tag].isSelected = true
        }
        self.tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 3)], with: .automatic)
    }
    @objc func onBtnToggle(sender:UIButton){
        switch sender.tag {
        case 2:
            sender.isSelected = !sender.isSelected
            self.flagCategories = !self.flagCategories
            self.tableView.reloadSections([sender.tag], with: .automatic)
        case 3:
            sender.isSelected = !sender.isSelected
            self.flagCountries = !self.flagCountries
            self.tableView.reloadSections([sender.tag], with: .automatic)
        default:
            break
        }
    }
    @objc func didUpdateValues(sender:RangeSeekSlider){
        self.isClear = false
    }
    @objc func onSwitchInternation(sender:UISwitch){
        self.is_international = sender.isOn
        self.isClear = false
    }
    func getAllSelectedCountryIDs() -> String {
        var returnStr = ""
        for filterItem in self.filterDataService.locations {
            if(filterItem.isSelected == true) {
                if(returnStr.isEmpty) {
                    returnStr = "\(filterItem.id)"
                }
                else {
                    returnStr += ",\(filterItem.id)"
                }
            }
        }
        return returnStr
    }
    func getAllSelectedCategoryIDs() -> String {
        var returnStr = ""
        for filterItem in self.filterDataService.entityModel {
            if(filterItem.isSelected == true) {
                if(returnStr.isEmpty) {
                    returnStr = "\(filterItem.id)"
                }
                else {
                    returnStr += ",\(filterItem.id)"
                }
            }
        }
        return returnStr
    }
}

// MARK: - LeftSideMenuController
extension NewsFilter: LGSideMenuDelegate {
    func willShowRightView(_ rightView: UIView, sideMenuController: LGSideMenuController) {
        print("I am willShowLeftView")
        self.isClear = false
        self.is_international = false
        self.flagCategories = true
        self.flagCountries = true
        self.tableView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
extension NewsFilter{
    private func getNewsFilterData(){
        let params : [String:Any] = ["Type":3]
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getFilterData(params: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let response = responseObject as NSDictionary
            let arrLocations = response.object(forKey: "Locations") as! Array<AnyObject>
            self.filterDataService.locations.removeAll()
            for res in arrLocations{
                print(res)
                let obj = Locations(JSON: res as! [String : Any])
                obj?.isSelected = false
                self.filterDataService.locations.append(obj!)
            }
            let arrCategories = response.object(forKey: "EntityModel") as! Array<AnyObject>
            self.filterDataService.entityModel.removeAll()
            for res in arrCategories{
                print(res)
                let obj = NewsEntityModel(JSON: res as! [String : Any])
                obj?.isSelected = false
                self.filterDataService.entityModel.append(obj!)
            }
            let resMinMaxSubscibersAndDuration = response.object(forKey: "MinMaxSubscibersAndDuration")
            let minMaxSubscibersAndDuration = Mapper<MinMaxSubscibersAndDuration>().map(JSON: resMinMaxSubscibersAndDuration as! [String : Any]) ?? MinMaxSubscibersAndDuration()
            self.filterDataService.minMaxSubscibersAndDuration = minMaxSubscibersAndDuration
            print(self.filterDataService)
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}


