//
//  NewsList.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsList: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var categoryId = 0    
    var arrNews = Array<NewsListModel>()
    var flag = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableview.register(UINib(nibName: "PodcastTableCell", bundle: nil), forCellReuseIdentifier: "PodcastTableCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if flag{
            self.getNewsListWith(CategoryId: self.categoryId)
        }
        else{
            self.lblTitle.text = self.title
            self.getSubscribedNews()
        }
    }
    private func navigateToNewsDetails(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsDetails") as? NewsDetails {
            controller.newsObject = self.arrNews[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - APIs
extension NewsList {
    public func getSubscribedNews() {
        Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.GetAllNewsSubscriptions(success: { (responseArray) in
            Utility.main.hideLoader()
            self.arrNews.removeAll()
            for item in responseArray {
                let model = NewsListModel(value: item)
                model.IsSubscribed = true
                self.arrNews.append(model)
            }
            self.tableview.reloadData()
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
    private func getNewsListWith(CategoryId categoryId: Int) {
        Utility.main.showLoader()
        let params: [String: Any] = ["NewsCategoryId": categoryId]
        APIManager.sharedInstance.newsApiManager.GetAllNewsByCategoryId(param: params, success: { (responseArray) in
            Utility.main.hideLoader()
            self.arrNews.removeAll()
            for item in responseArray {
                let model = NewsListModel(value: item as! [String: Any])
                self.arrNews.append(model)
            }
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print("Error: \(error.localizedDescription)")
        }
    }
    
    @objc func actionSubscribtionDown(_ sender: UIButton) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }

        let index = sender.tag
        if sender.isSelected {
            self.unSubscribeThisNews(with: self.arrNews[index].NewsID, index: index)
        } else {
            self.subscribeThisNews(with: self.arrNews[index].NewsID, index: index)
        }
    }
}
// MARK: - APIs
extension NewsList {
    func unSubscribeThisNews(with newsId: Int, index: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId" : newsId]
        APIManager.sharedInstance.newsApiManager.UnSubscribeThisNewsChannel(param: param, success: { (isSuccess) in
            Utility.main.hideLoader()
            if self.flag{
                Utility.main.postSubscribedNewsChannelNotification()
                self.arrNews[index].IsSubscribed = false
                self.tableview.reloadData()
            }
            else{
                self.getSubscribedNews()
            }
        }, failure: { (failure) in
            Utility.main.hideLoader()
            print(failure.localizedDescription)
        })
    }
    
    func subscribeThisNews(with newsId: Int, index: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId" : newsId]
        APIManager.sharedInstance.newsApiManager.SubscribeThisNewsChannel(param: param, success: { (isSuccess) in
            Utility.main.hideLoader()
            Utility.main.postSubscribedNewsChannelNotification()
            self.arrNews[index].IsSubscribed = true
            self.tableview.reloadData()
        }, failure: { (failure) in
            Utility.main.hideLoader()
            print(failure.localizedDescription)
        })
    }
}
// MARK: - UITableViewDataSource & Delegate
extension NewsList: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrNews.count > 0 {
            self.emptyMessage(message: "")
            return self.arrNews.count
        }else {
            self.emptyMessage(message: MiscStrings.noDataFound.text)
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastTableCell") as? PodcastTableCell else { return UITableViewCell() }
        let objNews = self.arrNews[indexPath.row]
        cell.BindData(with: objNews)
        
        cell.BtnSubscrptionBottom.tag = indexPath.row
        cell.BtnSubscrptionBottom.isSelected = objNews.IsSubscribed
        cell.BtnSubscrptionBottom.addTarget(self, action: #selector(self.actionSubscribtionDown(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        self.navigateToNewsDetails(indexPath)
    }
}
extension NewsList {
    func emptyMessage(message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Roboto-Medium", size: 15)
        messageLabel.sizeToFit()
        
        self.tableview.backgroundView = messageLabel
        self.tableview.separatorStyle = .none
    }
}


