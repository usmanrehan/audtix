//
//  NewsDetails.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsDetails: UIViewController {

    @IBOutlet weak var tableview: UITableView!

    let arrDaysTitle = [MiscStrings.today.text, MiscStrings.yesterday.text, MiscStrings.older.text]
    var newsObject = NewsListModel()
    
    var arrLaterEpisodes = Array<NewsEpisodeModel>()
    var arrTodayEpisodes = Array<NewsEpisodeModel>()
    var arrYesterdayEpisodes = Array<NewsEpisodeModel>()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getSortedNewsEpisodes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func navigateToNewsEpisodeListWith(_ indexPath: IndexPath) {
        var sectionTitle = ""
        self.newsObject.episodes.removeAll()
        switch indexPath.row {
        case 0:
            sectionTitle = MiscStrings.todayShows.text
            self.newsObject.episodes.append(objectsIn: self.arrTodayEpisodes)
            break
        case 1:
            sectionTitle = MiscStrings.yesterdayShows.text
            self.newsObject.episodes.append(objectsIn: self.arrYesterdayEpisodes)
            break
        case 2:
            sectionTitle = MiscStrings.laterShows.text
            self.newsObject.episodes.append(objectsIn: self.arrLaterEpisodes)
            break
        default:
            break
        }
        
        guard self.newsObject.episodes.count > 0 else {
            Utility.main.showAlert(message: MiscStrings.noEpisodeAvailable.text, title: MiscStrings.message.text, controller: self)
            return
        }
        
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsEpisodeList") as? NewsEpisodeList {
            controller.sectionTitle = sectionTitle
            controller.newsObject = self.newsObject
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK: - APIs
extension NewsDetails {
    private func getSortedNewsEpisodes() {
        Utility.main.showLoader()
        let params: [String: Any] = ["newsId": self.newsObject.NewsID]
        
        APIManager.sharedInstance.newsApiManager.GetSortedNewsEpisodes(param: params, success: { (responseObject) in
            Utility.main.hideLoader()
            if let todayEpisodes = responseObject["NewsEpisodesToday"] as? Array<AnyObject> {
                self.arrTodayEpisodes.removeAll()
                for item in todayEpisodes {
                    let model = NewsEpisodeModel(value: item as! [String: AnyObject])
                    print(model.EpisodeWebUrl)
                    self.arrTodayEpisodes.append(model)
                }
            }
            if let yesterdayEpisodes = responseObject["NewsEpisodesYesterday"] as? Array<AnyObject> {
                self.arrYesterdayEpisodes.removeAll()
                for item in yesterdayEpisodes {
                    let model = NewsEpisodeModel(value: item as! [String: AnyObject])
                    self.arrYesterdayEpisodes.append(model)
                }
            }
            if let laterEpisodes = responseObject["NewsEpisodesLater"] as? Array<AnyObject> {
                self.arrLaterEpisodes.removeAll()
                for item in laterEpisodes {
                    let model = NewsEpisodeModel(value: item as! [String: AnyObject])
                    self.arrLaterEpisodes.append(model)
                }
            }
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}

// MARK: - UITableView DataSource & Delegate
extension NewsDetails: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.arrDaysTitle.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailCategoryCell") as? NewsDetailCategoryCell else { return UITableViewCell() }
            
            cell.labelTitle.text = self.newsObject.Name ?? ""
            cell.labelAbout.text = self.newsObject.Description ?? ""
            
            if let url = URL(string: (self.newsObject.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
                cell.imageViewNewsCategory?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailsListCellIdentifier", for: indexPath) as? NewsDetailsListViewCell else { return UITableViewCell() }
            cell.titleLabel.text = self.arrDaysTitle[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 50.0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 1 { //News episodes list
            self.navigateToNewsEpisodeListWith(indexPath)
        }
    }
}
