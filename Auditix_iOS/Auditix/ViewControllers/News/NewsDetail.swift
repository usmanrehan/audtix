//
//  NewsDetail.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol NewsDetailsDelegate {
    func didSubscribedThisChannel(with Id: Int)
    func didUnSubscribeThisChannel(with Id: Int)
}

enum IsComingFrom {
    case search
    case news
    case profile
}

struct NewsDownloading {
    var isDownloading = false
    var isDownloaded = false
    var episode: PodcastTrack!
    var progress: Float = 0.0
}

class NewsDetail: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnFavorite: UIButton!
    
//    @IBOutlet weak var btnHeart: UIButton!
    var newsDetailShowsArray = [NewsModel]()
    
    public var currentNewsCategory: NewsCategory?
    
    public var currentNewsSubscribedCategory: NewsSubscribedModel?
    
    public var fromWhereIamComing: IsComingFrom?
    
    var showingWinless: Bool = true
    
    public var delegate: NewsDetailsDelegate? = nil
    
    public var heroID = ""
    
    var episodeDownloadProgressArray = [NewsDownloading]()
    
    let arrDaysTitle = [MiscStrings.today.text, MiscStrings.yesterday.text, MiscStrings.older.text]
    var newsId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Setup Favorite button state
        if self.fromWhereIamComing == .news {
            self.btnFavorite.isSelected = (self.currentNewsCategory?.IsFavoriteNews)!
        } else if self.fromWhereIamComing == .search {
            self.btnFavorite.isSelected = (self.currentNewsCategory?.IsFavoriteNews)!
        } else if self.fromWhereIamComing == .profile {
            self.btnFavorite.isSelected = (self.currentNewsSubscribedCategory?.IsFavorite)!
        }

//        self.tableview.windless
//            .apply {
//                $0.beginTime = 0.5
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//                $0.animationLayerColor = Constants.THEME_ORANGE_COLOR
//            }
//            .start()

//        if self.fromWhereIamComing == .search {
//            self.getNewsDetail(with: (self.currentNewsCategory?.Id)!)
//        } else if self.fromWhereIamComing == .news {
//            if (self.currentNewsCategory?.IsFavoriteNews)! {
////                self.btnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: .normal)
//            } else {
////                self.btnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: .normal)
//            }
//            self.getNewsDetail(with: (self.currentNewsCategory?.Id)!)
//        } else if self.fromWhereIamComing == .profile  {
//             self.getNewsDetail(with: (self.currentNewsSubscribedCategory?.NewsCategoryId)!)
//        }
    }
    
    @objc func actionSubscribtionDown(_ sender: UIButton) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        
        if self.fromWhereIamComing == .profile {
            self.unSubscribeThisNews(with: sender)
        } else if self.fromWhereIamComing == .news {
            if !(self.currentNewsCategory?.IsNewsSubscribed)! {
                self.subscribeThisNews(with: sender)
            } else {
                self.unSubscribeThisNews(with: sender)
            }
        } else if self.fromWhereIamComing == .search {
            if self.newsDetailShowsArray.count > 0 {
                if self.newsDetailShowsArray[0].IsSubscribed {
                     self.unSubscribeThisNews(with: sender)
                } else {
                    self.subscribeThisNews(with: sender)
                }
            }
        }
    }
    
    @objc func actionDownloadNews(_ sender: DownloadButton) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        
        let index = sender.index
        
//        if Utility.main.isThisPodcastFileExist(with: self.currentTrackID, EpisodeID: index, fileName: self.currentPodCast.TrackList[index].Name!) {
//            print("already in memory")
//            self.openPlayerController(with: index)
//        }
//        else {
//            self.processStartDownload(index: index, indexPath: sender.indexPath!)
//        }

        //        let index = sender.tag
        //        if (Utility.main.isThisFileExist(with: (self.currentBook?.BookID)!, bookChap: (self.booksEpisodeModel?.Chapters?.Chapter[index])!)) {
        //            print("already in memory")
        //            self.openPlayer(with: index)
        //        }
        //        else {
        //            self.processStartDownload(index: sender.tag)
        //        }
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFavorite(_ sender: UIButton) {
        
        let heartBtn = sender as UIButton
        
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        let alreadyFavoriteStatus: Bool?
        if self.fromWhereIamComing == .profile {
            alreadyFavoriteStatus = self.currentNewsSubscribedCategory?.IsFavorite
        } else {
            alreadyFavoriteStatus = self.currentNewsCategory?.IsFavoriteNews
        }
        
        if alreadyFavoriteStatus! {
            heartBtn.isSelected = false
            self.unfavoriteThisChannel(with: heartBtn)
        } else {
            heartBtn.isSelected = true
            self.makeThisChannelAFavorite(with: heartBtn)
        }
    }
    
    func openNewsEpisodeDetail(index: Int, indexPath: IndexPath) {
        if let newsEpisodeDetail = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsEpisodeDetail") as? NewsEpisodeDetail {
            newsEpisodeDetail.currentNewsModel = self.newsDetailShowsArray[index]
            newsEpisodeDetail.heroID = "\(indexPath.item) + \(indexPath.section)"
            newsEpisodeDetail.view.heroModifiers = [.arc]
            self.navigationController?.isHeroEnabled = true
            self.navigationController?.pushViewController(newsEpisodeDetail, animated: true)
        }
    }
    
}
// MARK: - API CALLING
extension NewsDetail
{
    
    func getNewsDetail(with categoryID: Int) {
        let param: [String : Any] = [
            "Id" : categoryID
        ]
        
        //            Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.GetAllNewsByCategoryId(param: param, success: { (successArray) in
            self.newsDetailShowsArray.removeAll()
            
            if successArray.count == 0 {
                Utility.main.showLabelIfNoData(withtext: AlertMessages.noNewNewsAvailable.message, controller: self, tableview: self.tableview)
                //                    return
            } else {
                self.tableview.backgroundView = UIView()
            }
            
            for newsShowsItem in successArray {
                self.newsDetailShowsArray.append(NewsModel(json: newsShowsItem as! [String : Any]))
            }
            
            if self.fromWhereIamComing == .search {
                if self.newsDetailShowsArray.count > 0 {
                    if self.newsDetailShowsArray[0].IsFavourite {
//                        self.btnHeart.isSelected = true
                    }
                    else {
//                        self.btnHeart.isSelected = false
                    }
                    self.currentNewsCategory?.IsNewsSubscribed = self.newsDetailShowsArray[0].IsSubscribed
                    self.currentNewsCategory?.IsFavoriteNews  = self.newsDetailShowsArray[0].IsFavourite
                }
            }
            
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
    }
    
    func subscribeThisNews(with buttonForText: UIButton) {
        if let currentCategoryID = self.currentNewsCategory?.Id {
            let param: [String : Any] = [
                "NewsCategoryId" : currentCategoryID
            ]
            Utility.main.showLoader()
            APIManager.sharedInstance.newsApiManager.SubscribeThisNewsChannel(param: param, success: { (success) in
                Utility.main.hideLoader()
                Utility.main.postSubscribedNewsChannelNotification()
                buttonForText.isSelected = true
                self.currentNewsCategory?.IsNewsSubscribed = true
                self.delegate?.didSubscribedThisChannel(with: currentCategoryID)
            }, failure: { (failure) in
                Utility.main.hideLoader()
            })
        }
    }
    
    func unSubscribeThisNews(with buttonForText: UIButton) {
        if let currentCategoryID = self.currentNewsCategory?.Id {
            let param: [String : Any] = [
                "NewsCategoryId" : currentCategoryID
            ]
            Utility.main.showLoader()
            APIManager.sharedInstance.newsApiManager.UnSubscribeThisNewsChannel(param: param, success: { (success) in
                Utility.main.hideLoader()
                Utility.main.postSubscribedNewsChannelNotification()
                self.currentNewsCategory?.IsNewsSubscribed = false
                self.delegate?.didUnSubscribeThisChannel(with: currentCategoryID)
                buttonForText.isSelected = false
            }, failure: { (failure) in
                Utility.main.hideLoader()
            })
        }
        else if let currentCategoryID = self.currentNewsSubscribedCategory?.NewsId {
            let param: [String : Any] = [
                "NewsCategoryId" : currentCategoryID
            ]
            Utility.main.showLoader()
            APIManager.sharedInstance.newsApiManager.UnSubscribeThisNewsChannel(param: param, success: { (success) in
                Utility.main.hideLoader()
                Utility.main.postSubscribedNewsChannelNotification()
                self.currentNewsCategory?.IsNewsSubscribed = false
                self.delegate?.didUnSubscribeThisChannel(with: currentCategoryID)
                buttonForText.isSelected = false
            }, failure: { (failure) in
                
                Utility.main.hideLoader()
            })
        }
    }
    
    func makeThisChannelAFavorite(with button: UIButton) {
        if self.fromWhereIamComing == .profile {
            if let currentCategoryID = self.currentNewsSubscribedCategory?.NewsId {
                let param: [String : Any] = [
                    "NewsCategoryId" : currentCategoryID
                ]
                Utility.main.showLoader()
                APIManager.sharedInstance.newsApiManager.makeThisNewsAFavorite(param: param, success: { (successBool) in
                    button.isSelected = true//self.btnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: .normal)
                    self.currentNewsCategory?.IsFavoriteNews = true
                    Utility.main.hideLoader()
                }, failure: { (failure) in
                    button.isSelected = false//self.btnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: .normal)
                    Utility.main.hideLoader()
                })
            }
        } else {
            if let currentCategoryID = self.currentNewsCategory?.Id {
                let param: [String : Any] = [
                    "NewsCategoryId" : currentCategoryID
                ]
                Utility.main.showLoader()
                APIManager.sharedInstance.newsApiManager.makeThisNewsAFavorite(param: param, success: { (successBool) in
                    button.isSelected = true//self.btnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: .normal)
                    self.currentNewsCategory?.IsFavoriteNews = true
                    Utility.main.hideLoader()
                }, failure: { (failure) in
                    button.isSelected = false//self.btnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: .normal)
                    Utility.main.hideLoader()
                })
            }
        }
    }
    
    func unfavoriteThisChannel(with button: UIButton) {
        if self.fromWhereIamComing == .profile {
            if let currentCategoryID = self.currentNewsSubscribedCategory?.NewsId {
                let param: [String : Any] = [
                    "NewsCategoryId" : currentCategoryID
                ]
                Utility.main.showLoader()
                APIManager.sharedInstance.newsApiManager.unFavoriteNews(param: param, success: { (successBool) in
                    button.isSelected = false//self.btnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: .normal)
                    self.currentNewsCategory?.IsFavoriteNews = false
                    Utility.main.hideLoader()
                }, failure: { (failure) in
                    button.isSelected = true//self.btnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: .normal)
                    Utility.main.hideLoader()
                })
            }
        } else {
            if let currentCategoryID = self.currentNewsCategory?.Id {
                let param: [String : Any] = [
                    "NewsCategoryId" : currentCategoryID
                ]
                Utility.main.showLoader()
                APIManager.sharedInstance.newsApiManager.unFavoriteNews(param: param, success: { (successBool) in
                    button.isSelected = false//self.btnHeart.setImage(#imageLiteral(resourceName: "herat2"), for: .normal)
                    self.currentNewsCategory?.IsFavoriteNews = false
                    Utility.main.hideLoader()
                }, failure: { (failure) in
                    button.isSelected = true//self.btnHeart.setImage(#imageLiteral(resourceName: "herat1"), for: .normal)
                    Utility.main.hideLoader()
                })
            }
        }

    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension NewsDetail: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.arrDaysTitle.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailCategoryCell") as? NewsDetailCategoryCell else { return UITableViewCell() }
            
            //cell.heroModifiers = [.arc]
            //cell.heroID = self.heroID
            
            if self.fromWhereIamComing == .profile {
                if let imageUrl = self.currentNewsSubscribedCategory?.NewsImageUrl {
                   let imageURL = URL(string: (imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!))
                    cell.imageViewNewsCategory.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                }
                
                cell.labelTitle.text = self.currentNewsSubscribedCategory?.NewsName
            } else {
                if let imageURLStr = self.currentNewsCategory?.SourceImageUrl {
                    if let imageURL = URL(string: (imageURLStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)) {
                        cell.imageViewNewsCategory.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                    } else {
                        cell.imageViewNewsCategory.image = #imageLiteral(resourceName: "placeholder_image")
                    }
                } else {
                    cell.imageViewNewsCategory.image = #imageLiteral(resourceName: "placeholder_image")
                }
                cell.labelTitle.text = self.currentNewsCategory?.SourceName
            }
            
            
            cell.BtnSubscrptionBottom.isHidden = false
            if self.fromWhereIamComing == .profile {
                cell.BtnSubscrptionBottom.isSelected = true
            } else {
                if (self.currentNewsCategory?.IsNewsSubscribed)! {
                    cell.BtnSubscrptionBottom.isSelected = true
                } else {
                    cell.BtnSubscrptionBottom.isSelected = false
                }
            }
            
//            if fromWhereIamComing == .profile {
//                 cell.btnFavorite.isSelected = (self.currentNewsSubscribedCategory?.IsFavorite)!
//            } else {
//                 cell.btnFavorite.isSelected = (self.currentNewsCategory?.IsFavoriteNews)!
//            }
           
            
            //cell.btnFavorite.tag = indexPath.row
            //cell.btnFavorite.addTarget(self, action: #selector(self.actionFavorite(_:)), for: .touchUpInside)
            
            cell.BtnSubscrptionBottom.tag = indexPath.row
            cell.BtnSubscrptionBottom.addTarget(self, action: #selector(self.actionSubscribtionDown(_:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailsListCellIdentifier", for: indexPath) as? NewsDetailsListViewCell else { return UITableViewCell() }
            cell.titleLabel.text = self.arrDaysTitle[indexPath.row]
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailShowsCell") as? NewsDetailShowsCell else { return UITableViewCell() }
            let newsModel = self.newsDetailShowsArray[indexPath.row]
            cell.heroModifiers = [.arc]
            cell.heroID = "\(indexPath.item) + \(indexPath.section)"
            if !self.showingWinless {
//                cell.constraintSeparatorViewBottom.constant = 1
                cell.BindData(with: newsModel)
                
                cell.traceID = Int(newsModel._id!)
                
                cell.BtnDownload.index = indexPath.row
                cell.BtnDownload.addTarget(self, action: #selector(self.actionDownloadNews(_:)), for: .touchUpInside)
                cell.BtnDownload.indexPath = indexPath
                cell.BtnDownload.progressView = cell.progressView

                cell.progressView.tintColor = Constants.THEME_ORANGE_COLOR
                
//                if self.episodeDownloadProgressArray[indexPath.row].isDownloaded {
//                    cell.imageDownloadPlay.image = #imageLiteral(resourceName: "play-circle")
//
//                } else {
//                    cell.imageDownloadPlay.image = #imageLiteral(resourceName: "dowload_arrow-circle")
//                }
                
//                if self.episodeDownloadProgressArray[indexPath.row].isDownloading {
//                    cell.progressView.isHidden = false
//                    cell.imageDownloadPlay.isHidden = true
//                    cell.BtnDownload.isHidden = true
//                    if cell.progressView.progress == 0.0 {
//                        cell.progressView.setProgress(0.0, animated: true)
//                    }
//                    else if cell.progressView.progress == 1 {
//                        cell.progressView.setProgress(cell.progressView.progress, animated: true)
//                        cell.imageDownloadPlay.image = #imageLiteral(resourceName: "play-circle")
//                        self.episodeDownloadProgressArray[indexPath.row].isDownloaded = true
//                        self.episodeDownloadProgressArray[indexPath.row].isDownloading = false
//                    }
//                }
//                else {
//                    cell.progressView.isHidden = true
//                    cell.imageDownloadPlay.isHidden = false
//                    cell.BtnDownload.isHidden = false
//                }
            }
            else {
//                cell.constraintSeparatorViewBottom.constant = -10
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            return 60.0
        default:
            return UITableViewAutomaticDimension
        }
        
//        if indexPath.section == 0 {
//            return UITableViewAutomaticDimension
//        } else {
//            return UITableViewAutomaticDimension//self.view.frame.size.height * 0.15
//        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let booksHeader: BooksHeader = .fromNib()
//        booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
//        booksHeader.labelTitle.text = "SHOWS & EPISODES".uppercased()
//        booksHeader.buttonSeeAll.isHidden = true
//        return booksHeader
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0

//        if self.showingWinless {
//            return 0
//        } else {
//            if section == 0 {
//                return 0
//            } else {
//                return 30
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        if indexPath.section == 1 {
//            if self.newsDetailShowsArray.count > 0 {
//                self.openNewsEpisodeDetail(index: indexPath.row, indexPath: indexPath)
//            }
//        }
    }
}
