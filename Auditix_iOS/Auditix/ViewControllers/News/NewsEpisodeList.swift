//
//  NewsEpisodeList.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift
class NewsEpisodeList: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var newsObject: NewsListModel!
    var sectionTitle = ""
    var downloader: Downloader? = nil
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = self.newsObject.Name ?? ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableview.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    private func playEpisode(atIndex index: Int) {
    //        let bottomPlayerView = BottomView.sharedInstance
    //
    //        bottomPlayerView.playingType = .news
    //        bottomPlayerView.newsObject = self.newsObject
    //        bottomPlayerView.trackIndex = index
    //
    //        if bottomPlayerView.currentBarState == .close {
    //            bottomPlayerView.showBottomView()
    //        }
    //
    //        bottomPlayerView.initNews()
    //    }
    
    private func navigateToEpisodeDetails(_ indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewsEpisodeDetails") as? NewsEpisodeDetails {
            controller.objNews = self.newsObject
            controller.episodeIndex = indexPath.row
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc private func actionDownloadAll(sender: UIButton) {
        self.checkAlreadyDownloadedNews(arrayNews: self.newsObject.episodes)
    }
    
    private func checkAlreadyDownloadedNews(arrayNews: List<NewsEpisodeModel>) {
        for item in arrayNews {
            let objNews = item
            if !Utility.main.isThisNewsFileExist(with: objNews.NewsID, EpisodeID: objNews.NewsEpisodeID) {
                if self.isFirstTime{
                    Utility.main.showToast(message: MiscStrings.downloading.text)
                    self.isFirstTime = false
                }
                self.processStartDownloadNews(objNews: objNews)
            }
        }
    }
    
    private func processStartDownloadNews(objNews: NewsEpisodeModel) {
        if let downloadPath = objNews.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let fileUrl = Utility.main.returnNewsFileUrl(with: objNews.NewsID, EpisodeID: objNews.NewsEpisodeID)
            self.downloader = Downloader.init(downloadPath, downloadPath, objNews.NewsID, fileUrl.absoluteString)
            self.downloader?.startDownload()
        }
    }
}

// MARK: - IBAction Methods
extension NewsEpisodeList {
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    @objc func actionPlayButton(_ sender: UIButton) {
    //        let index = sender.tag
    //        self.playEpisode(atIndex: index)
    //    }
}

// MARK: - UITableView DataSource & Delegate
extension NewsEpisodeList: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.newsObject.episodes.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailCategoryCell") as? NewsDetailCategoryCell else { return UITableViewCell() }
            cell.labelTitle.text = self.newsObject.Name
            cell.labelAbout.text = self.newsObject.Description
            if let url = URL(string: (self.newsObject.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
                cell.imageViewNewsCategory?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            cell.buttonDownloadAll.addTarget(self, action: #selector(self.actionDownloadAll(sender:)), for: .touchUpInside)
            
                //check if all chapters downloaded disable complete book download button
                if self.checkIfAllNewsEpisodesDownloaded(){
                    cell.buttonDownloadAll.isEnabled = false
                    cell.buttonDownloadAll.backgroundColor = UIColor.gray
                }else {
                    cell.buttonDownloadAll.isEnabled = true
                    cell.buttonDownloadAll.backgroundColor = Global.APP_Color
                }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDetailsListCellIdentifier") as? NewsDetailsListViewCell else { return UITableViewCell() }
            let episode = self.newsObject.episodes[indexPath.row]
            cell.titleLabel.text = String(format: "Episode No. %02d", episode.EpisodeNo)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NewsEpisodeHeaderView.instanceFromNib()
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        headerView.backgroundColor = UIColor.clear
        headerView.btnDownloadAll.isHidden = true
        headerView.lblTitle.text = sectionTitle.uppercased()
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 50.0
        default:
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 1:
            self.navigateToEpisodeDetails(indexPath)
            break
        default:
            break
        }
    }
}
//MARK:- Helper methods
extension NewsEpisodeList{
    func checkIfAllNewsEpisodesDownloaded()->Bool{
        if (self.newsObject.episodes.count) > 0{
            var chapterIDs = [String]()
            let episodes = self.newsObject.episodes
            for i in (episodes){
                chapterIDs.append("\(i.NewsEpisodeID)")
            }
            let downloadedItemsCSV = Utility.main.getIdfromFolders(directoryName: "news")
            let arrDownloadedItems = downloadedItemsCSV.components(separatedBy: ",")
            if chapterIDs.count > 0 && arrDownloadedItems.count > 0{
                if arrDownloadedItems.contains(array: chapterIDs){
                    return true
                    //print("All downloaded")
                }
                else{
                    return false
                    //print("none or some downloaded")
                }
            }
        }
        return false
    }
}
