//
//  NewsEpisodeDetail.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NewsEpisodeDetail: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    public var currentNewsModel :NewsModel?
    
    public var heroID: String = ""
    
    var bottomView = BottomView.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.register(UINib(nibName: "BookDetailDescriptionCell", bundle: nil), forCellReuseIdentifier: "BookDetailDescriptionCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func actionPlayBtn(_ sender: UIButton) {
        self.openPlayerController()
    }
    
    func openPlayerController() {        
//        if Singleton.sharedInstance.currentAdvertiseArray.count > 0 {
//            if self.bottomView.currentNews != nil {
//                if self.bottomView.currentNews._id == self.currentNewsModel?._id {
//                    return
//                }
//            }
//            if self.currentNewsModel != nil {
//                self.bottomView.actionClose(UIButton())
//                AdvertiseView.sharedInstance.delegate = self
//                AdvertiseView.sharedInstance.showAdvertiseView()
//            }
//            else {
//                Utility.main.showToast(message: AlertMessages.unableToOpenNews.rawValue, controller: self)
//            }
//        } else { // if no advertisement
//            self.bottomView.playingType = .news
//            //self.bottomView.currentPodcastTrackID = 0
//            self.bottomView.currentNews = self.currentNewsModel
//            //self.bottomView.playingThisEpisode = 0
//            if self.bottomView.currentBarState == .close {
//                self.bottomView.showBottomView()
//            }
//            self.bottomView.initNews()
//        }
    }
}

extension NewsEpisodeDetail: AdvertiseDelegate {
    func advertisementIsStop() {
//         self.bottomView.playingType = .news
//         //self.bottomView.currentPodcastTrackID = 0
//         self.bottomView.currentNews = self.currentNewsModel
//         //self.bottomView.playingThisEpisode = 0
//         if self.bottomView.currentBarState == .close {
//         self.bottomView.showBottomView()
//         }
//         self.bottomView.initNews()
    }
}
extension NewsEpisodeDetail: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "NewsEpisodeInfoCell") as? NewsEpisodeInfoCell else { return UITableViewCell() }
            
            cell.heroID = self.heroID
            cell.heroModifiers = [.arc]
            
            cell.bindData(with: self.currentNewsModel!)
            cell.btnPlay.addTarget(self, action: #selector(self.actionPlayBtn(_:)), for: .touchUpInside)
            return cell
        case 1:
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookDetailDescriptionCell") as? BookDetailDescriptionCell else { return UITableViewCell() }
            
            cell.labelTitle.text = AlertMessages.aboutTheNews.message
            if self.currentNewsModel?.descriptionValue == nil {
                cell.lableDescription.text = MiscStrings.noDescriptionAvailable.text
            } else {
                cell.lableDescription.text = self.currentNewsModel?.descriptionValue
            }
            
            
            return cell
        default:
           return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch indexPath.section {
//        case 0:
//            return self.view.frame.size.height * 0.20
//        case 1:
//            return UITableViewAutomaticDimension
//        default:
//            return 0
//        }
        
        return UITableViewAutomaticDimension
    }

}
