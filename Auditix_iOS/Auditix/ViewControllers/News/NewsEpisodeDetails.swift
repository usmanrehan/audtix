//
//  NewsEpisodeDetails.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/30/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView

class NewsEpisodeDetails: BaseController {
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var navigationTitleLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var narratorLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var progressView: UAProgressView!
    
    //var podcast: PodcastDetailModel? = nil
    var downloader: Downloader? = nil
    
    var objNews: NewsListModel!
    var episodeIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        addNotificationObservers()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //<adv>
        self.getAdvertise()
    }
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskCompleted), name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskIsInProgress), name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
    }
    
    private func setupUI() {
        let name = self.objNews.Name ?? ""
        if !name.isEmpty{
            self.navigationTitleLable.text = name
            self.titleLable.text = name
        }
        if self.episodeIndex >= 0, episodeIndex < self.objNews.episodes.count {
            let episode = self.objNews.episodes[episodeIndex]
            self.durationLabel.text = ParserHelper.getDuration(with: Int(episode.FileDuration?.stringValue ?? "0")!) 
//            self.titleLable.text = self.objNews.episodes[episodeIndex].NarratorName ?? "-"
            self.downloadButton.isEnabled = !Utility.main.isThisNewsFileExist(with: self.objNews.NewsID, EpisodeID: episode.NewsEpisodeID)
        }
        
        self.aboutLabel.text = self.objNews.Description
        self.narratorLabel.text = self.objNews.NarratorName
        
        if let imageURL = URL(string: (self.objNews.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
            //self.podcastImageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            self.newsImageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.newsImageView.image = #imageLiteral(resourceName: "placeholder_image")
        }
        
        //Setup Progress View
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.progressView.frame.size.width * 0.9, height: self.progressView.frame.size.height))
        label.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        label.textColor = Constants.THEME_ORANGE_COLOR
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        self.progressView.centralView = label
        self.progressView.tintColor = Constants.THEME_ORANGE_COLOR
        
        self.progressView.progressChangedBlock = {
            (progressView, progress) -> Void in
            print("progress \(progress)")
            if let label = progressView?.centralView as? UILabel {
                label.text = String(format: "%2.0f%%", progress * 100)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func processStartDownload() {
        if let downloadPath = self.objNews.episodes[episodeIndex].FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let fileUrl = Utility.main.returnNewsFileUrl(with: self.objNews.NewsID, EpisodeID: self.objNews.episodes[episodeIndex].NewsEpisodeID)
            
            self.downloader = Downloader.init(downloadPath, downloadPath, self.objNews.NewsID, fileUrl.absoluteString)
            //self.downloader?.download()
            
            //self.downloader = Downloader.init(downloadPath, downloadPath, self.podcast.TrackId, fileURL.absoluteString)
            
            let deletedItemsCSV = AppStateManager.sharedInstance.loggedInUser.deletedItems ?? ""
            var arrDeletedItems = deletedItemsCSV.components(separatedBy: ",")
            if !arrDeletedItems.contains("\(self.objNews.episodes[episodeIndex].NewsEpisodeID)"){
                self.downloader?.startDownload()
            }
            else{
                for (i,obj) in arrDeletedItems.enumerated(){
                    if obj == "\(self.objNews.episodes[episodeIndex].NewsEpisodeID)"{
                        arrDeletedItems.remove(at: i)
                        try! Global.APP_REALM?.write(){
                            AppStateManager.sharedInstance.loggedInUser.deletedItems = arrDeletedItems.joined(separator: ",")
                            Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                        }
                        break
                    }
                }
            }
        }
    }
    
    private func playAdvertisement() {
        if Singleton.sharedInstance.currentAdvertiseArray.count == 0 {
            self.playEpisode()
        } else {
            let advertiseView = AdvertiseView.sharedInstance
            advertiseView.delegate = self
            advertiseView.showAdvertiseView()
        }
    }
    
    private func playEpisode() {
        let bottomPlayerView = BottomView.sharedInstance
        
        bottomPlayerView.playingType = .news
        bottomPlayerView.newsObject = self.objNews
        bottomPlayerView.trackIndex = self.episodeIndex
        
        if bottomPlayerView.currentBarState == .close {
            bottomPlayerView.showBottomView()
        }
        
        bottomPlayerView.initNews()
        //3 = news
        let itemid = self.objNews.episodes[episodeIndex].NewsEpisodeID
        self.addToListen(type:"3",itemid:"\(itemid)")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK: - IBAction Methods
extension NewsEpisodeDetails {
    @IBAction func actionBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPlayButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.stopPlaying), object: nil)
        //<adv>
        self.playAdvertisement()
        //self.playEpisode()
    }
    
    @IBAction func actionDownloadButton(_ sender: UIButton) {
        if Utility.main.isThisNewsFileExist(with: self.objNews.NewsID, EpisodeID: self.objNews.episodes[episodeIndex].NewsEpisodeID) {
            Utility.main.showAlert(message: MiscStrings.fileIsalreadyDownloaded.text, title: MiscStrings.message.text)
            return
        }
        
        self.processStartDownload()
    }
    
    @IBAction func actionShareButton(_ sender: UIButton) {
        let episodeWebUrl = self.objNews.episodes[episodeIndex].EpisodeWebUrl ?? ""
        let filePath = self.objNews.episodes[episodeIndex].FilePath ?? ""
        if !episodeWebUrl.isEmpty{
            Utility.main.shareAcitivityWithURL(episodeWebUrl,
                                               title: self.objNews.Name,
                                               subtitle: self.objNews.episodes[episodeIndex].EpisodeTitle)
            return
        }
        if !filePath.isEmpty{
            Utility.main.shareAcitivityWithURL(filePath,
                                               title: self.objNews.Name,
                                               subtitle: self.objNews.episodes[episodeIndex].EpisodeTitle)
            return
        }
        Utility.main.showAlert(message: MiscStrings.sharingUrlNotFound.text, title: AlertTitles.error.message)
    }
}

//MARK: - Advertisement Delegate
extension NewsEpisodeDetails: AdvertiseDelegate {
    func advertisementIsStop() {
        self.playEpisode()
    }
}

//MARK: - Notification call backs
extension NewsEpisodeDetails {
    @objc func downloadTaskIsInProgress( data: NSNotification) {
        let aditionalText = data.userInfo
        self.progressView.isHidden = false
        self.downloadButton.isEnabled = false
        if let temp = aditionalText!["progress"] as? Double {
           self.progressView.setProgress(CGFloat(temp), animated: true)
        }
        
    }
    
    @objc func downloadTaskCompleted( data: NSNotification) {
        // Downlaod completed
        self.progressView.isHidden = true
        self.downloadButton.isEnabled = false
        
        //PodcastRealmHelper.sharedInstance.addThisPodcast(podcast: self.podcast, trackID: self.podcast.TrackId)
    }
    
    @objc func downloadCompleteSoUpdateArray( data: NSNotification) {
        print("DownlaodCompleteSoUpdateArray")
    }
}
//MARK:- APIs Calling
extension NewsEpisodeDetails{
    private func getAdvertise() {
        //MARK#
        let AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let Country = AppDelegate.shared.Country
        let params : [String:Any] = ["AccountID":AccountID,"Country":Country]
        APIManager.sharedInstance.miscellaneousAPIManager.getAdvertise(params: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let singleton = Singleton.sharedInstance
            singleton.currentAdvertiseArray.removeAll()
            singleton.currentAdvertiseArray.append(Advertise(value: responseObject as [String : Any]))
            print("advertisement \(singleton.currentAdvertiseArray)")
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
    func addToListen(type:String,itemid:String){
        let params:[String:Any] = ["Type":type,
                                   "ItemId":itemid]
        print(params)
        APIManager.sharedInstance.podcastApiManager.AddToListen(params: params, success: { (response) in
            print(response)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
