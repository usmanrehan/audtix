//
//  ViewController.swift
//  Auditix
//
//  Created by shardha on 12/14/17.
//  Copyright © 2017 Ingic. All rights reserved.
//

import UIKit
import Spring
class WalkThroughController: UIViewController {
    
    @IBOutlet weak var imageView: SpringImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var labelOne: SpringLabel!
    @IBOutlet weak var labelTwo: SpringLabel!
    @IBOutlet weak var labelThree: SpringLabel!
    @IBOutlet weak var viewBorder: SpringView!
    @IBOutlet weak var btnNext: UIButton!
    
    var walkThroughArray = [WalkThroughModel]()
    
    var swipeRightGesture: UISwipeGestureRecognizer?
    var swipeLeftGesture: UISwipeGestureRecognizer?
    
    var letsBegin: Bool = false
    
    var selectedForce: CGFloat = 1
    var selectedDuration: CGFloat = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.red
        

        
        self.swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.actionSwipeLeft(_:)))
        
        self.swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.actionSwipeRight(_:)))
        
        self.swipeRightGesture?.direction = .right
        self.swipeLeftGesture?.direction = .left
        
        self.view.addGestureRecognizer(self.swipeLeftGesture!)
        self.view.addGestureRecognizer(self.swipeRightGesture!)
        
        self.imageView.alpha = 0
        self.labelOne.alpha = 0
        self.labelTwo.alpha = 0
        self.labelThree.alpha = 0
        
        self.imageView.borderColor = UIColor(red: 0.945, green: 0.922, blue: 0.914, alpha: 0.5)
        
        
        if let temp = Singleton.sharedInstance.walkThroughDataArray as? [WalkThroughModel] {
            if temp.count > 0 {
                self.walkThroughArray = temp
            }
        }
        
        self.pageControl.numberOfPages = self.walkThroughArray.count
        self.pageControl.currentPage = 0
        if(self.walkThroughArray.count > 0) {
            if let urlStr = ParserHelper.createWalkThroughImageUrl(walkThrough: self.walkThroughArray[self.pageControl.currentPage]) {
                self.imageView.sd_setImage(with: urlStr, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            self.labelOne.text = self.walkThroughArray[self.pageControl.currentPage].Title
            self.labelTwo.text = self.walkThroughArray[self.pageControl.currentPage].Description
            //self.imageView.image = UIImage(named:"image\(self.pageControl.currentPage).jpg")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imageView.cornerRadius = self.imageView.frame.width / 2
        self.viewBorder.cornerRadius = self.viewBorder.frame.width / 2
        self.animateForStart()
    }
    
    @objc func actionSwipeLeft(_ sender: UISwipeGestureRecognizer) {
        self.pageControl.currentPage = self.pageControl.currentPage + 1
        if(!self.letsBegin) {
            if let urlStr = ParserHelper.createWalkThroughImageUrl(walkThrough: self.walkThroughArray[self.pageControl.currentPage]) {
                self.imageView.sd_setImage(with: urlStr, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            self.labelOne.text = self.walkThroughArray[self.pageControl.currentPage].Title
            self.labelTwo.text = self.walkThroughArray[self.pageControl.currentPage].Description
            //self.imageView.image = UIImage(named:"image\(self.pageControl.currentPage).jpg")
            self.animateForNext()
            if(self.pageControl.currentPage == self.walkThroughArray.count - 1) {
                self.btnNext.setTitle(MiscStrings.letsBegin.text, for: .normal)
                self.letsBegin = true
            }
        }
        else {
//            if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "LoginSelection") as? LoginSelection {
//                self.navigationController?.pushViewController(dvc, animated: true)
//            }
        }
    }
    @objc func actionSwipeRight(_ sender: UISwipeGestureRecognizer) {
        if self.pageControl.currentPage == 0 { // if on page zero do no animation
            return
        }
        self.pageControl.currentPage = self.pageControl.currentPage - 1
        self.letsBegin = false
        if let urlStr = ParserHelper.createWalkThroughImageUrl(walkThrough: self.walkThroughArray[self.pageControl.currentPage]) {
            self.imageView.sd_setImage(with: urlStr, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        self.labelOne.text = self.walkThroughArray[self.pageControl.currentPage].Title
        self.labelTwo.text = self.walkThroughArray[self.pageControl.currentPage].Description
        //self.imageView.image = UIImage(named:"image\(self.pageControl.currentPage).jpg")
        self.animateForPrevious()
        self.btnNext.setTitle(MiscStrings.next.text, for: .normal)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        self.pageControl.currentPage = self.pageControl.currentPage + 1
        if(!self.letsBegin) {
            if let urlStr = ParserHelper.createWalkThroughImageUrl(walkThrough: self.walkThroughArray[self.pageControl.currentPage]) {
                self.imageView.sd_setImage(with: urlStr, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            self.labelOne.text = self.walkThroughArray[self.pageControl.currentPage].Title
            self.labelTwo.text = self.walkThroughArray[self.pageControl.currentPage].Description
            //self.imageView.image = UIImage(named:"image\(self.pageControl.currentPage).jpg")
            self.animateForNext()
            if(self.pageControl.currentPage == self.walkThroughArray.count - 1) {
                sender.setTitle(MiscStrings.letsBegin.text, for: .normal)
                self.letsBegin = true
            }
        }
        else {
            if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "LoginSelection") as? LoginSelection {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
    }
    
    private func animateForNext() {
        //Image
        self.imageView.force = self.selectedForce
        self.imageView.duration = self.selectedDuration
        self.imageView.animation = "slideLeft"
        self.imageView.curve = "easeIn"
        
        //borderImage View
        self.viewBorder.force = self.selectedForce
        self.viewBorder.duration = self.selectedDuration
        self.viewBorder.animation = "slideLeft"
        self.viewBorder.curve = "easeIn"
        
        //LabelOne
        self.labelOne.force = self.selectedForce + 1
        self.labelOne.duration = self.selectedDuration
        self.labelOne.animation = "slideLeft"
        self.labelOne.curve = "easeIn"
        
        //LabelTwo
        self.labelTwo.force = self.selectedForce - 0.5
        self.labelTwo.duration = self.selectedDuration
        self.labelTwo.animation = "slideLeft"
        self.labelTwo.curve = "easeIn"
        
        //LabelThree
        self.labelThree.force = self.selectedForce + 2
        self.labelThree.duration = self.selectedDuration
        self.labelThree.animation = "slideLeft"
        self.labelThree.curve = "easeIn"
        
        self.imageView.animate()
        self.viewBorder.animate()
        self.labelOne.animate()
        self.labelTwo.animate()
        self.labelThree.animate()
        
    }
    
    private func animateForPrevious() {
        //Image
        self.imageView.force = self.selectedForce
        self.imageView.duration = self.selectedDuration
        self.imageView.animation = "slideRight"
        self.imageView.curve = "easeIn"
        
        //borderImage View
        self.viewBorder.force = self.selectedForce
        self.viewBorder.duration = self.selectedDuration
        self.viewBorder.animation = "slideRight"
        self.viewBorder.curve = "easeIn"
        
        //LabelOne
        self.labelOne.force = self.selectedForce + 1
        self.labelOne.duration = self.selectedDuration
        self.labelOne.animation = "slideRight"
        self.labelOne.curve = "easeIn"
        
        //LabelTwo
        self.labelTwo.force = self.selectedForce - 0.5
        self.labelTwo.duration = self.selectedDuration
        self.labelTwo.animation = "slideRight"
        self.labelTwo.curve = "easeIn"
        
        //LabelThree
        self.labelThree.force = self.selectedForce + 2
        self.labelThree.duration = self.selectedDuration
        self.labelThree.animation = "slideRight"
        self.labelThree.curve = "easeIn"
        
        self.imageView.animate()
        self.viewBorder.animate()
        self.labelOne.animate()
        self.labelTwo.animate()
        self.labelThree.animate()
        
    }
    
    private func animateForStart() {
        //Image
        self.imageView.force = self.selectedForce + 3
        self.imageView.duration = self.selectedDuration
        self.imageView.delay = 0.1
        self.imageView.animation = "fadeIn"
        self.imageView.curve = "easeIn"
        
        //borderImageView
        self.viewBorder.force = self.selectedForce + 3
        self.viewBorder.duration = self.selectedDuration
        self.viewBorder.delay = 0.1
        self.viewBorder.animation = "fadeIn"
        self.viewBorder.curve = "easeIn"
        
        //LabelOne
        self.labelOne.force = self.selectedForce + 1
        self.labelOne.duration = self.selectedDuration
        self.labelOne.delay = 0.65
        self.labelOne.animation = "fadeIn"
        self.labelOne.curve = "easeIn"
        
        //LabelTwo
        self.labelTwo.force = self.selectedForce - 0.5
        self.labelTwo.duration = self.selectedDuration
        self.labelTwo.delay = 0.75
        self.labelTwo.animation = "fadeIn"
        self.labelTwo.curve = "easeIn"
        
        //LabelThree
        self.labelThree.force = self.selectedForce + 2
        self.labelThree.duration = self.selectedDuration
        self.labelThree.delay = 0.85
        self.labelThree.animation = "fadeIn"
        self.labelThree.curve = "easeIn"
        
        self.imageView.animate()
        self.viewBorder.animate()
        self.labelOne.animate()
        self.labelTwo.animate()
        self.labelThree.animate()
        
    }
}

// MARK: - API CALLING
extension WalkThroughController
{
    func getWalkThroughData() {
        
    }
}
