//
//  PlayerListing.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/17/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift
import LGSideMenuController

protocol PlayerListingDelegate {
    func didChapEpisodeSelect(chapEpisodeNo: Int)
}

class PlayerListing: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    public var chaptersArray : List<BookChapter>!
    public var episodeArray : List<PodcastTrack>!
    var delegate: PlayerListingDelegate? = nil
    
    public var listType: PlayingType!
    
    var currentIndexPlaying: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.listType == .book {
            if let chapListing = Singleton.sharedInstance.chaptersArray {
                self.chaptersArray = chapListing
            }
            else {
                self.chaptersArray = List<BookChapter>()
            }
        }
        else if self.listType == .podcast {
            if let episodeListing = Singleton.sharedInstance.podcastEpisodeArray {
                self.episodeArray = episodeListing
            }
            else {
                self.episodeArray = List<PodcastTrack>()
            }
        }

        // Do any additional setup after loading the view.
    }
    

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let mainViewController = sideMenuController!
        mainViewController.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PlayerListing: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.listType == .book {
            return self.chaptersArray.count
        }
        else if self.listType == .podcast {
            return self.episodeArray.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.listType == .book {
             guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerListCell") as? PlayerListCell else { return UITableViewCell() }
            cell.labelChapEpisodeNo.text = "\(MiscStrings.chapter.text) \(indexPath.row + 1)".uppercased()
             return cell
        }
        else if self.listType == .podcast {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerListCell2") as? PlayerListCell else { return UITableViewCell() }
            if indexPath.row == self.currentIndexPlaying {
                cell.labelChapEpisodeDescribtion.textColor = Constants.THEME_ORANGE_COLOR
                cell.labelChapEpisodeNo.textColor = Constants.THEME_ORANGE_COLOR
            }
            else {
                cell.labelChapEpisodeDescribtion.textColor = UIColor.darkGray
                cell.labelChapEpisodeNo.textColor = UIColor.darkGray
            }
            cell.labelChapEpisodeDescribtion.text = "\(self.episodeArray[indexPath.row].Name ?? "")".uppercased()
            cell.labelChapEpisodeNo.text = "\(MiscStrings.episode.text) \(indexPath.row + 1)".uppercased()
            return cell
        }
        
       return UITableViewCell()
    }
}

extension PlayerListing: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.didChapEpisodeSelect(chapEpisodeNo: indexPath.row)
        self.sideMenuController?.hideRightView()
    }
}

// MARK: - LeftSideMenuController
extension PlayerListing: LGSideMenuDelegate
{
    func willShowRightView(_ rightView: UIView, sideMenuController: LGSideMenuController) {
        print("I am willShowLeftView")
        if self.classForCoder == PlayerListing.self {
            if self.listType == .book {
                if let chapListing = Singleton.sharedInstance.chaptersArray {
                    self.chaptersArray = chapListing
                }
                else {
                    self.chaptersArray = List<BookChapter>()
                }
            }
            else if self.listType == .podcast {
                if let episodeListing = Singleton.sharedInstance.podcastEpisodeArray {
                    self.episodeArray = episodeListing
                }
                else {
                    self.episodeArray = List<PodcastTrack>()
                }
            }
            self.currentIndexPlaying = Singleton.sharedInstance.currentlyPlayingChapEpisodeNumber
            self.tableview.reloadData()
        }
        
    }
}
