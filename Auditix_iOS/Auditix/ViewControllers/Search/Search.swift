//
//  Search.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Search: UIViewController {

    @IBOutlet weak var textfiledSearch: UITextField!
    @IBOutlet weak var tableview: UITableView!
    
    var servicePageNumber: Int = 1
    
    var textToSearch: String = ""

    var arrSearchBooks = Array<BookModel>()
    var arrSearchNews = Array<NewsCategory>()
    var arrSearchPodcasts = Array<PodcastModel>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textfiledSearch.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - HELPER METHODS
extension Search {
    func navigateToPodcastDetailsWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = self.arrSearchPodcasts[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToBookDetailsWith(_ indexPath: IndexPath) {
        if let bookDetail = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            bookDetail.currentBook = self.arrSearchBooks[indexPath.row]
            //bookDetail.currentBookID = self.arrSearchBooks[indexPath.row].BookID
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
    
    func navigateToNewsDetailsWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsList") as? NewsList {
            controller.title = self.arrSearchNews[indexPath.row].SourceName ?? ""
            controller.categoryId = self.arrSearchNews[indexPath.row].Id
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToSearchSeeAll(_ seeAllType: SeeAllType) {
        if let controller = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "SearchSeeAll") as? SearchSeeAll {
            controller.seeAllType = seeAllType
            controller.searchText = self.textToSearch
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK: - IB ACTIONS
extension Search {
    @IBAction func actionMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }

    @IBAction func actionBackBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchEditingChanged(_ sender: UITextField) {
    }
    
    @IBAction func actionSearchBtn(_ sender: Any) {
        if !(self.textfiledSearch.text?.isEmpty)! {
           
            if (self.textToSearch == self.textfiledSearch.text) {
                self.search(with: self.textToSearch, removeTheArray: false)
            } else {
                self.servicePageNumber = 1
                self.textToSearch = self.textfiledSearch.text!
                self.search(with: self.textToSearch, removeTheArray: true)
            }
        }
        self.textfiledSearch.resignFirstResponder()
    }
}
// MARK: - API CALLING
extension Search {
    public func search(with keyword: String, removeTheArray: Bool) {
        Utility.main.showLoader()
        let param: [String : Any] = ["pageNumber": self.servicePageNumber,
                                     "totalCount": 100,
                                     "Text" : keyword]

        APIManager.sharedInstance.miscellaneousAPIManager.getSearch(parameters: param, success: { (responseObject) in
            Utility.main.hideLoader()

            // Clear all data
            self.arrSearchPodcasts.removeAll()
            self.arrSearchBooks.removeAll()
            self.arrSearchNews.removeAll()

            if let podcastItems = responseObject["Podcasts"] as? Array<AnyObject> {
                for item in podcastItems {
                    let searchItem = PodcastModel(value: item)
                    self.arrSearchPodcasts.append(searchItem)
                }
            }
            
            if let booksItems = responseObject["Books"] as? Array<AnyObject> {
                for item in booksItems {
                    let searchItem = BookModel(value: item)
                    self.arrSearchBooks.append(searchItem)
                }
            }
            
            if let newsItems = responseObject["News"] as? Array<AnyObject> {
                for item in newsItems {
                    let searchItem = NewsCategory(value: item)
                    self.arrSearchNews.append(searchItem)
                }
            }
            
            self.tableview.reloadData()
            
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
}
// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension Search: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.arrSearchPodcasts.count
        case 1:
            return self.arrSearchBooks.count
        case 2:
            return self.arrSearchNews.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchCell else { return UITableViewCell() }

        switch indexPath.section {
        case 0: // Podcasts
            cell.stackNarrator.isHidden = false
            cell.BindData(with: self.arrSearchPodcasts[indexPath.row])
            break
        case 1:
            cell.stackNarrator.isHidden = false
            cell.BindData(with: self.arrSearchBooks[indexPath.row])
            break
        case 2:
            cell.stackNarrator.isHidden = true
            cell.BindData(with: self.arrSearchNews[indexPath.row])
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            self.navigateToPodcastDetailsWith(indexPath)
            break
        case 1:
            self.navigateToBookDetailsWith(indexPath)
            break
        case 2:
            self.navigateToNewsDetailsWith(indexPath)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.15
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: BooksHeader = .fromNib()
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30)
        header.backgroundColor = UIColor.clear
        header.buttonSeeAll.tag = section
        header.buttonSeeAll.addTarget(self, action: #selector(self.onBtnSeeAll(_:)), for: .touchUpInside)
        switch section {
        case 0:
            header.labelTitle.text = MiscStrings.podcast.text
            return (self.arrSearchPodcasts.count > 0) ? header : nil
        case 1:
            header.labelTitle.text = MiscStrings.books.text
            return (self.arrSearchBooks.count > 0) ? header : nil
        default:
            header.labelTitle.text = MiscStrings.news.text
            return (self.arrSearchNews.count > 0) ? header : nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return (self.arrSearchPodcasts.count > 0) ? 30 : 0
        case 1:
            return (self.arrSearchBooks.count > 0) ? 30 : 0
        default:
            return (self.arrSearchNews.count > 0) ? 30 : 0
        }
    }
    @objc func onBtnSeeAll(_ sender: UIButton){
        switch sender.tag {
        case 0://Podcast
            self.navigateToSearchSeeAll(.podcast)
        case 1://Books
            self.navigateToSearchSeeAll(.books)
        default://News
            self.navigateToSearchSeeAll(.news)
        }
    }
}
// MARK:- UITextField Delegate
extension Search: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !(self.textfiledSearch.text?.isEmpty)! {
            if (self.textToSearch == self.textfiledSearch.text) {
                self.search(with: self.textToSearch, removeTheArray: false)
            } else {
                self.servicePageNumber = 1
                self.textToSearch = self.textfiledSearch.text!
                self.search(with: self.textToSearch, removeTheArray: true)
            }
        }
        self.textfiledSearch.resignFirstResponder()
        return true
    }
}
