//
//  SearchSeeAll.swift
//  Auditix
//
//  Created by Usman Bin Rehan on 9/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//
enum SeeAllType{
    case podcast
    case news
    case books
}
import UIKit

class SearchSeeAll: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var seeAllType:SeeAllType = .podcast
    var arrSearchBooks = Array<BookModel>()
    var arrSearchNews = Array<NewsCategory>()
    var arrSearchPodcasts = Array<PodcastModel>()
    var searchText = ""
    var isLoading = false
    var displayCellCount = 0
    var pageNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.search()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Helper Methods
extension SearchSeeAll{
    func navigateToPodcastDetailsWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = self.arrSearchPodcasts[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func navigateToBookDetailsWith(_ indexPath: IndexPath) {
        if let bookDetail = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            bookDetail.currentBook = self.arrSearchBooks[indexPath.row]
            //bookDetail.currentBookID = self.arrSearchBooks[indexPath.row].BookID
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
    func navigateToNewsDetailsWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsList") as? NewsList {
            controller.title = self.arrSearchNews[indexPath.row].SourceName ?? ""
            controller.categoryId = self.arrSearchNews[indexPath.row].Id
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension SearchSeeAll: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.seeAllType {
        case .podcast:
            return self.arrSearchPodcasts.count
        case .books:
            return self.arrSearchBooks.count
        case .news:
            return self.arrSearchNews.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchCell else { return UITableViewCell() }
        switch self.seeAllType {
        case .podcast:
            cell.stackNarrator.isHidden = false
            cell.BindData(with: self.arrSearchPodcasts[indexPath.row])
        case .books:
            cell.stackNarrator.isHidden = false
            cell.BindData(with: self.arrSearchBooks[indexPath.row])
        case .news:
            cell.stackNarrator.isHidden = true
            cell.BindData(with: self.arrSearchNews[indexPath.row])
        }
        return cell
    }
}
extension SearchSeeAll: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.15
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch self.seeAllType {
        case .podcast:
            self.navigateToPodcastDetailsWith(indexPath)
        case .books:
            self.navigateToBookDetailsWith(indexPath)
        case .news:
            self.navigateToNewsDetailsWith(indexPath)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !self.isLoading && indexPath.row == displayCellCount - 10 {
            self.isLoading = true
            self.pageNumber += 1
            self.search()
        }
    }
}
// MARK: - API CALLING
extension SearchSeeAll {
    public func search() {
        Utility.main.showLoader()
        let param: [String : Any] = ["pageNumber": self.pageNumber,
                                     "totalCount": 100,
                                     "Text" : self.searchText]
        APIManager.sharedInstance.miscellaneousAPIManager.getSearch(parameters: param, success: { (responseObject) in
            Utility.main.hideLoader()
            if let podcastItems = responseObject["Podcasts"] as? Array<AnyObject> {
                for item in podcastItems {
                    let searchItem = PodcastModel(value: item)
                    self.arrSearchPodcasts.append(searchItem)
                }
            }
            if let booksItems = responseObject["Books"] as? Array<AnyObject> {
                for item in booksItems {
                    let searchItem = BookModel(value: item)
                    self.arrSearchBooks.append(searchItem)
                }
            }
            if let newsItems = responseObject["News"] as? Array<AnyObject> {
                for item in newsItems {
                    let searchItem = NewsCategory(value: item)
                    self.arrSearchNews.append(searchItem)
                }
            }
            switch self.seeAllType {
            case .podcast:
                self.displayCellCount = self.arrSearchPodcasts.count
            case .books:
                self.displayCellCount = self.arrSearchBooks.count
            case .news:
                self.displayCellCount = self.arrSearchNews.count
            }
            self.isLoading = false
            self.tableview.reloadData()
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
}
