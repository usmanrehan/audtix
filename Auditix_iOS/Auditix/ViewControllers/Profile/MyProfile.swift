//
//  MyProfile.swift
//  Auditix
//
//  Created by shardha on 1/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyProfile: UIViewController {
    
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var containerView: UIView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPageMenu()
        Utility.main.postSubscribedNewsChannelNotification()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupPageMenu() {
        var controllers = Array<UIViewController>()

        let controllerNews = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "YourNews") as! YourNews
        controllerNews.view.backgroundColor = UIColor.clear
        controllerNews.title = MiscStrings.news.text
        controllerNews.delegate = self
        controllers.append(controllerNews)
        
        let controllerPodcast = self.storyboard?.instantiateViewController(withIdentifier: "YourPodcast") as! YourPodcast
        controllerPodcast.view.backgroundColor = UIColor.clear
        controllerPodcast.title = MiscStrings.podcast.text
        controllerPodcast.delegate = self
        controllers.append(controllerPodcast)

        let controllerLibrary = self.storyboard?.instantiateViewController(withIdentifier: "YourLibrary") as! YourLibrary
        controllerLibrary.view.backgroundColor = UIColor.clear
        controllerLibrary.title = MiscStrings.books.text
        controllerLibrary.delegate = self
        controllers.append(controllerLibrary)
        
        let parameters: [CAPSPageMenuOption] = [ .MenuItemSeparatorWidth(0),
                                                .UseMenuLikeSegmentedControl(true),
                                                .MenuItemSeparatorPercentageHeight(0.1),
                                                .ScrollMenuBackgroundColor(UIColor.white),
                                                .SelectionIndicatorColor(UIColor.orange),
                                                .SelectedMenuItemLabelColor(UIColor.orange),
                                                .UnselectedMenuItemLabelColor(UIColor.darkGray),
                                                .BottomMenuHairlineColor(UIColor.white),
                                                .AddBottomMenuHairline(false),
                                                .EnableHorizontalBounce(false),
                                                .MenuMargin(10),
                                                .MenuHeight(50)
        ]
        var frame = CGRect()
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                frame = CGRect(x: 0, y: 84, width: self.view.frame.size.width, height: self.view.frame.height - 105)
            default:
                frame = CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.height - 105)
            }
        }
        self.pageMenu = CAPSPageMenu(viewControllers: controllers, frame: frame, pageMenuOptions: parameters)
        
        let navigationRoot = UINavigationController(rootViewController: pageMenu!)
        
        self.view.addSubview((navigationRoot.topViewController?.view!)!)
    }
    
    @IBAction func actionMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }
    @IBAction func actionSettingd(_ sender: Any) {
        self.pushToSettings()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
}

extension MyProfile: YourPodcastDelegate {
    func didPodcastSelect(podcastEpisodeModel: PodcastEpisode) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "EpisodeDetails") as? EpisodeDetails {
            let podcast = PodcastModel()
            podcast.episodes.append(podcastEpisodeModel)
            let imageURLString = "\(podcastEpisodeModel.Podcast?.ImageUrl ?? "http://auditix.stagingic.com/")"
            podcast.ImageUrl = imageURLString
            podcast.PodcastId = podcastEpisodeModel.PodcastId
            podcast.ArtistName = podcastEpisodeModel.Podcast?.ArtistName ?? "-"
            //controller.podcast.ArtistName = "Episode Number: \(podcastEpisodeModel.EpisodeNo)"
            controller.podcast = podcast
            controller.episodeIndex = 0
            self.navigationController?.pushViewController(controller, animated: true)
        }
            
//            if let indexPath = sender as? IndexPath {
//                let controller = segue.destination as! EpisodeDetails
//                controller.podcast = self.podcast
//                controller.episodeIndex = indexPath.row
//            }
//            podcastModel.IsSubscribed = true
//            controller.podcast = podcastModel
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
    }
    
    func didPodcastSelect(podcastModel: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            podcastModel.IsSubscribed = true
            controller.podcast = podcastModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func didDownloadedPodcastSelect(trackID: Int) {
        if let podcastDetail = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            //podcastDetail.currentTrackID = trackID
            self.navigationController?.pushViewController(podcastDetail, animated: true)
        }
    }
    
    func didSeeAllBtnPress() {
        if let bookDetail = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastAll") as? PodcastAll {
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
    
    func navigateToSubscribedPodcast() {
        if let podastGenreController = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastGenre") as? PodcastGenre {
            podastGenreController.titleHeading = AlertMessages.mySubscription.message
            podastGenreController.flag = false
            self.navigationController?.pushViewController(podastGenreController, animated: true)
        }
    }
}

extension MyProfile: YourLibraryDelegate {
    func didBookSelect(bookModel: BookModel) {
        if let bookDetail = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            bookDetail.currentBook = bookModel
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
}

extension MyProfile: YourNewsDelegate {
    func didTapOnThisNews(with downloadedEpisode: NewsEpisodeModel, index:Int) {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsEpisodeDetails") as? NewsEpisodeDetails {
            let newsModel = NewsListModel()
            newsModel.NewsID = downloadedEpisode.NewsID
            newsModel.ImageUrl = downloadedEpisode.CoverImage ?? ""
            newsModel.NarratorName = downloadedEpisode.NarratorName
            newsModel.episodes.append(downloadedEpisode)
            newsModel.Name = downloadedEpisode.Name
            newsModel.Description = downloadedEpisode.Description
            controller.objNews = newsModel
            controller.episodeIndex = 0
            self.navigationController?.pushViewController(controller, animated: true)
        }

    }
    func didTapOnThisNews(with subscribedNews:NewsListModel){
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsDetails") as? NewsDetails {
            controller.newsObject = subscribedNews
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func navigateToSubscribedNewsList() {
        if let controller = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsList") as? NewsList {
            controller.title = AlertMessages.mySubscription.message
            controller.flag = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension MyProfile{
    private func pushToSettings(){
        if let svc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Settings") as? Settings {
            self.navigationController?.pushViewController(svc, animated: true)
        }
        
    }
}
