//
//  EditProfile.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import DatePickerDialog
import SkyFloatingLabelTextField

class EditProfile: UIViewController {
    
    @IBOutlet weak var dobLabel: UILabel!
    var dobText: String = ""
    
    @IBOutlet weak var fullNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    var chosenImaeg: UIImage?
    
    var ImagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ImagePicker.delegate = self
        
        self.populateProfileData()
        
        self.fullNameTextField.delegate = self
        // Do any additional setup after loading the view.
        self.setArabicData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // show date picker
    func datePickerTapped() {
        DatePickerDialog().show( MiscStrings.dateofBirth.text, doneButtonTitle: MiscStrings.done.text, cancelButtonTitle: MiscStrings.cancel.text, maximumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MMM dd,yyyy"
                
                self.dobLabel.text = formatter.string(from: dt)
                self.dobLabel.textColor = Constants.THEME_ORANGE_COLOR
                
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//"yyyy-MM-dd"
                
                self.dobText = formatter.string(from: dt)
            }
        }
    }
    @IBAction func fullNameFieldDidChange(_ sender: SkyFloatingLabelTextField) {
        if !(sender.text?.isEmpty)! {
            self.fullNameTextField.errorMessage = ""
        } else {
            self.fullNameTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            self.fullNameTextField.errorMessage = AlertMessages.NameNotEmpty.message
        }
    }
}
// MARK: - HELPER METHODS
extension EditProfile
{
    // populate profile data from defaults
    func populateProfileData() {
        if let userModel = AppStateManager.sharedInstance.loggedInUser {
            
            
            if let imageURLStr = AppStateManager.sharedInstance.loggedInUser.ImageName {
                if let imageURL = URL(string: (Constants.ImageBaseURL + imageURLStr).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    self.profileImageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "userProfile"))
                }
                else {
                    self.profileImageView.image = #imageLiteral(resourceName: "userProfile")
                }
            }
            else {
                self.profileImageView.image = #imageLiteral(resourceName: "userProfile")
            }
            if Validation.isValidEmail(userModel.EmailAddress ?? ""){
                self.emailTextField.text = userModel.EmailAddress
                self.emailTextField.isEnabled = false
            }
            else{
                self.emailTextField.text = ""
                self.emailTextField.isEnabled = true
            }
            self.fullNameTextField.text = userModel.FullName
            if let dobStr = userModel.DOB {
                 self.dobText = dobStr
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                if let date = dateFormatter.date(from: dobStr) {
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    self.dobLabel.text = dateFormatter.string(from: date)
                }
                else {
                    self.dobLabel.text = userModel.DOB
                }
            } else {
                 self.dobText = ""
            }
           
            
           

//            self.dobLabel.text =
            
        }
    }
    
    // if user successfully updates its profile call this method. It will update your local cache of user model
    func updateLocalUserRecord(with imageName: String)
    {
        let updatedUser: User = AppStateManager.sharedInstance.loggedInUser
        
        // updating profile info so token remain same...
        try! Global.APP_REALM?.write { () -> Void in
            updatedUser.FullName = self.fullNameTextField.text!
            updatedUser.EmailAddress = self.emailTextField.text!
            updatedUser.DOB = self.dobText
            updatedUser.ImageName = imageName
        }

        print("user: \(updatedUser)")

//        if (AppStateManager.sharedInstance.loggedInUser.AccountID) > 0
//        {
////            Utility.main.showToast(message:, controller: self)
////            Utility.main.hideLoader()
//        }
        
    }
    
    func cameraOpens() {
        let alert = UIAlertController(title: MiscStrings.uploadPhoto.text , message: MiscStrings.howDoYouWantToSetYourPhoto.text, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: MiscStrings.camera.text, style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: MiscStrings.gallery.text, style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: MiscStrings.cancel.text, style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            ImagePicker =  UIImagePickerController()
            ImagePicker.allowsEditing = true
            ImagePicker.delegate = self
            ImagePicker.sourceType = .camera
            present(ImagePicker, animated: true, completion: nil)
        }
    }
    
    func uploadFromGallery(){
        ImagePicker =  UIImagePickerController()
        ImagePicker.allowsEditing = true
        ImagePicker.delegate = self
        ImagePicker.sourceType = .photoLibrary
        present(ImagePicker, animated: true, completion: nil)
        
    }
    
    func setArabicData(){
        if LanguageManager.sharedInstance.getSelectedLocale().contains("ar"){
            self.fullNameTextField.placeholder = "الاسم الكامل"
            self.emailTextField.placeholder = "البريد الإلكتروني"
        }
    }
}
// MARK: - API CALLING
extension EditProfile
{
    func updateProfile() {
        let image = UIImageJPEGRepresentation(self.profileImageView.image!, 0.1)
        let param: [String : Any] = [
            "FullName" : self.fullNameTextField.text!,
            "DOB" : self.dobText,
            "ProfileImage" : image!
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.editProfile(param: param, success: {(responce) in
            let tempUser = User(value: responce)
            if let imageName = tempUser.ImageName as? String {
                self.updateLocalUserRecord(with: imageName)
            }
            else {
                self.updateLocalUserRecord(with: "")
            }
            self.emailTextField.isEnabled = false
            self.emailTextField.errorMessage = ""
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.editProfileDoneChangeTheSideMenuName), object: nil, userInfo: nil)
            
            Utility.main.showToast(message: AlertMessages.profileUpdated.message, controller: self)
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
}

// MARK: - IBACTIONS
extension EditProfile
{
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNotification(_ sender: Any) {
        if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
    
    @IBAction func actionChangeImage(_ sender: Any) {
//        ImagePicker.allowsEditing = false
//        ImagePicker.sourceType = .photoLibrary
//        ImagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
//        present(ImagePicker, animated: true, completion: nil)
        self.cameraOpens()
    }
    @IBAction func actionSave(_ sender: Any) {
        if(!(self.fullNameTextField.text?.isEmpty)!) {
            if(!(self.emailTextField.text?.isEmpty)!) && Validation.isValidEmail(self.emailTextField.text ?? ""){
                self.updateProfile()
            }
            else{
                self.emailTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
                self.emailTextField.errorMessage = AlertMessages.emailNotValid.message
            }
        }
        else {
            self.fullNameTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            self.fullNameTextField.errorMessage = AlertMessages.NameNotEmpty.message
        }
    }
    @IBAction func actionDOB(_ sender: Any) {
        self.datePickerTapped()
    }
    
}

// MARK: - UIImagePickerController Delegate, UINavigationController Delegate
extension EditProfile:  UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    @objc func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.image = chosenImage
        self.chosenImaeg = chosenImage
        dismiss(animated:true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfile: MDDatePickerDialogDelegate
{
    func datePickerDialogDidSelectDate(_ dialog: UIControl!, date: Date) {
        

        print("date: \(date)")
    }
}

// MARK:- UITextField Delegate
extension EditProfile: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
