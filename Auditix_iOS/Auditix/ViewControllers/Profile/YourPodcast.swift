//
//  YourPodcast.swift
//  Auditix
//
//  Created by shardha on 1/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift
import MGSwipeTableCell

protocol YourPodcastDelegate {
    func didPodcastSelect(podcastEpisodeModel: PodcastEpisode)
    func didPodcastSelect(podcastModel: PodcastModel)
    func didDownloadedPodcastSelect(trackID: Int)
    func didSeeAllBtnPress()
    func navigateToSubscribedPodcast()
}

class YourPodcast: UIViewController {
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    var arrSubscribePodcasts = Array<PodcastModel>()
    var arrDownloadedPodcasts = Array<PodcastEpisode>()
    
    
    var showingWinless: Bool = true
    
    var delegate: YourPodcastDelegate? = nil
    
    var selectedDownload: Int = 1 // 0 for today, 1 for this month, 2 for older
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.addNotificationObservers()
        
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOpacity = 1
        shadow.layer.shadowOffset = CGSize.zero
        shadow.layer.shadowRadius = 10
        let dummyViewHeight = CGFloat(40)
        self.tableview.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableview.bounds.size.width, height: dummyViewHeight))
        self.tableview.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        self.tableview.register(UINib(nibName: "PodcastCollection", bundle: nil), forCellReuseIdentifier: "PodcastCollection")
        self.tableview.register(UINib(nibName: "PodcastDownloadedCell", bundle: nil), forCellReuseIdentifier: "PodcastDownloadedCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.tableview.windless
//            .apply {
//                $0.beginTime = 0.5
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//                $0.animationLayerColor = Constants.THEME_ORANGE_COLOR
//            }
//            .start()
        
        self.getOnlySubscribedPodcast()
        self.getPodcastDownloadedEpisodes()
        //self.getPodcastBookNews()
        //self.getDownloadedPodcasts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func getDownloadedPodcasts() {
    //        let results = PodcastRealmHelper.sharedInstance.getAllDownloadedPodcast()
    //        self.arrDownloadedPodcasts = Array(results)
    //
    //        self.tableview.reloadData()
    //    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadSubscribedPodcasts(_:)),
                                               name: Constants.subscribedPodcastNotification,
                                               object: nil)
    }
    
    @objc func reloadSubscribedPodcasts(_ data: NSNotification) {
        self.getOnlySubscribedPodcast()
    }
    
    @objc func actionSubscribtion(_ sender: UIButton) {
        // call on nusub method
        let index = sender.tag
        self.unsubscibeMyPodcastUp(with: self.arrSubscribePodcasts[index].TrackId, index: index)
    }
    
    @IBAction func actionSeeAll(_ sender: UIButton) {
        self.delegate?.didSeeAllBtnPress()
    }
    
    @objc func actionSectionTabs(_ sender: UIButton) {
        self.selectedDownload = sender.tag
        self.tableview.reloadData()
        
        let addtionalText: [AnyHashable: Any] = ["BtnTag" : 0]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.yourPodcastSectionTab), object: nil, userInfo: addtionalText)
    }
    
    func openPodcastDetail(with trackID: Int) {
        if let podcastDetail = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            //podcastDetail.currentTrackID = trackID
            self.navigationController?.pushViewController(podcastDetail, animated: true)
        }
    }
    
    func deleteThisPodcast(with indexGlobal: Int, selectedType: Int) {
        print("I am delete")
    }

}

// MARK: - API CALLING
extension YourPodcast {
    func getOnlySubscribedPodcast() {
        APIManager.sharedInstance.podcastApiManager.getSubscribePodcasts(success: {(responseArray) in
            Utility.main.hideLoader()
            self.arrSubscribePodcasts.removeAll()
            for item in responseArray {
                let model = PodcastModel(value: item)
                self.arrSubscribePodcasts.append(model)
            }
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
        }, failure:{ (failure) in
            Utility.main.hideLoader()
        })
    }
    
    func unsubscibeMyPodcastUp(with trackID: Int, index: Int) {
        let param: [String: Any] = ["TrackId" : trackID]
        
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.unSubscribeThisPodcast(with: param, success: {(responceObj) in
            Utility.main.hideLoader()
            Utility.main.postSubscribedPodcastNotification()
            self.arrSubscribePodcasts.remove(at: index)
            self.tableview.reloadData()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
}

// MARK: - UITableView DataSource Delegate
extension YourPodcast: UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.arrDownloadedPodcasts.count
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tablesHeader: BooksHeader = .fromNib()
        tablesHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        
        if section == 0 {
            tablesHeader.labelTitle.text = AlertMessages.mySubscription.message.uppercased()
            tablesHeader.buttonSeeAll.isHidden = false
            tablesHeader.buttonSeeAll.addTarget(self, action: #selector(self.seeAllSubscribedPodcasts), for: .touchUpInside)
        } else {
            tablesHeader.labelTitle.text = AlertMessages.downloads.message.uppercased()
            tablesHeader.buttonSeeAll.isHidden = true
        }
        return tablesHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastCollection") as? PodcastCollection else { return UITableViewCell() }
            cell.collectionView.register(UINib(nibName: "PodcastCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PodcastCollectionCell")
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            cell.hero.modifiers = [.arc]
            cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
            
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastDownloadedCell") as? PodcastDownloadedCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.labelTitle.text = "\(MiscStrings.episodeNumber.text) \(self.arrDownloadedPodcasts[indexPath.row].PodcastEpisodeID)"
            cell.labelNarrator.text = self.arrDownloadedPodcasts[indexPath.row].ArtistName ?? "-"
            let createdAt = self.arrDownloadedPodcasts[indexPath.row].CreatedDate ?? "2018-06-23T14:03:34"
            if createdAt.contains("."){
                cell.labelDownloadedDate.text = Utility.stringDateFormatter(dateStr: createdAt, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.zzz", formatteddate: "d MMM yyyy")
            }
            else{
                cell.labelDownloadedDate.text = Utility.stringDateFormatter(dateStr: createdAt, dateFormat: "yyyy-MM-dd'T'HH:mm:ss", formatteddate: "d MMM yyyy")
            }
            if let imageURLString = self.arrDownloadedPodcasts[indexPath.row].Podcast?.ImageUrl{
                if let imageURL = URL(string: imageURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    cell.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage:    #imageLiteral(resourceName: "placeholder_image"))
                }
            }
            else {
                cell.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
            }
            
            cell.tag = indexPath.row
            cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
            cell.rightSwipeSettings.transition = .rotate3D
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (self.view.frame.size.width / 4.0) + 60.0 //Add padding for subscribe button and text label
        default:
            return self.view.frame.size.height * 0.25
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            self.delegate?.didPodcastSelect(podcastEpisodeModel: self.arrDownloadedPodcasts[indexPath.row])
            //self.delegate?.didPodcastSelect(podcastModel: self.arrDownloadedPodcasts[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true;
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        let fileName = self.arrDownloadedPodcasts[cell.tag].PodcastEpisodeID
        //print(Utility.main.deleteDownloadedFile(directoryName: "podcast", fileName: "\(fileName)"))
        if Utility.main.deleteDownloadedFile(directoryName: "podcast", fileName: "\(fileName)"){
            self.getPodcastDownloadedEpisodes()
        }
        return true
    }
    
    @objc func seeAllSubscribedPodcasts(){
        self.delegate?.navigateToSubscribedPodcast()
    }
}
// MARK
extension YourPodcast: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSubscribePodcasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PodcastCollectionCell", for: indexPath) as? PodcastCollectionCell else { return UICollectionViewCell() }
        cell.labelTitle.textColor = UIColor.darkGray
        
        if !self.showingWinless {
            let podcast = self.arrSubscribePodcasts[indexPath.row]
            cell.labelTitle.text = podcast.Title
            if let imageURL = podcast.ImageUrl {
                cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage:   #imageLiteral(resourceName: "placeholder_image"))
            }
            
            cell.btnSubscription.tag = indexPath.row
            cell.btnSubscription.addTarget(self, action: #selector(self.actionSubscribtion(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
        return CGSize(width: width, height: width + 60.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if self.delegate != nil {
            self.delegate?.didPodcastSelect(podcastModel: self.arrSubscribePodcasts[indexPath.row])
        }
    }
}
//MARK:- Webservice
extension YourPodcast{
    private func getPodcastDownloadedEpisodes(){
        var params = [String:Any]()
        params["BookEpisodeIds"] = ""
        params["PodcastEpisodeIds"] = Utility.main.getIdfromFolders(directoryName: "podcast")
        params["NewsEpisodeIds"] = ""
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getPodcastNewsAndBookByEpisodesId(param: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let response = responseObject as NSDictionary
            guard let CustomNews = response.object(forKey: "PodcastEpisode") as? [[String:Any]] else {return}
            self.arrDownloadedPodcasts.removeAll()
            for item in CustomNews {
                let model = PodcastEpisode(value: item)
                self.arrDownloadedPodcasts.append(model)
            }
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}
