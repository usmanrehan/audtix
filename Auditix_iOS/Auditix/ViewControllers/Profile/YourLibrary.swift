//
//  YourLibrary.swift
//  Auditix
//
//  Created by shardha on 1/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol YourLibraryDelegate {
    func didBookSelect(bookModel: BookModel)
}

class YourLibrary: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shadow: UIView!
    
    //var myLibraryBooksArray = [BookModel]()
    
    var arrFavoriteBooks = Array<BookModel>()
    var arrDownLoadedFavoriteBooks = Array<BookModel>()
    var showingWinless: Bool = true
    
    var delegate: YourLibraryDelegate? = nil
    var pageNo: Int = 1
    
    var currentSelectedIndex = 0
    
    var bottomView = BottomView.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.addNotificationObservers()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOpacity = 1
        shadow.layer.shadowOffset = CGSize.zero
        shadow.layer.shadowRadius = 10
        let dummyViewHeight = CGFloat(40)
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: dummyViewHeight))
        self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        self.tableView.register(UINib(nibName: "PodcastCollection", bundle: nil), forCellReuseIdentifier:
            "PodcastCollection")
        self.tableView.register(UINib(nibName: "YourLibraryCell", bundle: nil), forCellReuseIdentifier: "YourLibraryCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Get favorites books
        self.getFavoriteBooks()
        self.getDownloadedBook()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadFavoriteBooks(_:)),
                                               name: Constants.favoriteBookNotification,
                                               object: nil)
    }
    
    @objc func reloadFavoriteBooks(_ data: NSNotification) {
        self.getFavoriteBooks()
    }
}

// MARK: - API CALLING
extension YourLibrary {
    public func getFavoriteBooks() {
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.getFavoriteBooks(with: [:], success: { (responseObject) in
            Utility.main.hideLoader()
            if let books = responseObject["Books"] as? Array<AnyObject> {
                self.arrFavoriteBooks.removeAll()
                for item in books {
                    let model = BookModel(value: item)
                    self.arrFavoriteBooks.append(model)
                }
            }
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
}

// MARK: - UITableView DataSource Delegate
extension YourLibrary: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.arrDownLoadedFavoriteBooks.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastCollection") as? PodcastCollection else { return UITableViewCell() }
            cell.collectionView.register(UINib(nibName: "PodcastCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PodcastCollectionCell")
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            cell.hero.modifiers = [.arc]
            cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
            
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "YourLibraryCell") as? YourLibraryCell  else { return UITableViewCell() }
            cell.BindData(with: self.arrDownLoadedFavoriteBooks[indexPath.row])
            cell.imageCover.hero.modifiers = [.arc]
            cell.imageCover.hero.id = "DetailToPlayer"
            cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
            cell.rightSwipeSettings.transition = .rotate3D
            cell.tag = indexPath.row
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tablesHeader: BooksHeader = .fromNib()
        tablesHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        
        if section == 0 {
            tablesHeader.labelTitle.text = AlertMessages.books.message.uppercased()
        } else {
            tablesHeader.labelTitle.text = AlertMessages.downloads.message.uppercased()
        }
        
        tablesHeader.buttonSeeAll.isHidden = true
        
        return tablesHeader
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (self.view.frame.size.width / 4.0) + 60.0 //Add padding for subscribe button and text label
        default:
             return self.view.frame.size.height * 0.25
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.delegate != nil && indexPath.section == 1{
            let book = self.arrDownLoadedFavoriteBooks[indexPath.row]
            self.delegate?.didBookSelect(bookModel: book)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
extension YourLibrary: MGSwipeTableCellDelegate{
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true;
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        print("Index \(index)")
        print("Tag \(cell.tag)")
        let fileName = self.arrDownLoadedFavoriteBooks[cell.tag].BookID
        //print(Utility.main.deleteDownloadedFile(directoryName: "books", fileName: "\(fileName)"))
        if Utility.main.deleteDownloadedFile(directoryName: "books", fileName: "\(fileName)"){
            self.getDownloadedBook()
        }
        return true
    }
}
//extension YourLibrary: AdvertiseDelegate {
//    func advertisementIsStop() {
//        self.bottomView.playingType = .book
//        self.bottomView.currentBook = self.myLibraryBooksArray[self.currentSelectedIndex]//self.currentBook
//        //self.bottomView.playingThisEpisode = 0//index
//        if self.bottomView.currentBarState == .close {
//            self.bottomView.showBottomView()
//        }
//        self.bottomView.initBook()
//    }
//}

// MARK
extension YourLibrary: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrFavoriteBooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PodcastCollectionCell", for: indexPath) as? PodcastCollectionCell else { return UICollectionViewCell() }
        cell.labelTitle.textColor = UIColor.darkGray
        
        if self.showingWinless {
            let podcast = self.arrFavoriteBooks[indexPath.row]
            cell.labelTitle.text = podcast.AuthorName ?? "-"
            if let imageURL = podcast.ImageUrl {
                if let imgURL = imageURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
                    cell.imageView.sd_setImage(with: URL(string: imgURL), placeholderImage:   #imageLiteral(resourceName: "placeholder_image"))
                }
            }
            if LanguageManager.sharedInstance.getSelectedLocale().contains("en"){
                cell.btnSubscription.setTitle("Unfavorite", for: .normal)
            }
            else{
                cell.btnSubscription.setTitle("إزالة من المفضلة", for: .normal)
            }
            cell.btnSubscription.tag = indexPath.row
            cell.btnSubscription.addTarget(self, action: #selector(self.actionSubscribtion(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
        return CGSize(width: width, height: width + 60.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if self.delegate != nil {
            self.delegate?.didBookSelect(bookModel: self.arrFavoriteBooks[indexPath.row])
        }
    }
    @objc func actionSubscribtion(_ sender: UIButton) {
        // call on nusub method
        let index = sender.tag
        if self.arrFavoriteBooks.count > 0{
           self.removFromFavoriteStatus(bookID: self.arrFavoriteBooks[index].BookID)
        }
    }
}
//MARK:- Webservice
extension YourLibrary{
    private func getDownloadedBook(){
        var params = [String:Any]()
        params["BookIds"] = Utility.main.getIdfromBooksFolders(directoryName: "books")
        params["PodcastIds"] = ""
        params["NewsIds"] = ""
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getPodcastBookNews(param: params, success: { (responseObject) in
            Utility.main.hideLoader()
            self.arrDownLoadedFavoriteBooks.removeAll()
            let response = responseObject as NSDictionary
            let CustomBooks = response.object(forKey: "CustomBooks") as! [String:Any]
            guard let Books = CustomBooks["Books"] as? [[String:Any]] else {
                self.tableView.reloadSections([1], with: .automatic)
                return
            }
            for item in Books {
                let model = BookModel(value: item)
                self.arrDownLoadedFavoriteBooks.append(model)
            }
            self.tableView.reloadSections([1], with: .automatic)
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
    func removFromFavoriteStatus(bookID: Int) {
        let param: [String : Any] = [
            "bookId" : bookID
        ]
        
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.removeFromFvt(with: param, success: {
            (responceObj) in
            Utility.main.hideLoader()
            for (index, favoriteModel) in self.arrFavoriteBooks.enumerated() {
                if favoriteModel.BookID == bookID {
                    self.arrFavoriteBooks.remove(at: index)
                    break
                }
            }
            self.tableView.reloadData()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()})
    }
}

