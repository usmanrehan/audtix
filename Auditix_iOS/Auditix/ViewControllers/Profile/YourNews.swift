//
//  YourNews.swift
//  Auditix
//
//  Created by shardha on 1/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol YourNewsDelegate {
    func didTapOnThisNews(with subscribedNews: NewsListModel)
    func didTapOnThisNews(with downloadedEpisode: NewsEpisodeModel,index: Int)
    func navigateToSubscribedNewsList()
}

class YourNews: UIViewController {
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    var delegate: YourNewsDelegate? = nil
    
    var arrayDownloadedNewsEpisodes = Array<NewsEpisodeModel>()
    
    var arrNews = Array<NewsListModel>()
    var arrDownloadedNewsEpisodes = Array<NewsEpisodeModel>()
    
    var showingWinless: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dummyViewHeight = CGFloat(40)
        self.tableview.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableview.bounds.size.width, height: dummyViewHeight))
        self.tableview.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        self.tableview.register(UINib(nibName: "PodcastCollection", bundle: nil), forCellReuseIdentifier: "PodcastCollection")
        self.tableview.register(UINib(nibName: "PodcastDownloadedCell", bundle: nil), forCellReuseIdentifier: "PodcastDownloadedCell")
        // Do any additional setup after loading the view.
        self.addNotificationObservers()
        
        shadow.layer.shadowColor = UIColor.black.cgColor
        shadow.layer.shadowOpacity = 1
        shadow.layer.shadowOffset = CGSize.zero
        shadow.layer.shadowRadius = 10
        self.getNewsDownloadedEpisodes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Load subscribed news channel
        self.getSubscribedNews()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reloadSubscribedNews(_:)),
                                               name: Constants.subscribedNewsNotification,
                                               object: nil)
    }
    
    @objc func reloadSubscribedNews(_ data: NSNotification) {
        self.getSubscribedNews()
    }
}

// MARK: - API CALLING
extension YourNews {
    public func getSubscribedNews() {
        Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.GetAllNewsSubscriptions(success: { (responseArray) in
            Utility.main.hideLoader()
            self.arrNews.removeAll()
            for item in responseArray {
                let model = NewsListModel(value: item)
                self.arrNews.append(model)
            }
            self.tableview.reloadData()
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
}

// MARK: - UITableView DataSource Delegate
extension YourNews: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.arrDownloadedNewsEpisodes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastCollection") as? PodcastCollection else { return UITableViewCell() }
            cell.collectionView.register(UINib(nibName: "PodcastCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PodcastCollectionCell")
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            cell.hero.modifiers = [.arc]
            cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastDownloadedCell") as? PodcastDownloadedCell else { return UITableViewCell() }
            cell.labelTitle.text = "\(MiscStrings.episodeNumber.text) \(self.arrDownloadedNewsEpisodes[indexPath.row].NewsEpisodeID)"
            cell.labelNarrator.text = "\(String(describing: self.arrDownloadedNewsEpisodes[indexPath.row].NarratorName ?? ""))"
            let createdAt = self.arrDownloadedNewsEpisodes[indexPath.row].CreatedDate ?? "2018-06-23T14:03:34"
            if createdAt.contains("."){
                cell.labelDownloadedDate.text = Utility.stringDateFormatter(dateStr: createdAt, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.zzz", formatteddate: "d MMM yyyy")
            }
            else{
                cell.labelDownloadedDate.text = Utility.stringDateFormatter(dateStr: createdAt, dateFormat: "yyyy-MM-dd'T'HH:mm:ss", formatteddate: "d MMM yyyy")
            }
            let imageURLString = "\(self.arrDownloadedNewsEpisodes[indexPath.row].CoverImage!)"
            
            if let imageURL = URL(string: imageURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                cell.imageViewPodcast.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            else {
                cell.imageViewPodcast.image = #imageLiteral(resourceName: "placeholder_image")
            }
            cell.tag = indexPath.row
            cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
            cell.rightSwipeSettings.transition = .rotate3D
            cell.delegate = self
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tablesHeader: BooksHeader = .fromNib()
        tablesHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        
        if section == 0 {
            tablesHeader.labelTitle.text = AlertMessages.news.message.uppercased()
            tablesHeader.buttonSeeAll.isHidden = false
            tablesHeader.buttonSeeAll.addTarget(self, action: #selector(self.onBtnSeeAllSubscribedNews), for: .touchUpInside)
        } else {
            tablesHeader.labelTitle.text = AlertMessages.downloads.message.uppercased()
            tablesHeader.buttonSeeAll.isHidden = true
        }
        return tablesHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return (self.view.frame.size.width / 4.0) + 60.0 //Add padding for subscribe button and text label
        default:
            return self.view.frame.size.height * 0.25
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            self.delegate?.didTapOnThisNews(with: self.arrDownloadedNewsEpisodes[indexPath.row], index: indexPath.row)
            //self.delegate?.didTapOnThisNews(with: self.arrDownloadedNewsEpisodes[indexPath.row])
            //self.navigateToEpisodeDetails(indexPath)
        }
    }
    private func navigateToEpisodeDetails(_ indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "NewsEpisodeDetails") as? NewsEpisodeDetails {
            let newsModel = NewsListModel()
            newsModel.NewsID = self.arrDownloadedNewsEpisodes[indexPath.row].NewsID
            newsModel.ImageUrl = "\(self.arrDownloadedNewsEpisodes[indexPath.row].CoverImage!)"
            newsModel.episodes.append(self.arrDownloadedNewsEpisodes[indexPath.row])
            controller.objNews = newsModel
            controller.episodeIndex = 0
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc func onBtnSeeAllSubscribedNews(){
        self.delegate?.navigateToSubscribedNewsList()
    }
}
// MARK
extension YourNews: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrNews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PodcastCollectionCell", for: indexPath) as? PodcastCollectionCell else { return UICollectionViewCell() }
        cell.labelTitle.textColor = UIColor.darkGray
        
        if self.showingWinless {
            let podcast = self.arrNews[indexPath.row]
            cell.labelTitle.text = podcast.Name ?? "-"
            if let imageURL = podcast.ImageUrl {
                cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
            cell.btnSubscription.tag = indexPath.row
            cell.btnSubscription.addTarget(self, action: #selector(self.actionSubscribtion(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
        return CGSize(width: width, height: width + 60.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if self.delegate != nil {
            self.delegate?.didTapOnThisNews(with: self.arrNews[indexPath.row])
        }
    }
    @objc func actionSubscribtion(_ sender: UIButton) {
        // call on nusub method
        let index = sender.tag
        if self.arrNews.count > 0{
            self.unSubscribeThisNews(with: self.arrNews[index].NewsID)
        }
    }
}
//MARK:- Webservice
extension YourNews{
    private func getNewsDownloadedEpisodes(){
        var params = [String:Any]()
        params["BookEpisodeIds"] = ""
        params["PodcastEpisodeIds"] = ""
        params["NewsEpisodeIds"] = Utility.main.getIdfromFolders(directoryName: "news")
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getPodcastNewsAndBookByEpisodesId(param: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let response = responseObject as NSDictionary
            guard let CustomNews = response.object(forKey: "NewsEpisode") as? [[String:Any]] else {return}
            self.arrDownloadedNewsEpisodes.removeAll()
            for item in CustomNews {
                let model = NewsEpisodeModel(value: item)
                self.arrDownloadedNewsEpisodes.append(model)
            }
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func unSubscribeThisNews(with newsId: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["NewsId": newsId]
        APIManager.sharedInstance.newsApiManager.UnSubscribeThisNewsChannel(param: param, success: { (isSuccess) in
            Utility.main.hideLoader()
            for (index, subscribedModel) in self.arrNews.enumerated() {
                if subscribedModel.NewsID == newsId {
                    self.arrNews.remove(at: index)
                    break
                }
            }
            self.tableview.reloadSections([0], with: .automatic)
        }, failure: { (failure) in
            Utility.main.hideLoader()
            print(failure.localizedDescription)
        })
    }
}
extension YourNews: MGSwipeTableCellDelegate{
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        print(cell.tag)
        let fileName = self.arrDownloadedNewsEpisodes[cell.tag].NewsEpisodeID
        //print(Utility.main.deleteDownloadedFile(directoryName: "news", fileName: "\(fileName)"))
        if Utility.main.deleteDownloadedFile(directoryName: "news", fileName: "\(fileName)"){
            self.getNewsDownloadedEpisodes()
        }
        return true
    }
}
