//
//  ChangePassword.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class ChangePassword: UIViewController {
    
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var oldPasswordTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func oldPasswordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordInValidShort.message
            
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func newPasswordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordInValidShort.message
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func confirmPasswordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordNotMatchShort.message
        }
        else {
            textfield.errorMessage = ""
        }
    }
    
    @IBAction func actionSubmit(_ sender: Any) {
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
