//
//  BillingDetails.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BillingDetails: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
