//
//  Cart.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

class Cart: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnProceedCheckout: UIButton!
    var bookCartList : Results<BookCartModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        
       self.initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initData() {
         self.bookCartList = BooksRealmHelper.sharedInstance.getAllBooksInCart()
        if(self.bookCartList.count <= 0) {
            self.btnProceedCheckout.isHidden = true
        }
        self.tableView.reloadData()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionProceed(_ sender: UIButton) {
        if(self.bookCartList.count > 0) {
            self.checkoutBooksToLibrary()
        }
    }
    
    func getAllBooksIDsInCommaSeparated() -> String {
        var returnStr: String = ""
        for cartBookItem in self.bookCartList {
            if returnStr.isEmpty {
                returnStr = "\(ParserHelper.convertIntToString(value: cartBookItem.CartBook.BookID))"
            }
            else {
                returnStr = returnStr + ",\(ParserHelper.convertIntToString(value: cartBookItem.CartBook.BookID))"
            }
        }
        return returnStr
    }
    
    @IBAction func actionCartCross(_ sender: UIButton) {
        BooksRealmHelper.sharedInstance.deleteThisBook(cartbook: self.bookCartList[sender.tag])
        self.initData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bookCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as? CartCell  else { return UITableViewCell() }
        cell.BindData(with: self.bookCartList[indexPath.row].CartBook, showCrossBtn: true)
        cell.ButtonCross.tag = indexPath.row
        cell.ButtonCross.addTarget(self, action: #selector(self.actionCartCross(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.20
    }

}

// MARK: - APICALLING
extension Cart
{
    func checkoutBooksToLibrary() {
        let param: [String : Any] = [
            "BookIds" : self.getAllBooksIDsInCommaSeparated()
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.addToLibrary(params: param, success: {
            (responceObj) in
            Utility.main.showToast(message: AlertMessages.addToLibrarySuccss.message)
            Utility.main.hideLoader()
            BooksRealmHelper.sharedInstance.emptyCartList()
            self.navigationController?.popViewController(animated: true)
        }, failure: {
            (failure) in
            Utility.main.hideLoader()
        })
    }
}
