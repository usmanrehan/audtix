//
//  NotificationController.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell

enum NotificationType {
    case normal
    case settings
}

class NotificationController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrPodcastList = Array<PodcastModel>()
    var arrNewsChannelList = Array<NewsListModel>()
    var arrayNotification = [NotificationModel]()
    var notificationType = NotificationType.normal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.getAllNewsPodcasts()
        if self.notificationType == .normal {
            self.getAllNotification()
        }
    }
}

// MARK: - IBAction Methods
extension NotificationController {
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionToggleSwitch(_ sender: UISwitch) {
        let position = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: position) {
            switch indexPath.section {
            case 0:
                let podcast = self.arrPodcastList[indexPath.row]
                if sender.isOn {
                    self.setNotificationSetting(podcast.PodcastId, entityType: 1, sender: sender)
                } else {
                    self.unsetNotificationSetting(podcast.PodcastId, entityType: 1, sender: sender)
                }
                break
            case 1:
                let newsChannel = self.arrNewsChannelList[indexPath.row]
                if sender.isOn {
                    self.setNotificationSetting(newsChannel.NewsID, entityType: 2, sender: sender)
                } else {
                    self.unsetNotificationSetting(newsChannel.NewsID, entityType: 2, sender: sender)
                }
                break
            default:
                break
            }
        }
    }
}

//MARK: - API Methods
extension NotificationController {
    
    private func getAllNewsPodcasts() {
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAllNewsPodcasts(success: { (response) in
            Utility.main.hideLoader()
            if let arrNews = response["News"] as? Array<AnyObject> {
                self.arrNewsChannelList.removeAll()
                for item in arrNews {
                    let model = NewsListModel(value: item)
                    self.arrNewsChannelList.append(model)
                }
            }
            if let arrPodcasts = response["Podcast"] as? Array<AnyObject> {
                self.arrPodcastList.removeAll()
                for item in arrPodcasts {
                    let model = PodcastModel(value: item)
                    self.arrPodcastList.append(model)
                }
            }
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
    
    private func setNotificationSetting(_ entityId: Int, entityType: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "EntityId": entityId,
            "EntityType": entityType
        ]
        APIManager.sharedInstance.miscellaneousAPIManager.setNotificationSettings(with: params, success: { (response) in
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
            sender.isOn = true
        }
    }
    
    private func unsetNotificationSetting(_ entityId: Int, entityType: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "EntityId": entityId,
            "EntityType": entityType
        ]
        APIManager.sharedInstance.miscellaneousAPIManager.unsetNotificationSettings(with: params, success: { (response) in
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
            sender.isOn = false
        }
    }
    private func getAllNotification() {
        let param: [String:Any] = ["devToken": Constants.DEVICE_TOKEN]
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAllNotifications(param: param, success: { (responseArray) in
            Utility.main.hideLoader()
            self.arrayNotification.removeAll()
            for item in responseArray {
                self.arrayNotification.append(NotificationModel(value: item as! [String : Any]))
            }
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
}

// MARK: - UITableView DataSource Delegate
extension NotificationController: UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch self.notificationType {
        case .normal:
            return 1
        case .settings:
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch self.notificationType {
        case .normal:
            return ""
        case .settings:
            switch section {
            case 0:
                return MiscStrings.podcastChannels.text
            case 1:
                return MiscStrings.newsChannels.text
            default:
                return nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.notificationType {
        case .normal:
            return self.arrayNotification.count
        case .settings:
            switch section {
            case 0:
                return self.arrPodcastList.count
            case 1:
                return self.arrNewsChannelList.count
            default:
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.notificationType {
        case .normal:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell else { return UITableViewCell() }
            cell.labelNotification.text = self.arrayNotification[indexPath.item].NotificationMessage
            cell.rightButtons = [MGSwipeButton(title: "", icon:  #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
            cell.rightSwipeSettings.transition = .rotate3D
            cell.tag = indexPath.item
            cell.delegate = self
            return cell
        case .settings:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AutoDownloadCellIdentifier", for: indexPath) as? AutoDownloadViewCell else { return UITableViewCell() }
            
            if indexPath.section == 0 {
                let podcast = self.arrPodcastList[indexPath.row]
                cell.titleLabel.text = podcast.Name
                cell.toggleSwith.isOn = podcast.NotificationEnabled
                if let imageURL = podcast.ImageUrl {
                    cell.channelImageView.sd_setImage(with: URL(string: imageURL), placeholderImage:  #imageLiteral(resourceName: "placeholder_image"))
                }
            } else {
                let newsChannel = self.arrNewsChannelList[indexPath.row]
                cell.titleLabel.text = newsChannel.Name
                cell.toggleSwith.isOn = newsChannel.NotificationEnabled
                if let imageURL = newsChannel.ImageUrl {
                    cell.channelImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.notificationType {
        case .normal:
            return UITableViewAutomaticDimension
        case .settings:
            return 100
        }
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        switch self.notificationType {
        case .normal:
            return true
        case .settings:
            return false
        }
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        self.removFromNotification(with: cell.tag)
        return true
    }
    
    func removFromNotification(with index: Int) {
        let notificationID: Int = self.arrayNotification[index].NotificationID
        let param: [String : Any] = [
            "NotificationId" : notificationID
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.removeNotification(param: param, success: {
            (responceObj) in
            Utility.main.hideLoader()
            self.arrayNotification.remove(at: index)
            self.tableView.reloadData()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()})
    }
}


