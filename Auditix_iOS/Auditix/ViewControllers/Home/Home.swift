//
//  Home.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseInstanceID
import LGSideMenuController

import Alamofire
class Home: UIViewController, SideMenuProtocol {
    
    @IBOutlet weak var constraintBooksTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintNewsTrailing: NSLayoutConstraint!
    
    var arrNewNews = Array<NewsEpisodeModel>()
    var arrNewPodcast = Array<PodcastPollingModel>()
    var downloader: Downloader? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.view.semanticContentAttribute = .forceLeftToRight
        // constraint check for differenet devices
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                self.constraintNewsTrailing.constant = -48
                self.constraintBooksTrailing.constant = -48
            case 1334:
                print("iPhone 6/6S/7/8")
                self.constraintNewsTrailing.constant = -53
                self.constraintBooksTrailing.constant = -53
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                self.constraintNewsTrailing.constant = -60
                self.constraintBooksTrailing.constant = -60
            case 2436:
                print("iPhone X")
                self.constraintNewsTrailing.constant = -70
                self.constraintBooksTrailing.constant = -70
                
                self.view.layoutIfNeeded()
            default:
                print("unknown")
            }
        }
        
        self.registerUserDevice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Singleton.sharedInstance.isComingFromHome = true
        
        self.startTimer()
        
        //self.getAdvertise()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapped(type: SideMenuOptions) {
        print(type)
        let when = DispatchTime.now() + 0.1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            switch type {
            case .Home:
                break
            case .Dashboard:
                if AppStateManager.sharedInstance.isGuestLogin() {
                    Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
                    return
                }
                if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Dashboard") as? Dashboard {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            case .Notifications:
                if AppStateManager.sharedInstance.isGuestLogin() {
                    Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
                    return
                }
                if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            case .Settings:
                if let svc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Settings") as? Settings {
                    self.navigationController?.pushViewController(svc, animated: true)
                }
                break
            case .About:
                if let avc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "AboutAndTnC") as? AboutAndTnC {
                    avc.TitleText = AlertMessages.aboutUsTitle.message
                    avc.AboutUsOrTnC = 0
                    self.navigationController?.pushViewController(avc, animated: true)
                }
                break
            case .Logout:
                if AppStateManager.sharedInstance.isGuestLogin() {
                    BottomView.sharedInstance.actionClose(UIButton())
                    //                     AppStateManager.sharedInstance.logoutUser()
                    Constants.APP_DELEGATE.showLoginScreen()
                    return
                }
                Utility.main.showAlert(message: AlertMessages.LogoutMessage.message, title: AlertTitles.alert.message, controller: self, completionHandler: {
                    (didTapYes, _) in
                    if(didTapYes != nil) {
                        BottomView.sharedInstance.actionClose(UIButton())
                        AppStateManager.sharedInstance.logoutUser()
                    }
                })
                break
            }
        }
    }
    
    
    weak var timer: Timer?
    var timerDispatchSourceTimer : DispatchSourceTimer?
    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] _ in
            // do something here
            print("time Start")
            self?.getNewsPodcastPolling()
            //Utility.main.getIdfromFolders()
        }
    }
    func stopTimer() {
        timer?.invalidate()
        //timerDispatchSourceTimer?.suspend() // if you want to suspend timer
        timerDispatchSourceTimer?.cancel()
    }
    
    private func getNewsPodcastPolling() {
        APIManager.sharedInstance.miscellaneousAPIManager.getNewsPodcastPolling(param: Parameters(), success: { (responseObject) in
            if let newNews = responseObject["News"] as? Array<AnyObject> {
                self.arrNewNews.removeAll()
                for item in newNews {
                    let model = NewsEpisodeModel(value: item as! [String: AnyObject])
                    self.arrNewNews.append(model)
                }
            }
            if let newPodcast = responseObject["Podcast"] as? Array<AnyObject> {
                self.arrNewPodcast.removeAll()
                for item in newPodcast {
                    let model = PodcastPollingModel(value: item as! [String: AnyObject])
                    self.arrNewPodcast.append(model)
                }
            }
            print(self.arrNewNews.count)
            print(self.arrNewPodcast.count)
            
            self.checkAlreadyDownloadedPodcast()
            self.checkAlreadyDownloadedNews()
            
        }, failure: { (error) in
            
        })
    }
    
    private func checkAlreadyDownloadedPodcast() {
        for item in self.arrNewPodcast {
            for episode in item.PodcastEpisodes {
                let podcast = episode
                if !Utility.main.isThisPodcastFileExist(with: podcast.PodcastId, EpisodeID: podcast.PodcastEpisodeID) {
                    self.processStartDownloadPodCast(podcast: podcast, trackID: item.TrackId)
                }else {
                    
                }
                
                
            }
            
        }
    }
    
    private func processStartDownloadPodCast(podcast: PodcastEpisode, trackID: Int) {
        if let downloadPath = podcast.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let fileURL = Utility.main.returnPodcastFileUrl(with: podcast.PodcastId, EpisodeID: podcast.PodcastEpisodeID)
            self.downloader = Downloader.init(downloadPath, downloadPath, trackID, fileURL.absoluteString)
            self.downloader?.startDownload()
        }
    }
    private func checkAlreadyDownloadedNews() {
        for item in self.arrNewNews {
            let objNews = item
            if !Utility.main.isThisNewsFileExist(with: objNews.NewsID, EpisodeID: objNews.NewsEpisodeID) {
                  //Utility.main.showAlert(message: "File is already downlaoded", title: "Message")
                self.processStartDownloadNews(objNews: objNews)
            }else {
                
            }
            
        }
    }
    private func processStartDownloadNews(objNews: NewsEpisodeModel) {
        if let downloadPath = objNews.FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let fileUrl = Utility.main.returnNewsFileUrl(with: objNews.NewsID, EpisodeID: objNews.NewsEpisodeID)
            self.downloader = Downloader.init(downloadPath, downloadPath, objNews.NewsID, fileUrl.absoluteString)
            self.downloader?.startDownload()
        }
    }
}

//MARK: - IBAction Methods
extension Home {
    @IBAction func actionMenu(_ sender: UIButton) {
        self.sideMenuController?.showLeftView()
    }
    
    @IBAction func actionNotification(_ sender: UIButton) {
        if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
            dvc.notificationType = .normal
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionBook(_ sender: Any) {
        if let dvc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "TabBar") as? TabBar {
            dvc.selectedIndex = 3
            let navigationController = UINavigationController(rootViewController: dvc)
            navigationController.navigationBar.isHidden = true
            self.sideMenuController?.rootViewController = navigationController
            //self.present(dvc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionNews(_ sender: Any) {
        if let dvc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "TabBar") as? TabBar {
            dvc.selectedIndex = 0
            let navigationController = UINavigationController(rootViewController: dvc)
            navigationController.navigationBar.isHidden = true
            self.sideMenuController?.rootViewController = navigationController
            //self.present(dvc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionPodcast(_ sender: Any) {
        if let dvc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "TabBar") as? TabBar {
            dvc.selectedIndex = 1
            let navigationController = UINavigationController(rootViewController: dvc)
            navigationController.navigationBar.isHidden = true
            self.sideMenuController?.rootViewController = navigationController
            //self.present(dvc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionProfile(_ sender: Any) {
        if let dvc = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "TabBar") as? TabBar {
            dvc.selectedIndex = 4
            let navigationController = UINavigationController(rootViewController: dvc)
            navigationController.navigationBar.isHidden = true
            self.sideMenuController?.rootViewController = navigationController
            //self.present(dvc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}


//MARK: - API Methods
extension Home {
    private func registerUserDevice() {
        if let token = FIRInstanceID.instanceID().token() {
            
            let user = AppStateManager.sharedInstance.loggedInUser
            
            guard user != nil else { return }
            
            Utility.main.showLoader()
            
            let params: [String: Any] = [
                "DeviceName": Constants.DEVICE_NAME,
                "UniversalDeviceID": Constants.DEVICE_UDID,
                "IsAndroidPlatform": false,
                "IsPlayStore": false,
                "AccountID": (user?.AccountID)!,
                "IsProduction": true,
                "AuthToken": token,
                "IsGuest": false
            ]
            
            APIManager.sharedInstance.usersAPIManager.registerDevice(params: params, success: { (responseObject) in
                Utility.main.hideLoader()
                print("Device registered for push notification")
            }) { (error) in
                Utility.main.hideLoader()
                print("Failded to registe device for push notification")
            }
        }
    }
//    private func getAdvertise() {
//        //MARK#
//        let AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
//        let Country = AppDelegate.shared.Country
//        let params : [String:Any] = ["AccountID":AccountID,"Country":Country]
//        APIManager.sharedInstance.miscellaneousAPIManager.getAdvertise(params: params, success: { (successArray) in
//            Utility.main.hideLoader()
//            let singleton = Singleton.sharedInstance
//            for advertiseItem in successArray {
//                singleton.currentAdvertiseArray.append(Advertise(value: advertiseItem as! [String : Any]))
//            }
//            print("advertisement \(singleton.currentAdvertiseArray)")
//        }) { (failure) in
//            Utility.main.hideLoader()
//        }
//    }
}

