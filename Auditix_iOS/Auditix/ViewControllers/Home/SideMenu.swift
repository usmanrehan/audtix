//
//  SideMenu.swift
//  Auditix
//
//  Created by shardha on 1/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import LGSideMenuController
enum SideMenuOptions {
    case Home
    case Dashboard
    case Notifications
    case Settings
    case About
    case Logout
}

protocol SideMenuProtocol {
    func didTapped(type: SideMenuOptions)
}
class SideMenu: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var forgroundImageView: UIImageView!
    @IBOutlet weak var LabelEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var LabelName: UILabel!
    
    var dataOptions = [String]()
    var dataImages = ["home", "settings", "about","about","Change Language","logout"]
    var delegate: SideMenuProtocol? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // self.tableView.register(SideBarCell.self, forCellReuseIdentifier: "SideBarCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.LabelEmail.text = ""
        if let userModel = AppStateManager.sharedInstance.loggedInUser {
            self.LabelName.text = userModel.FullName
        }
        if AppStateManager.sharedInstance.isGuestLogin() {
            dataOptions = [MiscStrings.home.text,MiscStrings.settings.text,MiscStrings.about.text,MiscStrings.contactUs.text,MiscStrings.changeLanguage.text,MiscStrings.login.text]
        } else {
            dataOptions = [MiscStrings.home.text,MiscStrings.settings.text,MiscStrings.about.text,MiscStrings.contactUs.text,MiscStrings.changeLanguage.text,MiscStrings.logOut.text]
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTheUserName), name: NSNotification.Name(rawValue: Constants.editProfileDoneChangeTheSideMenuName), object: nil)
    }
    @objc func updateTheUserName( data: NSNotification) {
        if let userModel = AppStateManager.sharedInstance.loggedInUser {
            self.LabelName.text = userModel.FullName
            if let imageURLStr = AppStateManager.sharedInstance.loggedInUser.ImageName {
                if let imageURL = URL(string: (Constants.ImageBaseURL + imageURLStr).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    self.forgroundImageView.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "userProfile"))
                    self.backgroundImageView.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "userProfile"))
                }
                else {
                    self.forgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
                    self.backgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
                }
            }
            else {
                self.forgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
                self.backgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let imageURLStr = AppStateManager.sharedInstance.loggedInUser.ImageName {
            if let imageURL = URL(string: (Constants.ImageBaseURL + imageURLStr).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                self.forgroundImageView.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "userProfile"))
                self.backgroundImageView.sd_setImage(with: imageURL, placeholderImage:  #imageLiteral(resourceName: "userProfile"))
            }
            else {
                self.forgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
                self.backgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
            }
        }
        else {
            self.forgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
            self.backgroundImageView.image =  #imageLiteral(resourceName: "userProfile")
        }
        if let userModel = AppStateManager.sharedInstance.loggedInUser {
            self.LabelName.text = userModel.FullName
            
        }
        //        self.sideMenuController?.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataOptions.count > 0 && dataImages.count > 0 {
            return self.dataOptions.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //ChangeLanguage cell
        if indexPath.item == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideBarCellLang") as? SideBarCellLang else { return UITableViewCell() }
            if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
                cell.switchLanguage.isOn = true
            }
            else{
                cell.switchLanguage.isOn = false
            }
            cell.switchLanguage.addTarget(self, action: #selector(self.onLangChange(sender:)), for: .touchUpInside)
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideBarCell") as? SideBarCell else { return UITableViewCell() }
            cell.imageViewMenu.image = UIImage(named: self.dataImages[indexPath.row]) ?? UIImage()
            cell.labelMenu.text = self.dataOptions[indexPath.row]
            if indexPath.row == 2 {
                //            cell.viewCounter.isHidden = false
                cell.viewCounter.isHidden = true
                // notification count
                cell.viewCounter.textColor = UIColor.white
                cell.viewCounter.text = "1"
            }else {
                cell.viewCounter.isHidden = true
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.removePlayerAndStopPlaying), object: nil)
        switch indexPath.row {
        case 0:
            // load home view controller
            loadHomeViewController()
            break
            //        case 1:
            //            // dashboard view controller
            //            loadDashboardViewController()
            //            break
            //        case 2:
            //            // notification view controller
            //            loadNotificationViewController()
        //            break
        case 1:
            // settings view controller
            loadSettingsViewController()
            break
        case 2:
            // about view controller
            loadAboutViewController()
            break
        case 3:
            // contactUs view controller
            loadConatactUsController()
            break
        case 5:
            // logout view controller
            logout()
            break
        default:
            break
        }
        
    }
    
    @objc func onLangChange(sender: UISwitch){
        if sender.isOn{
            //print("english")
            self.askForRestart(lang: "en")
        }
        else{
            //print("arabic")
            self.askForRestart(lang: "ar")
        }
    }
    private func askForRestart(lang: String){
        let alertController = UIAlertController(title: AlertTitles.alert.message, message: MiscStrings.pleaseRestartTheApplication.text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default) {
            UIAlertAction in
            if lang == "en"{
                self.setLocaleWith(culture: 0)
            }
            else{
                self.setLocaleWith(culture: 1)
            }
        }
        let cancelAction = UIAlertAction(title: MiscStrings.cancel.text, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.tableView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension SideMenu {
    
    func loadHomeViewController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            let rootViewController = navController.viewControllers.first
            if (rootViewController?.isKind(of: Home.self))! {
                self.sideMenuController?.hideLeftView()
                return
            } else {
                let controller = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "Home") as! Home
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.navigationBar.isHidden = true
                navigationController.isHeroEnabled = true
                
                self.sideMenuController?.rootViewController = navigationController
            }
        }
        
        self.sideMenuController?.hideLeftView()
    }
    
    func loadDashboardViewController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Dashboard") as? Dashboard {
                navController.pushViewController(dvc, animated: true)
            }
        }
        self.sideMenuController?.hideLeftView()
    }
    
    func loadConatactUsController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "ContactUs") as? ContactUs {
                navController.pushViewController(dvc, animated: true)
            }
        }
        self.sideMenuController?.hideLeftView()
    }
    
    func loadNotificationViewController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            if let dvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
                navController.pushViewController(dvc, animated: true)
            }
        }
        
        self.sideMenuController?.hideLeftView()
    }
    
    func loadSettingsViewController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            if let svc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Settings") as? Settings {
                navController.pushViewController(svc, animated: true)
            }
        }
        
        self.sideMenuController?.hideLeftView()
    }
    
    func loadAboutViewController() {
        if let navController = self.sideMenuController?.rootViewController as? UINavigationController {
            if let avc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "AboutAndTnC") as? AboutAndTnC {
                avc.TitleText = AlertMessages.aboutUsTitle.message
                avc.AboutUsOrTnC = 0
                navController.pushViewController(avc, animated: true)
            }
        }
        
        self.sideMenuController?.hideLeftView()
    }
    
    func logout() {
        if AppStateManager.sharedInstance.isGuestLogin() {
            BottomView.sharedInstance.actionClose(UIButton())
            //                     AppStateManager.sharedInstance.logoutUser()
            Constants.APP_DELEGATE.showLoginScreen()
            return
        }
        Utility.main.showAlert(message: AlertMessages.LogoutMessage.message , title: AlertTitles.alert.message, controller: self, completionHandler: {
            (didTapYes, _) in
            if(didTapYes != nil) {
                BottomView.sharedInstance.actionClose(UIButton())
                AppStateManager.sharedInstance.logoutUser()
            }
        })
    }
}

extension SideMenu: LGSideMenuDelegate {
    func willShowLeftView(_ leftView: UIView, sideMenuController: LGSideMenuController) {
        self.LabelName.text = AppStateManager.sharedInstance.loggedInUser.FullName
    }
}
extension SideMenu{
    private func setLocaleWith(culture: Int){
        var param = [String:Any]()
        param["Culture"] = culture
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getChangeLanguage(params: param, success: { (bool) in
            Utility.main.hideLoader()
            if culture == 0{
                LanguageManager.sharedInstance.setLocale("en")
                exit(0)
            }
            else{
                LanguageManager.sharedInstance.setLocale("ar")
                exit(0)
            }
            
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}
