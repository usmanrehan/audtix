//
//  TabBar.swift
//  Auditix
//
//  Created by shardha on 1/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class TabBar: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        // Do any additional setup after loading the view.
     //   self.selectedIndex = 2
        
//        BottomView.sharedInstance.showBottomView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item \(item)")
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if ((self.viewControllers![4] == viewController) && AppStateManager.sharedInstance.isGuestLogin()) {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return false
        }
        return true
    }
    
}

