//
//  Books.swift
//  Auditix
//
//  Created by shardha on 1/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Windless
import ObjectMapper

class Books: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let heroID = ""
    var categoryArray = [BookCategory]()
    
    let arrSections = [MiscStrings.recommended.text, MiscStrings.categories.text]
    var arrFeaturedBooks = Array<BookModel>()
    var arrBookCategories = Array<BookCategory>()
    
    var allBookCateID = 0
    var mostRecommendCateID = 0
    var bestOfTheBestCateID = 0
    
    var filterStr: String = ""
    
    var showingWindless: Bool = true
    
    var allBooksArray = [BookModel]()
    var isFilter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //Following three lines of code is to disable sticky header in tableview.
        let dummyViewHeight = CGFloat(40)
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: dummyViewHeight))
        self.tableView.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "NewsCollection", bundle: nil), forCellReuseIdentifier: "NewsCollection")
        self.tableView.register(UINib(nibName: "BooksCollection", bundle: nil), forCellReuseIdentifier: "BooksCollection")
        self.tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")

        if let bookFilter  = sideMenuController?.rightViewController as? BookFilter {
            bookFilter.delegate = self
        }
        
//        self.tableView.windless
//            .apply {
//                $0.beginTime = 0.5
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//                $0.animationLayerColor = Constants.THEME_ORANGE_COLOR
//            }
//            .start()
        
//        self.getBookCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Get recommended books
        self.getBookCategories()
        self.getFeaturedBooks()
    }
}

// MARK: - HELPER METHODS
extension Books {
    func navigateToBookDetailsWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            controller.currentBook = self.arrFeaturedBooks[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToAllBooksWith(_ indexPath: IndexPath) {
        if let controller = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookAll") as? BookAll {
            let category = self.arrBookCategories[indexPath.row]
            controller.categoryID = category.BookCategoryID
            controller.titleString = category.Name ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK:- IB ACTIONS
extension Books
{
    @IBAction func actionMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }

    @IBAction func actionBack(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCart(_ sender: Any) {
        if let cart = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Cart") as? Cart {
            self.navigationController?.pushViewController(cart, animated: true)
        }
    }
    
    @IBAction func actionFilter(_ sender: Any) {
        let bookFilter = Constants.bookFilterRightSideMenu
        bookFilter.delegate = self
        self.sideMenuController?.rightViewController = bookFilter
        self.sideMenuController?.showRightView()
    }
    
    @IBAction func actionSeeAll(_ sender: UIButton) {
        if let bookAll = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookAll") as? BookAll {
            bookAll.flag = false
            self.navigationController?.pushViewController(bookAll, animated: true)
        }
    }
}

// MARK:- API CALLING
extension Books {
    private func getBookCategories() {
        Utility.main.showLoader()
        let param: [String : Any] = [:]
        APIManager.sharedInstance.bookApiManager.getBookCategories(params: param, success: { (responseArray) in
            Utility.main.hideLoader()
            
            self.arrBookCategories.removeAll()
            for item in responseArray {
                let model = BookCategory(value: item as! [String: Any])
                self.arrBookCategories.append(model)
            }
            
            self.showingWindless = false
            self.tableView.windless.end()
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getFeaturedBooks() {
        let params: [String: Any] = [:]
        APIManager.sharedInstance.bookApiManager.getFeaturedBooks(with: params, success: { (responseObject) in
            if let featureBooks = responseObject["Books"] as? Array<AnyObject> {
                self.arrFeaturedBooks.removeAll()
                for item in featureBooks {
                    let model = BookModel(value: item as! [String: Any])
                    self.arrFeaturedBooks.append(model)
                }
            }
            self.showingWindless = false
            self.tableView.windless.end()
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}

// MARK: - BookFilter Delegate
extension Books: BookFilterDelegate{
    func didApplyFilterSelect(filterData: FilterDataSet) {
        if filterData.shouldFilter{
            self.getFilteredBooks(data: filterData)
        }
        else{
            self.isFilter = false
            self.tableView.reloadData()
        }
    }
}
// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension Books {
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isFilter{
            return self.arrSections.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isFilter{
            return 1
        }
        else{
            return self.allBooksArray.count
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !self.isFilter{
            let booksHeader: BooksHeader = .fromNib()
            booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
            if section == 0{
                booksHeader.buttonSeeAll.isHidden = false
            }
            else{
                booksHeader.buttonSeeAll.isHidden = true
            }
            booksHeader.buttonSeeAll.tag = section
            booksHeader.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAll(_:)), for: .touchUpInside)
            
            if !self.showingWindless {
                booksHeader.labelTitle.text = self.arrSections[section].uppercased()
                //booksHeader.labelTitle.text = self.categoryArray[section].CategoryName?.uppercased()
            }
            return booksHeader
        }
        else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !self.isFilter{
            return 50
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.isFilter{
            switch indexPath.section {
            case 0:
                return (self.view.frame.size.width / 4.0) + 30.0 // Add padding for title
            case 1:
                let height = ((self.view.frame.size.width - (8 * 2)) / 3.0) + 30.0 //Add padding text label
                return ceil(CGFloat(self.arrBookCategories.count) / 3.0) * height
            default:
                return 0
            }
        }
        else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !self.isFilter{
            if indexPath.section == 0 { //Most recommended books
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BooksCollection") as? BooksCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "BookCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BookCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                
                cell.hero.modifiers = [.arc]
                cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
                
                return cell
            }
            else { //All categories
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCollection") as? NewsCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "NewsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NewsCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                
                cell.hero.modifiers = [.arc]
                cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
                
                return cell
            }
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as? CartCell else { return UITableViewCell() }
            cell.BindData(with: self.allBooksArray[indexPath.row], showCrossBtn: false)
            cell.hero.modifiers = [.arc]
            cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isFilter{
            self.openBookDetail(with: self.allBooksArray[indexPath.row], indexPath: indexPath)
        }
     }
    
    func openBookDetail(with book: BookModel, indexPath: IndexPath) {
        if let bookDetail = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            bookDetail.currentBook = book
            bookDetail.heroID = "\(indexPath.item) + \(indexPath.section)"
            bookDetail.view.hero.modifiers = [.arc]
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
}
// MARK: - UICOLLECTIONVIEW DATASOURCE & DELEGATE
extension Books
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0: //Recommended Books
            return self.arrFeaturedBooks.count
            //return self.showingWindless ? 10 : self.arrFeaturedBooks.count
        case 1: //All Categories
            return self.arrBookCategories.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0: //Recommended Books
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCollectionCell", for: indexPath) as? BookCollectionCell else { return UICollectionViewCell() }
            if !self.showingWindless {
                cell.BindData(with: self.arrFeaturedBooks[indexPath.row])
            }
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionCell", for: indexPath) as? NewsCollectionCell else { return UICollectionViewCell() }
            cell.BindData(with: self.arrBookCategories[indexPath.row])
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 0:
            let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
            return CGSize(width: width, height: width + 30)
        case 1:
            let width = (collectionView.frame.size.width - (8 * 2)) / 3.0
            return CGSize(width: width, height: width + 30.0)
        default:
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch collectionView.tag {
        case 0: //Most Recommended
            self.navigateToBookDetailsWith(indexPath)
            break
        case 1: //Categories
            self.navigateToAllBooksWith(indexPath)
            break
        default:
            break
        }
    }
}
//MARK:- GetFilterResponse
extension Books{
    private func getFilteredBooks(data:FilterDataSet){
        let MinDuration = data.minDuration
        let MaxDuration = data.maxDuration
        let MinSubscriber = data.minSubscribers
        let MaxSubscriber = data.maxSubscribers
        let Type = 2 //books
        let pageNumber = 1
        let count = 100
        let CountryIds = data.commaSeparatedLocationIDs
        let CategoryIds = data.commaSeparatedEntityIDs
        var params : [String:Any] = [
            "MinDuration":MinDuration,
            "MaxDuration":MaxDuration,
            "MinSubscriber":MinSubscriber,
            "MaxSubscriber":MaxSubscriber,
            "Type":Type,
            "pageNumber":pageNumber,
            "count":count
        ]
        if Validation.validateStringLength(CountryIds){
            params["CountryIds"] = CountryIds
        }
        if Validation.validateStringLength(CategoryIds){
            params["CategoryIds"] = CategoryIds
        }
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getFilterResponse(params: params, success: { (responseArray) in
            Utility.main.hideLoader()
            //BookModel
            self.allBooksArray.removeAll()
            let booksArray = responseArray
            for bookItem in booksArray {
                self.allBooksArray.append(BookModel(value: bookItem))
            }
            self.isFilter = true
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}
