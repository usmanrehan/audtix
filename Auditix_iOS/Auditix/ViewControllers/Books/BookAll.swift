//
//  BookAll.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/15/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BookAll: UIViewController {

    @IBOutlet public weak var LabelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var allBooksArray = [BookModel]()
    
    public var categoryID: Int = 0
    public var titleString = MiscStrings.books.text
    var flag = true //for category id
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.LabelTitle.text = self.titleString
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        if self.flag{
            self.getAllBooks(with: categoryID)
        }
        else{
            self.getFeaturedBooks()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func openBookDetail(with book: BookModel, indexPath: IndexPath) {
        if let bookDetail = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookDetail") as? BookDetail {
            bookDetail.currentBook = book
            bookDetail.heroID = "\(indexPath.item) + \(indexPath.section)"
            bookDetail.view.hero.modifiers = [.arc]
            self.navigationController?.pushViewController(bookDetail, animated: true)
        }
    }
    
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - API CALLING
extension BookAll
{
    func getAllBooks(with categoryID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["categoryId" : ParserHelper.convertIntToString(value: self.categoryID),
                                     "PageNo" : "1",
                                     "PageSize" : Constants.PAGINATION_PAGE_SIZE,
                                     "culture" : "0"]
        
        APIManager.sharedInstance.bookApiManager.getCategoryIDBooks(params: param, success:{
            (responceDic) in
            Utility.main.hideLoader()
            if let booksArray = responceDic["Books"] as? NSArray {
                for bookItem in booksArray {
                    self.allBooksArray.append(BookModel(value: bookItem as! [String : Any]))
                }
            }
            self.tableView.reloadData()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()
        })
    }
    private func getFeaturedBooks() {
        let params: [String: Any] = [:]
        APIManager.sharedInstance.bookApiManager.getFeaturedBooks(with: params, success: { (responseObject) in
            if let featureBooks = responseObject["Books"] as? Array<AnyObject> {
                self.allBooksArray.removeAll()
                for item in featureBooks {
                    let model = BookModel(value: item as! [String: Any])
                    self.allBooksArray.append(model)
                }
            }
            self.tableView.windless.end()
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
// MARK: - UITableView Datasource & Delegate
extension BookAll: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allBooksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as? CartCell else { return UITableViewCell() }
        cell.BindData(with: self.allBooksArray[indexPath.row], showCrossBtn: false)
        cell.hero.modifiers = [.arc]
        cell.hero.id = "\(indexPath.item) + \(indexPath.section)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.openBookDetail(with: self.allBooksArray[indexPath.row], indexPath: indexPath)
    }
}
