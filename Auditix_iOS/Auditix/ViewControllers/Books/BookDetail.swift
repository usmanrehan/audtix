//
//  BookDetail.swift
//  Auditix
//
//  Created by shardha on 1/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift
import UAProgressView

struct ChapterDownloading {
    var isDownloading = false
    var isDownloaded = false
    var chapter: BookChapter!
    var progress: Float = 0.0
}


class BookDetail: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var LabelTitle: UILabel!
    var btnDownloadAll = UIButton()
    var heroID = ""
    
    public var currentBook: BookModel!
    public var trackIndex = 0
    
    //public var currentBookID: Int = 0 // require when coming from search result
    
    
    //var booksEpisodeModel: BookModel?
    var downloadProgressArray = [ChapterDownloading]()
    var downloader: Downloader? = nil
    let ratingView = RatingView.instanceFromNib()
    var bottomView = BottomView.sharedInstance
    var isDownloadingCompleteBook = false
    var completeListCount = 0
    var isFirstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "BookDetailInfoCell", bundle: nil), forCellReuseIdentifier: "BookDetailInfoCell")
        self.tableView.register(UINib(nibName: "BookDetailDescriptionCell", bundle: nil), forCellReuseIdentifier: "BookDetailDescriptionCell")
        self.tableView.register(UINib(nibName: "BookDetailActionsCell", bundle: nil), forCellReuseIdentifier: "BookDetailActionsCell")
        self.tableView.register(UINib(nibName: "BookChaptersListCell", bundle: nil), forCellReuseIdentifier: "BookChaptersListCell")
        
        self.tableView.estimatedRowHeight = self.view.frame.size.height * 0.28
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.addNotificationObservers()
        
        self.setupDownloadProgressArray()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getBookDetails(with: self.currentBook.BookID)
        self.getAdvertise()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.downloadTaskIsInProgress),
                                               name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.downloadCompleteSoUpdateArray),
                                               name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray),
                                               object: nil)
    }
    
    private func setupDownloadProgressArray() {
        if let chapters = self.currentBook.Chapters?.Chapter {
            self.downloadProgressArray.removeAll()
            for (index, item) in chapters.enumerated() {
                print("Chapter index: \(index)")
                if Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: item.ChapterID) {
                    item.isChapterDownloaded = true
                    self.downloadProgressArray.append(ChapterDownloading(isDownloading: false, isDownloaded: true, chapter: item, progress: 0.0))
                } else {
                    self.downloadProgressArray.append(ChapterDownloading(isDownloading: false, isDownloaded: false, chapter: item, progress: 0.0))
                }
            }
        }
    }
    
    private func playAdvertisement() {
        if Singleton.sharedInstance.currentAdvertiseArray.count == 0 {
            self.playBookChapter()
        } else {
            let advertiseView = AdvertiseView.sharedInstance
            advertiseView.delegate = self
            advertiseView.showAdvertiseView()
        }
    }

    private func playBookChapter() {
        let playerView = BottomView.sharedInstance
        
        playerView.playingType = .book
        playerView.currentBook = self.currentBook
        playerView.chapterIndex = self.trackIndex
        
        if playerView.currentBarState == .close {
            playerView.showBottomView()
        }
        
        playerView.initBook()
        //1 = book
        let itemid = self.currentBook.Chapters?.Chapter[self.trackIndex].ChapterID
        self.addToListen(type: "1", itemid: "\(itemid ?? 0)")
    }
    
    private func processStartDownload(index: Int) {
        if self.isFirstTime{
            Utility.main.showToast(message: MiscStrings.downloading.text)
            self.isFirstTime = false
        }
        if let chapters = self.currentBook.Chapters?.Chapter {
            if index >= 0, index < chapters.count {
                let chapter = chapters[index]
                if let downloadPath = chapter.AudioUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                    let fileURL = Utility.main.returnBookFileUrl(with: self.currentBook.BookID, chapterID: chapter.ChapterID)
                    self.downloader = Downloader.init(downloadPath, downloadPath, chapter.ChapterID, fileURL.absoluteString)
                    self.downloader?.startDownload()
                }
            }
        }
    }
    
//    private func checkIfAllChaptersDownloaded() -> Bool {
//        if let chapters = self.currentBook.Chapters?.Chapter {
//            var index = 0
//            var isAllBooksDownloaded = true
//            for chapter in chapters {
//                if Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: chapter.ChapterID) {
//                    isAllBooksDownloaded = true
//                    index += 1
//
//                }else {
//                    return false
//                }
//            }
//            return isAllBooksDownloaded
//        }
//        return false
//    }
    
    func openPlayer(with index: Int) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.stopPlaying), object: nil)
        self.trackIndex = index
        //<adv>
        self.playAdvertisement()
        //self.playBookChapter()
    }
}

//MARK: - IBAction Methods
extension BookDetail {
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Target-Action Methods
extension BookDetail {
    @objc func actionDownloadCompleteBook(sender: UIButton) {
        self.tableView.reloadData()
        print("Download Book \(self.currentBook.description)")
        if let chapters = self.currentBook.Chapters?.Chapter {
            var index = 0
            for item in chapters {
                let chapter = item
                if !Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: chapter.ChapterID) {
                    self.isDownloadingCompleteBook = true
                    self.processStartDownload(index: index)
                }
                index += 1
                self.tableView.reloadData()
            }
            self.tableView.reloadData()
        }
    }
    
    
    @objc func actionAddToCart() {
        print("Action add to cart item!")
    }
    
    @objc func actionListenPreview() {
        if (self.currentBook.Chapters?.Chapter.count)! > 0 {
            self.openPlayer(with: 0)
        } else {
            Utility.main.showAlert(message: MiscStrings.message.text, title: MiscStrings.noPreviewAvailable.text)
        }
    }
    
    @objc func actionAddToFvt() {
        if self.currentBook.IsFavorite {
            self.removeFromFavorite(with: self.currentBook.BookID)
        } else {
            self.addToFavorite(with: self.currentBook.BookID)
        }
    }

    @objc func actionRateThisBook() {
        
        if self.currentBook.IsRated {
            Utility.main.showAlert(message: MiscStrings.youHaveAlreadyGivenRatingToThisBook.text, title: MiscStrings.message.text)
            return
        }
        
        self.ratingView.frame = self.view.frame
        self.ratingView.viewRating.rating = self.currentBook.Rating
        
        //Add target actions
        self.ratingView.BtnCross.addTarget(self, action: #selector(self.actionRatingCross(_:)), for: .touchUpInside)
        self.ratingView.BtnSubmit.addTarget(self, action: #selector(self.actionRatingSubmit(_:)), for: .touchUpInside)
        
        self.view.addSubview(ratingView)
    }
    
    @objc func actionRatingCross(_ sender: Any) {
        self.ratingView.removeFromSuperview()
    }

    @objc func actionRatingSubmit(_ sender: Any) {
        self.ratingView.removeFromSuperview()
        self.rateThisBook(with: Int(self.ratingView.viewRating.rating))
    }

    @objc func actionDownloadChapters(_ sender: UIButton) {
        let index = sender.tag
        if let chapters = self.currentBook.Chapters?.Chapter {
            if index >= 0, index < chapters.count {
                let chapter = chapters[index]
                if Utility.main.isThisBookFileExist(with: self.currentBook.BookID, chapterId: chapter.ChapterID) {
                    self.openPlayer(with: index)
                } else {
                    self.processStartDownload(index: index)
                }
            }
        }
    }
    
    @objc func downloadCompleteSoUpdateArray(data: NSNotification) {
        if let dict = data.userInfo as? [String: Any] {
            for (index, item) in (self.currentBook.Chapters?.Chapter.enumerated())! {
                let trackId = dict["traceID"] as! Int
                if trackId == item.ChapterID {
                    item.isChapterDownloaded = true
                    
                    // Update download progress object
                    self.downloadProgressArray[index].isDownloading = false
                    self.downloadProgressArray[index].isDownloaded = true
                    self.downloadProgressArray[index].chapter = item
                    self.downloadProgressArray[index].progress = 1.0
                    
                    break
                }
            }
            
            self.tableView.reloadData()
        }
        //
        self.completeListCount += 1
        if self.completeListCount == self.currentBook.Chapters?.Chapter.count {
            self.isDownloadingCompleteBook = true
            self.completeListCount = 0
        }
        
    }
    
    @objc func downloadTaskIsInProgress( data: NSNotification) {
        //let aditionalText = data.userInfo
        if let dict = data.userInfo as? [String: Any] {
            for (index, item) in (self.currentBook.Chapters?.Chapter.enumerated())! {
                let trackId = dict["traceID"] as! Int
                if trackId == item.ChapterID {
                    
                    // Update download progress object
                    self.downloadProgressArray[index].isDownloading = true
                    self.downloadProgressArray[index].isDownloaded = false
                    self.downloadProgressArray[index].chapter = item
                    self.downloadProgressArray[index].progress = Float(dict["progress"] as! Double)
                    
                    break
                }
            }
            
            self.tableView.reloadData()
        }

        
        //self.progressView.isHidden = false
        //self.progressView.setProgress(CGFloat(aditionalText!["progress"] as! Double), animated: true)
    }
}

extension BookDetail: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let booksHeader: BooksHeader = .fromNib()
        booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        booksHeader.labelTitle.text = AlertMessages.bookChapters.message.uppercased()
        booksHeader.buttonSeeAll.isHidden = true
        
        return booksHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return self.currentBook.Chapters?.Chapter.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.item {
            case 0:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookDetailInfoCell") as? BookDetailInfoCell else { return UITableViewCell() }
                cell.BindData(with: self.currentBook!)
                cell.buttonDownloadCompleteBook.tag = indexPath.item
                cell.buttonDownloadCompleteBook.addTarget(self, action: #selector(self.actionDownloadCompleteBook(sender:)), for: .touchUpInside)
                self.btnDownloadAll = cell.buttonDownloadCompleteBook
                return cell
            case 1:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookDetailDescriptionCell") as? BookDetailDescriptionCell else { return UITableViewCell() }
                cell.labelTitle.text = AlertMessages.aboutTheBook.message
                cell.lableDescription.text = self.currentBook?.AboutTheBook
                //check if all chapters downloaded disable complete book download button
                if self.checkIfAllChaptersDownloaded() {
                    self.btnDownloadAll.isEnabled = false
                    self.btnDownloadAll.backgroundColor = UIColor.gray
                }else {
                    self.btnDownloadAll.isEnabled = true
                    self.btnDownloadAll.backgroundColor = Global.APP_Color
                }
                return cell
            case 2:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookDetailActionsCell") as? BookDetailActionsCell else { return UITableViewCell() }
                cell.buttonAddToFavourite.isSelected = self.currentBook.IsFavorite
                
                // Acd target action for UIButtons
                cell.buttonListenPreview.addTarget(self, action: #selector(self.actionListenPreview), for: .touchUpInside)
                cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddToCart), for: .touchUpInside)
                cell.buttonAddToFavourite.addTarget(self, action: #selector(self.actionAddToFvt), for: .touchUpInside)
                cell.buttonRateThis.addTarget(self, action: #selector(self.actionRateThisBook), for: .touchUpInside)
                return cell
            default:
                return UITableViewCell()
            }
        }
        else if indexPath.section == 1 {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookChaptersListCell") as? BookChaptersListCell else { return UITableViewCell() }
            
            let index: Int = indexPath.row
            //cell.traceID = self.booksEpisodeModel?.Chapters?.Chapter[index].ChapterID
            cell.traceID = self.currentBook.Chapters?.Chapter[index].ChapterID
            cell.chapterNumber.text = "\(MiscStrings.chapter.text) \(index + 1)"
            
            cell.BtnDownload.tag = index
            cell.BtnDownload.addTarget(self, action: #selector(self.actionDownloadChapters(_:)), for: .touchUpInside)
            cell.progressView.tintColor = Constants.THEME_ORANGE_COLOR
            
            if self.downloadProgressArray[index].isDownloaded {
                cell.imageDownloadPlay.image = #imageLiteral(resourceName: "play-circle")
                cell.BtnDownload.isHidden = false
                cell.imageDownloadPlay.isHidden = false
                cell.progressView.isHidden = true
            } else {
                if self.downloadProgressArray[index].isDownloading {
                    cell.BtnDownload.isHidden = true
                    cell.imageDownloadPlay.isHidden = true
                    cell.progressView.isHidden = false
                    cell.progressView.setProgress(CGFloat(self.downloadProgressArray[index].progress), animated: true)
                } else {
                    cell.imageDownloadPlay.image = #imageLiteral(resourceName: "dowload_arrow-circle")
                    cell.BtnDownload.isHidden = false
                    cell.imageDownloadPlay.isHidden = false
                    cell.progressView.isHidden = true
                }
            }
            
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - ADVERTISEMENT DELEGATE
extension BookDetail: AdvertiseDelegate {
    func advertisementIsStop() {
        self.playBookChapter()
    }
}

// MARK: - APICALLING
extension BookDetail {
    public func getBookDetails(with bookId: Int) {
        Utility.main.showLoader()
        let params = ["BookID": bookId]
        APIManager.sharedInstance.bookApiManager.getBookDetails(params: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let book = BookModel(value: responseObject)
            self.currentBook = book
            self.setupDownloadProgressArray()
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
    
    func addToFavorite(with bookID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["bookID" : bookID]
        APIManager.sharedInstance.bookApiManager.addThisBooksToFvtList(with: param, success: {(success) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = true
            self.tableView.reloadData()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func removeFromFavorite(with bookID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = ["bookID" : bookID]
        APIManager.sharedInstance.bookApiManager.removeFromFvt(with: param, success: {(responceObj) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.currentBook?.IsFavorite = false
            self.tableView.reloadData()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()
        })
    }
    
    func rateThisBook(with rating: Int) {
        Utility.main.showLoader()
        let parameter: [String : Any] = ["BookID": self.currentBook.BookID,
                                         "RatingScore": rating]
        APIManager.sharedInstance.bookApiManager.rateThisBook(with: parameter, success: {
            (response) in
            Utility.main.hideLoader()
            if let rating = response["AverageRating"] as? Double {
                self.currentBook.Rating = rating
                self.currentBook.IsRated = true
                self.tableView.reloadData()
                Utility.main.showAlert(message: MiscStrings.yourRatingHasBeenSubmitted.text, title: MiscStrings.message.text)
            }
            self.ratingView.removeFromSuperview()
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
//        APIManager.sharedInstance.bookApiManager.rateThisBook(with: parameter, success: { (responceObj) in
//            Utility.main.hideLoader()
//            //self.currentBook?.Rating = Double(rating)
//            //self.tableView.reloadData()
//            if let rating = responceObj["AverageRating"] as? Double {
//                self.currentBook.Rating = rating
//                self.currentBook.IsRated = true
//                self.tableView.reloadData()
//                Utility.main.showAlert(message: "Your rating has been submitted", title: "Message")
//            }
//            self.ratingView.removeFromSuperview()
//        }) { (failure) in
//            Utility.main.hideLoader()
//        }
    }
    
    private func getAdvertise() {
        //MARK#
        let AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let Country = AppDelegate.shared.Country
        let params : [String:Any] = ["AccountID":AccountID,"Country":Country]
        APIManager.sharedInstance.miscellaneousAPIManager.getAdvertise(params: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let singleton = Singleton.sharedInstance
            singleton.currentAdvertiseArray.removeAll()
            singleton.currentAdvertiseArray.append(Advertise(value: responseObject as [String : Any]))
            print("advertisement \(singleton.currentAdvertiseArray)")
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }

    func addToListen(type:String,itemid:String){
        let params:[String:Any] = ["Type":type,
                                   "ItemId":itemid]
        print(params)
        APIManager.sharedInstance.podcastApiManager.AddToListen(params: params, success: { (response) in
            print(response)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
//MARK:- Helper methods
extension BookDetail {
    func checkIfAllChaptersDownloaded()->Bool{
        if (self.currentBook.Chapters?.Chapter.count)! > 0{
            var chapterIDs = [String]()
            guard let chapters = self.currentBook.Chapters?.Chapter else {return false}
            for i in (chapters){
                chapterIDs.append("\(i.ChapterID)")
            }
            let downloadedItemsCSV = Utility.main.getIdfromFolders(directoryName: "books")
            let arrDownloadedItems = downloadedItemsCSV.components(separatedBy: ",")
            if chapterIDs.count > 0 && arrDownloadedItems.count > 0{
                if arrDownloadedItems.contains(array: chapterIDs){
                    return true
                    //print("All downloaded")
                }
                else{
                    return false
                    //print("none or some downloaded")
                }
            }
        }
        return false
    }
}
extension Array where Element: Equatable {
    func contains(array: [Element]) -> Bool {
        for item in array {
            if !self.contains(item) { return false }
        }
        return true
    }
}
