//
//  BooksHeader.swift
//  Auditix
//
//  Created by shardha on 1/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class CountryHeader: UIView {
    @IBOutlet weak var isInternational: UISwitch!
    @IBOutlet weak var lblInternational: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnToggle: UIButton!
    
}
