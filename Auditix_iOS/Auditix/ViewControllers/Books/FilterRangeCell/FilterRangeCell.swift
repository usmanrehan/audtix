//
//  FilterRangeCell.swift
//  Auditix
//
//  Created by Usman Bin Rehan on 7/14/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RangeSeekSlider


class FilterRangeCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var rangeSeekBar: RangeSeekSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rangeSeekBar.tintColor = Constants.THEME_ORANGE_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
