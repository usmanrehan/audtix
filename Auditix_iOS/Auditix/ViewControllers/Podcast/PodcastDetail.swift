      //
//  PodcastDetail.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView

struct EpisodeDownloading {
    var isDownloading = false
    var isDownloaded = false
    var episode: PodcastTrack!
    var progress: Float = 0.0
}

class PodcastDetail: UIViewController {

    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var heroID = ""
    var showingWindless: Bool = true

    let ratingView = RatingView.instanceFromNib()

    var bottomView = BottomView.sharedInstance

    var podcast: PodcastModel!

    var downloader: Downloader? = nil

    //var currentPodCast: PodcastDetailModel = PodcastDetailModel()
    //var episodeDownloadProgressArray = [EpisodeDownloading]()
    //public var currentTrackID: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if (self.podcast.Title ?? "").count > 0{
            self.lblTitle.text = self.podcast.Title ?? "-"
        }
        if (self.podcast.Name ?? "").count > 0{
            self.lblTitle.text = self.podcast.Name ?? "-"
        }
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.tableview.register(UINib(nibName: "PodcastDetailInfoCell", bundle: nil), forCellReuseIdentifier: "PodcastDetailInfoCell")
        self.tableview.register(UINib(nibName: "BookDetailDescriptionCell", bundle: nil), forCellReuseIdentifier: "BookDetailDescriptionCell")
        self.tableview.register(UINib(nibName: "PodcastDetailActionCell", bundle: nil), forCellReuseIdentifier: "PodcastDetailActionCell")
        self.tableview.register(UINib(nibName: "PodcastEpisodeListViewCell", bundle: nil), forCellReuseIdentifier: "PodCastEpisodeListCellIdentifier")
        
//        self.tableview.windless
//            .apply {
//                $0.beginTime = 0.5
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//                $0.animationLayerColor = Constants.THEME_ORANGE_COLOR
//            }
//            .start()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.downloadCompleteSoUpdateArray), name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray), object: nil)
        
        self.getPodcastEpisodes()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAdvertise()
    }
    
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushEpisodeDetialsSegue" {
            if let indexPath = sender as? IndexPath {
                let controller = segue.destination as! EpisodeDetails
                controller.podcast = self.podcast
                controller.episodeIndex = indexPath.row
            }
        }
     }
}

// MARK: - IBAction Methods
extension PodcastDetail {
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Target-Action Methods
extension PodcastDetail {
    @objc func actionSubscribe(_ sender: Any) {
        if self.podcast.IsSubscribed {
            self.unSubscibeThisPodcast(with: self.podcast.TrackId)
        } else {
            self.subscribeThisPodcast(with: self.podcast.TrackId)
        }
    }
    
    @objc func actionRateThis(_ sender: Any) {
        if self.podcast.IsRated {
            Utility.main.showAlert(message: MiscStrings.youHaveAlreadyGivenRatingToThisPodcast.text, title: MiscStrings.message.text)
            return
        }
        
        self.ratingView.frame = self.view.frame
        self.ratingView.viewRating.rating = self.podcast.Rating
        self.ratingView.BtnCross.addTarget(self, action: #selector(self.actionRatingCross(_:)), for: .touchUpInside)
        self.ratingView.BtnSubmit.addTarget(self, action: #selector(self.actionRatingSubmit(_:)), for: .touchUpInside)
        self.view.addSubview(ratingView)
    }
    
    @objc func actionRatingCross(_ sender: Any) {
        self.ratingView.removeFromSuperview()
    }
    
    @objc func actionRatingSubmit(_ sender: Any) {
        self.ratingView.removeFromSuperview()
        self.rateThisPodcast(with: self.ratingView.viewRating.rating)
    }
}

// MARK: - API CALLING
extension PodcastDetail {
    func getPodcastEpisodes() {
        guard self.podcast != nil else { return }
        let params: [String: Any] = [
            "PodcastID": self.podcast.PodcastId
        ]
        APIManager.sharedInstance.podcastApiManager.getPodcastEpisodes(with: params, success: { (responseArray) in
            var episodes = Array<PodcastEpisode>()
            for item in responseArray {
                let model = PodcastEpisode(value: item)
                episodes.append(model)
            }
            self.podcast.episodes.removeAll()
            self.podcast.episodes.append(objectsIn: episodes)
            
            self.showingWindless = false
            self.tableview.windless.end()
            
            self.tableview.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func subscribeThisPodcast(with trackID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = [
            "TrackId": trackID,
            "CategoryId": self.podcast.CategoryId
        ]
        APIManager.sharedInstance.podcastApiManager.subscribeThisPodcast(with: param, success: {(responceObj) in
            Utility.main.hideLoader()
            //Utility.main.postSubscribedPodcastNotification()
            self.podcast.IsSubscribed = true
            self.tableview.reloadData()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func unSubscibeThisPodcast(with trackID: Int) {
        Utility.main.showLoader()
        let param: [String : Any] = [
            "TrackId" : trackID
        ]
        APIManager.sharedInstance.podcastApiManager.unSubscribeThisPodcast(with: param, success: { (responseObject) in
            Utility.main.hideLoader()
            //Utility.main.postSubscribedPodcastNotification()
            self.podcast.IsSubscribed = false
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
    
    func rateThisPodcast(with rating: Double) {
        Utility.main.showLoader()
        let param : [String : Any] = [
            "trackId": self.podcast.TrackId,
            "rating": Int(rating)
        ]
        APIManager.sharedInstance.podcastApiManager.addPodcastRating(params: param, success: {(responceObj) in
            Utility.main.hideLoader()
            if let rating = responceObj["AverageRating"] as? Double {
                self.podcast.Rating = rating
                self.podcast.IsRated = true
                self.tableview.reloadData()
                Utility.main.showAlert(message: MiscStrings.yourRatingHasBeenSubmitted.text, title: MiscStrings.message.text)
            }
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }

    private func getAdvertise() {
        //MARK#
        let AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let Country = AppDelegate.shared.Country
        let params : [String:Any] = ["AccountID":AccountID,"Country":Country]
        APIManager.sharedInstance.miscellaneousAPIManager.getAdvertise(params: params, success: { (responseObject) in
            Utility.main.hideLoader()
            let singleton = Singleton.sharedInstance
            singleton.currentAdvertiseArray.removeAll()
            singleton.currentAdvertiseArray.append(Advertise(value: responseObject as [String : Any]))
            print("advertisement \(singleton.currentAdvertiseArray)")
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }

}

// MARK: - UITableView DataSource Delegate
extension PodcastDetail: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return self.showingWindless ? 0 : 40
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let booksHeader: BooksHeader = .fromNib()
        booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        booksHeader.labelTitle.text = AlertMessages.episodes.message.uppercased()
        booksHeader.buttonSeeAll.isHidden = true
        
        return booksHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return self.podcast.episodes.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "PodcastDetailInfoCell") as? PodcastDetailInfoCell else { return UITableViewCell() }
                cell.hero.id = self.heroID
                cell.hero.modifiers = [.arc]
                cell.BindData(with: self.podcast)
                return cell
            case 1:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BookDetailDescriptionCell") as? BookDetailDescriptionCell else { return UITableViewCell() }
                cell.labelTitle.text = AlertMessages.aboutThePodcast.message
                cell.lableDescription.text = self.podcast.Description
                return cell
            case 2:
                guard let cell  = tableView.dequeueReusableCell(withIdentifier: "PodcastDetailActionCell") as? PodcastDetailActionCell else { return UITableViewCell() }
                if !self.showingWindless {
                    //cell.btnRate.isEnabled = !(self.podcast.IsRated)
                    cell.btnAddToFvt.isSelected = self.podcast.IsSubscribed
                    if self.podcast.IsSubscribed{
                        cell.btnAddToFvt.setTitle(AlertMessages.unsubscribe.message, for: .selected)
                    }
                    cell.btnRate.addTarget(self, action: #selector(self.actionRateThis(_:)), for: .touchUpInside)
                    cell.btnAddToFvt.addTarget(self, action: #selector(self.actionSubscribe(_:)), for: .touchUpInside)
                }
                return cell
            default:
                return UITableViewCell()
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PodCastEpisodeListCellIdentifier", for: indexPath) as! PodcastEpisodeListViewCell
            if self.showingWindless == false {
                cell.lblEpisodeName.text = self.podcast.episodes[indexPath.row].EpisodeTitle
                cell.lblEpisodeDate.text = Utility.stringDateFormatter(dateStr: self.podcast.episodes[indexPath.row].CreatedDate ?? "2001-01-01T13:52:02.133", dateFormat: "yyyy-MM-dd'T'HH:mm:ss.zzz", formatteddate: "dd/MM/yyyy")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 1:
            self.performSegue(withIdentifier: "pushEpisodeDetialsSegue", sender: indexPath)
            break
        default:
            break
        }
    }
}
