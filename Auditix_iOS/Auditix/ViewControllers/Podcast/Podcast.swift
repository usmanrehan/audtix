//
//  Podcast.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift

struct PodcastGenreModel {
    var id: Int
    var imageURL: String
    var title : String
    var isSelected: Bool
}

class Podcast: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var arrNewAndNoteworthyPodcast = Array<PodcastModel>()
    var arrFeaturedPodcasts = Array<PodcastModel>()
    var arrPodcastCategories = Array<PodcastCategoryModel>()
    
    //var podcastFeaturedArray = [PodcastModel]()

    var mySubscribedPodcast = [PodcastSubscribtionModel]()
    var podcastDefaultArray = [PodcastModel]()
    var genreArray = [PodcastGenreModel]()
    
    var showingWinless: Bool = true
    
    var filterStr: String = ""
    
    var isComingFromPodcastSeeAll: Bool = false
    
    var podcastPageNumber: Int = 1
    var filterPageingNumber: Int = 1
    
    var arrPodcasts = Array<PodcastModel>()
    var isFilter = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Following three lines of code is to disable sticky header in tableview.
        let dummyViewHeight = CGFloat(40)
        self.tableview.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableview.bounds.size.width, height: dummyViewHeight))
        self.tableview.contentInset = UIEdgeInsetsMake(-dummyViewHeight, 0, 0, 0)
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.tableview.register(UINib(nibName: "NewsCollection", bundle: nil), forCellReuseIdentifier: "NewsCollection")
        self.tableview.register(UINib(nibName: "BooksCollection", bundle: nil), forCellReuseIdentifier: "BooksCollection")
        self.tableview.register(UINib(nibName: "PodcastTableCell", bundle: nil), forCellReuseIdentifier: "PodcastTableCell")
        self.tableview.register(UINib(nibName: "PodcastCollection", bundle: nil), forCellReuseIdentifier: "PodcastCollection")
        
//        self.tableview.windless
//            .apply {
//                $0.beginTime = 0.5
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//                $0.animationLayerColor = Constants.THEME_ORANGE_COLOR
//            }
//            .start()
//        self.getFeaturedPodcasts()
//        self.getPodcastCategories()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getFeaturedPodcasts()
        self.getPodcastCategories()
        self.getNewAndNoteworthyPodcasts()
    }
}

// MARK: - HELPER METHODS
extension Podcast {
    func navigateToPodcastGenreWith(CategoryId categoryId: Int) {
        if let podastGenreController = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastGenre") as? PodcastGenre {
            podastGenreController.titleHeading = MiscStrings.podcasts.text
            podastGenreController.categoryId = categoryId
            self.navigationController?.pushViewController(podastGenreController, animated: true)
        }
    }
    
    func navigateToPodcastDetailsWith(Podcast podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = podcast
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func navigateToEpisodeDetails(With podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "EpisodeDetails") as? EpisodeDetails {
            controller.podcast = podcast
            controller.episodeIndex = 0
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
// MARK: - IB ACTIONS
extension Podcast {
    @IBAction func actionSeeAll(_ sender: UIButton) {
//        if let PodcastAll = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastAll") as? PodcastAll {
//            self.isComingFromPodcastSeeAll = true
//            PodcastAll.delegate = self
//            self.navigationController?.pushViewController(PodcastAll, animated: true)
//        }
    }
    
    @IBAction func actionSeeAllNewsNetworthyEpisodes(_ sender: UIButton) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastAll") as? PodcastAll {
            controller.arrNewsNoteworthyPodcasts = self.arrNewAndNoteworthyPodcast
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func actionSeeAllMostRecommended(_ sender: UIButton) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastAll") as? PodcastAll {
            controller.arrFeaturedPodcasts = self.arrFeaturedPodcasts
            controller.isRecommended = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func actionMenu(_ sender: Any) {
        self.sideMenuController?.showLeftView()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionFilterBtn(_ sender: Any) {
        let podcastFilter = Constants.podcastFilterRightSideMenu
        podcastFilter.delegate = self
        self.sideMenuController?.rightViewController = podcastFilter
        self.sideMenuController?.showRightView()
    }
}

// MARK: - API CALLING
extension Podcast {
    func getNewAndNoteworthyPodcasts() {
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.getNewAndNoteworthyPodcasts(success: { (responseArray) in
            self.arrNewAndNoteworthyPodcast.removeAll()
            for item in responseArray {
                if let podcastItem = item["Podcast"] {
                    let episode = PodcastEpisode(value: item)
                    let podcast = PodcastModel(value: podcastItem!)
                    podcast.episodes.append(episode)
                    self.arrNewAndNoteworthyPodcast.append(podcast)
                }
            }
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    func getPodcastCategories() {
        let param: [String : Any] = [:]
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.getPodcastCategories(params: param, success: {(responceArray) in
            self.arrPodcastCategories.removeAll()
            for item in responceArray {
                let model = PodcastCategoryModel(value: item)
                self.arrPodcastCategories.append(model)
            }
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }

    func getPodcastCategories(withCountryIds countryIds: String) {
        let param: [String : Any] = ["CountryId": countryIds]
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.getPodcastCategoriesByCountryIds(params: param, success: {(responceArray) in
            self.arrPodcastCategories.removeAll()
            for item in responceArray {
                let model = PodcastCategoryModel(value: item)
                self.arrPodcastCategories.append(model)
            }
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func getFeaturedPodcasts() {
        let param: [String : Any] = ["pageNumber" : self.podcastPageNumber,
                                     "totalCount" : "100"]
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.getPodcasts(with: param, success: {(responceObj) in
            let podcastBase = PodcastBaseCamp(value: responceObj)
            print(podcastBase)
            if (podcastBase.FeaturedCategories.count == 0 || podcastBase.DefaultCategories.count == 0) { // if filter is not applied
                self.podcastPageNumber = -1
            }
            for featuredPodcastsBase in podcastBase.FeaturedCategories {
                for featuredPodcast in featuredPodcastsBase.Podcastdetails  {
                    self.arrFeaturedPodcasts.append(featuredPodcast)
                }
            }
            self.showingWinless = false
            self.tableview.windless.end()
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
}

// MARK: - PodcastFilter Delegate
extension Podcast: PodcastFilterDelegate {
    func didApplyFilterSelect(filterData: FilterDataSet) {
        if filterData.shouldFilter{
            self.getFilteredPodcast(data: filterData)
        }
        else{
            self.isFilter = false
            self.tableview.reloadData()
        }
    }
}

// MARK: - UITableView DataSource Delegate
extension Podcast: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if !self.isFilter{
            return 3
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isFilter{
            switch section {
            case 0: // New & Noteworthy
                return 1
            case 1: // Recommended
                return 1
            case 2: // Categories
                return 1
            default:
                return 0
            }
        }
        else{
            return self.arrPodcasts.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !self.isFilter{
            let booksHeader: BooksHeader = .fromNib()
            booksHeader.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
            booksHeader.backgroundColor = UIColor.clear
            booksHeader.buttonSeeAll.tag = section
            booksHeader.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAll(_:)), for: .touchUpInside)
            switch section {
            case 0:
                booksHeader.labelTitle.text = MiscStrings.newAndNoteworthy.text.uppercased()
                booksHeader.buttonSeeAll.isHidden = true
                booksHeader.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAllNewsNetworthyEpisodes(_:)), for: .touchUpInside)
                break
            case 1:
                booksHeader.labelTitle.text = MiscStrings.mostRecommended.text.uppercased()
                booksHeader.buttonSeeAll.isHidden = false
                booksHeader.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAllMostRecommended(_:)), for: .touchUpInside)
                break
            case 2:
                booksHeader.labelTitle.text = MiscStrings.categories.text .uppercased()
                booksHeader.buttonSeeAll.isHidden = true
                break
            default:
                booksHeader.labelTitle.text = "".uppercased()
                booksHeader.buttonSeeAll.isHidden = true
            }
            return booksHeader
        }
        else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !self.isFilter{
            return 50
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !self.isFilter{
            switch indexPath.section {
            case 0: //New & Noteworthy
                return (self.view.frame.size.width / 4.0) + 30.0
            case 1: //Most Recommended
                return (self.view.frame.size.width / 3.0) + 30.0
            case 2: //Categories
                let height = ((self.view.frame.size.width - (8 * 2)) / 3.0) + 30.0
                return ceil(CGFloat(self.arrPodcastCategories.count) / 3.0) * height
            default:
                return 0
            }
        }
        else{
            return CGFloat(120)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !self.isFilter{
            if indexPath.section == 0 { // New & noteworthy cell
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BooksCollection") as? BooksCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "BookCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BookCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                return cell
            }
            else  if indexPath.section == 1 { // most recommended podcast cell
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "BooksCollection") as? BooksCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "BookCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BookCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                return cell
            }
            else if indexPath.section == 2 { // all categories
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCollection") as? NewsCollection else { return UITableViewCell() }
                cell.collectionView.register(UINib(nibName: "NewsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NewsCollectionCell")
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = indexPath.section
                cell.collectionView.reloadData()
                
                return cell
            }
            else { // Others
                return UITableViewCell()
            }
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastTableCell") as? PodcastTableCell else { return UITableViewCell() }
            let podcast = self.arrPodcasts[indexPath.row]
            cell.BindData(with: podcast)
            cell.BtnSubscrptionBottom.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.isFilter{
            let podcast = self.arrPodcasts[indexPath.row]
            self.navigateToPodcastDetails(podcast)
        }
    }
    
    private func navigateToPodcastDetails(_ podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = podcast
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK: - UICOLLECTIONVIEW DATASOURCE & DELEGATE
extension Podcast: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0: // New & Noteworthy
            return self.arrNewAndNoteworthyPodcast.count
//            if self.showingWinless {
//                return 10
//            } else {
//                return self.arrNewAndNoteworthyPodcast.count
//            }
        case 1:
            if self.arrFeaturedPodcasts.count < 3 {
                return self.arrFeaturedPodcasts.count
            }else {
                return 3
            }
//            if self.showingWinless {
//                return 10
//            } else {
//                if self.arrFeaturedPodcasts.count < 3 {
//                    return self.arrFeaturedPodcasts.count
//                }else {
//                    return 3
//                }
//            }
        case 2:
            return self.arrPodcastCategories.count
//            if self.showingWinless {
//                return 10
//            } else {
//                return self.arrPodcastCategories.count
//            }
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCollectionCell", for: indexPath) as? BookCollectionCell else { return UICollectionViewCell() }
            cell.labelName.textAlignment = .center
            if !self.showingWinless {
                let podcast = self.arrNewAndNoteworthyPodcast[indexPath.row]
                //Set podcast episode title
                if podcast.episodes.count > 0 {
                    cell.labelName.text = podcast.episodes.first?.EpisodeTitle
                } else {
                    cell.labelName.text = "-"
                }
                
                //Set podcast image
                if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    cell.imageBookCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                } else {
                    cell.imageBookCover.image = #imageLiteral(resourceName: "placeholder_image")
                }
            }
            
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCollectionCell", for: indexPath) as? BookCollectionCell else { return UICollectionViewCell() }
            cell.labelName.textAlignment = .center
            if !self.showingWinless {
                let podcast = self.arrFeaturedPodcasts[indexPath.row]
                cell.labelName.text = podcast.Name
                
                if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    cell.imageBookCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                } else {
                    cell.imageBookCover.image = #imageLiteral(resourceName: "placeholder_image")
                }
            }
            return cell
        case 2:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionCell", for: indexPath) as? NewsCollectionCell else { return UICollectionViewCell() }
            if !self.showingWinless {
                let category = self.arrPodcastCategories[indexPath.row]
                cell.labelName.text = self.arrPodcastCategories[indexPath.row].Title
                if let imageURL = URL(string: (category.ImagePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!) {
                    cell.imageNewsCover.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                }
            }
            return cell

        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 0:
            let width = (collectionView.frame.size.width - (8 * 2)) / 4.0
            return CGSize(width: width, height: width + 30.0)
        case 1:
            let width = (collectionView.frame.size.width - (8 * 2)) / 3.0
            return CGSize(width: width, height: width + 30.0)
        case 2:
            let width = (collectionView.frame.size.width - (8 * 2)) / 3.0
            return CGSize(width: width, height: width + 30.0)
        default:
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch collectionView.tag {
        case 0:
            if self.arrNewAndNoteworthyPodcast.count > indexPath.row - 1 {
                self.navigateToEpisodeDetails(With: self.arrNewAndNoteworthyPodcast[indexPath.row])
            }
            break
        case 1:
            if self.arrFeaturedPodcasts.count > indexPath.row - 1 {
                self.navigateToPodcastDetailsWith(Podcast: self.arrFeaturedPodcasts[indexPath.row])
            }
            break
        case 2:
            if self.arrPodcastCategories.count > indexPath.row - 1 {
                self.navigateToPodcastGenreWith(CategoryId: self.arrPodcastCategories[indexPath.row].PodcastCategoryId)
            }
            break
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            if indexPath.row == self.arrFeaturedPodcasts.count - 1 {
                if self.podcastPageNumber != -1 && self.filterStr.count == 0 {
                    print("I am last cell collection \(podcastPageNumber)")
                    podcastPageNumber += 1
                    self.showingWinless = true
                    self.getFeaturedPodcasts()
                }
            }
        }
    }
}

//MARK:- GetFilterResponse
extension Podcast{
    private func getFilteredPodcast(data:FilterDataSet){
        let MinDuration = data.minDuration
        let MaxDuration = data.maxDuration
        let MinSubscriber = data.minSubscribers
        let MaxSubscriber = data.maxSubscribers
        let Type = 1 //podcast
        let pageNumber = 1
        let count = 100
        let CountryIds = data.commaSeparatedLocationIDs
        let CategoryIds = data.commaSeparatedEntityIDs
        var params : [String:Any] = [
            "MinDuration":MinDuration,
            "MaxDuration":MaxDuration,
            "MinSubscriber":MinSubscriber,
            "MaxSubscriber":MaxSubscriber,
            "Type":Type,
            "pageNumber":pageNumber,
            "count":count
        ]
        if Validation.validateStringLength(CountryIds){
            params["CountryIds"] = CountryIds
        }
        if Validation.validateStringLength(CategoryIds){
            params["CategoryIds"] = CategoryIds
        }
        print(params)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.getFilterResponse(params: params, success: { (responseArray) in
            Utility.main.hideLoader()
            //BookModel
            let podcasts = responseArray
                self.arrPodcasts.removeAll()
                for item in podcasts {
                    self.arrPodcasts.append(PodcastModel(value: item as! [String: Any]))
                }
            self.isFilter = true
            self.tableview.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
            print(error.localizedDescription)
        }
    }
}
