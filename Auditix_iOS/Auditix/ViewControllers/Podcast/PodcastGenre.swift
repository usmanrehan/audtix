//
//  PodcastGenre.swift
//  Auditix
//
//  Created by Ahmed Shahid on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol PodcastGenreDelegate {
    func podcastDidSubscribe(with trackID: Int)
    func podcastDidUnsubscribe(with tracKID: Int)
}

class PodcastGenre: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var arrPodcasts = Array<PodcastModel>()
    var categoryId = 0
    var filterCountryCode: String = ""
    var flag = true //with podcast category id
    var titleHeading = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableview.register(UINib(nibName: "PodcastTableCell", bundle: nil), forCellReuseIdentifier: "PodcastTableCell")
        self.lblTitle.text = self.titleHeading
        //Get podcast with category ID
        if self.flag{
            self.getPodcastWith(CategoryId: self.categoryId)
        }
        else{
            self.getSubscribedPodcast()
        }
    }
    
    private func navigateToPodcastDetails(_ podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = podcast
            //podcastDetail.trackId = trackId
            //podcastDetail.categoryId = categoryId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK: - IBAction Methods
extension PodcastGenre {
    @IBAction func actionBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFilterBtn(_ sender: Any) {
        let podcastFilterView = Constants.podcastFilterRightSideMenu
        podcastFilterView.delegate = self
        self.sideMenuController?.rightViewController = podcastFilterView
        self.sideMenuController?.showRightView()
    }
}

//MARK: - Target-Action Methods
extension PodcastGenre {
    @objc func actionSubscribtionDown(_ sender: UIButton) {
        let index = sender.tag
        let podcast = self.arrPodcasts[index]
        if podcast.IsSubscribed {
            self.unsubscibeMyPodcastDown(with: self.arrPodcasts[index].TrackId, index: index, buttonForTextChange: sender)
        } else {
            self.subscribeThisPodcast(with: self.arrPodcasts[index].TrackId, index: index, buttonForTextChange: sender)
        }
    }
}


// MARK: - PodcastFilter Delegate
extension PodcastGenre: PodcastFilterDelegate {
    func didApplyFilterSelect(filterData: FilterDataSet) {
        print(filterData)
    }
    
//    func didApplyFilterSelect(idsStringCommaSeparated: String) {
//        self.filterCountryCode = idsStringCommaSeparated
//        self.getPodcastWith(CategoryId: self.categoryId)
//    }
}

// MARK: - UITableView DataSource Delegate
extension PodcastGenre: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrPodcasts.count > 0 {
            self.emptyMessage(message: "")
        }else {
            self.emptyMessage(message: MiscStrings.noDataFound.text)
        }
        return self.arrPodcasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastTableCell") as? PodcastTableCell else { return UITableViewCell() }
        let podcast = self.arrPodcasts[indexPath.row]
        cell.BindData(with: podcast)
        cell.BtnSubscrptionBottom.tag = indexPath.row
        cell.BtnSubscrptionBottom.addTarget(self, action: #selector(self.actionSubscribtionDown(_:)), for: .touchUpInside)
        
        cell.BtnSubscrptionBottom.isSelected = podcast.IsSubscribed
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        let podcast = self.arrPodcasts[indexPath.row]
        self.navigateToPodcastDetails(podcast)
    }
}

// MARK: - Web API Methods
extension PodcastGenre {
    public func getPodcastWith(CategoryId categoryId: Int) {
        Utility.main.showLoader()
        var params: [String: Any] = ["categoryId": categoryId]
        
        if self.filterCountryCode.isEmpty == false {
            params["countryCode"] = self.filterCountryCode
        }
        
        APIManager.sharedInstance.podcastApiManager.getPodcastsByCategoryID(with: params, success: { (response) in
            Utility.main.hideLoader()
            if let podcasts = response["podcastListResultlist"] as? Array<AnyObject> {
                self.arrPodcasts.removeAll()
                for item in podcasts {
                    self.arrPodcasts.append(PodcastModel(value: item as! [String: Any]))
                }
                
                self.tableview.reloadData()
            }
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
    func getSubscribedPodcast() {
        APIManager.sharedInstance.podcastApiManager.getSubscribePodcasts(success: {(responseArray) in
            Utility.main.hideLoader()
            self.arrPodcasts.removeAll()
            for item in responseArray {
                let model = PodcastModel(value: item)
                model.IsSubscribed = true
                self.arrPodcasts.append(model)
            }
            self.tableview.windless.end()
            self.tableview.reloadData()
        }, failure:{ (failure) in
            Utility.main.hideLoader()
        })
    }
    func unsubscibeMyPodcastDown(with trackID: Int, index: Int, buttonForTextChange: UIButton) {
        Utility.main.showLoader()
        let param: [String: Any] = [
            "TrackId" : trackID
        ]
        APIManager.sharedInstance.podcastApiManager.unSubscribeThisPodcast(with: param, success: {(responceObj) in
            Utility.main.hideLoader()
            if self.flag{
                Utility.main.postSubscribedPodcastNotification()
                self.arrPodcasts[index].IsSubscribed = false
                self.tableview.reloadData()
            }
            else{
                self.getSubscribedPodcast()
            }
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func subscribeThisPodcast (with trackID: Int, index: Int, buttonForTextChange: UIButton) {
        Utility.main.showLoader()
        let param: [String : Any] = [
            "TrackId": trackID,
            "CategoryId": self.categoryId
        ]
        APIManager.sharedInstance.podcastApiManager.subscribeThisPodcast(with: param, success: {(responceObj) in
            Utility.main.hideLoader()
            Utility.main.postSubscribedPodcastNotification()
            self.arrPodcasts[index].IsSubscribed = true
            self.tableview.reloadData()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
}

extension PodcastGenre {
    func emptyMessage(message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Roboto-Medium", size: 15)
        messageLabel.sizeToFit()
        
        self.tableview.backgroundView = messageLabel
        self.tableview.separatorStyle = .none
    }
}
