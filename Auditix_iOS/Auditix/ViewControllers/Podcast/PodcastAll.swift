//
//  PodcastAll.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PodcastAll: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var titleString: UILabel!
    var isRecommended = false
    
    var arrNewsNoteworthyPodcasts = Array<PodcastModel>()
    var arrFeaturedPodcasts = Array<PodcastModel>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if self.isRecommended {
            self.titleString.text = MiscStrings.mostRecommended.text
        }else {
            self.titleString.text = MiscStrings.newAndNoteworthy.text
        }
        self.tableview.register(UINib(nibName: "PodcastNewsNoteworthyViewCell", bundle: nil), forCellReuseIdentifier: "PodcastNewsNoteworthyViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigateToEpisodeDetails(With podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "EpisodeDetails") as? EpisodeDetails {
            controller.podcast = podcast
            controller.episodeIndex = 0
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension PodcastAll {
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableView DataSource Delegate
extension PodcastAll: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isRecommended {
            return self.arrFeaturedPodcasts.count
        }else {
            return self.arrNewsNoteworthyPodcasts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isRecommended {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastNewsNoteworthyViewCell") as? PodcastNewsNoteworthyViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            let podcast = self.arrFeaturedPodcasts[indexPath.row]
            cell.lblTitle.text = podcast.Name
            
            if let imageURL = URL(string: podcast.ImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                cell.podcastImageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            } else {
                cell.podcastImageView.image = #imageLiteral(resourceName: "placeholder_image")
            }
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PodcastNewsNoteworthyViewCell") as? PodcastNewsNoteworthyViewCell else {
                return UITableViewCell()
            }
            
            let podcast = self.arrNewsNoteworthyPodcasts[indexPath.row]
            if podcast.episodes.count > 0 {
                cell.lblTitle.text = podcast.episodes.first?.EpisodeTitle
                if let imageURL = podcast.ImageUrl {
                    cell.podcastImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isRecommended {
            self.navigateToPodcastDetailsWith(Podcast: self.arrFeaturedPodcasts[indexPath.row])
        }else {
            self.tableview.deselectRow(at: indexPath, animated: true)
            self.navigateToEpisodeDetails(With: self.arrNewsNoteworthyPodcasts[indexPath.row])
        }
    }
    func navigateToPodcastDetailsWith(Podcast podcast: PodcastModel) {
        if let controller = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastDetail") as? PodcastDetail {
            controller.podcast = podcast
            //podcastDetail.categoryId = categoryId
            //podcastDetail.trackId = trackId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
