//
//  EpisodeDetails.swift
//  Auditix
//
//  Created by Ahmed Shahid on 6/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import UAProgressView

class EpisodeDetails: UIViewController {
    
    @IBOutlet weak var podcastImageView: UIImageView!
    @IBOutlet weak var navigationTitleLable: UILabel!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var narratorLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateAddedLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var progressView: UAProgressView!
    
    //var podcast: PodcastDetailModel? = nil
    var downloader: Downloader? = nil
    
    var podcast: PodcastModel!
    var episodeIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        addNotificationObservers()
        setupUI()
        self.addListeningEventPodcast()
    }
    
    private func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskCompleted), name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadTaskIsInProgress), name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.downloadCompleteSoUpdateArray), name: NSNotification.Name(rawValue: Constants.downloadCompleteSoUpdateArray), object: nil)
    }
    
    private func setupUI() {
        if episodeIndex >= 0, episodeIndex < self.podcast.episodes.count {
            let episode = self.podcast.episodes[episodeIndex]
            self.titleLable.text = episode.EpisodeTitle
            self.durationLabel.text = ParserHelper.getDuration(with: Int(episode.FileDuration?.stringValue ?? "0")!) 
            self.dateAddedLabel.text = Utility.stringDateFormatter(dateStr: episode.CreatedDate ?? "2001-01-01T13:52:02.133", dateFormat: "yyyy-MM-dd'T'HH:mm:ss.zzz", formatteddate: "dd/MM/yyyy")
        }
        self.aboutLabel.text = self.podcast.Description
        self.narratorLabel.text = self.podcast.ArtistName
        if (self.podcast.Title ?? "").count > 0{
            self.navigationTitleLable.text = self.podcast.Title ?? "-"
        }
        if (self.podcast.Name ?? "").count > 0{
            self.navigationTitleLable.text = self.podcast.Name ?? "-"
        }
        
        if let imageURL = URL(string: (self.podcast.ImageUrl?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) {
            self.podcastImageView.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        } else {
            self.podcastImageView.image = #imageLiteral(resourceName: "placeholder_image")
        }
        
        self.downloadButton.isEnabled = !(Utility.main.isThisPodcastFileExist(with: self.podcast.PodcastId, EpisodeID: self.podcast.episodes[episodeIndex].PodcastEpisodeID))
        
        //Setup Progress View
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.progressView.frame.size.width * 0.9, height: self.progressView.frame.size.height))
        label.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        label.textColor = Constants.THEME_ORANGE_COLOR
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        self.progressView.centralView = label
        self.progressView.tintColor = Constants.THEME_ORANGE_COLOR
        
        self.progressView.progressChangedBlock = {
            (progressView, progress) -> Void in
            print("progress \(progress)")
            if let label = progressView?.centralView as? UILabel {
                label.text = String(format: "%2.0f%%", progress * 100)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func processStartDownload() {
        if let downloadPath = self.podcast.episodes[episodeIndex].FilePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            let fileURL = Utility.main.returnPodcastFileUrl(with: self.podcast.PodcastId, EpisodeID: self.podcast.episodes[episodeIndex].PodcastEpisodeID)
            self.downloader = Downloader.init(downloadPath, downloadPath, self.podcast.TrackId, fileURL.absoluteString)
            self.downloader?.startDownload()
        }
    }
    
    private func playAdvertisement() {
        if Singleton.sharedInstance.currentAdvertiseArray.count == 0 {
            self.playEpisode()
        } else {
            let advertiseView = AdvertiseView.sharedInstance
            advertiseView.delegate = self
            advertiseView.showAdvertiseView()
        }
    }
    
    private func playEpisode() {
        let playerView = BottomView.sharedInstance
        
        playerView.playingType = .podcast
        playerView.podcast = self.podcast
        playerView.episodeIndex = self.episodeIndex
        
        if playerView.currentBarState == .close {
            playerView.showBottomView()
        }
        
        playerView.initPodcast()
        //2 = podcast
        let itemid = self.podcast.episodes[episodeIndex].PodcastEpisodeID
        self.addToListen(type: "2", itemid: "\(itemid)")
    }
}

//MARK: - Notification call backs
extension EpisodeDetails {
    @objc func downloadCompleteSoUpdateArray( data: NSNotification) {
    }
    
    @objc func downloadTaskIsInProgress( data: NSNotification) {
        let aditionalText = data.userInfo
        self.progressView.isHidden = false
        self.downloadButton.isEnabled = false
        if let temp = aditionalText!["progress"] as? Double {
            self.progressView.setProgress(CGFloat(temp), animated: true)
        }
        
    }
    
    @objc func downloadTaskCompleted( data: NSNotification) {
        // Downlaod completed
        self.progressView.isHidden = true
        self.downloadButton.isEnabled = false
        
        PodcastRealmHelper.sharedInstance.addThisPodcast(podcast: self.podcast, trackID: self.podcast.TrackId)
    }
}

//MARK: - IBAction Methods
extension EpisodeDetails {
    @IBAction func actionBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPlayButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.stopPlaying), object: nil)
        //<adv>
        self.playAdvertisement()
        //self.playEpisode()
    }
    
    @IBAction func actionDownloadButton(_ sender: UIButton) {
        //        if Utility.main.isThisPodcastFileExist(with: self.podcast.PodcastId, EpisodeID: self.podcast.episodes[episodeIndex].PodcastEpisodeID) {
        //            Utility.main.showAlert(message: "File is already downlaoded", title: "Message")
        //            return
        //        }
        let deletedItemsCSV = AppStateManager.sharedInstance.loggedInUser.deletedItems ?? ""
        var arrDeletedItems = deletedItemsCSV.components(separatedBy: ",")
        if !arrDeletedItems.contains("\(self.podcast.episodes[episodeIndex].PodcastEpisodeID)"){
            self.processStartDownload()
        }
        else{
            for (i,obj) in arrDeletedItems.enumerated(){
                if obj == "\(self.podcast.episodes[episodeIndex].PodcastEpisodeID)"{
                    arrDeletedItems.remove(at: i)
                    try! Global.APP_REALM?.write(){
                        AppStateManager.sharedInstance.loggedInUser.deletedItems = arrDeletedItems.joined(separator: ",")
                        Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                    }
                    break
                }
            }
        }
    }
    
    @IBAction func actionShareButton(_ sender: UIButton) {
        if let urlString = self.podcast.episodes[episodeIndex].EpisodeWebUrl {
            guard let _ = URL(string: urlString) else {
                Utility.main.showAlert(message: MiscStrings.sharingUrlNotFound.text, title: AlertTitles.error.message)
                return
            }
            Utility.main.shareAcitivityWithURL(urlString,
                                               title: self.podcast.Name,
                                               subtitle: self.podcast.episodes[episodeIndex].EpisodeTitle)
        } else {
            Utility.main.showAlert(message: MiscStrings.sharingUrlNotFound.text, title: AlertTitles.error.message)
        }
    }
}

//MARK: - Web APIs Methods
extension EpisodeDetails {
    func addListeningEventPodcast() {
        let params: [String: Any] = ["PodcastID": self.podcast.PodcastId,
                                     "EpisodeID": self.podcast.episodes[episodeIndex].PodcastEpisodeID,
                                     "PodcastPath": "a"]
        
        APIManager.sharedInstance.podcastApiManager.addListeningEventPodcast(with: params, success: { (success) in
            print("AddListeningEventPodcast")
        }) { (error) in
            print(error.localizedDescription)
        }
    }// type and itemid
    func addToListen(type:String,itemid:String){
        let params:[String:Any] = ["Type":type,
                                  "ItemId":itemid]
        print(params)
        APIManager.sharedInstance.podcastApiManager.AddToListen(params: params, success: { (response) in
            print(response)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}

extension EpisodeDetails: AdvertiseDelegate {
    func advertisementIsStop() {
        self.playEpisode()
    }
}
