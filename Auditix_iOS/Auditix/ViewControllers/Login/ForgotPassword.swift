//
//  ForgotPassword.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPassword: UIViewController {
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.delegate = self
        if LanguageManager.sharedInstance.getSelectedLocale().contains("ar"){
            self.setArabicPlaceHolder()
        }
        // Do any additional setup after loading the view.
    }
}
// MARK: - SENT EVETNS
extension ForgotPassword
{
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)
        let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        if(self.checkAllFieldsAreNotEmpty() && Validation.isValidEmail(self.emailTextField.text!))  {
            self.resetPassword()
        }
        
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.emailNotValid.message
        }
        else {
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.email.text
        }
    }
    
    func checkAllFieldsAreNotEmpty() -> Bool
    {
        if(!(self.emailTextField.text?.isEmpty)!) {
            return true
        }
        self.emailTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
        self.emailTextField.errorMessage = AlertMessages.emailNotValid.message
        return false
        
    }
    func setArabicPlaceHolder(){
        self.emailTextField.placeholder = "البريد الإلكتروني"
    }
}
//MARK: - API CALLING
extension ForgotPassword
{
    func resetPassword() {
        self.view.endEditing(true)
        let param : [String : Any] = [
            "Email" : self.emailTextField.text!
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.ResetPassword(param: param, success: {(responceBool) in
            if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "ResetCode") as? ResetCode {
                dvc.emailAddress = self.emailTextField.text!
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
}

// MARK:- UITextField Delegate
extension ForgotPassword: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
