//
//  ResetCode.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import PinCodeTextField

enum ResetCodeType: Int {
    case SignUp
    case Email
}

class ResetCode: UIViewController {
    
    var type: ResetCodeType = .Email
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var didNotGetCodeLabel: UILabel!
    
    @IBOutlet weak var pinCodeTextFieldsView: PinCodeTextField!
    
    public var emailAddress: String = "" // need this email address for reset password...
    var flag = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.pinCodeTextFieldsView.keyboardType = .numberPad
        self.pinCodeTextFieldsView.delegate = self
        self.setupUI()
        self.setDoneOnKeyboard()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupUI() {
        self.didNotGetCodeLabel.text = MiscStrings.didNotRecieveCodeYet.text
        if self.type == .SignUp {
            self.titleLabel.text = MiscStrings.enterEmailVerificationCode.text
            self.subTitleLabel.text = String(format: "\(MiscStrings.codeHasBeenSentToYour.text) %@", self.emailAddress)
        } else {
            self.titleLabel.text = MiscStrings.enterResetCode.text
            self.subTitleLabel.text = MiscStrings.resetCodeHasBeenSentToYourEmail.text
        }
    }
}

// MARK: - IB ACTIONS
extension ResetCode {
    @IBAction func actionSubmit(_ sender: Any) {
        if let pinCode: String = self.pinCodeTextFieldsView.text,
            pinCode.count == self.pinCodeTextFieldsView.characterLimit {
            switch self.type {
            case .SignUp:
                self.verifyPhoneCode(with: pinCode)
                break
            case .Email:
                self.checkIsCodeValid(with: pinCode)
                break
            }
        }
    }
    
    @IBAction func actionResetCode(_ sender: Any) {
        switch self.type {
        case .SignUp:
            self.resetEmailCode()
            break
        case .Email:
            self.resetPassword()
            break
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - API CALLING
extension ResetCode
{
    func resetPassword() {
        let param : [String : Any] = [
            "Email" : self.emailAddress
        ]
        
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.ResetPassword(param: param, success: {(responceBool) in
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    func resetEmailCode() {
        let param : [String : Any] = ["Email" : self.emailAddress]
        print(param)
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.ResetPassword(param: param, success: {(responseBool) in
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    func checkIsCodeValid(with pinCode: String) {
        Utility.main.showLoader()
        let param : [String : Any] = [
            "Email" : self.emailAddress,
            "Code" : pinCode
        ]
        APIManager.sharedInstance.usersAPIManager.validCode(param: param, success: {(responceObj) in
            let user: User = User(value: responceObj)
            AppStateManager.sharedInstance.createUser(with: user)
            Constants.APP_DELEGATE.showHome()
            
            Utility.main.hideLoader()
            }, failure: {(failure) in
                Utility.main.hideLoader()
        })
    }
    
    func verifyPhoneCode(with pinCode: String) {
        Utility.main.showLoader()
        let param : [String : Any] = [
            "Code" : pinCode
        ]
        APIManager.sharedInstance.usersAPIManager.verifyPhoneCode(param: param, success: { (response) in
            Utility.main.hideLoader()
            AppStateManager.sharedInstance.updateUserVerification(with: true)
            Constants.APP_DELEGATE.showHome()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
}
extension ResetCode{
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dismissKeyboard))
        doneBarButton.tintColor = UIColor.gray
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.pinCodeTextFieldsView.inputAccessoryView = keyboardToolbar
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
extension ResetCode: PinCodeTextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        if !flag {return}
        self.view.frame.origin.y -= 100
        flag = false
    }
    func textFieldDidEndEditing(_ textField: PinCodeTextField) {
        if flag {return}
        self.view.frame.origin.y += 100
        flag = true
    }
}
