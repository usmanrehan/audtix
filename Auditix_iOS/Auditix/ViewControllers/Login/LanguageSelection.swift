//
//  LanguageSelection.swift
//  Auditix
//
//  Created by shardha on 1/1/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
//import Hero
class LanguageSelection: UIViewController {
    
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonEnglish: UIButton!
    @IBOutlet weak var buttonArabic: UIButton!
    let colorOrange = UIColor(displayP3Red: 246/255.0, green: 144/255.0, blue: 119/255.0, alpha: 1.0)
    
   var lang = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if LanguageManager.sharedInstance.getSelectedLocale() == "en" {
            self.buttonEnglish.setTitleColor(self.colorOrange, for: .normal)
            self.buttonArabic.setTitleColor(UIColor.darkGray, for: .normal)
            self.lang = "en"
        }else {
            self.buttonArabic.setTitleColor(self.colorOrange, for: .normal)
            self.buttonEnglish.setTitleColor(UIColor.darkGray, for: .normal)
            self.lang = "ar"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        if self.lang == LanguageManager.sharedInstance.getSelectedLocale() {
            if AppStateManager.sharedInstance.isUserLoggedIn() {
                if let user = AppStateManager.sharedInstance.loggedInUser, user.IsVerified {
                    Constants.APP_DELEGATE.showHome()
                    return
                }
            }
            
            if Singleton.sharedInstance.walkThroughDataArray.count > 0 {
                if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "WalkThroughController") as? WalkThroughController {
                    self.navigationController?.pushViewController(dvc, animated: true)
                    return
                }
            }
            
            Constants.APP_DELEGATE.showLoginScreen()
            
        }else {
            let alertController = UIAlertController(title: AlertTitles.alert.message, message: MiscStrings.pleaseRestartTheApplication.text, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default) {
                UIAlertAction in
                LanguageManager.sharedInstance.setLocale(self.lang)
                exit(0)
            }
            let cancelAction = UIAlertAction(title: MiscStrings.cancel.text, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func actionEnglish(_ sender: Any) {
        self.buttonEnglish.setTitleColor(self.colorOrange, for: .normal)
        self.buttonArabic.setTitleColor(UIColor.darkGray, for: .normal)
        self.lang = "en"
        
    }
    
    @IBAction func actionArabic(_ sender: UIButton) {
        //Utility.main.showToast(message: AlertMessages.WillImplementInNextMode.message, controller: self)
        self.buttonArabic.setTitleColor(self.colorOrange, for: .normal)
        self.buttonEnglish.setTitleColor(UIColor.darkGray, for: .normal)
        self.lang = "ar"
    }
}
