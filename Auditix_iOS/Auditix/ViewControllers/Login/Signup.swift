//
//  Signup.swift
//  Auditix
//
//  Created by shardha on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import DatePickerDialog
import SkyFloatingLabelTextField
import MICountryPicker

class Signup: UIViewController {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var fullNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var dialCodeTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var dobLabel: UILabel!
    
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    
    @IBOutlet weak var constraintViewTop: NSLayoutConstraint!
    // sent this string in service
    var dobText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.emailTextField.delegate = self
        self.phoneTextField.delegate = self
        self.countryTextField.delegate = self
        self.fullNameTextField.delegate = self
        self.passwordTextField.delegate = self
        self.dialCodeTextField.delegate = self
        self.confirmPasswordTextField.delegate = self
        
        self.setupUI()
        if LanguageManager.sharedInstance.getSelectedLocale() == "ar"{
            self.setArabicPlaceHolders()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupUI() {

        //Add right view on country textfield
        self.countryTextField.rightViewMode = .always
        let imageView = UIImageView(image: UIImage(named: "down-arrow-orange"))
        imageView.frame = CGRect(x: 0, y: 0, width: 20, height: self.countryTextField.frame.size.height)
        imageView.contentMode = .scaleAspectFit
        self.countryTextField.rightView = imageView
        
        if LanguageManager.sharedInstance.getSelectedLocale() == "ar" {
            self.maleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            self.femaleBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        }
    }
    
    private func setArabicPlaceHolders(){
        self.emailTextField.placeholder = "البريد الإلكتروني"
        self.fullNameTextField.placeholder = "الاسم الكامل"
        self.confirmPasswordTextField.placeholder = "تأكيد كلمة المرور"
        self.passwordTextField.placeholder = "كلمه السر"
        self.countryTextField.placeholder = "بلد"
        self.phoneTextField.placeholder = "رقم الهاتف (اختيارى)"
        self.dialCodeTextField.placeholder = "الشفرة"
    }
    // show date picker
    func datePickerTapped() {
        let currentDate: Date = Date()
        var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(identifier: "UTC")!
        var components: DateComponents = DateComponents()
        components.calendar = calendar
        components.year = -20
        let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
        components.year = -20
        DatePickerDialog().show("Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", maximumDate: maxDate, datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                if date! > Date() {
                    Utility.main.showToast(message: AlertMessages.invalidDate.message, controller: self)
                }
                else {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MMM dd,yyyy"
                    self.dobLabel.text = formatter.string(from: dt)
                    self.dobLabel.textColor = Constants.THEME_ORANGE_COLOR
                    
                    formatter.dateFormat = "yyyy-MM-dd"
                    
                    self.dobText = formatter.string(from: dt)
                }
               
            }
        }
    }
    
    func showCountryPicker() {
        //Initialize country picker controller
        let controller = MICountryPicker { (country, code) in
            self.countryTextField.text = country
            self.dismiss(animated: true, completion: nil)
        }
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.dissmissCountryPicker))
        controller.navigationItem.rightBarButtonItem = closeItem
        
        let navigationController = UINavigationController(rootViewController: controller)

        self.present(navigationController, animated: true, completion: nil)
    }
    
    func showDialCodePicker() {
        //Initialize country picker controller
        let controller = MICountryPicker()
        controller.showCallingCodes = true
        controller.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
            self.dialCodeTextField.text = dialCode
            self.dismiss(animated: true, completion: nil)
        }
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.dissmissCountryPicker))
        controller.navigationItem.rightBarButtonItem = closeItem
        
        let navigationController = UINavigationController(rootViewController: controller)
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func dissmissCountryPicker() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func navigateToResetCode(_ email: String) {
        if let controller = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "ResetCode") as? ResetCode {
            controller.type = .SignUp
            controller.emailAddress = email
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

// MARK:- VALIDATION METHODS
extension Signup
{
    /*
     if(!(self.emailTextField.text?.isEmpty)! && !(self.passwordTextField.text?.isEmpty)! && !(self.fullNameTextField.text?.isEmpty)! && !(self.confirmPasswordTextField.text?.isEmpty)! && !(self.dobLabel.text?.isEmpty)! && !(self.dialCodeTextField.text?.isEmpty)! && !(self.phoneTextField.text?.isEmpty)! && !(self.countryTextField.text?.isEmpty)!) {
     return true
     
     }
     */
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!(self.emailTextField.text?.isEmpty)! && !(self.passwordTextField.text?.isEmpty)! && !(self.fullNameTextField.text?.isEmpty)! && !(self.confirmPasswordTextField.text?.isEmpty)! && !(self.countryTextField.text?.isEmpty)!) {
            return true
        
        }
        self.emailTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
        self.emailTextField.errorMessage = AlertMessages.allFieldsRequired.message
        return false
        
    }
    func validateEmailAddress() -> Bool {
        
        if(Validation.isValidEmail(self.emailTextField.text!)) {
            return true
        }

        return false
    }
    
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.passwordTextField.text!)) {
            return true
        }

        return false
    }
    
    func validConfirmPassword() -> Bool {
        if(Validation.isConfirmPasswordIsEqualToPassword(password: self.passwordTextField.text!, confirm: self.confirmPasswordTextField.text!)) {
            return true
        }
        
        return false
    }
    
    func validPhoneNumber() -> Bool {
        if Validation.isValidPhone(value: self.phoneTextField.text!) {
            return true
        }
        
        return false
    }
    
    func returnMyGender() -> Int {
        if(self.maleBtn.isSelected) {
            return 0
        }
        else if(self.femaleBtn.isSelected) {
            return 1
        }
        return 0
    }
}

// MARK:- IB ACTIONS
extension Signup
{
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)
        let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionDateOfBirth(_ sender: Any) {
        self.datePickerTapped()
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        if self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword() && self.validConfirmPassword(){
            if (self.dialCodeTextField.text ?? "").isEmpty && !(self.phoneTextField.text ?? "").isEmpty{return}
            self.registerUser()
        }
    }
    
    @IBAction func actionMaleBtn(_ sender: Any) {
        self.maleBtn.isSelected = true
        self.femaleBtn.isSelected = false
    }
    @IBAction func actionFemaleBtn(_ sender: Any) {
        self.maleBtn.isSelected = false
        self.femaleBtn.isSelected = true
    }
    
    // MARK:-
    
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.emailNotValid.message
        }
        else {
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.email.text
        }
    }
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordInValidShort.message
        }
        else {
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.password.text
        }
    }
    @IBAction func confirmPasswordChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isConfirmPasswordIsEqualToPassword(password: self.passwordTextField.text!, confirm: textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordNotMatchShort.message
        }
        else {
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.confirmPassword.text
        }
    }
}

// MARK:- API CALLING
extension Signup {
    public func registerUser() {
        Utility.main.showLoader()
        let phoneNo = String(format: "%@%@", self.dialCodeTextField.text!, self.phoneTextField.text!)
        let email = self.emailTextField.text ?? ""
        let param: [String : Any] = [
            "FullName" : self.fullNameTextField.text!,
            "Email" : self.emailTextField.text!,
            "Password" : self.passwordTextField.text!,
            "ConfirmPassword" : self.passwordTextField.text!,
            "DOB" : self.dobText,
            "Gender" : self.returnMyGender(),
            "Country": self.countryTextField.text!,
            "PhoneNo": phoneNo
        ]
        
        APIManager.sharedInstance.usersAPIManager.RegisterUser(params: param, success: { (responceDic) in
            Utility.main.hideLoader()
            let user: User = User(value: responceDic)
            AppStateManager.sharedInstance.createUser(with: user)
            self.navigateToResetCode(email)
        }, failure: {
            (error) in
            Utility.main.hideLoader()
        })
    }
}

// MARK:- UITextField Delegate
extension Signup: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.countryTextField {
            showCountryPicker()
            return false
        } else if textField == self.dialCodeTextField {
            showDialCodePicker()
            return false
        }
        
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


