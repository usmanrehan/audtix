//
//  LoginSelection.swift
//  Auditix
//
//  Created by shardha on 1/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class LoginSelection: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

// MARK:- IB ACTIONS
extension LoginSelection
{
    @IBAction func actionSignIn(_ sender: Any) {
        if let login = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "Login") as? Login {
            self.navigationController?.pushViewController(login, animated: true)
        }
    }
    
    @IBAction func actionSignUp(_ sender: Any) {
        if let signup = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "Signup") as? Signup {
            self.navigationController?.pushViewController(signup, animated: true)
        }
    }
    @IBAction func actionSkip(_ sender: Any) {
//         Utility.main.showToast(message: AlertMessages.WillImplementInNextMode.rawValue, controller: self)
        self.guestRegistration()
    }
    
}

// MARK: - API CALLING
extension LoginSelection
{
    func guestRegistration() {
        let param: [String : Any] = [
            "DeviceName" : Constants.DEVICE_NAME.lowercased(),
            "UniversalDeviceID" : Constants.DEVICE_UDID,
            "IsAndroidPlatform" : false,
            "IsPlayStore" : false,
            "IsProduction" : false,
            "AuthToken" : false
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.GuestRegistration(params: param, success: { (responceDic) in
            let user: User = User(value: responceDic)
            user.isGuestLogin = true
            AppStateManager.sharedInstance.createUser(with: user)
            Constants.APP_DELEGATE.showHome()
            Utility.main.hideLoader()
        }) { (failure) in
            Utility.main.hideLoader()
        }
        
    }
}
