//
//  Login.swift
//  Auditix
//
//  Created by shardha on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SkyFloatingLabelTextField

class Login: UIViewController {
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var viewForgotPasswordLine: UIView!
    @IBOutlet weak var signInGoogleBtn: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Google Sign in delegate
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        if LanguageManager.sharedInstance.getSelectedLocale().contains("ar"){
            self.setArabicPlaceHolders()
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func passwordEditingBegins(_ sender: Any) {
        self.viewForgotPasswordLine.backgroundColor = Constants.THEME_ORANGE_COLOR
    }
    
    @IBAction func passwordEditingEnd(_ sender: Any) {
        if(!validationPassword()) {
            self.viewForgotPasswordLine.backgroundColor = Constants.FIELD_VALIDATION_RED_COLOR
        }
        else {
            self.viewForgotPasswordLine.backgroundColor = UIColor.darkGray
        }
    }
    
    func navigateToResetCode(_ email: String) {
        if let controller = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "ResetCode") as? ResetCode {
            controller.emailAddress = email
            controller.type = .SignUp
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}
// MARK:- HELPER METHODS
extension Login
{
    func checkAllFieldsAreNotEmpty() -> Bool
    {
        if(!(self.emailTextField.text?.isEmpty)! && !(self.passwordTextField.text?.isEmpty)!) {
            self.emailTextField.errorMessage = ""
            return true
        }
        self.emailTextField.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
        self.emailTextField.errorMessage = AlertMessages.allFieldsRequired.message
        return false
        
    }
    func validateEmailAddress() -> Bool {
        
        if(Validation.isValidEmail(self.emailTextField.text!)) {
            return true
        }
        return false
        
    }
    
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.passwordTextField.text!)) {
            return true
        }
        return false
    }
    
    func getFBUserData() {
        if(FBSDKAccessToken.current() != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name,picture.type(large), email, gender, birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                if (result != nil){
                    //everything works print the user data
                    self.registerFBUser(result as AnyObject)
                }
                else {
                    print(error?.localizedDescription ?? "facebook login error occures")
                }
            })
        }
    }
    
    func registerFBUser(_ result: AnyObject) {
        
        var id = ""
        var fullName = ""
        var profilePicture = ""
        var gender: Int = 0
        var birthday = ""
        
        if let socialMediaId = result["id"] as? String { id = socialMediaId }
        if let name = result["name"] as? String { fullName = name }
        if let profilePic = ((result["picture"] as! [String : Any])["data"] as! [String : Any])["url"] as? String { profilePicture = profilePic }
        if let gen = result["gender"] as? String { if(gen == "male") { gender = 0 } else { gender = 1 }}
        if let bday = result["birthday"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/DD/YYYY"
            
            if let date = dateFormatter.date(from: bday) {
                dateFormatter.dateFormat = "yyyy-MM-dd"
                birthday = dateFormatter.string(from: date)
            }
            
        }
        
        let params: [String: Any] = [
            "ProviderKey": id,
            "FullName": fullName,
            "Gender" : gender,
            "DOB": birthday,
            "AuthProvider" : 1, // 1 for facebook
            "ImageUrl": profilePicture
        ]
        
        print(params)
        
        self.socialLogin(with: params)
    }
    
    func setArabicPlaceHolders(){
        self.emailTextField.placeholder = "البريد الإلكتروني"
        self.passwordTextField.placeholder = "كلمه السر"
    }
}
// MARK:- API CALLING
extension Login {
    private func onLoginSuccessResponse(_ response: Dictionary<String,AnyObject>) {
        let user: User = User(value: response)
        AppStateManager.sharedInstance.createUser(with: user)
        if user.IsVerified {
            Constants.APP_DELEGATE.showHome()
        } else {
            if let email = user.EmailAddress {
                self.navigateToResetCode(email)
            } else {
                Utility.main.showAlert(message: MiscStrings.accountIsNotVerifiedAndPhoneNumberIsNotProvided.text, title: AlertTitles.error.message)
            }
        }
    }
    
    private func LoginThisUser() {
        Utility.main.showLoader()
        var param = [String : Any]()
        param["Email"] = self.emailTextField.text!
        param["Password"] = self.passwordTextField.text!
        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
            param["culture"] = 0
        }
        else{
            param["culture"] = 1
        }
        APIManager.sharedInstance.usersAPIManager.LoginUser(params: param, success: { (responceDic) in
            Utility.main.hideLoader()
            self.onLoginSuccessResponse(responceDic)
        }, failure: {
            (error) in
            Utility.main.hideLoader()
        })
    }
    func socialLogin(with param: [String : Any]) {
        Utility.main.showLoader()
        APIManager.sharedInstance.usersAPIManager.socailLogin(param: param, success: {
            (responceDic) in
            let user: User = User(value: responceDic)
            user.IsVerified = true
            AppStateManager.sharedInstance.createUser(with: user)
            Constants.APP_DELEGATE.showHome()
            Utility.main.hideLoader()
            
        }, failure: {
            (error) in
            Utility.main.hideLoader()
        })
    }
}
// MARK:- Events
extension Login
{
    @IBAction func actionBack(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func actionForgot(_ sender: Any) {
        self.view.endEditing(true)
        if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword {
            let when = DispatchTime.now() + 0.2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            
        }
    }
    
    @IBAction func actionSignup(_ sender: Any) {
        
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword()) {
            self.LoginThisUser()
        }
    }
    
    @IBAction func actionFacebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        //withReadPermissions: ["public_profile", "email"]
        fbLoginManager.logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) -> Void in
            if (error == nil){
                if(result != nil)
                {
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    
                    if(fbloginresult.grantedPermissions != nil)
                    {
                        self.getFBUserData()
                    }
                }
            }
            else{
                Utility.main.showToast(message: (error?.localizedDescription ?? "Facebook login failed."), controller: self)
            }
        }
    }
    
    @IBAction func actionGoogle(_ sender: GIDSignInButton) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    //MARK: -
    
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.emailNotValid.message
        }
        else {
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.email.text
        }
    }
    
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = Constants.FIELD_VALIDATION_RED_COLOR
            self.viewForgotPasswordLine.backgroundColor = Constants.FIELD_VALIDATION_RED_COLOR
            textfield.errorMessage = AlertMessages.passwordInValidShort.message
        }
        else {
            self.viewForgotPasswordLine.backgroundColor = Constants.THEME_ORANGE_COLOR
            textfield.errorMessage = ""
            textfield.selectedTitle = MiscStrings.password.text
        }
    }
}

// MARK: - GIDSignInUI Delegate

extension Login: GIDSignInUIDelegate, GIDSignInDelegate
{
    // MARK: GOOGLE SIGN IN
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            
            
            let url = URL(string: Constants.getGoogleUserInfo + user.authentication.accessToken!)
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "GET"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            let session = URLSession.shared
            Utility.main.showLoader()
            session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                do {
                    let id = user.authentication.clientID
                    let fullName = user.profile.name
                    var email = ""
                    var profilePicture = ""
                    var gender: Int = 0
                    let birthday = ""
                    
                    if let userData = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String:AnyObject] {
                        if let picture = userData["picture"] as? String { profilePicture = picture }
                        if let genderOfUser = userData["gender"] as? String { if(genderOfUser == "male") { gender = 0 } else { gender = 1 } }
                        if let g_email = userData["email"] as? String{email = g_email}
                    }
                    
                    let params: [String: Any] = [
                        "ProviderKey": id ?? "0",
                        "FullName": fullName ?? "-",
                        "email":email,
                        "Gender" : gender,
                        "DOB": birthday,
                        "AuthProvider" : 2, // 1 for facebook
                        "ImageUrl": profilePicture
                    ]
                    
                    self.socialLogin(with: params)
                    
                } catch {
                    Utility.main.showToast(message: AlertMessages.googleSignInFail.message, controller: self)
                    NSLog("Account Information could not be loaded")
                }
                Utility.main.hideLoader()
            }).resume()
            
            
        } else {
            Utility.main.showToast(message: AlertMessages.googleSignInFail.message, controller: self)
            Utility.main.hideLoader()
            print("\(error.localizedDescription)")
        }
    }
    
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}

// MARK:- UITextField Delegate
extension Login: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}




