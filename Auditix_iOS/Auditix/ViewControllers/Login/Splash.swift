//
//  Splash.swift
//  Auditix
//
//  Created by shardha on 1/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Hero

class Splash: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            if let dvc = AppStoryboard.LoginModule.instance.instantiateViewController(withIdentifier: "LanguageSelection") as? LanguageSelection {
                    dvc.isHeroEnabled = true
                    self.navigationController?.heroNavigationAnimationType = .fade
                    self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
/*
let delegate = UIApplication.shared.delegate as! AppDelegate
delegate.showLanguageSelection()
*/
