//
//  Settings.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

/*myAccount
 
 notifications
 privacyPolicy
 termsAndConditions*/

enum SettingsCellIds: String
{
    case myAccount = "myAccount"
    case PaymentDetails = "PaymentDetails"
    case continousPlay = "continousPlay"
    case newEpisode = "newEpisode"
    case playedEpisode = "playedEpisode"
    case notifications = "notifications"
    case privacyPolicy = "privacyPolicy"
    case termsAndConditions = "termsAndConditions"
    
    var message: String { return self.rawValue }
}

class Settings: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    //let settingIDsArray: [SettingsCellIds] = [.myAccount, .PaymentDetails, .continousPlay, .newEpisode, .playedEpisode, .notifications, .privacyPolicy, .termsAndConditions]
    let settingIDsArray: [SettingsCellIds] = [.myAccount, .continousPlay, .newEpisode, .playedEpisode, .notifications, .privacyPolicy, .termsAndConditions]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
// MARK: - IB ACTIONS
extension Settings
{
    @IBAction func actionSwitchChange(_ sender: Any) {
        //         Utility.main.showToast(message: AlertMessages.WillImplementInNextMode.rawValue, controller: self)
        let switchObj = sender as? UISwitch
        if (switchObj?.isOn)! {
            Constants.USER_DEFAULTS.set(true, forKey: Constants.ContinuousPlay)
        }
        else {
            Constants.USER_DEFAULTS.set(false, forKey: Constants.ContinuousPlay)
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension Settings: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settingIDsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.settingIDsArray[indexPath.row].rawValue) as? ContinousPlayCell else { return UITableViewCell() }
            if let boolean = Constants.USER_DEFAULTS.value(forKey: Constants.ContinuousPlay) as? Bool {
                cell.continuousPlaySwtich.isOn = boolean
            }
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.settingIDsArray[indexPath.row].rawValue) else { return UITableViewCell() }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cellID = self.settingIDsArray[indexPath.row]
        switch cellID {
        case .myAccount:
            if let evc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
                self.navigationController?.pushViewController(evc, animated: true)
            }
            break
        case .PaymentDetails:
            Utility.main.showToast(message: AlertMessages.WillImplementInNextMode.message, controller: self)
            break
        case .continousPlay:
            break
        case .newEpisode:
            if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "NewEpisodes") as? NewEpisodes {
                self.navigationController?.pushViewController(nvc, animated: true)
            }
            break
        case .playedEpisode:
            if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "PlayedEpisode") as? PlayedEpisode {
                self.navigationController?.pushViewController(nvc, animated: true)
            }
            break
        case .notifications:
            if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
                nvc.notificationType = .settings
                self.navigationController?.pushViewController(nvc, animated: true)
            }
            break
        case .privacyPolicy:
            if let pvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Privacy") as? Privacy {
                self.navigationController?.pushViewController(pvc, animated: true)
            }
            break
        case .termsAndConditions:
            if let tvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "AboutAndTnC") as? AboutAndTnC {
                tvc.TitleText = AlertMessages.TncTitle.message
                
                tvc.AboutUsOrTnC = 1 // 1 for TnC
                self.navigationController?.pushViewController(tvc, animated: true)
            }
            break
        }
    }
}

class ContinousPlayCell: UITableViewCell {
    @IBOutlet weak var continuousPlaySwtich: UISwitch!
}
