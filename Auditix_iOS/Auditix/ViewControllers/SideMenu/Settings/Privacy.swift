//
//  Privacy.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Privacy: UIViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var cmsArray = [CMSContent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.text = ""
        self.getCMSContent()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCMSContent() {
        var param = [String:Any]()
        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
            param["culture"] = 0
        }
        else{
            param["culture"] = 1
        }
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAllCMS(success: { (cmsArray) in
            for cmsContent in cmsArray {
                self.cmsArray.append(CMSContent(ID: cmsContent["ID"] as! Int, Key: cmsContent["Key"] as! Int, Name: cmsContent["Name"] as? String, Value: cmsContent["Value"] as? String))// .append())
            }
            for cms in self.cmsArray {
                if cms.Key == 2 {
                    self.textView.text = cms.Value
                    break
                }
                
            }
            Utility.main.hideLoader()
        }, failure: { (failure) in
            Utility.main.hideLoader()
        }, param: param)
    }
}

