//
//  AutoDownload.swift
//  Auditix
//
//  Created by Ahmed Shahid on 7/2/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class AutoDownload: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrPodcastList = Array<PodcastModel>()
    var arrNewsChannelList = Array<NewsListModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getAllNewsPodcasts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - IBAction Methods
extension AutoDownload {
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionToggleSwitch(_ sender: UISwitch) {
        let position = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: position) {
            switch indexPath.section {
            case 0:
                let podcast = self.arrPodcastList[indexPath.row]
                if !sender.isOn {
                    self.unsetAutodownloadPodcast(podcast.PodcastId, sender: sender)
                } else {
                    self.setAutodownloadPodcast(podcast.PodcastId, sender: sender)
                }
                break
            case 1:
                let newsChannel = self.arrNewsChannelList[indexPath.row]
                if !sender.isOn {
                    self.unsetAutodownloadNews(newsChannel.NewsID, sender: sender)
                } else {
                    self.setAutodownloadNews(newsChannel.NewsID, sender: sender)
                }
                break
            default:
                break
            }
        }
    }
}

//MARK: - API Methods
extension AutoDownload {
    private func getAllNewsPodcasts() {
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAllNewsPodcasts(success: { (response) in
            Utility.main.hideLoader()
            if let arrNews = response["News"] as? Array<AnyObject> {
                self.arrNewsChannelList.removeAll()
                for item in arrNews {
                    let model = NewsListModel(value: item)
                    self.arrNewsChannelList.append(model)
                }
            }
            if let arrPodcasts = response["Podcast"] as? Array<AnyObject> {
                self.arrPodcastList.removeAll()
                for item in arrPodcasts {
                    let model = PodcastModel(value: item)
                    self.arrPodcastList.append(model)
                }
            }
            
            self.tableView.reloadData()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }

    private func setAutodownloadPodcast(_ podcastId: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "PodcastId": podcastId
        ]
        APIManager.sharedInstance.podcastApiManager.setAutodownloadPodcast(with: params, success: { (response) in
            //Utility.main.hideLoader()
            //sender.isOn = true
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
            //sender.isOn = false
        }
    }

    private func unsetAutodownloadPodcast(_ podcastId: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "PodcastId": podcastId
        ]
        APIManager.sharedInstance.podcastApiManager.unsetAutodownloadPodcast(with: params, success: { (response) in
            //Utility.main.hideLoader()
            //sender.isOn = false
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
            //sender.isOn = true
        }
    }
    
    private func setAutodownloadNews(_ newsId: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "NewsId": newsId
        ]
        APIManager.sharedInstance.newsApiManager.setAutodownloadNews(with: params, success: { (response) in
            //Utility.main.hideLoader()
            //sender.isSelected = true
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
    
    private func unsetAutodownloadNews(_ newsId: Int, sender: UISwitch) {
        Utility.main.showLoader()
        let params: [String: Any] = [
            "NewsId": newsId
        ]
        APIManager.sharedInstance.newsApiManager.unsetAutodownloadNews(with: params, success: { (response) in
//            Utility.main.hideLoader()
//            sender.isSelected = false
            self.getAllNewsPodcasts()
        }) { (error) in
            Utility.main.hideLoader()
        }
    }
}


// MARK: - UITableView DataSource Delegate
extension AutoDownload: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return MiscStrings.podcastChannels.text//"Podcast Channels"
        case 1:
            return MiscStrings.newsChannels.text//"News Channels"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.arrPodcastList.count
        case 1:
            return self.arrNewsChannelList.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AutoDownloadCellIdentifier", for: indexPath) as? AutoDownloadViewCell else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            let podcast = self.arrPodcastList[indexPath.row]
            cell.titleLabel.text = podcast.Name
            cell.toggleSwith.isOn = podcast.AutoDownload
            if let imageURL = podcast.ImageUrl {
                cell.channelImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
        } else {
            let newsChannel = self.arrNewsChannelList[indexPath.row]
            cell.titleLabel.text = newsChannel.Name
            cell.toggleSwith.isOn = newsChannel.AutoDownload
            if let imageURL = newsChannel.ImageUrl {
                cell.channelImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
