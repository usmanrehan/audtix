//
//  AboutAndTnC.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

struct CMSContent {
    var ID = 0
    var Key = 0
    var Name: String?
    var Value: String?
    //
    //    init(parameter: [String : Any]) {
    //        ID = parameter["ID"] as! Int
    //        Key = parameter["Key"] as! Int
    //        if let nameString  = parameter["Name"] as? String {
    //             Name = nameString
    //        }
    //        if let valueStr = parameter["Value"] as? String {
    //            Value = valueStr
    //        }
    //    }
}

class AboutAndTnC: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var cmsArray = [CMSContent]()
    
    public var TitleText: String = ""
    public var AboutUsOrTnC: Int = -1 // 0 for about us, 1 for tnc
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.text = ""
        self.labelTitle.text = self.TitleText
        self.getCMSContent()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AboutAndTnC
{
    func getCMSContent() {
        var param = [String:Any]()
        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
            param["culture"] = 0
        }
        else{
            param["culture"] = 1
        }
        Utility.main.showLoader()
        APIManager.sharedInstance.miscellaneousAPIManager.getAllCMS(success: { (cmsArray) in
            for cmsContent in cmsArray {
                self.cmsArray.append(CMSContent(ID: cmsContent["ID"] as! Int, Key: cmsContent["Key"] as! Int, Name: cmsContent["Name"] as? String, Value: cmsContent["Value"] as? String))// .append())
            }
            for cms in self.cmsArray {
                if self.AboutUsOrTnC == 0 && cms.Key == 3 {
                    self.textView.text = cms.Value
                    break
                } else if self.AboutUsOrTnC == 1 && cms.Key == 1 {
                    self.textView.text = cms.Value
                    break
                }
                
            }
            Utility.main.hideLoader()
        }, failure: { (failure) in
            Utility.main.hideLoader()
        }, param: param)
    }
}

