//
//  PlayedEpisode.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

struct PlayedEpisodeOptions{
    var option = ""
    var isSelected = false
}

class PlayedEpisode: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var arrPlayedEpisodeOptions = [PlayedEpisodeOptions(option: MiscStrings.deleteWhenFinished.text, isSelected: false),
                                   PlayedEpisodeOptions(option: MiscStrings.keepUntilIDelete.text, isSelected: false)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Utility.main.isKeepUntilIDeleteOn(){
            self.arrPlayedEpisodeOptions[0].isSelected = false
            self.arrPlayedEpisodeOptions[1].isSelected = true
        }
        else{
            self.arrPlayedEpisodeOptions[0].isSelected = true
            self.arrPlayedEpisodeOptions[1].isSelected = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: - IB ACTIONS
extension PlayedEpisode
{
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension PlayedEpisode: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPlayedEpisodeOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayedOption", for: indexPath) as! PlayedOption
        cell.lblOption.text = self.arrPlayedEpisodeOptions[indexPath.row].option
        if self.arrPlayedEpisodeOptions[indexPath.row].isSelected{
            cell.btnRadioButton.isSelected = true
        }
        else{
            cell.btnRadioButton.isSelected = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrPlayedEpisodeOptions[indexPath.row].isSelected{return}
        for i in 0..<self.arrPlayedEpisodeOptions.count{
            self.arrPlayedEpisodeOptions[i].isSelected = false
        }
        self.arrPlayedEpisodeOptions[indexPath.row].isSelected = true
        self.tableview.reloadData()
        if self.arrPlayedEpisodeOptions[1].isSelected{
            Constants.USER_DEFAULTS.set(true, forKey: Constants.KeepUntilIDelete)
        }
        else{
            Constants.USER_DEFAULTS.set(false, forKey: Constants.KeepUntilIDelete)
        }
    }
}
