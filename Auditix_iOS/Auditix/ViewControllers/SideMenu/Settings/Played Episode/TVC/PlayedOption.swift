//
//  PlayedOption.swift
//  Auditix
//
//  Created by Usman Bin Rehan on 7/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class PlayedOption: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnRadioButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
