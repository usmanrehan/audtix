//
//  NewEpisodes.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/25/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

enum NewEpisodeCellIds: String
{
    case autoDownload = "autoDownload"
    case downloadOnWifi = "downloadOnWifi"
    case downloadOnBoth = "downloadOnBoth"
    var message: String { return self.rawValue }
}

class NewEpisodes: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var onlyWifiSwitch: UISwitch? = nil
    var bothWifiAndDataSwitch: UISwitch? = nil
    
    let cellIDsArray: [NewEpisodeCellIds] = [.autoDownload, .downloadOnWifi, .downloadOnBoth]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - IB ACTIONS
extension NewEpisodes
{
    @IBAction func actionDownloadOnWifi(_ sender: UISwitch) {
        self.onlyWifiSwitch?.isOn = true
        self.bothWifiAndDataSwitch?.isOn = false
        Constants.USER_DEFAULTS.set(true, forKey: Constants.OnlyUseWifi)
        Constants.USER_DEFAULTS.set(false, forKey: Constants.UseBothWifiAndCellular)
    }
    
    @IBAction func actionDownloadOnBoth(_ sender: UISwitch) {
        self.onlyWifiSwitch?.isOn = false
        self.bothWifiAndDataSwitch?.isOn = true
        Constants.USER_DEFAULTS.set(false, forKey: Constants.OnlyUseWifi)
        Constants.USER_DEFAULTS.set(true, forKey: Constants.UseBothWifiAndCellular)
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension NewEpisodes: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellIDsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIDsArray[indexPath.row].rawValue) else { return UITableViewCell() }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIDsArray[indexPath.row].rawValue) as? SwitchCellClass else { return UITableViewCell() }
            if let boolean = Constants.USER_DEFAULTS.value(forKey: Constants.OnlyUseWifi) as? Bool {
                cell.cellSwitch.isOn = boolean
            }
            self.onlyWifiSwitch = cell.cellSwitch
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIDsArray[indexPath.row].rawValue) as? SwitchCellClass else { return UITableViewCell() }
            if let boolean = Constants.USER_DEFAULTS.value(forKey: Constants.UseBothWifiAndCellular) as? Bool {
                cell.cellSwitch.isOn = boolean
            }
            self.bothWifiAndDataSwitch = cell.cellSwitch
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cellID = self.cellIDsArray[indexPath.row]
        
        switch cellID {
        case .autoDownload:
            if let controller = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "AutoDownload") as? AutoDownload {
                self.navigationController?.pushViewController(controller, animated: true)
            }
            break
        default:
            break
        }
    }
}

class SwitchCellClass: UITableViewCell {
    @IBOutlet weak var cellSwitch: UISwitch!
//    @IBOutlet weak var bothWifiAndDataSwitch: UISwitch!
}
