//
//  BooksFavorites.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell
class BooksFavorites: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var fvtBooksArray = [BookModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.register(UINib(nibName: "FavoriteCell", bundle: nil), forCellReuseIdentifier: "FavoriteCell")
        // Do any additional setup after loading the view.
        self.getFavoriteBooks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.getFavoriteBooks()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: API CALLING
extension BooksFavorites
{
    func getFavoriteBooks() {
        let param: [String : Any] = [
            "culture" : 0
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.getFavoriteBooks(with: param, success: {(responceObj) in
            self.fvtBooksArray.removeAll()
            //Old Code
            //self.fvtBooksArray.append(BookModel(value: bookItem as! [String : Any]))
            //New Code Start
            if let books = responceObj["Books"] as? Array<AnyObject> {
                for bookItem in books {
                    self.fvtBooksArray.append(BookModel(value: bookItem))
                }
            }
            //New Code End
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }, failure: {(failure) in
            Utility.main.hideLoader()
        })
    }
    
    func removFromFavoriteStatus(with index: Int) {
        let bookID: Int = self.fvtBooksArray[index].BookID
        let param: [String : Any] = [
            "bookId" : bookID
        ]
        
        Utility.main.showLoader()
        APIManager.sharedInstance.bookApiManager.removeFromFvt(with: param, success: {
            (responceObj) in
            Utility.main.hideLoader()
            Utility.main.postFavoriteBookNotification()
            self.fvtBooksArray.remove(at: index)
            self.tableview.reloadData()
        }, failure: {
            (failure) in
            Utility.main.hideLoader()})
    }
}

extension BooksFavorites : UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fvtBooksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell") as? FavoriteCell  else { return UITableViewCell() }
        cell.BindData(bookModel: self.fvtBooksArray[indexPath.row])
        cell.delegate = self
        
        cell.tag = indexPath.row
        //configure right buttons 220,48,46
        cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
        cell.rightSwipeSettings.transition = .rotate3D
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.20
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true;
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        self.removFromFavoriteStatus(with: cell.tag)
        return true
    }
    
    
    
}
