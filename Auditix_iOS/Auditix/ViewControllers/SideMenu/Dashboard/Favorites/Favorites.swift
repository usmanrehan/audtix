//
//  Favorites.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Favorites: UIViewController {

        var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerNewsFavt = self.storyboard?.instantiateViewController(withIdentifier: "NewsFavorites") as! NewsFavorites

        let controllerBooksFavt = self.storyboard?.instantiateViewController(withIdentifier: "BooksFavorites") as! BooksFavorites
        let controllerPodcastFavt = self.storyboard?.instantiateViewController(withIdentifier: "PodcastFavorites") as! PodcastFavorites
        
        controllerNewsFavt.title = MiscStrings.news.text
        controllerBooksFavt.title = MiscStrings.books.text
        controllerPodcastFavt.title = MiscStrings.podcast.text
        

//        controllerArray.append(controllerNewsFavt)
        controllerArray.append(controllerPodcastFavt)
        controllerArray.append(controllerBooksFavt)
        
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.1),
            .ScrollMenuBackgroundColor(UIColor.white),
            .SelectionIndicatorColor(UIColor.orange),
            .SelectedMenuItemLabelColor(UIColor.orange),
            .UnselectedMenuItemLabelColor(UIColor.darkGray),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(10),
            .MenuHeight(50)
            
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        let navigationRoot = UINavigationController(rootViewController: pageMenu!)
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        //        self.view.addSubview(pageMenu!.view)
        self.view.addSubview((navigationRoot.topViewController?.view!)!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
