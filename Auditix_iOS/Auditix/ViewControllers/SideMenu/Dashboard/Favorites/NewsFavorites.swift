//
//  NewsFavorites.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell
class NewsFavorites: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var favoriteNews = [NewsFavoriteModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllFavoriteNews()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - API CALLING
extension NewsFavorites {
    func getAllFavoriteNews() {
        Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.getAllFavoriteNews(success: { (successArray) in
            for favoriteObj in successArray {
                self.favoriteNews.append(NewsFavoriteModel(value: favoriteObj))
            }
            self.tableview.reloadData()
            Utility.main.hideLoader()
        }) { (failure) in
            Utility.main.hideLoader()
        }
    }
    
    func unfavoriteThisChannel(with newsCategoryID: Int) {
        let param: [String : Any] = [
            "NewsCategoryId" : newsCategoryID
        ]
        Utility.main.showLoader()
        APIManager.sharedInstance.newsApiManager.unFavoriteNews(param: param, success: { (successBool) in
            for (index, favoriteObj) in self.favoriteNews.enumerated() {
                if favoriteObj.NewsCategoryId == newsCategoryID {
                    self.favoriteNews.remove(at: index)
                    self.tableview.reloadData()
                    break
                }
            }
            Utility.main.hideLoader()
        }, failure: { (failure) in
            Utility.main.hideLoader()
        })
    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension NewsFavorites: UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.favoriteNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFavoriteCell") as? NewsFavoriteCell else { return UITableViewCell() }
        if let imageURL = URL(string: self.favoriteNews[indexPath.row].SourceImageUrl!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
            cell.imageNewsChannel.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "placeholder_image"))
        }
        else {
            cell.imageNewsChannel.image = #imageLiteral(resourceName: "placeholder_image")
        }
        cell.labelName.text = self.favoriteNews[indexPath.row].SourceName
        
        cell.delegate = self
        
        cell.tag = indexPath.row
        //configure right buttons 220,48,46
        cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
        cell.rightSwipeSettings.transition = .rotate3D
        
        return cell
    }
    
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true;
    }
    
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
//        self.updatePodcastFavoriteStatus(with: cell.tag)
        self.unfavoriteThisChannel(with: self.favoriteNews[cell.tag].NewsCategoryId)
        print("Deleta")
        return true
    }
}
