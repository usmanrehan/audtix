//
//  PodcastFavorites.swift
//  Auditix
//
//  Created by Ingic Development Team on 2/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import MGSwipeTableCell
class PodcastFavorites: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var fvtPodcastArray = [PodcastSubscribtionModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

         self.tableview.register(UINib(nibName: "FavoriteCell", bundle: nil), forCellReuseIdentifier: "FavoriteCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getMyFavoritePodcast()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - API CALLING
extension PodcastFavorites
{
    func getMyFavoritePodcast() {
        let param : [String : Any] = [:]
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.GetFavoriteAccountPodcasts(params: param, success: {(responceArray) in
            
            self.fvtPodcastArray.removeAll()
            for podcastItem in responceArray {
                self.fvtPodcastArray.append(PodcastSubscribtionModel(value: podcastItem as! [String : Any]))
            }
            self.tableview.reloadData()
            Utility.main.hideLoader()
            
        }, failure: {(failure) in
            Utility.main.hideLoader()
            
        })
    }
    
    func updatePodcastFavoriteStatus(with index: Int) {
        let trackID: Int = self.fvtPodcastArray[index].TrackId
        let param: [String : Any] = [
            "TrackId" : trackID,
            "favorite" : false
        ]
        
        Utility.main.showLoader()
        APIManager.sharedInstance.podcastApiManager.updateFavoritePodcast(with: param, success: {
            (responceObj) in
            self.fvtPodcastArray.remove(at: index)
            self.tableview.reloadData()
            Utility.main.hideLoader()
            
        }, failure: {
            (failure) in
            Utility.main.hideLoader()})
    }
}
// MARK: - UITableView DataSource, Delegate & MGSwipeTableCell Delegate
extension PodcastFavorites: UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fvtPodcastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell") as? FavoriteCell else { return UITableViewCell() }
//        cell.BindData(with: self.fvtPodcastArray[indexPath.row])
        cell.BindData(podcastModel: self.fvtPodcastArray[indexPath.row])
        cell.delegate = self
        
        cell.tag = indexPath.row
        //configure right buttons 220,48,46
        cell.rightButtons = [MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete-icon"), backgroundColor: UIColor(red: 220, green: 48, blue: 46))]
        cell.rightSwipeSettings.transition = .rotate3D
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.20
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return true;
    }
    
    
    func swipeTableCell(_ cell: MGSwipeTableCell, tappedButtonAt index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        self.updatePodcastFavoriteStatus(with: cell.tag)
        return true
    }
    
    
}
