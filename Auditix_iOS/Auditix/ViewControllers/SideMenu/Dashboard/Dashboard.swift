//
//  Dashboard.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Dashboard: UIViewController {

    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var viewImageBorder: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let imageURLStr = AppStateManager.sharedInstance.loggedInUser.ImageName {
            if let imageURL = URL(string: (Constants.ImageBaseURL + imageURLStr).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                self.imageViewProfile.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "userProfile"))
                self.imageViewBackground.sd_setImage(with: imageURL, placeholderImage: #imageLiteral(resourceName: "userProfile"))
            }
            else {
                self.imageViewProfile.image = #imageLiteral(resourceName: "userProfile")
                self.imageViewBackground.image = #imageLiteral(resourceName: "userProfile")
            }
        }
        else {
            self.imageViewProfile.image = #imageLiteral(resourceName: "userProfile")
            self.imageViewBackground.image = #imageLiteral(resourceName: "userProfile")
        }
        
        self.nameLabel.text = AppStateManager.sharedInstance.loggedInUser.FullName
    }
}

// MARK: - IB ACTIONS
extension Dashboard
{
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNotification(_ sender: Any) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Notification") as? NotificationController {
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
    @IBAction func actionMyCart(_ sender: Any) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Cart") as? Cart {
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
    @IBAction func actionFeatures(_ sender: Any) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        if let fvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "Favorites") as? Favorites {
            self.navigationController?.pushViewController(fvc, animated: true)
        }
    }
    @IBAction func actionEditProfile(_ sender: Any) {
        if AppStateManager.sharedInstance.isGuestLogin() {
            Utility.main.showToast(message: Constants.PleaseLoginGuestUser)
            return
        }
        if let nvc = AppStoryboard.SideMenu.instance.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
            self.navigationController?.pushViewController(nvc, animated: true)
        }
    }
}
