//
//  ContactUs.swift
//  Auditix
//
//  Created by Usman Bin Rehan on 7/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {

    @IBOutlet weak var tvCommentBox: UITextView!
    let placeHolder = MiscStrings.typeYourMessageHere.text
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvCommentBox.delegate = self
        self.tvCommentBox.text = placeHolder
        self.tvCommentBox.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validate()
    }
}
//MARK:- UITextViewDelegate
extension ContactUs: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    func textViewDidEndEditing(_ textView: UITextView)   {
        if textView.text.isEmpty {
            textView.text = self.placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK:- Helper Method
extension ContactUs{
    private func validate(){
        if self.tvCommentBox.text == self.placeHolder{
            Utility.main.showAlert(message: MiscStrings.pleaseTypeYourMessage.text, title: AlertTitles.alert.message)
            return
        }
        self.processContactUs()
    }
}
//MARK:- Service
extension ContactUs{
    private func processContactUs(){
        let Message = self.tvCommentBox.text
        let param: [String:Any] = ["Message":Message ?? ""]
        APIManager.sharedInstance.miscellaneousAPIManager.ContactUs(params: param, success: {
            (responceObject) in
            self.navigationController?.popViewController(animated: true)
            Utility.main.showAlert(message: MiscStrings.weWillGetBackToYouSoon.text, title: AlertTitles.alert.message)
            Utility.main.hideLoader()
        }, failure: {
            (error) in
            Utility.main.hideLoader()
        })
    }
}
