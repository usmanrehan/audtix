//
//  RADownloader.swift
//  RawiAlkotob
//
//  Created by taimoorsuleman on 26/01/2018.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import AssetsLibrary
import Alamofire
import SwiftyJSON


protocol DownloaderDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL)
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64, progress: Float)
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
}


class Downloader : NSObject, URLSessionDownloadDelegate {
    
    var url : URL?
    var urlStr: String?
    var tracekID: Int?
    var fileName: String?
    
    init( _ item : String, _ urlStr: String, _ traceID: Int, _ fileName: String){
        self.url  = URL(string: item.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        self.urlStr = urlStr
        self.tracekID = traceID
        self.fileName = fileName
    }
    
//    var delegate : DownloaderDelegate!
    // MARK: DOWNLOAD FILE
    
    func startDownload() {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (URL(string: self.fileName!)!, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        Alamofire.download(self.urlStr!, to: destination).downloadProgress { progress in
                let notificationInfo: [String : Any] = ["traceID": self.tracekID ?? 0,
                                                        "progress" : progress.fractionCompleted]
            
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil, userInfo: notificationInfo)
            }
            .responseData(completionHandler: {
                response in
                if response.result.error == nil {
                    //print("Success: Downloading file: \(response.request) completed.")
                    if let data = response.result.value {
                        let notificationInfo: [String : Any] = [
                            "data": data,
                            "traceID": self.tracekID ?? 0,
                            ]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil, userInfo: notificationInfo)
                    }
                } else {
                    print("Error : \(response.result.error.debugDescription)")
                    let errorMessage = response.result.error.debugDescription
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    let error = NSError(domain: "Domain", code: 1000 , userInfo: userInfo)
                    if response.resumeData != nil {
                        //                        failure(error,response.resumeData!, true)
                    } else {
                        //                        failure(error,Data(),false)
                    }
                }
            })
    }
    
    func downloadWith(/*downloadUrl:String, saveUrl:String, index: Int, success: @escaping DefaultDownloadSuccessClosure, downloadProgress: @escaping DefaultDownloadProgressClosure, failure: @escaping DefaultDownloadFailureClosure*/) {
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent(self.fileName!, isDirectory: false)
            print("saving at \(file)")
            return (file, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        Alamofire.download(self.urlStr!, to: destination)
            .downloadProgress { progress in
                //                print("Download Progress: \(progress.fractionCompleted)")
//                downloadProgress(progress.fractionCompleted,index)
                let notificationInfo: [String : Any] = [
                    "traceID": self.tracekID ?? 0,
                    "progress" : progress.fractionCompleted
                ]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil, userInfo: notificationInfo)
            }
            .responseData(completionHandler: {
                response in
                if response.result.error == nil {
                    print("Success: Downloading file: \(response.request) completed.")
                    if let data = response.result.value {
                        let notificationInfo: [String : Any] = [
                            "data": data,
                             "traceID": self.tracekID ?? 0,
                        ]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadTaskCompleted), object: nil, userInfo: notificationInfo)
                    }
                }else {
                    print("Error : \(response.result.error.debugDescription)")
                    let errorMessage = response.result.error.debugDescription
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    let error = NSError(domain: "Domain", code: 1000 , userInfo: userInfo)
                    if response.resumeData != nil {
//                        failure(error,response.resumeData!, true)
                    }else {
//                        failure(error,Data(),false)
                    }
                    
                }
            })
    }
    
    //is called once the download is complete
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL){
        //copy downloaded data to your documents directory with same names as source file
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let destinationUrl = RAFileManager.createRawiAlKutabDirectory().appendingPathComponent(url!.lastPathComponent)
        let dataFromURL = NSData(contentsOf: location)
        dataFromURL?.write(to: destinationUrl, atomically: true)
//        self.delegate.urlSession(session, downloadTask: downloadTask, didFinishDownloadingTo: location)
    }
    
    //this is to track progress
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        print(progress)
        let notificationInfo: [String : Any] = [
            "traceID": self.tracekID ?? 0,
            "progress" : progress
        ]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.downloadTaskInProgress), object: nil, userInfo: notificationInfo)
        
//        self.delegate.urlSession(session, downloadTask: downloadTask, didWriteData: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpectedToWrite: totalBytesWritten, progress: progress)
    }
    
    // if there is an error during download this will be called
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?){
        if(error != nil){
            //handle the error
            print("Download completed with error: \(error!.localizedDescription)");
//            self.delegate.urlSession(session, task: task, didCompleteWithError: error)
        }
    }
    
    //method to be called to download
    func download(){
        
        //        let sessionConfig = URLSessionConfiguration.background(withIdentifier: (url?.absoluteString)!)
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: (url?.absoluteString)! )
        let defaultSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        let downloadTask = defaultSession.downloadTask(with: url!)
        downloadTask.resume()
    }
    
}

class RAFileManager{
    
    static func createRawiAlKutabDirectory() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let RawiAlKotobDocumentsDirectory = documentsDirectory.appendingPathComponent("auditix")
        
        if !(FileManager.default.fileExists(atPath: RawiAlKotobDocumentsDirectory.path)){
            do {
                try FileManager.default.createDirectory(atPath: RawiAlKotobDocumentsDirectory.path, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
        }
        return RawiAlKotobDocumentsDirectory
        
    }
    
    static func isAlreadyExists(fileName:String) -> Bool {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsDirectory.appendingPathComponent("").appendingPathComponent(fileName)
        
        if (FileManager.default.fileExists(atPath: filePath.path)){
            return true
        }else{
            return false
        }
        
    }
    
    static func getFilePath(fileName:String) -> String {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsDirectory.appendingPathComponent("").appendingPathComponent(fileName)
        return filePath.absoluteString
        
    }
    
    static func getAllFiles() -> [URL]{
        
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("")
        var fileURLs = [URL]()
        
        do {
            fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            return fileURLs
            // process files
        } catch {
            print("Error while enumerating files ")
        }
        
        return fileURLs
    }
    
}

