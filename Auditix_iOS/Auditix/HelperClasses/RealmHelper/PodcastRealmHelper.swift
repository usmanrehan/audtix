//
//  PodcastRealmHelper.swift
//  Auditix
//
//  Created by Ahmed Shahid on 3/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class PodcastRealmHelper: NSObject {
    
    static let sharedInstance = PodcastRealmHelper()
        var realm: Realm!
    
    // create downloaded podcast record
    func addThisPodcast(podcast: PodcastModel, trackID: Int) {
        for podcastDownloadedItem in self.getAllDownloadedPodcast() {
            if podcastDownloadedItem.trackID == trackID {
                self.updateThisPodcast(podcast: podcast, trackID: trackID)
                return
            }
        }
        
        let podcastDownloaded = PodcastDownloadedModel()
        podcastDownloaded.createdDate = Date()
        podcastDownloaded.updatedDate = Date()
        podcastDownloaded.AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
        podcastDownloaded.DownloadedPodcast = podcast
        podcastDownloaded.trackID = trackID
        
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add([podcastDownloaded])
        }
    }
    
//    func addThisPodcast(podcast: PodcastDetailModel, trackID: Int) {
//        for podcastDownloadedItem in self.getAllDownloadedPodcast() {
//            if podcastDownloadedItem.trackID == trackID {
//                self.updateThisPodcast(podcast: podcast, trackID: trackID)
//                return
//            }
//        }
//
//        let podcastDownloaded = PodcastDownloadedModel()
//        podcastDownloaded.createdDate = Date()
//        podcastDownloaded.updatedDate = Date()
//        podcastDownloaded.AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
//        podcastDownloaded.DownloadedPodcast = podcast
//        podcastDownloaded.trackID = trackID
//
//        try! Global.APP_REALM?.write(){
//            Global.APP_REALM?.add([podcastDownloaded])
//        }
//    }

    
    // MARK: GET ALL DOWNLOADED PODCAST
    func getAllDownloadedPodcast() -> Results<PodcastDownloadedModel> {
        return (Global.APP_REALM?.objects(PodcastDownloadedModel.self))!.filter("AccountID == \(AppStateManager.sharedInstance.loggedInUser.AccountID)")
    }
    
    func getTodaysDownloadedPodcast() -> Results<PodcastDownloadedModel> {
        return (Global.APP_REALM?.objects(PodcastDownloadedModel.self))!.filter("updatedDate >= %@ AND AccountID == %@",Date(timeIntervalSinceNow: -86400), AppStateManager.sharedInstance.loggedInUser.AccountID) // kal ki date sy sare bare...
    }
    
    func getThisMonthDownloadedPodcast() -> Results<PodcastDownloadedModel> {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        return (Global.APP_REALM?.objects(PodcastDownloadedModel.self))!.filter("updatedDate >= %@ AND updatedDate <= %@ AND AccountID == %@",Date(timeIntervalSinceNow: -Double(60 * 60 * 24 * day)), Date(timeIntervalSinceNow: -86400),  AppStateManager.sharedInstance.loggedInUser.AccountID) // sec * min * hrs * no of days in this month
    }

    func getOlderDownloadedPodcast() -> Results<PodcastDownloadedModel> {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        return (Global.APP_REALM?.objects(PodcastDownloadedModel.self))!.filter("updatedDate <= %@", Date(timeIntervalSinceNow: -Double(60 * 60 * 24 * day))) // sec * min * hrs * no of days in this month
    }
    
    // MARK: DEL THIS DOWNLOADED PODCAST
    func deleteThisPodcast(podcastDownloadedModel: PodcastDownloadedModel) {
        try! Global.APP_REALM?.write({ () -> Void in
            Global.APP_REALM?.delete(podcastDownloadedModel)
        })
    }
    
    // UPDATE THIS DOWNLOADED PODCAST
//    private func updateThisPodcast(podcast: PodcastDetailModel, trackID: Int) {
//        for podcastDownloadedItem in self.getAllDownloadedPodcast() {
//            if podcastDownloadedItem.trackID == trackID {
//                try! Global.APP_REALM?.write() {
//                    podcastDownloadedItem.updatedDate = Date()
//                }
//            }
//        }
//    }
    
    private func updateThisPodcast(podcast: PodcastModel, trackID: Int) {
        for podcastDownloadedItem in self.getAllDownloadedPodcast() {
            if podcastDownloadedItem.trackID == trackID {
                try! Global.APP_REALM?.write() {
                    podcastDownloadedItem.updatedDate = Date()
                }
            }
        }
    }

    func checkThisPodcastIsAvailable(trackID: Int) -> Bool {
        for podcastDownloadedItem in self.getAllDownloadedPodcast() {
            if podcastDownloadedItem.trackID == trackID {
                return true
            }
        }
        return false
    }
}
