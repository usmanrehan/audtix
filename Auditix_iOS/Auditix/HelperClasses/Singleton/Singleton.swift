//
//  Singleton.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift
class Singleton {
    static let sharedInstance = Singleton()
    
    var chaptersArray : List<BookChapter>!
    var podcastEpisodeArray : List<PodcastTrack>!
    
    var currentlyPlayingChapEpisodeNumber: Int = -1
    
    var walkThroughDataArray = [WalkThroughModel]()
    
    var currentAdvertiseArray = [Advertise]()
    
    var isComingFromHome: Bool = false
    
    // cms array
    var cmsArray: [CMSContent]?
}
