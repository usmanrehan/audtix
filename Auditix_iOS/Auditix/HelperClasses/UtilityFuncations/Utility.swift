import Foundation
import UIKit
import AVFoundation
//import Toast_Swift
import Toast_Swift
import NVActivityIndicatorView

//// MARK: AppHelperUtility setup
@objc class Utility: NSObject
{
    static let main = Utility()
    fileprivate override init() {}
}


extension Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String {
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
    }
    
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
    
    
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, collectionView: UICollectionView)
    {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        
        collectionView.backgroundView = messageLabel;
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, tableview: UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        tableview.backgroundView = messageLabel;
    }
    
    func createBookFilePath(with bookID: Int, chapterNumber: Int, fileName: String) -> String {
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        return "\(userID)/books/\(bookID)/\(chapterNumber)/\(fileName.removingWhitespaces()).mp3"
    }
    
    func isThisBookFileExist(with bookId: Int, chapterId: Int) -> Bool {
        let filePath = self.returnBookFileUrl(with: bookId, chapterID: chapterId)
        return FileManager.default.fileExists(atPath: filePath.path)
    }
    
    func isThisPodcastFileExist(with podcastTrackID: Int, EpisodeID: Int) -> Bool {
        let filePath = self.returnPodcastFileUrl(with: podcastTrackID, EpisodeID: EpisodeID)
        return FileManager.default.fileExists(atPath: filePath.path)
    }
    
    func isThisNewsFileExist(with newsId: Int, EpisodeID: Int) -> Bool {
        let filePath = self.returnNewsFileUrl(with: newsId, EpisodeID: EpisodeID)
        return FileManager.default.fileExists(atPath: filePath.path)
    }
    
    func returnBookFileUrl(with bookID: Int, chapterID: Int) -> URL {
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let fileName = String(format: "Book%02d%02d", bookID, chapterID)
        return Constants.filePath.appendingPathComponent("\(userID)/books/\(bookID)/\(chapterID)/\(fileName.removingWhitespaces())" + ".mp3", isDirectory: false)
    }
    
    func returnPodcastFileUrl(with podcastTrackID: Int, EpisodeID: Int) -> URL {
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let fileName = String(format: "Podcast%2d%2d", podcastTrackID, EpisodeID)
        return Constants.filePath.appendingPathComponent("\(userID)/podcast/\(podcastTrackID)/\(EpisodeID)/\(fileName.removingWhitespaces())" + ".mp3", isDirectory: false)
    }
    
    func returnNewsFileUrl(with newsId: Int, EpisodeID: Int) -> URL {
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        let fileName = String(format: "News%2d%2d", newsId, EpisodeID)
        return Constants.filePath.appendingPathComponent("\(userID)/news/\(newsId)/\(EpisodeID)/\(fileName.removingWhitespaces())" + ".mp3", isDirectory: false)
    }
    
    func createPodcastFilePath(with podcastTrackID: Int, EpisodeID: Int, fileName: String) -> String {
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        return "\(userID)/podcast/\(podcastTrackID)/\(EpisodeID)/\(fileName.removingWhitespaces()).mp3"
    }
    
    func shareAcitivityWithURL(_ url: String, title: String?, subtitle: String?) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let textToShare = String(format: "%@\n%@", title ?? "-", subtitle ?? "-")
            let shareItems: [Any] = [textToShare, NSURL(string: url)!]
            let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            
            topController.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func shareActivity() {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let urlString = "http://auditix.stagingic.com/"
            let activityViewController = UIActivityViewController(activityItems: ["Check out this app", NSURL(string: urlString)!], applicationActivities: nil)
            
            topController.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func postSubscribedPodcastNotification() {
        NotificationCenter.default.post(name: Constants.subscribedPodcastNotification, object: nil)
    }
    
    func postSubscribedNewsChannelNotification() {
        NotificationCenter.default.post(name: Constants.subscribedNewsNotification, object: nil)
    }
    
    func postFavoriteBookNotification() {
        NotificationCenter.default.post(name: Constants.favoriteBookNotification, object: nil)
    }
}

// MARK: Alert related functions
extension Utility {
    func showAlert(message:String,title:String,controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default, handler: nil))
            
            return alertController
        }
        else
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            
            return nil
        }
    }
    
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.tintColor = Constants.THEME_ORANGE_COLOR
        alertController.addAction(UIAlertAction(title: MiscStrings.yes.text, style: UIAlertActionStyle.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: MiscStrings.no.text, style: UIAlertActionStyle.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: MiscStrings.ok.text, style: UIAlertActionStyle.default, handler: nil))
            topController.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    func showActivityViewController(with sharingText: String)
    {
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        dateFormatter.timeZone = TimeZone.current
        if LanguageManager.sharedInstance.getSelectedLocale().contains("en"){
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        else{
            dateFormatter.locale = Locale(identifier: "ar")
        }
        return dateFormatter.string(from: date!)
    }
    
}
// MARK:- INDICATOR
extension Utility
{
    func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor.clear//UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: Constants.THEME_ORANGE_COLOR , padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
// MARK:- TOAST HELPER UTILITY
extension Utility
{
    func showToast(message: String, controller: UIViewController)
    {
        // Show the message.
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: .center)
    }
    
    func showToast(message: String, controller: UIViewController, position: ToastPosition)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: position)
    }
    func showToast(message: String, controller: UIViewController, duration: TimeInterval)
    {
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: duration, position: .center)
        
    }
    
    func showToast(message: String)
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.view.hideToastActivity()
        topController?.view.makeToast(message, duration: 3.0, position: .center)
    }
}

extension Utility {
    func isKeepUntilIDeleteOn() -> Bool {
        if let contiBool = Constants.USER_DEFAULTS.value(forKey: Constants.KeepUntilIDelete) as? Bool {
            if contiBool {
                return true
            }
            else{
                return false
            }
        }
        return true
    }
    func contentsOfDirectoryAtPath(path: String) -> [String]? {
        guard let paths = try? FileManager.default.contentsOfDirectory(atPath: path) else { return nil}
        return paths
    }
    func getIdfromBooksFolders(directoryName:String)->String {
        let searchPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        //let newPath = searchPath + "/\(userID)/news/"
        let filesPath = searchPath + "/\(userID)/\(directoryName)/"
        let allContents = contentsOfDirectoryAtPath(path: filesPath) ?? [""]
        let commaSeparated = allContents.joined(separator: ",")
        print(commaSeparated)
        return commaSeparated
    }
    func getIdfromFolders(directoryName:String)->String {
        let searchPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        //let newPath = searchPath + "/\(userID)/news/"
        let filesPath = searchPath + "/\(userID)/\(directoryName)/"
        return self.getEpId(path: filesPath)
        //        let allContents = contentsOfDirectoryAtPath(path: filesPath) ?? [""]
        //        let commaSeparated = allContents.joined(separator: ",")
        //        print(commaSeparated)
        //        return commaSeparated
    }
    func getEpId(path: String)-> String {
        var commaSeparated = [String]()
        let allContents = contentsOfDirectoryAtPath(path: path) ?? [""]
        let deletedItemsCSV = AppStateManager.sharedInstance.loggedInUser.deletedItems ?? ""
        let arrDeletedItems = deletedItemsCSV.components(separatedBy: ",")
        for item in allContents {
            let newPath = path + "/\(item)"
            let allEpIds = contentsOfDirectoryAtPath(path: newPath) ?? [""]
            for i in allEpIds {
                if !arrDeletedItems.contains(i){
                    commaSeparated.append(i)
                }
            }
        }
        var allIDs = ""
        allIDs = commaSeparated.joined(separator: ",")
        return allIDs
    }
    func deleteDownloadedFile(directoryName:String,fileName:String)->Bool{
        let searchPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        //let newPath = searchPath + "/\(userID)/news/"
        let filesPath = searchPath + "/\(userID)/\(directoryName)/"
        let allContents = contentsOfDirectoryAtPath(path: filesPath) ?? [""]
        var isFileExist = false
        var existedFilePath = ""
        if directoryName == "books"{
            for _ in allContents {
                let newPath = filesPath
                let allEpIds = contentsOfDirectoryAtPath(path: newPath) ?? [""]
                for i in allEpIds {
                    print(i)
                    if FileManager.default.fileExists(atPath: newPath) && i == fileName{
                        isFileExist = true
                        existedFilePath = newPath
                    }
                }
            }
        }
        else{
            for item in allContents {
                let newPath = filesPath + "/\(item)" + "/\(fileName)"
                let allEpIds = contentsOfDirectoryAtPath(path: newPath) ?? [""]
                for i in allEpIds {
                    print(i)
                    if FileManager.default.fileExists(atPath: newPath){
                        isFileExist = true
                        existedFilePath = newPath
                    }
                }
            }
        }
        if isFileExist{
            do {
                let fileManager = FileManager.default
                // Check if file exists
                if fileManager.fileExists(atPath: existedFilePath) {
                    // Delete file
                    if directoryName == "books"{
                        try fileManager.removeItem(atPath: existedFilePath + fileName)
                    }
                    else{
                        try fileManager.removeItem(atPath: existedFilePath)
                    }
                    try! Global.APP_REALM?.write(){
                        if let deletedItems = AppStateManager.sharedInstance.loggedInUser.deletedItems{
                            if Validation.validateStringLength(deletedItems){
                                AppStateManager.sharedInstance.loggedInUser.deletedItems = deletedItems + "," + fileName
                                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                            }
                            else{
                                AppStateManager.sharedInstance.loggedInUser.deletedItems = fileName
                                Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                            }
                        }
                        else{
                            AppStateManager.sharedInstance.loggedInUser.deletedItems = fileName
                            Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser, update: true)
                        }   
                    }
                    return true
                } else {
                    print("File does not exist")
                    return false
                }
            }
            catch let error as NSError {
                print("An error took place: \(error)")
                return false
            }
        }
        else{
            return false
        }
    }
    func isFileExist(directoryName:String,fileName:String)->Bool{
        let searchPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let userID = AppStateManager.sharedInstance.loggedInUser.AccountID
        //let newPath = searchPath + "/\(userID)/news/"
        let filesPath = searchPath + "/\(userID)/\(directoryName)/"
        let allContents = contentsOfDirectoryAtPath(path: filesPath) ?? [""]
        var isFileExist = false
        for item in allContents {
            let newPath = filesPath + "/\(item)" + "/\(fileName)"
            let allEpIds = contentsOfDirectoryAtPath(path: newPath) ?? [""]
            for i in allEpIds {
                print(i)
                if FileManager.default.fileExists(atPath: newPath){
                    isFileExist = true
                }
            }
        }
        return isFileExist
    }
}
