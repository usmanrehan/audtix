//
//  Constants.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

struct Global{ //246 144 119
    
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static let APP_Color                     = UIColor(displayP3Red: 246/255.0, green: 144/255.0, blue: 119/255.0, alpha: 1.0)
    //    static var USER                          = APP_MANAGER.loggedInUser
}

struct Constants {
    
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let DEVICE_UDID                 = UIDevice.current.identifierForVendor!.uuidString
    
    static let DEVICE_NAME                 = "ios"
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    static let SINGLETON                   = Singleton.sharedInstance
    
    static let DEFAULTS_USER_KEY           = "User"
    
    static var DEVICE_TOKEN                = "12345"
    
    static let filePath                    = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    
    static let bottomBarHeightSize: CGFloat         = 0.11
    
    static let PleaseLoginGuestUser        = AlertMessages.guestLogin.message
    
    //MARK: - THEME COLORS
    static let THEME_ORANGE_COLOR          = UIColor(red: 0xFF, green: 0x7E, blue: 0x5B) //FF-7E-5B
    static let FIELD_VALIDATION_RED_COLOR       = UIColor(red: 0xC4, green: 0x13, blue: 0x02) //C41302
    
    static let bookFilterRightSideMenu = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "BookFilter") as! BookFilter
    static let ChapterPodcastRightSideMenu = AppStoryboard.Home.instance.instantiateViewController(withIdentifier: "PlayerListing") as! PlayerListing
    static let podcastFilterRightSideMenu = AppStoryboard.Podcast.instance.instantiateViewController(withIdentifier: "PodcastFilter") as! PodcastFilter
    static let newsFilterRightSideMenu = AppStoryboard.News.instance.instantiateViewController(withIdentifier: "NewsFilter") as! NewsFilter
    
    //MARK: - User Defaults Keys
    static let ApplyingUserSettingsForFirstTime = "ApplyingUserSettingsForFirstTime"
    static let OnlyUseWifi = "OnlyUseWifi"
    static let UseBothWifiAndCellular = "UserBothWifiAndCellular"
    static let ContinuousPlay = "ContinuousPlay"
    static let UserDeviceToken = "UserDeviceToken"
    static let KeepUntilIDelete = "KeepUntilIDelete"
    
    //MARK: - NOTIFICATION OBSERVERS
    static let yourPodcastSectionTab = "yourPodcastSectionTab"
    //static let podcastUnSubFromLibrary = "podcastUnSubFromLibrary"
    static let reloadPodcastSubscription = "reloadPodcastSubscription"
    static let editProfileDoneChangeTheSideMenuName = "ChangeTheSideMenuName"
    
    static let downloadTaskInProgress = "downloadTaskInProgress"
    static let downloadTaskCompleted = "downloadTaskCompleted"
    static let downloadCompleteSoUpdateArray = "downloadCompleteSoUpdateArray"
    static let stopPlaying = "stopPlaying"
    static let removePlayerAndStopPlaying = "stopPlayingAndRemove"
    
    
    
    static let subscribedPodcastNotiicaiton = "PodcastSubscribedNotificaiton"
    static let unsubscribedPodcastNotificaiton = "PodcastUnsubscribedNotification"
    
    //MARK: - Notification Constants
    static let subscribedPodcastNotification = Notification.Name("SubscribedPodcast")
    static let subscribedNewsNotification = Notification.Name("SubscribedNews")
    static let favoriteBookNotification = Notification.Name("FavoriteBook")
    
    //MARK: - Base URLs
    //static let BaseURL = "http://auditix.stagingic.com/API/" // Live
    //static let BaseURL = "http://18.213.243.115/API/"//
    static let BaseURL = "https://audtix.com/api/" // Live
    
    static let ImageBaseURL = ""
    static let PAGINATION_PAGE_SIZE = 100
    
    // MARK: - Google Route
    static let getGoogleUserInfo = "https://www.googleapis.com/oauth2/v3/userinfo?access_token="
    
    // MARK: - Users Route
    static let Login = "Login"
    static let SocialLogin = "SocialLogin"
    static let Register = "Register"
    static let RegisterDevice = "RegisterPushNotification"
    static let GuestRegistration = "GuestRegistration"
    static let ResetPassword = "ResetPassword"
    static let ValidateCode = "ValidateCode"
    static let VerifyPhoneCode = "VerifySMSCode" ///VerifySMSCode
    static let EditProfile = "EditProfile"
    static let GetAllNotifications = "GetAllNotifications"
    static let GetAllWalkThrough = "GetAllWalkThrough"
    static let GetPodcastBookNews = "GetPodcastBookNews"
    
    
    //
    static let ImgBaseURL = "http://auditix.stagingic.com"
    // MARK: - CMS
    static let CMS = "GetAllContent"
    
    // MARK: - Books Routs
    static let BookHome = "DefaultCategoryBooks"
    static let GetBookCategories = "GetBookCategories"
    static let BooksByCategoryId = "BooksByCategoryId"
    static let BookDetail = "BookDetail"
    static let AddBooksToLibrary = "AddBooksToLibrary"
    static let MyLibraryBooks = "MyLibraryBooks"
    static let GetAllGenre = "GetAllGenre"
    static let GetFavoriteBooks = "GetFavoriteBooks"
    static let GetFeaturedBooks = "GetFeaturedBooks"
    static let AddBookToFavorite = "AddBookToFavorite"
    static let RemoveBookFromFavorite = "RemoveBookFromFavorite"
    static let BookRating = "BookRating"
    
    // MARK: - Podcast Routs
    static let getListeningEventPodcast = "GetListeningEventPodcast"
    static let addListeningEventPodcast = "AddListeningEventPodcast"
    static let getPodcastCategories = "GetAllCategories"
    static let getPodcastCategoriesByCountryId = "GetPodcastCategoriesbyCountryId"
    static let subscribePodcast = "GetAccountPodcasts"
    static let getPodcasts = "GetDefaultPodCast"
    static let getPodCastByCategoryId = "GetPodCastByCategoryId"
    static let getPodcastDetail = "GetPodcastFeedByTrackId"
    static let getPodcastEpisodes = "GetPodcastEpisodesByPodcastID"
    static let updateFavoritePodcast = "AddUpdateFavoritePodcast"
    static let subscribeThisPodcast = "SubscribePodcast"
    static let unSubscribeThisPodcast = "UnSubscribePodcast"
    static let getFilterPodcastCategories = "GetAllCategories"
    static let GetLocationList = "GetLocationList"
    static let AddPodcastRating = "AddPodcastRating"
    static let GetFavoriteAccountPodcasts = "GetFavoriteAccountPodcasts"
    static let SetAutodownloadPodcasts = "SetAutodownloadPodcast"
    static let UnsetAutodownloadPodcasts = "UnsetAutodownloadPodcast"
    static let SetNotificationSettings = "SetNotificationSetting"
    static let UnsetNotificationSettings = "UnsetNotificationSetting"
    
    // MARK: - GetAllNewsCategory
    static let getAllNewsCategory = "GetAllNewsCategory"
    static let getAllNewsByCategoryId = "GetAllNewsByCategoryId"
    static let getSortedNewsEpisodes = "GetSortedNewsEpisodes"
    static let unSubscribeNews = "UnSubscribeNews"
    static let subscribeNews = "SubscribeNews"
    static let subscribeNewsCategory = "SubscribeNewsCategory"
    static let unSubscribeNewsCategory = "UnSubscribeNewsCategory"
    static let getAllNewsSubscriptions = "GetAllNewsSubscriptions"
    
    static let favoriteNews = "FavoriteNews"
    static let unFavoriteNews = "UnFavoriteNews"
    static let getAllFavoriteNews = "GetAllFavoriteNews"
    static let SetAutodownloadNews = "SetAutodownloadNews"
    static let UnsetAutodownloadNews = "UnsetAutodownloadNews"
    
    // MARK: - Miscellaneous Route
    static let getSearch = "GetSearch"
    static let GetAdvertisement = "GetAllAdvertisement"
    static let GetAllNewsPodcast = "GetAllPodcastsAndNews"
//    static let GetAllNotificaiton = "GetAllNotifications"
    static let GetNewsPodcastPolling = "GetPodcastNewsDownloadDetail"
    static let RemoveNotifications = "RemoveNotifications"
    
    //MARK:- Common
    static let GetFilterData = "GetFilterData"
    static let GetPodcastNewsAndBookByEpisodesId = "GetPodcastNewsAndBookByEpisodesId"
    static let GetFilterResponse = "GetFilterResponse"
    static let Contact = "Contact"
    static let ChangeLanguage = "ChangeLanguage"
    
    //MARK:- Listen Counts
    static let AddToListen = "AddToListen"
}

